//
//  LoginLandingPage.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 25/02/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class LoginLandingPage: AppBaseVC {

    @IBOutlet weak var landingTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
      self.setTableViewDatasource()
        self.addNotification()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
                self.navigationController?.navigationBar.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
        deinit {
            NotificationCenter.default.removeObserver(self)
        }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LoginLandingPage {
    
    func addNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(launchCreateScreen),
                                               name:
            NSNotification.Name(rawValue: APPNotifications.createNewAccountNotification),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(changeRootView),
                                               name:
            NSNotification.Name(rawValue: APPNotifications.changeRootView),
                                               object: nil)
    }
    
    @objc func  launchCreateScreen(info: NSNotification) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as? RegisterViewController {
            let navController = APPNavigationController(rootViewController: controller)
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    @objc func  changeRootView(info: NSNotification) {
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        guard let rootViewController = window.rootViewController else {
            return
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "APPTabbarController")
        vc.view.frame = rootViewController.view.frame
        vc.view.layoutIfNeeded()
        
        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
            window.rootViewController = vc
        }, completion: { completed in
            // maybe do something here
        })

    }
}

extension LoginLandingPage: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.landingTableView.delegate = self
        self.landingTableView.dataSource = self
        self.landingTableView.estimatedRowHeight = 300
        self.landingTableView.rowHeight = UITableViewAutomaticDimension
        
        
       /// self.landingTableView.backgroundView = nil
        
        
//        [self.tableView setBackgroundView:nil];
//        [self.tableView setBackgroundColor:[UIColor clearColor]];
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:
            CellIdentifiers.LandingCellIdentifier, for: indexPath)
        if let cellIs = cell as? LandingCell {
            cellIs.selectionStyle = .none
            cellIs.delegate = self
            ///cellIs.setData(data: notificationsArray[indexPath.row])
        }
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return UIScreen.main.bounds.height - 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
}

extension LoginLandingPage: LandingCellDelegate {
    func inviteSelected() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "InviteCodeVC") as? InviteCodeVC {
            let navController = APPNavigationController(rootViewController: controller)
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    func signSelected() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
            let navController = APPNavigationController(rootViewController: controller)
            self.present(navController, animated: true, completion: nil)
        }
    }
    //ServiceAgreement
    func newProjectClicked() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "RegisterController") as? RegisterController {
            let navController = APPNavigationController(rootViewController: controller)
             self.present(navController, animated: true, completion: nil)
        }
    }
    func joinUsClicked() {

            /// self.navigationController?.pushViewController(controller, animated: true)
        
//        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//
//        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddingCardController") as? AddingCardController {
//            let navController = APPNavigationController(rootViewController: controller)
//            self.present(navController, animated: true, completion: nil)
//        }
    }
    
    
}
