//
//  ProfileResumeCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 15/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

protocol ProfileResumeCellDelegate: class {
    func chooseResumeClicked()
}

class ProfileResumeCell: UITableViewCell {

    weak var delegate: ProfileResumeCellDelegate?
    @IBOutlet weak var chooseButton: UIButton!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var descriptionText: UITextField!
    var resumeLink: String?
    
    func setData(data: String?, tag: Int, profile: ProfileModel) {
        self.descriptionText.isEnabled = false
        headerLabel.text = data ?? ""
        self.resumeLink = profile.resume ?? ""
        descriptionText.text = profile.resume ?? ""
        
        let lowercase = (data ?? "").lowercased()
        descriptionText.placeholder = "Select \(lowercase)"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func chooseButtonClicked(_ sender: Any) {
        
        self.delegate?.chooseResumeClicked()
        print(resumeLink ?? "")
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
