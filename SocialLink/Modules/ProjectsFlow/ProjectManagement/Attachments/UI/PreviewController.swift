//
//  PreviewController.swift
//  SocialLink
//
//  Created by Appcare Apple on 17/06/19.
//

import UIKit
import WebKit

class PreviewController: AppBaseVC, WKNavigationDelegate  {
    
    var webView: WKWebView!

    var fileUrl: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Preview"
        createNavigationItems()

    }
    func setProject(url: String) {
        self.fileUrl = url
        
    }
    override func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("finish to load")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showTabBar()
        let imageUrl = "https://app.sociallink.com/uploads/projects/" + fileUrl!
        print(imageUrl)
        let url = URL(string: imageUrl)!
        webView.load(URLRequest(url: url))
        //        let refresh = UIBarButtonItem(barButtonSystemItem: .refresh, target: webView, action: #selector(webView.reload))
        //        toolbarItems = [refresh]
        //        navigationController?.isToolbarHidden = false
    }
    
    func showTabBar() {
        self.setTabBarHidden(false)
    }
    
}
extension PreviewController {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
}
