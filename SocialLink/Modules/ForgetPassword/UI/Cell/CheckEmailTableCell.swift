//
//  CheckEmailTableCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

protocol CheckEmailTableCellDelegate: class {
    func submitSelected()
}

class CheckEmailTableCell: UITableViewCell {

    weak var delegate: CheckEmailTableCellDelegate?

    @IBOutlet weak var emailText: AppTextField!
    @IBOutlet weak var submitButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func getData() -> String {
        return emailText.text ?? ""
    }

    @IBAction func submitClicked(_ sender: Any) {
        
        self.delegate?.submitSelected()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
