//
//  AppDataManager.swift
//  BaseProject
//
//  Created by Santhosh Marripelli on 23/02/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation

class AppDataManager {
    
    class func getSampleData(parametes: [String: Any]?,
                             onSuccess: @escaping (Bool?) -> Void,
                             onFailure: @escaping APIErrorHandler) {
        
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.SampleURL,
                                      parametes: parametes,
                                      httpMethd: APIMethod.get,
                                      onSuccess: { (apiResponse) in
                                        onSuccess(true)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    
    
    class func loginUser(parametes: [String: Any]?,
                         onSuccess: @escaping (SignUpModel?) -> Void,
                         onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.LoginURL,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        ///   APPDBModel.saveUserInDB(apiResponse["response"]["profile"][0])
                                        let loginObject = SignUpModel.init(apiResponse["response"])
                                        onSuccess(loginObject)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func loginPasswordUser(parametes: [String: Any]?,
                                 onSuccess: @escaping (SignUpPasswordModel?) -> Void,
                                 onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.LoginURL,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        APPDBModel.saveUserInDB(apiResponse["data"])
                                        let tokenData = APPUtility.constructTokenObject(data: ["token": apiResponse["data"]["token"],                                                                             "userId": apiResponse["data"]["userId"]])
                                        APPDBModel.saveTokenInDB(tokenData)
                                        let loginObject = SignUpPasswordModel.init(apiResponse["data"])
                                        onSuccess(loginObject)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    class func chatHistory(parametes: [String: Any]?,
                           onSuccess: @escaping (ChatHistory?) -> Void,
                           onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.slackChatHistoryURL,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let channelsObject = ChatHistory.init(apiResponse["messages"])
                                        onSuccess(channelsObject)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    class func postChatMessage(parametes: [String: Any]?,
                               onSuccess: @escaping (PostChat?) -> Void,
                               onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.postChatMessageURL,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let channelsObject = PostChat.init(apiResponse["message"])
                                        onSuccess(channelsObject)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    class func getSkills(parametes: [String: Any]?,
                         onSuccess: @escaping (RegistrationSkillsModel?) -> Void,
                         onFailure: @escaping APIErrorHandler) {
        
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.SkillsURL,
                                      parametes: parametes,
                                      httpMethd: APIMethod.get,
                                      onSuccess: { (apiResponse) in
                                        let skillsObject = RegistrationSkillsModel.init(apiResponse["data"])
                                        onSuccess(skillsObject)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    class func slackAuth(parametes: [String: Any]?, url: String,
                         onSuccess: @escaping (SlackAuthModel?) -> Void,
                         onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: url,
                                      parametes: parametes,
                                      httpMethd: APIMethod.get,
                                      onSuccess: { (apiResponse) in
                                        let slackObject = SlackAuthModel.init(apiResponse)
                                        onSuccess(slackObject)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    class func slackUserList(parametes: [String: Any]?,
                             onSuccess: @escaping (SlackUserDetails?) -> Void,
                             onFailure: @escaping APIErrorHandler) {
   
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.slackUsersListURL,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        let slackObject = SlackUserDetails.init(apiResponse["members"])
                                        onSuccess(slackObject)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    class func projectsList(parametes: [String: Any]?,
                            onSuccess: @escaping (ProjectListModel?) -> Void,
                            onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.projectList,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        APPDBModel.saveProjectsInDB(apiResponse["data"])
                                        let tokenData = APPUtility.constructTokenObject(data: ["token": apiResponse["records"]["token"],                                                                             "userId": apiResponse["records"]["userId"]])
                                        APPDBModel.saveTokenInDB(tokenData)
                                        let loginObject = ProjectListModel.init(apiResponse["data"])
                                        onSuccess(loginObject)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func registerUser(parametes: [String: Any]?,
                            onSuccess: @escaping (SignUpModel?) -> Void,
                            onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.RegisterURL,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        APPDBModel.saveUserInDB(apiResponse["records"])
                                        let tokenData = APPUtility.constructTokenObject(data: ["token": apiResponse["records"]["token"],                                                                             "userId": apiResponse["records"]["userId"]])
                                        APPDBModel.saveTokenInDB(tokenData)
                                        let loginObject = SignUpModel.init(apiResponse["records"])
                                        onSuccess(loginObject)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    class func registerGmailUser(parametes: [String: Any]?,
                                 onSuccess: @escaping (SignUpModel?) -> Void,
                                 onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.gmailURL,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        APPDBModel.saveUserInDB(apiResponse["data"])
                                        //                                        let tokenData = APPUtility.constructTokenObject(data: ["token": apiResponse["response"]["token"],
                                        //                                                                                               "userId": apiResponse["response"]["profile"]["userId"]])
                                        //                                        APPDBModel.saveTokenInDB(tokenData)
                                        let loginObject = SignUpModel.init(apiResponse["data"])
                                        onSuccess(loginObject)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    class func bankDetails(parametes: [String: Any]?,
                           onSuccess: @escaping (SignUpModel?) -> Void,
                           onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.BanksDetailsURL,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        
                                        let loginObject = SignUpModel.init(apiResponse["data"])
                                        onSuccess(loginObject)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    class func cardDetails(parametes: [String: Any]?,
                           onSuccess: @escaping (SignUpModel?) -> Void,
                           onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.CardDetailsURL,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        
                                        let loginObject = SignUpModel.init(apiResponse["data"])
                                        onSuccess(loginObject)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    class func getBanksList(parametes: [String: Any]?,
                            onSuccess: @escaping (ListOfBanksModel?) -> Void,
                            onFailure: @escaping APIErrorHandler) {
        
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.BanksListURL,
                                      parametes: parametes,
                                      httpMethd: APIMethod.get,
                                      onSuccess: { (apiResponse) in
                                        let banksListObject = ListOfBanksModel.init(apiResponse["data"])
                                        onSuccess(banksListObject)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    //creativesNearby
    class func creativesNearby(parametes: [String: Any]?,
                               onSuccess: @escaping (CreativesNearByModel?) -> Void,
                               onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.CreativesNearByURL,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        
                                        let creativesObject = CreativesNearByModel.init(apiResponse["data"])
                                        onSuccess(creativesObject)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func validateEmail(parametes: [String: Any]?,
                             onSuccess: @escaping (OTPModule?) -> Void,
                             onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.validateEmail,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let object = OTPModule.init(apiResponse["response"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    class func invite(parametes: [String: Any]?,
                      onSuccess: @escaping (OTPModule?) -> Void,
                      onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.invite,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let object = OTPModule.init(apiResponse["response"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func confirmOTP(parametes: [String: Any]?,
                          onSuccess: @escaping (SignUpModel?) -> Void,
                          onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.validateEmail,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        // APPDBModel.saveUserInDB(apiResponse["response"][0]["data"][0])
                                        //let loginObject = MyProfile.init(apiResponse["response"][0]["data"][0])
                                        // onSuccess(loginObject)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func updatePassword(parametes: [String: Any]?,
                              onSuccess: @escaping (Bool?) -> Void,
                              onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.updatePassword,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        // APPDBModel.saveUserInDB(apiResponse["response"][0]["data"][0])
                                        //let loginObject = MyProfile.init(apiResponse["response"][0]["data"][0])
                                        onSuccess(true)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    
    class func addProject(parametes: [String: Any]?,
                          onSuccess: @escaping (Project?) -> Void,
                          onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.addProject,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let project = Project.init(apiResponse["data"])
                                        onSuccess(project)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func editProject(parametes: [String: Any]?,
                           onSuccess: @escaping (Project?) -> Void,
                           onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.editProject,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let project = Project.init(apiResponse["data"])
                                        APPDBModel.updateProjectInDB(apiResponse["data"])
                                        onSuccess(project)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func getProjectList(parametes: [String: Any]?,
                              onSuccess: @escaping (ProjectListModel?) -> Void,
                              onFailure: @escaping APIErrorHandler) {
        var pendeningProjectsURL : String?
        
        if APPStore.sharedStore.appUserID == "1"{
            
            pendeningProjectsURL = APPURL.getContractorAwaitingProject

        }else
        {
            pendeningProjectsURL = APPURL.getPendingProject
        }
        
        RestClient.dataRequestAPICall(apiEndUrl: pendeningProjectsURL!,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        // APPDBModel.saveUserInDB(apiResponse["response"][0]["data"][0])
                                        let object = ProjectListModel.init(apiResponse["data"])
                                        APPDBModel.saveProjectsInDB(apiResponse["data"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    class func getCurrentProjectList(parametes: [String: Any]?,
                                     onSuccess: @escaping (ProjectListModel?) -> Void,
                                     onFailure: @escaping APIErrorHandler) {
        
        var currentOrQotationProjectsURL : String?
        
        if APPStore.sharedStore.appUserID == "1"{
            
            currentOrQotationProjectsURL = APPURL.getContractorQuotationProject
            
            
        }else
        {
            currentOrQotationProjectsURL = APPURL.getCurrentProject
        }
        
        RestClient.dataRequestAPICall(apiEndUrl: currentOrQotationProjectsURL!,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        // APPDBModel.saveUserInDB(apiResponse["response"][0]["data"][0])
                                        let object = ProjectListModel.init(apiResponse["data"])
                                        APPDBModel.saveProjectsInDB(apiResponse["data"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    class func getAttachmentsList(parametes: [String: Any]?,
                                  onSuccess: @escaping (AttachmentsModel?) -> Void,
                                  onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.getAttachments,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        // APPDBModel.saveUserInDB(apiResponse["response"][0]["data"][0])
                                        let object = AttachmentsModel.init(apiResponse["data"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    class func getCompletedProjectList(parametes: [String: Any]?,
                                       onSuccess: @escaping (ProjectListModel?) -> Void,
                                       onFailure: @escaping APIErrorHandler) {
        var completedOrInProgressProjectsURL : String?
        
        if APPStore.sharedStore.appUserID == "1"{
            
            completedOrInProgressProjectsURL = APPURL.getContractorInProgressProject
            
            
        }else
        {
            completedOrInProgressProjectsURL = APPURL.getCompletedProject
        }
        
        RestClient.dataRequestAPICall(apiEndUrl: completedOrInProgressProjectsURL!,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        // APPDBModel.saveUserInDB(apiResponse["response"][0]["data"][0])
                                        let object = ProjectListModel.init(apiResponse["data"])
                                        APPDBModel.saveProjectsInDB(apiResponse["data"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    class func getCancelledProjectList(parametes: [String: Any]?,
                                       onSuccess: @escaping (ProjectListModel?) -> Void,
                                       onFailure: @escaping APIErrorHandler) {
        
        var cancelledOrCompletedProjectsURL : String?
        
        if APPStore.sharedStore.appUserID == "1"{
            
            cancelledOrCompletedProjectsURL = APPURL.getContractorCompletedProject
            
            
        }else
        {
            cancelledOrCompletedProjectsURL = APPURL.getCancelledProject
        }
        
        
        RestClient.dataRequestAPICall(apiEndUrl: cancelledOrCompletedProjectsURL!,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        // APPDBModel.saveUserInDB(apiResponse["response"][0]["data"][0])
                                        let object = ProjectListModel.init(apiResponse["data"])
                                        APPDBModel.saveProjectsInDB(apiResponse["data"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    class func getEstimatedProjectList(parametes: [String: Any]?,
                                       onSuccess: @escaping (EstimatedProjectListModel?) -> Void,
                                       onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.getEstimatedProject,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        // APPDBModel.saveUserInDB(apiResponse["response"][0]["data"][0])
                                        let object = EstimatedProjectListModel.init(apiResponse["data"])
                                        //                                        APPDBModel.saveProjectsInDB(apiResponse["data"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    class func getFeed(parametes: [String: Any]?,
                       onSuccess: @escaping (FeedModel?) -> Void,
                       onFailure: @escaping APIErrorHandler) {
        var feedUrl = ""
        if parametes!["project_id"] != nil {
            feedUrl = APPURL.getProjectFeed
        } else {
            feedUrl = APPURL.getFeed
        }
        RestClient.dataRequestAPICall(apiEndUrl: feedUrl,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let object = FeedModel.init(apiResponse["data"])
                                        APPDBModel.saveFeedsInDB(apiResponse["data"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func getComments(parametes: [String: Any]?,
                           onSuccess: @escaping (FeedCommentModel?) -> Void,
                           onFailure: @escaping APIErrorHandler) {
        var getCommentsUrl = ""
        if parametes!["project_id"] != nil {
            getCommentsUrl = APPURL.contractorGetComment
        } else {
            getCommentsUrl = APPURL.getComment
        }
        RestClient.dataRequestAPICall(apiEndUrl: getCommentsUrl,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let object = FeedCommentModel.init(apiResponse["data"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func postComment(parametes: [String: Any]?,
                           onSuccess: @escaping (AddCommentModel?) -> Void,
                           onFailure: @escaping APIErrorHandler) {
        var postCommentsUrl = ""
        if parametes!["project_id"] != nil {
            postCommentsUrl = APPURL.contractorPostComment
        } else {
            postCommentsUrl = APPURL.postComment
        }
        RestClient.dataRequestAPICall(apiEndUrl: postCommentsUrl,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let object = AddCommentModel.init(apiResponse["data"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func postFeed(parametes: [String: Any]?,
                        onSuccess: @escaping (OTPModule?) -> Void,
                        onFailure: @escaping APIErrorHandler) {
        var postFeedUrl = ""
        if parametes!["project_id"] != nil {
            postFeedUrl = APPURL.contractorPostFeed
        } else {
            postFeedUrl = APPURL.postFeed
        }
        RestClient.dataRequestAPICall(apiEndUrl: postFeedUrl,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let object = OTPModule.init(apiResponse["response"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func getFeedLikes(parametes: [String: Any]?,
                            onSuccess: @escaping (FeedLikesModel?) -> Void,
                            onFailure: @escaping APIErrorHandler) {
        var getLikesUrl = ""
        if parametes!["project_id"] != nil {
            getLikesUrl = APPURL.ContractorGetLike
        } else {
            getLikesUrl = APPURL.getLike
        }
        RestClient.dataRequestAPICall(apiEndUrl: getLikesUrl,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let object = FeedLikesModel.init(apiResponse["data"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func getRolesList(parametes: [String: Any]?,
                            onSuccess: @escaping (RoleModel?) -> Void,
                            onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.getRoles,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        // APPDBModel.saveUserInDB(apiResponse["response"][0]["data"][0])
                                        let object = RoleModel.init(apiResponse["response"])
                                        APPDBModel.saveRolesInDB(apiResponse["response"]["List"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func getServiceList(parametes: [String: Any]?,
                              onSuccess: @escaping (ServiceListModel?) -> Void,
                              onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.getServices,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let object = ServiceListModel.init(apiResponse["response"])
                                        APPDBModel.saveServicesInDB(apiResponse["response"]["List"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func inviteTeamMember(parametes: [String: Any]?,
                                onSuccess: @escaping (InviteTeamMemberModel?) -> Void,
                                onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.inviteTeamMenber,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        // APPDBModel.saveUserInDB(apiResponse["response"][0]["data"][0])
                                        let object = InviteTeamMemberModel.init(apiResponse["response"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func getProjectContacts(parametes: [String: Any]?,
                                  onSuccess: @escaping (ProjectContactsModel?) -> Void,
                                  onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.projectContacts,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let object = ProjectContactsModel.init(apiResponse["data"])
                                        //APPDBModel.saveFeedsInDB(apiResponse["response"]["List"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func addTask(parametes: [String: Any]?,
                       onSuccess: @escaping (OTPModule?) -> Void,
                       onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.createTask,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let object = OTPModule.init(apiResponse["response"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func editTask(parametes: [String: Any]?,
                        onSuccess: @escaping (Task) -> Void,
                        onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.editTask,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        if let taskIs = Task.init(apiResponse["response"]["data"]) {
                                            APPDBModel.updateTaskInDB(apiResponse["response"]["data"])
                                            APPDBModel.saveTimeLogsInDB(apiResponse["response"]["data"]["timeLog"])
                                            onSuccess(taskIs)
                                        }
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    
    class func sendLikeUnlike(parametes: [String: Any]?,
                              onSuccess: @escaping (FeedLikeUnlikeModel?) -> Void,
                              onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.postLike,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let object = FeedLikeUnlikeModel.init(apiResponse["data"])
                                        APPDBModel.updateFeedLike(apiResponse["data"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func getProjectTasks(parametes: [String: Any]?,
                               onSuccess: @escaping (TaskModel?) -> Void,
                               onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.projectTasks,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        print("data is",apiResponse)
                                        let loginObject = TaskModel.init(apiResponse["data"])
                                        APPDBModel.saveTasksInDB(apiResponse["data"])
                                        for task in apiResponse["data"].arrayValue {
                                            APPDBModel.saveTimeLogsInDB(task["timeLog"])
                                        }
                                        onSuccess(loginObject)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func getContractorTimesheets(parametes: [String: Any]?,
                                       onSuccess: @escaping (TimeSheetModel?) -> Void,
                                       onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.timesheetTasks,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let object = TimeSheetModel.init(apiResponse["response"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    
    
    class func joinTeam(parametes: [String: Any]?,
                        onSuccess: @escaping (JoinMemberModel?) -> Void,
                        onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.joinTeam,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let object = JoinMemberModel.init(apiResponse["response"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func switchRole(parametes: [String: Any]?,
                          onSuccess: @escaping (UserRoleModel?) -> Void,
                          onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.switchRole,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        // APPDBModel.saveUserInDB(apiResponse["response"][0]["data"][0])
                                        let object = UserRoleModel.init(apiResponse["response"])
                                        if let dataIs = object?.data {
                                            APPDBModel.updateUserRoleInDB(dataIs.user_id ?? 0, role: dataIs.user_type ?? 0)
                                            APPStore.sharedStore.user.user_type = dataIs.user_type ?? 0
                                            APPUtility.setUserType()
                                        }
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func getUserContacts(parametes: [String: Any]?,
                               onSuccess: @escaping (UserContactsModel?) -> Void,
                               onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.userContacts,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let object = UserContactsModel.init(apiResponse["response"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    class func getContactProfile(parametes: [String: Any]?,
                                 onSuccess: @escaping (ContractorProfileModel?) -> Void,
                                 onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.contactProfile,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let object = ContractorProfileModel.init(apiResponse["response"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func updateProfile(parametes: [String: Any]?,
                             onSuccess: @escaping (SignUpPasswordModel?) -> Void,
                             onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.profileUpdate,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let object = SignUpPasswordModel.init(apiResponse["records"])
                                        APPDBModel.saveUserInDB(apiResponse["records"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func getCountryList(parametes: [String: Any]?,
                              onSuccess: @escaping (CountryModel?) -> Void,
                              onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.country,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let object = CountryModel.init(apiResponse["response"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func getStateList(parametes: [String: Any]?,
                            onSuccess: @escaping (StateModel?) -> Void,
                            onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.state,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let object = StateModel.init(apiResponse["response"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func getCityList(parametes: [String: Any]?,
                           onSuccess: @escaping (CityModel?) -> Void,
                           onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.city,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let object = CityModel.init(apiResponse["response"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func taskAcceptDecline(parametes: [String: Any]?,
                                 onSuccess: @escaping (Task?) -> Void,
                                 onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.taskAcceptDecline,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        if let taskIs = Task.init(apiResponse["response"]["data"]) {
                                            APPDBModel.updateTaskInDB(apiResponse["response"]["data"])
                                            onSuccess(taskIs)
                                        }
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func taskStatusUpdate(parametes: [String: Any]?,
                                onSuccess: @escaping (OTPModule?) -> Void,
                                onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.taskStatusUpdate,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                     let taskIs = OTPModule.init(apiResponse["response"]) 
                                           
                                            onSuccess(taskIs)
                        
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func updateTaskLog(parametes: [String: Any]?,
                             onSuccess: @escaping (TaskLog?) -> Void,
                             onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.updateTaskLog,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                     let taskIs = TaskLog.init(apiResponse["data"])
                                            onSuccess(taskIs)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func timeUpdate(parametes: [String: Any]?,
                          onSuccess: @escaping (OTPModule?) -> Void,
                          onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.startTimeURL,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let object = OTPModule.init(apiResponse["response"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    class func stopTimeUpdate(parametes: [String: Any]?,
                              onSuccess: @escaping (OTPModule?) -> Void,
                              onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.endTimeURL,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let object = OTPModule.init(apiResponse["response"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    class func getContractorTasks(parametes: [String: Any]?,
                                  onSuccess: @escaping (ProjectListModel?) -> Void,
                                  onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.contractorActiveTasks,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let loginObject = ProjectListModel.init(apiResponse["data"])
                                        
                                        onSuccess(loginObject)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    class func getContractorInProgressTasks(parametes: [String: Any]?,
                                  onSuccess: @escaping (TaskModel?) -> Void,
                                  onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.contractorInProgressTasks,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let loginObject = TaskModel.init(apiResponse["data"])
                                        
                                        onSuccess(loginObject)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    class func getContractorAssignedTasks(parametes: [String: Any]?,
                                            onSuccess: @escaping (TaskModel?) -> Void,
                                            onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.contractorActiveTasks,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let loginObject = TaskModel.init(apiResponse["data"])
                                        
                                        onSuccess(loginObject)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    class func getContractorPaymentsTasks(parametes: [String: Any]?,
                                            onSuccess: @escaping (TaskModel?) -> Void,
                                            onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.contractorPaymentsTasks,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let loginObject = TaskModel.init(apiResponse["data"])
                                        
                                        onSuccess(loginObject)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    
    class func sendQuotation(parametes: [String: Any]?,
                          onSuccess: @escaping (OTPModule?) -> Void,
                          onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.sendQuotaion,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let object = OTPModule.init(apiResponse["response"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    class func estimationStatus(parametes: [String: Any]?,
                             onSuccess: @escaping (OTPModule?) -> Void,
                             onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.estimationStatus,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let object = OTPModule.init(apiResponse["response"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
    class func cancelProject(parametes: [String: Any]?,
                             onSuccess: @escaping (OTPModule?) -> Void,
                             onFailure: @escaping APIErrorHandler) {
        RestClient.dataRequestAPICall(apiEndUrl: APPURL.cancelledProject,
                                      parametes: parametes,
                                      httpMethd: APIMethod.post,
                                      onSuccess: { (apiResponse) in
                                        dprint(object: apiResponse)
                                        let object = OTPModule.init(apiResponse["response"])
                                        onSuccess(object)
        },
                                      onError: { (error) in
                                        Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
                                        onFailure(error)
        })
    }
}
