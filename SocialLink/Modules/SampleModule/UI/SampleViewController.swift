//
//  SampleViewController.swift
//  BaseProject
//
//  Created by Santhosh Marripelli on 23/02/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit
import CoreLocation

class SampleViewController: AppBaseVC {
    
    @IBOutlet weak var appImageView: UIImageView!
    @IBOutlet weak var networkRechability: UILabel!
    var viewModel: SampleViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = SampleViewModel()
        
          networkRechability.setReguralFont(size: 30)
        
        ////// Loading Image using SWWEB Image
        let imgURL = URL(string: "https://media.alienwarearena.com/media/1327-a.jpg")
        appImageView.sd_setImage(with: imgURL, placeholderImage: #imageLiteral(resourceName: "dangerIcon"))
        
       ///// Accessing UserLocation
       self.startListiningLocaton()
        
        

    }
    
    @IBAction func checkNetwork(_ sender: Any) {
        if self.isReachable! {
            networkRechability.text =  "Recahable"
        } else {
            networkRechability.text =  "Not Recahable"
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func getSampleDataClicked(_ sender: Any) {
        if let model = viewModel {
            let parms = ["key1": "value1",
                         "key2": "value2"]
            self.showHUD(title: "Getting Data")
            model.getSampleData(parameters: parms, onSuccess: { [weak self] (_) in
                print("Success")
                self?.hideHud()

                }, onError: { (error) in
                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
                    self.hideHud()

            })
        }
  
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
 
    
}


