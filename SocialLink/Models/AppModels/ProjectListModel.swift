//
//  ProjectListModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 16/08/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON


class ProjectListModel: AppBaseModel {
    
    var service: String?
    var count: Int64?
    var list: [Project]?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    
    private func parseResponse(with response: JSON) {
        print(response)
        self.service = response["service"].stringValue
         self.count = response["count"].int64Value
        for item in response.arrayValue {
            if let projectModel = self.createObject(with: item) {
                if self.list == nil {
                    self.list = [Project]()
                }
                self.list?.append(projectModel)
            }
        }
    }
    
    private func createObject(with jsonObject: JSON) -> Project? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return Project.init(jsonObject)
        }
    }
    
}
class EstimatedProjectListModel: AppBaseModel {
    
    
    var list: [EstimatedProject]?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
    }
    
    private func parseResponse(with response: JSON) {
        print(response)
        for item in response.arrayValue {
            if let projectModel = self.createObject(with: item) {
                if self.list == nil {
                    self.list = [EstimatedProject]()
                }
                self.list?.append(projectModel)
            }
        }
    }
    private func createObject(with jsonObject: JSON) -> EstimatedProject? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return EstimatedProject.init(jsonObject)
        }
    }
    
}

class EstimatedProject: AppBaseModel {
    
    var quot_id: Int64?
    var user_id: Int64?
    var ptype: Int64?
    var pid: Int64?
    var quote_scope: String?
    var ebudget: String?
    var etime: String?
    var q_status: Int64?
    var cdate: String?
    var pname: String?
   
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        print(response)
       self.quot_id = response["quot_id"].int64Value
        self.user_id = response["user_id"].int64Value
        self.ptype = response["ptype"].int64Value
        self.pid = response["pid"].int64Value
        self.quote_scope = response["quote_scope"].stringValue
        self.ebudget = response["ebudget"].stringValue
        self.etime = response["etime"].stringValue
        self.q_status = response["q_status"].int64Value
        self.cdate = response["cdate"].stringValue
        self.pname = response["pname"].stringValue
        
    }
    
    
    private func createObject(with jsonObject: JSON) -> CreatedBy? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return CreatedBy.init(jsonObject)
        }
    }
}

class Project: AppBaseModel {
    
    var project_id: Int64?
    var user_id: Int64?
    var project_name: String?
    var project_Type: Int64?
    var start_date: String?
    var endDate: String?
    var statusIs: Int64?
    var progress: Float?
    var created_by: CreatedBy?
    var skills: String?
    var description: String?
    var estimatedTime: String?
    var budget: String?
    var pupload: String?
    var cdate: String?

    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        print(response)
        self.project_id = response["pid"].int64Value
        self.project_Type = response["ptype"].int64Value
        self.user_id = response["user_id"].int64Value
        self.project_name = response["pname"].stringValue
        self.start_date = response["psdate"].stringValue
        self.endDate = response["pdeadline"].stringValue
        self.statusIs = response["pstatus"].int64Value
        self.progress = response["progress"].floatValue
        self.created_by = self.createObject(with: response["created_by"])
        self.skills = response["pskills"].stringValue
        self.description = response["pscope"].stringValue
        self.estimatedTime = response["etime"].stringValue
        self.budget = response["ebudget"].stringValue
        self.cdate = response["cdate"].stringValue
        self.pupload = response["pupload"].stringValue
        
    }
    
    private func createObject(with jsonObject: JSON) -> CreatedBy? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return CreatedBy.init(jsonObject)
        }
    }
}

class CreatedBy: AppBaseModel {
    
    var name: String?
    var image: String?
    var id: Int64?
    var designation: String?
    var emailid: String?
    var phoneNumber: String?

    
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        self.name = response["mname"].stringValue
        self.image = response["image"].stringValue
        self.emailid = response["emailid"].stringValue
        self.designation = response["design"].stringValue
        self.id = response["user_id"].int64Value
        self.phoneNumber = response["phone"].stringValue
    }
}
