//
//  ChatHistoryModel.swift
//  SocialLink
//
//  Created by Harsha on 25/04/19.
//

import Foundation
import SwiftyJSON

class ChatHistory: AppBaseModel {
    var type = [String]()
    var subtype = [String]()
    var text = [String]()
    var user = [String]()
    var ts = [String]()
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    private func parseResponse(with response: JSON) {
        
        for i in 0..<response.count{
            self.type.append(response[i]["type"].stringValue)
            self.subtype .append(response[i]["subtype"].stringValue)
            self.text.append(response[i]["text"].stringValue)
            self.user .append(response[i]["user"].stringValue)
            self.ts.append(response[i]["ts"].stringValue)
        }
        
    }
    
}
class PostChat: AppBaseModel {
    var bot_id : String?
    var type : String?
    var text : String?
    var user : String?
    var ts : String?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    private func parseResponse(with response: JSON) {
        self.bot_id = response["bot_id"].stringValue
        self.type = response["type"].stringValue
        self.text = response["text"].stringValue
        self.user = response["user"].stringValue
        self.ts = response["ts"].stringValue
    }
}
