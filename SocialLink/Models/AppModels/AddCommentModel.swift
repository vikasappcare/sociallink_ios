//
//  AddCommentModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 11/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON


class AddCommentModel: AppBaseModel {
    
    var service: String?
    var count: Int64?
    var comment: CommentedUsers?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    
    private func parseResponse(with response: JSON) {
        self.service = response["service"].stringValue
        self.count = response["count"].int64Value
        self.comment = self.createObject(with: response["data"])

    }
    
    private func createObject(with jsonObject: JSON) -> CommentedUsers? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return CommentedUsers.init(jsonObject)
        }
    }
    
}



