//
//  ReportViewController.swift
//  InternalMembers
//
//  Created by Santhosh Marripelli on 23/04/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ReportViewController: UIViewController {
    
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var reportText: UITextView!
    @IBOutlet weak var reportLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nametextField: AppTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createNavigationItems()
        // Do any additional setup after loading the view.
        reportText.font = UIFont.regularFont(15)
        reportLabel.font = UIFont.regularFont(15)
        
        nameLabel.setReguralFont(size: 15)
        nametextField.setReguralFont(size: 15)
        
        submitButton.setSemiBoldFont(size: 15)
        
        
        self.navigationItem.title = "REPORT"
    }
    
    @IBAction func submitClicked(_ sender: Any) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ReportViewController {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
}
