//
//  CountryListVC.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 16/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

protocol CountryListVCDelegate: class {
    func selectedCountry(country: Country)
    func selectedState(state: State)
    func selectedCity(city: City)
}


class CountryListVC: AppBaseVC {
    weak var delegate: CountryListVCDelegate?
    @IBOutlet weak var searchController: UISearchBar!
    @IBOutlet weak var countryTable: UITableView!
    var controllerType: String?
    var viewModel: CountryViewModel?
    var countryId: Int64?
    var stateId: Int64?
    override func viewDidLoad() {
        super.viewDidLoad()
        searchController.delegate = self
        viewModel = CountryViewModel()
        // Do any additional setup after loading the view.
        if let type = controllerType {
            switch type {
            case ProfileSelect.Country:
                self.title = "Select Country"
                self.getCountries()
            case ProfileSelect.State:
                self.title = "Select State"
                self.getStates()
            case ProfileSelect.City:
                self.title = "Select City"
                self.getCities()
            default:
                break
            }
        }
        self.createNavigationItems()
        self.setTableViewDatasource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setControllerType(type: String) {
        self.controllerType = type
    }
    
    func setCountryId(id: Int64) {
        self.countryId = id
    }
    
    func setStateId(id: Int64) {
        self.stateId = id
    }
    
    
    func getCountries() {
        if let model = viewModel {
            self.showHUD(title: "")
            let parms = ["token": APPStore.sharedStore.token] as [String : Any]
            model.getCountryList(parameters: parms, onSuccess: { [weak self] (dataSource) in
                self?.hideHud()
                   self?.countryTable.reloadData()
                }, onError: { (error) in
                    self.hideHud()
                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
                  //  self.showAlert(title: Message.Alert, message: error.statusMessage)
            })
            
        }
    }
    
    func getStates() {
        if let model = viewModel {
            self.showHUD(title: "")
            if let countyrIdIs = self.countryId {
                let parms = ["token": APPStore.sharedStore.token,
                             "country_id": countyrIdIs] as [String : Any]
                model.getStateList(parameters: parms, onSuccess: { [weak self] (dataSource) in
                    self?.hideHud()
                        self?.countryTable.reloadData()
                    }, onError: { (error) in
                        self.hideHud()
                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
                      //  self.showAlert(title: Message.Alert, message: error.statusMessage)
                })
            }
            
            
        }
    }
    
    func getCities() {
        if let model = viewModel {
            self.showHUD(title: "")
            if let countyrIdIs = self.countryId, let stateIdIs = self.stateId {
                let parms = ["token": APPStore.sharedStore.token,
                             "country_id": countyrIdIs,
                             "state_id": stateIdIs] as [String : Any]
                model.getCityList(parameters: parms, onSuccess: { [weak self] (dataSource) in
                    self?.hideHud()
                    self?.countryTable.reloadData()
                    }, onError: { (error) in
                        self.hideHud()
                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
                       // self.showAlert(title: Message.Alert, message: error.statusMessage)
                })
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension CountryListVC {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
    
}


extension CountryListVC: UISearchBarDelegate {
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let model = viewModel {
            if let type = controllerType {
                switch type {
                case ProfileSelect.Country:
                    if !searchText.isEmpty {
                        model.countryArray = model.tempCountryArray?.filter{($0.name?.contains(searchText))!}
                    } else {
                        model.countryArray = model.tempCountryArray
                    }
                case ProfileSelect.State:
                    if !searchText.isEmpty {
                        model.stateArray = model.tempStateArray?.filter{($0.name?.contains(searchText))!}
                    } else {
                        model.stateArray = model.tempStateArray
                    }
                case ProfileSelect.City:
                    if !searchText.isEmpty {
                        model.cityArray = model.tempCityArray?.filter{($0.name?.contains(searchText))!}
                    } else {
                        model.cityArray = model.tempCityArray
                    }
                default:
                   break
                }
            }
            self.countryTable.reloadData()
        }
    }
    
    
}



extension CountryListVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.countryTable.delegate = self
        self.countryTable.dataSource = self
        self.countryTable.estimatedRowHeight = 300
        self.countryTable.rowHeight = UITableViewAutomaticDimension
        self.countryTable.tableFooterView = UIView()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let model = viewModel {
            if let type = controllerType {
                switch type {
                case ProfileSelect.Country:
                    return model.countryArray?.count ?? 0
                case ProfileSelect.State:
                    return model.stateArray?.count ?? 0
                case ProfileSelect.City:
                    return model.cityArray?.count ?? 0
                default:
                    return 0
                }
            }
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:
            CellIdentifiers.CountryCellIdentifier, for: indexPath)
        if let cellIs = cell as? CountryCell {
            cellIs.selectionStyle = .none
            if let model = viewModel {
                if let type = controllerType {
                    switch type {
                    case ProfileSelect.Country:
                        cellIs.setData(data: model.countryArray![indexPath.row].name ?? "")
                    case ProfileSelect.State:
                        cellIs.setData(data: model.stateArray![indexPath.row].name ?? "")
                    case ProfileSelect.City:
                        cellIs.setData(data: model.cityArray![indexPath.row].name ?? "")
                    default:
                        break
                    }
                }
            }
        }
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.001, execute: {
            if let model = self.viewModel {
                if let type = self.controllerType {
                switch type {
                case ProfileSelect.Country:
                    self.delegate?.selectedCountry(country: model.countryArray![indexPath.row])
                case ProfileSelect.State:
                    self.delegate?.selectedState(state: model.stateArray![indexPath.row])
                case ProfileSelect.City:
                    self.delegate?.selectedCity(city: model.cityArray![indexPath.row])
                default:
                    break
                }
            }
        }
        
        self.dismiss(animated: true, completion: nil)
             })
    }
    
}


