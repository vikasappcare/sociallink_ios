//
//  UserPermissions.swift
//  BaseProject
//
//  Created by Santhosh Marripelli on 25/02/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

protocol LocationServiceDelegate: class {
    func tracingLocation(currentLocation: CLLocation)
    func tracingLocationDidFailWithError(error: NSError)
}

class LocationSingleton: NSObject, CLLocationManagerDelegate {
    var locationManager: CLLocationManager?
    var lastLocation: CLLocation?
     var currentLoc: CLLocation?
    var currentAddress: String?
   weak var delegate: LocationServiceDelegate?
    
    static let sharedInstance: LocationSingleton = {
        let instance = LocationSingleton()
        return instance
    }()
    
    override init() {
        super.init()
        self.locationManager = CLLocationManager()
        
        guard let locationManagers=self.locationManager else {
            return
        }
        
        if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManagers.requestAlwaysAuthorization()
            locationManagers.requestWhenInUseAuthorization()
        }
        if #available(iOS 9.0, *) {
            //            locationManagers.allowsBackgroundLocationUpdates = true
        } else {
            // Fallback on earlier versions
        }
        locationManagers.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManagers.pausesLocationUpdatesAutomatically = false
        locationManagers.distanceFilter = 0.1
        locationManagers.delegate = self
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {
            return
        }
        self.lastLocation = location
        updateLocation(currentLocation: location)
        
    }
    
    @nonobjc func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            locationManager?.requestAlwaysAuthorization()
            break
        case .authorizedWhenInUse:
            locationManager?.startUpdatingLocation()
            break
        case .authorizedAlways:
            locationManager?.startUpdatingLocation()
            break
        case .restricted:
            // restricted by e.g. parental controls. User can't enable Location Services
            break
        case .denied:
            // user denied your app access to Location Services, but can grant access from Settings.app
            break
        default: break
        }
    }
    
    
    
    // Private function
    private func updateLocation(currentLocation: CLLocation) {
        
        guard let delegate = self.delegate else {
            return
        }
        self.currentLoc = currentLocation
        self.getAddressFromLatLon(latitude: currentLocation.coordinate.latitude, withLongitude: currentLocation.coordinate.longitude)
        delegate.tracingLocation(currentLocation: currentLocation)
    }
    
    private func updateLocationDidFailWithError(error: NSError) {
        
        guard let delegate = self.delegate else {
            return
        }
        
        delegate.tracingLocationDidFailWithError(error: error)
    }
    
    
    private func getAddressFromLatLon(latitude: Double, withLongitude longitude: Double) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(latitude)")!
        let lon: Double = Double("\(longitude)")!
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
//        ceo.reverseGeocodeLocation(loc, completionHandler:
//            {(placemarks, error) in
//                if (error != nil)
//                {
//                    print("reverse geodcode fail: \(error!.localizedDescription)")
//                }
//                let pm = placemarks! as [CLPlacemark]
//                
//                if pm.count > 0 {
//                    let pm = placemarks![0]
////                    print(pm.country ?? "")
////                    print(pm.locality ?? "")
////                    print(pm.subLocality ?? "")
////                    print(pm.thoroughfare ?? "")
////                    print(pm.postalCode ?? "")
////                    print(pm.subThoroughfare ?? "")
//                    if pm.locality != nil {
//                        self.currentAddress = pm.locality
//                    }
//                    if pm.country != nil {
//                        self.currentAddress = self.currentAddress ?? "" + pm.country! ?? ""
//                    }
////                    var addressString : String = ""
////                    if pm.subLocality != nil {
////                        addressString = addressString + pm.subLocality! + ", "
////                    }
////                    if pm.thoroughfare != nil {
////                        addressString = addressString + pm.thoroughfare! + ", "
////                    }
////                    if pm.locality != nil {
////                        addressString = addressString + pm.locality! + ", "
////                    }
////                    if pm.country != nil {
////                        addressString = addressString + pm.country! + ", "
////                    }
////                    if pm.postalCode != nil {
////                        addressString = addressString + pm.postalCode! + " "
////                    }
//                   // print(addressString)
//                    
//                }
//        })
        
    }
    
    
    
    
    
    
    
    
    
    func startUpdatingLocation() {
        print("Starting Location Updates")
        self.locationManager?.startUpdatingLocation()
        //        self.locationManager?.startMonitoringSignificantLocationChanges()
    }
    
    func stopUpdatingLocation() {
        print("Stop Location Updates")
        self.locationManager?.stopUpdatingLocation()
    }
    
    func startMonitoringSignificantLocationChanges() {
        self.locationManager?.startMonitoringSignificantLocationChanges()
    }
}
