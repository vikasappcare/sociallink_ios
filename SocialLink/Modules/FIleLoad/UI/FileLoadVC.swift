//
//  FileLoadVC.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 27/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit
import WebKit


class FileLoadVC: AppBaseVC, WKNavigationDelegate {

    
    var loadWebView: WKWebView!
    var showURL: String?
    
    override func loadView() {
        loadWebView = WKWebView()
        loadWebView.navigationDelegate = self
        view = loadWebView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let urlIs = showURL {
            if let url = URL(string: urlIs) {
                loadWebView.load(URLRequest(url: url))
                loadWebView.allowsBackForwardNavigationGestures = true
            }
        }
        self.addActivityIndicatior()
        self.createNavigationItems()

        // Do any additional setup after loading the view.
    }
    
    func setUrl(url: String) {
        showURL = url
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("FInish")
        self.removeActivityIndicator()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FileLoadVC {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
    
}
