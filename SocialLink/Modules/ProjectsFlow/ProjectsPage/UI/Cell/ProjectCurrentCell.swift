//
//  ProjectCurrentCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 26/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ProjectCurrentCell: UITableViewCell {

    @IBOutlet weak var projectProgress: UIProgressView!
    @IBOutlet weak var progressBarLabel: UILabel!
    @IBOutlet weak var projectId: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var endDate: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var cornerImage: UIImageView!
    @IBOutlet weak var estimatedTime: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.projectProgress.layer.cornerRadius = 5.0
//        self.projectProgress.layer.masksToBounds = true
//        
        
    }
    func setData(data: Project, color: UIColor) {
        projectId.text = "\(String(describing: data.project_name ?? ""))"
        self.startDate.text = data.start_date ?? ""
        self.endDate.text = data.endDate ?? ""
      //  self.estimatedTime.text = data.estimatedTime ?? ""
        self.descriptionLabel.text = data.description ?? ""
        statusLabel.backgroundColor = color
       // let progress = (data.progress ?? 0.0) / 100
       // projectProgress.progress = progress
        statusLabel.text = APPUtility.getProjectStatus(value: data.statusIs ?? 5)

        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
