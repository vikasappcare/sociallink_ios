//
//  ChooseServiceCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 12/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ChooseServiceCell: UITableViewCell {

    @IBOutlet weak var backView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
//        let maskPath = UIBezierPath.init(roundedRect: backView.bounds, byRoundingCorners:[.topLeft, .topRight], cornerRadii: CGSize.init(width: 10.0, height: 10.0))
//        let maskLayer = CAShapeLayer()
//        maskLayer.frame = backView.bounds
//        maskLayer.path = maskPath.cgPath
//        backView.layer.mask = maskLayer

        backView.clipsToBounds = true
        backView.layer.cornerRadius = 20
        if #available(iOS 11.0, *) {
            backView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
        
        
//        backView.roundCorners(corners: .topLeft, radius: 10.0)
//        backView.roundCorners(corners: .topRight, radius: 10.0)
//        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
