//
//  ForgetPasswordVC.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 09/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ForgetPasswordVC: AppBaseVC {
    
    @IBOutlet weak var forgetPasswordTable: UITableView!
    var viewModel: ForgetPasswordViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
          viewModel = ForgetPasswordViewModel()
        self.setTableViewDatasource()
        self.addNotification()
        self.createNavigationItems()
        self.navigationItem.title = "FORGET PASSWORD"
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //    deinit {
    //        NotificationCenter.default.removeObserver(self)
    //    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ForgetPasswordVC {
    
    func addNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(dismissView),
                                               name:
            NSNotification.Name(rawValue: APPNotifications.removeForgetPassword),
                                               object: nil)
    }
    
    @objc func  dismissView(info: NSNotification) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension ForgetPasswordVC {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension ForgetPasswordVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.forgetPasswordTable.delegate = self
        self.forgetPasswordTable.dataSource = self
        self.forgetPasswordTable.estimatedRowHeight = 300
        self.forgetPasswordTable.rowHeight = UITableViewAutomaticDimension
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:
            CellIdentifiers.ForgetPasswordCellIdentifier, for: indexPath)
        if let cellIs = cell as? ForgetPasswordCell {
            cellIs.selectionStyle = .none
            cellIs.delegate = self
        }
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UIScreen.main.bounds.height - 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
extension ForgetPasswordVC: ForgetPasswordCellDelegate {
    func submitSelected() {
        
        
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = forgetPasswordTable.cellForRow(at: indexPath) as? ForgetPasswordCell
        
        let email = cell?.getData().trimmingCharacters(in: .whitespaces)
        
            if (email?.isEmpty ?? false) || (email == "") {
                self.showAlert(title: "Alert!", message: "Please enter email to continue")
                return
            } else if !APPUtility.validateEmail(enteredEmail: email ?? "") {
                self.showAlert(title: "Alert!", message: "Please enter a valid email to continue")
                return
            }
                if let model = viewModel {
                    let parms = ["uemailid": email ?? ""]
                    APPStore.sharedStore.forgetPasswordEmail = email ?? ""
                    print(parms)
                    self.showHUD(title: "Validating ...")
                    model.validateEmail(parameters: parms as [String : Any] , onSuccess: { [weak self] (dataSource) in
                        print("Success")
                        self?.hideHud()
                        let loginStoryBoard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)

                        if let controller = loginStoryBoard.instantiateViewController(withIdentifier: "CheckYourEmail") as? CheckYourEmail {
                            self?.present(controller, animated: true, completion: nil)
                            /// self.navigationController?.pushViewController(controller, animated: true)
                        }
                        }, onError: { (error) in
                            Logger.log(message: "Error \(error.statusMessage)", event: .error)
                            self.showAlert(title: "Alert", message: error.statusMessage)
                            self.hideHud()
                    })
                }


    }
    
    func goBack()
    {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
            let navController = APPNavigationController(rootViewController: controller)
            self.present(navController, animated: true, completion: nil)
            /// self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
}
