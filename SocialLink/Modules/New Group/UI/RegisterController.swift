//
//  RegisterController.swift
//  SocialLink
//
//  Created by Banda Anil Reddy on 21/02/19.
//  Copyright © 2019 Santhosh Marripelli. All rights reserved.
//

import UIKit
import iOSDropDown

class RegisterController: AppBaseVC {
    
    var viewModel: RegisterViewModel?
    var passwordTextShowing = false
    var skills = ""
    var serviceRequired = ""

    var userType = 1
    
    @IBOutlet weak var profileImageView: AppRoundImage!
    @IBOutlet weak var fullNameTextField: AppRoundBorderTextField!
    @IBOutlet weak var lastNameTextField: AppRoundBorderTextField!
    @IBOutlet weak var emailTextField: AppRoundBorderTextField!
    
    @IBOutlet weak var showPasswordButton: UIButton!
    @IBOutlet weak var passwordTextField: AppRoundBorderTextField!
    @IBOutlet weak var phoneNumberTextField: AppRoundBorderTextField!
    
    @IBOutlet weak var zipCodeTextField: AppRoundBorderTextField!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var skillsTextField: DropDown!
    @IBOutlet weak var servicesTextField: DropDown!
    
    var skillsTextFieldText = ""
    var servicesTextFieldText = ""
    var skillsArray = [String]()
    var skillsIdArray = [String]()
    var servicesSkillsIdArray = [String]()
    
    @IBOutlet weak var joinButton: UIButton!
    @IBOutlet weak var gmailLoginButton: UIButton!
    
    @IBOutlet weak var socialLinkTermsAndConditionsButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentedControl.selectedSegmentIndex = 1
        viewModel = RegisterViewModel()
        self.createNavigationItems()
        addNotification()
        let image: UIImage = UIImage(named: "SocilaLinkLogo")!
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        imageView.contentMode = .scaleAspectFit
        imageView.image = image
        self.navigationItem.titleView = imageView

        servicesTextField.isHidden = true
        segmentedControl.addTarget(self, action: #selector(segmentedControlValueChanged(_:)), for: .valueChanged)

        callServiceForSkills()
       
    }
    func callServiceForSkills()
    {
        if let model = viewModel {
            let param = ["": ""]
            self.showHUD(title: "Loading...")
            model.skillsList(parameters: param as [String : Any], onSuccess: { [weak self] (dataSource) in
                self?.hideHud()
                self?.skillsArray = (dataSource?.sName)!
                self?.skillsTextFieldDataLoading()
                },  onError: { (error) in
                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
                    self.showAlert(title: Message.Alert, message: error.statusMessage)
                    self.hideHud()
            })
        }
    }
    
    func skillsTextFieldDataLoading()
    {
        skillsTextField.optionArray = skillsArray
        skillsTextField.didSelect{(selectedText , index ,id) in
            self.skillsTextFieldText = "\(self.skillsTextFieldText), \(selectedText)"
            self.skillsIdArray.append(String(index + 1))
        
        }
        servicesTextField.optionArray = skillsArray
        servicesTextField.didSelect{(selectedText , index ,id) in
            self.servicesTextFieldText = "\(self.servicesTextFieldText), \(selectedText)"
            self.servicesSkillsIdArray.append(String(index + 1))

        }

        skillsTextField.listDidDisappear{
            if self.skillsTextFieldText.first == ","
            {
                self.skillsTextFieldText.remove(at: self.skillsTextFieldText.startIndex)
            }
            self.skillsTextField.text = self.skillsTextFieldText

            
        }
        servicesTextField.listDidDisappear{
            if self.servicesTextFieldText.first == ","
            {
                self.servicesTextFieldText.remove(at: self.servicesTextFieldText.startIndex)
            }

            self.servicesTextField.text = self.servicesTextFieldText


        }
    }
    @objc func segmentedControlValueChanged(_ sender: UISegmentedControl) {
        if(sender.selectedSegmentIndex == 0)
        {
            skillsTextField.placeholder = "Skill (ex: Web Design)";
            servicesTextField.isHidden = true
            userType = 2

        }
        else if(sender.selectedSegmentIndex == 1)
        {
            skillsTextField.placeholder = "What kind of talent do you need?";
            servicesTextField.isHidden = true
            userType = 1


        }
        else if(sender.selectedSegmentIndex == 2)
        {
            skillsTextField.placeholder = "Skill (ex: Web Design)";
            servicesTextField.isHidden = false
            userType = 3

        }
        
    }
    @IBAction func showPasswordAction(_ sender: Any) {
        if passwordTextShowing == false
        {
            passwordTextField.isSecureTextEntry = false
            passwordTextShowing = true
            showPasswordButton.setTitle("Hide password", for: .normal)
            
        }
        else
        {
            passwordTextField.isSecureTextEntry = true
            passwordTextShowing = false
            showPasswordButton.setTitle("Show password", for: .normal)

        }
    }
    @IBAction func joinButtonAction(_ sender: Any) {
        let fullName = fullNameTextField.text ?? ""
       // let lastName = lastNameTextField.text ?? ""
        let email = emailTextField.text ?? ""
        let password = passwordTextField.text?.trimmingCharacters(in: .whitespaces) ?? ""
        let phoneNumber = phoneNumberTextField.text?.replacingOccurrences(of: "+1", with: "") ?? ""
        let zipcode = zipCodeTextField.text ?? ""
        
        if(segmentedControl.selectedSegmentIndex == 0)
        {
            
            skills = skillsIdArray.joined(separator: ",")
            
        }
        else if(segmentedControl.selectedSegmentIndex == 1)
        {
            
            serviceRequired = skillsIdArray.joined(separator: ",")
            
            
        }
        else if(segmentedControl.selectedSegmentIndex == 2)
        {
            
            skills = skillsIdArray.joined(separator: ",")
            serviceRequired = servicesSkillsIdArray.joined(separator: ",")
            
        }
        
       
        let name = "\(fullName)"
       
        if (email.isEmpty == true) || (email == "") {
            self.showAlert(title: "Alert!", message: "Please enter email to continue")
            return
        } else if !APPUtility.validateEmail(enteredEmail: email ) {
            self.showAlert(title: "Alert!", message: "Please enter a valid email to continue")
            return
        }
        if (fullName.isEmpty == true) || (name == "") {
            self.showAlert(title: "Alert!", message: "Please enter name to continue")
            return
        }
        if (password.isEmpty == true) || (password == "") {
            self.showAlert(title: "Alert!", message: "Please enter password to continue")
            return
            
        }
        if (phoneNumber.isEmpty == true) || (phoneNumber == "") {
            self.showAlert(title: "Alert!", message: "Please enter phone number to continue")
            return
            
        }
        if (phoneNumber.count > 10) {
            self.showAlert(title: "Alert!", message: "Please enter a valid phone number to continue")
            return
            
        }
        if (zipcode.isEmpty == true) || (zipcode == "") {
            self.showAlert(title: "Alert!", message: "Please enter zipcode to continue")
            return
        }
        if (zipcode.count > 5)  {
            self.showAlert(title: "Alert!", message: "Please enter a valid zipcode to continue")
            return
        }
        if (skillsTextField.text?.isEmpty == true) || (skillsTextField.text == "") {
            self.showAlert(title: "Alert!", message: "Please enter skills to continue")
            return
        }
        
        if let model = viewModel {
            
            let parms = ["ufullname":name, "uemailid":email, "uphone":phoneNumber, "upassword":password, "uzipcode":zipcode, "utype":userType, "uskills":skills, "required_skills":serviceRequired] as [String : Any]
            print(parms)
             self.showHUD(title: "Loading...")
            model.registerUser(parameters: parms, onSuccess: { [weak self] (dataSource) in
                APPUtility.getUserData()
                self?.hideHud()
                self?.moveToNextController()
                
                }, onError: { (error) in
                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
                    self.showAlert(title: Message.Alert, message: error.statusMessage)
                     self.hideHud()
            })
        }
    }
    func moveToNextController() {
        
        if APPStore.sharedStore.appUserID == "2"{
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddingCardController") as? AddingCardController {
            self.present(controller, animated: true, completion: nil)
            /// self.navigationController?.pushViewController(controller, animated: true)
        }
        }
        else
        {
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddingBankAccController") as? AddingBankAccController {
                self.present(controller, animated: true, completion: nil)
                /// self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    @IBAction func gmailLoginButtonAction(_ sender: Any) {
        GIDSignIn.sharedInstance().uiDelegate = self
        // Uncomment to automatically sign in the user.
        GIDSignIn.sharedInstance().signOut()
        
        GIDSignIn.sharedInstance().signInSilently()
        
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func socialLinkTermsAndConditionsButtonAction(_ sender: Any) {
    }
    
}
extension RegisterController {

    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "back"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }

    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
}
extension RegisterController: GIDSignInUIDelegate {
    
    @objc func googleSignResponse(info: NSNotification) {
        
      
        fullNameTextField.text = info.userInfo?["firstname"] as? String
        emailTextField.text = (info.userInfo?["email"] as! String)
        
        

//        if let model = viewModel {
//        model.registerGmailUser(parameters: parms, onSuccess: { [weak self] (dataSource) in
//            APPStore.sharedStore.userIs = USERTYPE.CONTRACTOR
//            self?.navigationController?.popViewController(animated: true)
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue:
//                APPNotifications.dismissLoginView),
//                                            object: nil, userInfo: nil)
//            self?.hideHud()
//            }, onError: { (error) in
//                Logger.log(message: "Error \(error.statusMessage)", event: .error)
//                self.showAlert(title: Message.Alert, message: error.statusMessage)
//                self.hideHud()
//        })
//        }
    }
}
extension RegisterController {
    
    func addNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(googleSignResponse),
                                               name: NSNotification.Name(rawValue:
                                                APPNotifications.googleSignInNotification),
                                               object: nil)
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(dismissView),
//                                               name:
//            NSNotification.Name(rawValue: APPNotifications.dismissLoginView),
//                                               object: nil)
    }

//    @objc func  dismissView(info: NSNotification) {
//        self.dismiss(animated: true, completion: nil)
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue:
//            APPNotifications.changeRootView),
//                                        object: nil, userInfo: nil)
//    }
}
