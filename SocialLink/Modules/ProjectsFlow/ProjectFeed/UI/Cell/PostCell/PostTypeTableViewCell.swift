//
//  PostTypeTableViewCell.swift
//  SampleSliding
//
//  Created by Santhosh Marripelli on 20/04/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class PostTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var postText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(data: [String: Any]) {
        postImage.image = UIImage(named: data["image"] as? String ?? "")
        postText.text = data["text"] as? String ?? ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
