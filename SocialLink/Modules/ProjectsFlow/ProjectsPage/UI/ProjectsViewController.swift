//
//  ProjectsViewController.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ProjectsViewController: AppBaseVC {
    
    var skillsViewModel: RegisterViewModel?
    var estimatedProjectsArray: [EstimatedProject]?
    
    var indexNumber: Int?
    
    @IBOutlet weak var addNewProjectHeight: NSLayoutConstraint!
    @IBOutlet weak var addNewProjectView: UIView!
    @IBOutlet weak var projectTypeSegment: UISegmentedControl!
    @IBOutlet weak var projectTableview: UITableView!
    @IBOutlet weak var projectSearch: UISearchBar!
    var viewModel: ProjectsControllerViewModel?
    // var projectsArray = [Project]()
    var employerArray = ["New Projects",
                         "Current Projects",
                         "Completed Projects",
                         "Cancelled Projects",
                         "Estimates"]
    
    var contractorArray = ["AWaiting Projects",
                           "Quotation sent",
                           "In Progress",
                           "Completed"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "PROJECTS"
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        viewModel = ProjectsControllerViewModel()
        self.setTableViewDatasource()
        // Do any additional setup after loading the view.
        skillsViewModel = RegisterViewModel()
        print(projectTypeSegment.selectedSegmentIndex)
        validateUser()
        callServiceForSkills()
        self.addNotification()
        projectTypeSegment.makeTitleMultiline()
        projectTypeSegment.setSelectedTitleColor()
        
        UISegmentedControl.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for: .selected)
        let attr = NSDictionary(object: UIFont.boldFont(16), forKey: NSAttributedStringKey.font as NSCopying)
        UISegmentedControl.appearance().setTitleTextAttributes(attr as [NSObject: AnyObject], for: .normal)
        
        viewModel?.updateProjects()
        
        self.getProjects()
        
        self.addUserTypeSwitchNotification()
        viewModel?.updateProjectList(state: 0)
    }
    func validateUser() {
        switch APPStore.sharedStore.appUserID {
        case "2":
            projectTypeSegment.replaceSegments(segments: employerArray)
            projectTypeSegment.selectedSegmentIndex = 0
        case "1":
            projectTypeSegment.replaceSegments(segments: contractorArray)
            projectTypeSegment.selectedSegmentIndex = 0
            
        default:
            break
        }
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    func callServiceForSkills()
    {
        if let model = skillsViewModel {
            
            let param = ["": ""]
            self.showHUD(title: "Loading...")
            model.skillsList(parameters: param as [String : Any], onSuccess: { [weak self] (dataSource) in
                self?.hideHud()
                for i in 0..<dataSource!.sId.count{
                    skillsDic["\(dataSource?.sId[i] ?? "")"] = dataSource?.sName[i]
                }
                //self?.skillsTextFieldDataLoading()
                },  onError: { (error) in
                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
                    //  self.showAlert(title: Message.Alert, message: error.statusMessage)
                    self.hideHud()
            })
            
        }
    }
    func getProjects() {
        if let model = viewModel {
            var parms = [String: Any]()
            if APPStore.sharedStore.appUserID == "2"{
                parms = ["user_id": APPStore.sharedStore.user.userId ?? ""] as [String : Any]
            }
            else{
                parms = ["emailid": APPStore.sharedStore.user.email ?? ""] as [String : Any]
            }
            print(parms)
            self.showHUD(title: "Loading...")
            self.addActivityIndicatior()
            // DispatchQueue.global(qos: .background).async {
            model.getProjectList(parameters: parms, onSuccess: { [weak self] (dataSource) in
                DispatchQueue.main.async {
                    if APPStore.sharedStore.appUserID == "1"{
                        model.contractorProjectsArray = dataSource?.list
                    }
                    else
                    {
                        model.employerProjectsArray = dataSource?.list
                    }
                    self?.projectTableview.reloadData()
                    self?.removeActivityIndicator()
                }
                self?.projectTableview.reloadData()
                self?.hideHud()
                }, onError: { (error) in
                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
                    DispatchQueue.main.async {
                        // self.showAlert(title: Message.Alert, message: error.statusMessage)
                        self.removeActivityIndicator()
                        self.hideHud()
                    }
            })
            //}
        }
    }
    func getCurrentProjects() {
        if let model = viewModel {
            var parms = [String: Any]()
            if APPStore.sharedStore.appUserID == "2"{
                parms = ["user_id": APPStore.sharedStore.user.userId ?? ""] as [String : Any]
            }
            else{
                parms = ["emailid": APPStore.sharedStore.user.email ?? ""] as [String : Any]
            }
            print(parms)
            self.addActivityIndicatior()
            self.showHUD(title: "Loading...")
            DispatchQueue.global(qos: .background).async {
                model.getCurrentProjectList(parameters: parms, onSuccess: { [weak self] (dataSource) in
                    DispatchQueue.main.async {
                        
                        if APPStore.sharedStore.appUserID == "1"{
                            model.contractorProjectsArray = dataSource?.list
                        }
                        else
                        {
                            model.employerProjectsArray = dataSource?.list
                        }
                        self?.projectTableview.reloadData()
                        self?.removeActivityIndicator()
                    }
                    self?.hideHud()
                    }, onError: { (error) in
                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
                        DispatchQueue.main.async {
                            //  self.showAlert(title: Message.Alert, message: error.statusMessage)
                            self.removeActivityIndicator()
                            self.hideHud()
                        }
                })
            }
        }
    }
    func getCancelledProjects() {
        if let model = viewModel {
            var parms = [String: Any]()
            if APPStore.sharedStore.appUserID == "2"{
                parms = ["user_id": APPStore.sharedStore.user.userId ?? ""] as [String : Any]
            }
            else{
                parms = ["emailid": APPStore.sharedStore.user.email ?? ""] as [String : Any]
            }
            print(parms)
            self.addActivityIndicatior()
            self.showHUD(title: "Loading...")
            DispatchQueue.global(qos: .background).async {
                model.getCancelledProjectList(parameters: parms, onSuccess: { [weak self] (dataSource) in
                    DispatchQueue.main.async {
                        if APPStore.sharedStore.appUserID == "1"{
                            model.contractorProjectsArray = dataSource?.list
                        }
                        else
                        {
                            model.employerProjectsArray = dataSource?.list
                        }
                        self?.projectTableview.reloadData()
                        self?.removeActivityIndicator()
                    }
                    self?.hideHud()
                    }, onError: { (error) in
                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
                        DispatchQueue.main.async {
                            //self.showAlert(title: Message.Alert, message: error.statusMessage)
                            self.removeActivityIndicator()
                            self.hideHud()
                        }
                })
            }
        }
    }
    func getEstimatedProjects()
    {
        if let model = viewModel {
            var parms = [String: Any]()
            if APPStore.sharedStore.appUserID == "2"{
                parms = ["user_id": APPStore.sharedStore.user.userId ?? ""] as [String : Any]
            }
            else{
                parms = ["emailid": APPStore.sharedStore.user.email ?? ""] as [String : Any]
            }
            print(parms)
            //APPStore.sharedStore.user.userId ?? ""
            self.addActivityIndicatior()
            self.showHUD(title: "Loading...")
            
            DispatchQueue.global(qos: .background).async {
                model.getEstimatedProjectList(parameters: parms, onSuccess: { [weak self] (dataSource) in
                    DispatchQueue.main.async {
                        // print(dataSource?.list![0].pname)
                        self?.estimatedProjectsArray = dataSource?.list
                        
                        self?.projectTableview.reloadData()
                        self?.removeActivityIndicator()
                    }
                    self?.hideHud()
                    }, onError: { (error) in
                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
                        DispatchQueue.main.async {
                            //  self.showAlert(title: Message.Alert, message: error.statusMessage)
                            self.removeActivityIndicator()
                            self.hideHud()
                        }
                })
            }
        }
    }
    
    func getCompletedProjects() {
        if let model = viewModel {
            var parms = [String: Any]()
            if APPStore.sharedStore.appUserID == "2"{
                parms = ["user_id": APPStore.sharedStore.user.userId ?? ""] as [String : Any]
            }
            else{
                parms = ["emailid": APPStore.sharedStore.user.email ?? ""] as [String : Any]
            }
            print(parms)
            self.addActivityIndicatior()
            self.showHUD(title: "Loading...")
            DispatchQueue.global(qos: .background).async {
                model.getCompletedProjectList(parameters: parms, onSuccess: { [weak self] (dataSource) in
                    DispatchQueue.main.async {
                        if APPStore.sharedStore.appUserID == "1"{
                            model.contractorProjectsArray = dataSource?.list
                        }
                        else
                        {
                            model.employerProjectsArray = dataSource?.list
                        }
                        self?.projectTableview.reloadData()
                        self?.removeActivityIndicator()
                    }
                    self?.hideHud()
                    }, onError: { (error) in
                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
                        DispatchQueue.main.async {
                            //  self.showAlert(title: Message.Alert, message: error.statusMessage)
                            self.removeActivityIndicator()
                            self.hideHud()
                        }
                })
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func projectSegmentChanged(_ sender: Any) {
        if let model = viewModel {
            model.updateProjectList(state: Int64(self.projectTypeSegment.selectedSegmentIndex))
            self.projectTableview.reloadData()
        }
        if projectTypeSegment.selectedSegmentIndex == 0
        {
            getProjects()
            self.projectTableview.reloadData()
        }
        else if projectTypeSegment.selectedSegmentIndex == 1
        {
            getCurrentProjects()
            self.projectTableview.reloadData()
        }
        else if projectTypeSegment.selectedSegmentIndex == 2
        {
            getCompletedProjects()
            self.projectTableview.reloadData()
        }
        else if projectTypeSegment.selectedSegmentIndex == 3
        {
            getCancelledProjects()
            self.projectTableview.reloadData()
            
        }
        else if projectTypeSegment.selectedSegmentIndex == 4
        {
            getEstimatedProjects()
            self.projectTableview.reloadData()
        }
    }
    
    @IBAction func appprojectsClicked(_ sender: Any) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "NewProjectVC") as? NewProjectVC {
            controller.showServiceAgreement(show: true)
            controller.showClose(bool: false)
            // let navController = APPNavigationController(rootViewController: controller)
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}

extension ProjectsViewController {
    
    func addUserTypeSwitchNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(userSwitched),
                                               name:
            NSNotification.Name(rawValue: APPNotifications.userSwitched),
                                               object: nil)
    }
    @objc func userSwitched(info: NSNotification) {
        self.getProjects()
    }
}

extension ProjectsViewController {
    
    func addNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(showAddScreen),
                                               name:
            NSNotification.Name(rawValue: APPNotifications.plusProjects),
                                               object: nil)
    }
    
    @objc func  showAddScreen(info: NSNotification) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = mainStoryboard.instantiateViewController(withIdentifier: "AddViewController")
        if let addController = controller as? AddViewController {
            addController.modalPresentationStyle = .overCurrentContext
            addController.view.alpha = 0.0
            self.tabBarController?.present(addController, animated: true) { () -> Void in
                UIView.animate(withDuration: 0.025) { () -> Void in
                    addController.view.alpha = 1.0
                }
            }
        }
    }
}

extension ProjectsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.projectTableview.delegate = self
        self.projectTableview.dataSource = self
        self.projectTableview.estimatedRowHeight = 300
        self.projectTableview.rowHeight = UITableViewAutomaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if projectTypeSegment.selectedSegmentIndex == 4{
                if self.estimatedProjectsArray?.count != 0{
                    if let model = self.estimatedProjectsArray {
                        return model.count
                    }
                }
            }
            else
            {
                if APPStore.sharedStore.appUserID == "1"{
                    if let model = viewModel?.contractorProjectsArray {
                        return model.count
                    }
                }
                else
                {
                    if let model = viewModel?.employerProjectsArray {
                        return model.count
                    }
                }
            }
            
        case 1:
            if APPStore.sharedStore.appUserID == "2"
            {
                return 1
            }
            else
            {
                return 0
            }
            
        default:
            return 0
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            if projectTypeSegment.selectedSegmentIndex == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier:
                    CellIdentifiers.ProjectCurrentCellIdentifier, for: indexPath)
                if let cellIs = cell as? ProjectCurrentCell {
                    cellIs.selectionStyle = .none
                    if let model = viewModel {
                        if APPStore.sharedStore.appUserID == "1"{
                            //print(model.contractorProjectsArray![indexPath.row].project_name)
                            cellIs.setData(data: (model.contractorProjectsArray![indexPath.row]), color: UIColor.appBlueColor)
                        }
                        else
                        {
                            cellIs.setData(data: (model.employerProjectsArray![indexPath.row]), color: UIColor.appBlueColor)
                        }
                    }
                }
                cell.selectionStyle = .none
                return cell
                
            }
            else if projectTypeSegment.selectedSegmentIndex == 4 {
                let cell = tableView.dequeueReusableCell(withIdentifier:
                    CellIdentifiers.ProjectEstimationCellIdentifier, for: indexPath)
                if let cellIs = cell as? ProjectEstimationCell {
                    cellIs.selectionStyle = .none
                    cellIs.acceptButton.addTarget(self, action: #selector(acceptButton), for: .touchUpInside)
                    cellIs.declineButton.addTarget(self, action: #selector(declineButton), for: .touchUpInside)
                    cellIs.setData(data: (self.estimatedProjectsArray![indexPath.row]), color: UIColor.appBlueColor)
                    //}
                }
                cell.selectionStyle = .none
                return cell
                
            }
            else if projectTypeSegment.selectedSegmentIndex == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier:
                    CellIdentifiers.CancelledProjectsCellIdentifier, for: indexPath)
                if let cellIs = cell as? CancelledProjectsCell {
                    cellIs.selectionStyle = .none
                    if let model = viewModel {
                        if APPStore.sharedStore.appUserID == "1"{
                            cellIs.setData(data: (model.contractorProjectsArray![indexPath.row]))
                            //}
                        }
                        else{
                            
                            cellIs.setData(data: (model.employerProjectsArray![indexPath.row]))
                            
                        }
                    }
                }
                cell.selectionStyle = .none
                return cell
                
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier:
                    CellIdentifiers.ProjectTableViewCellIdentifier, for: indexPath)
                if let cellIs = cell as? ProjectTableViewCell {
                    cellIs.selectionStyle = .none
                    cellIs.quotationButton.addTarget(self, action: #selector(quotationButtonClicked(_:)), for: .touchUpInside)
                    
                    var color: UIColor?
                    switch projectTypeSegment.selectedSegmentIndex {
                    case 0:
                        color = UIColor.appBlueColor
                        if APPStore.sharedStore.appUserID == "1"{
                            cellIs.quotationButton.isHidden = false
                        }
                        else{
                            cellIs.quotationButton.isHidden = true
                        }
                    case 1:
                        color = UIColor.appprojectGreenColor
                        cellIs.quotationButton.isHidden = true
                        
                    case 2:
                        cellIs.quotationButton.isHidden = true
                        color = UIColor.appprojectGreenColor
                    default:
                        if APPStore.sharedStore.appUserID == "1"{
                            cellIs.quotationButton.isHidden = false
                        }
                        else{
                            cellIs.quotationButton.isHidden = true
                        }
                    }
                    
                    if let model = viewModel, let colorIs = color {
                        if APPStore.sharedStore.appUserID == "1"{
                            //print(model.contractorProjectsArray[indexPath.ro])
                            cellIs.setData(data: (model.contractorProjectsArray![indexPath.row]), color: colorIs, index: projectTypeSegment.selectedSegmentIndex)
                        }
                        else{
                            cellIs.setData(data: (model.employerProjectsArray![indexPath.row]), color: colorIs, index: projectTypeSegment.selectedSegmentIndex)
                            
                        }
                    }
                }
                cell.selectionStyle = .none
                return cell
            }
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.AddProjectCellIdentifier, for: indexPath)
            if let cellIs = cell as? AddProjectCell {
                cellIs.selectionStyle = .none
                cellIs.delegate = self
            }
            cell.selectionStyle = .none
            return cell
        default:
            return UITableViewCell()
        }
        
    }
    @objc func quotationButtonClicked(_ sender: UIButton)
    {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(
            title: "", style: .done, target: nil, action: nil)
        let indexNumber = sender.tag
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "QuotationController") as? QuotationController {
            if APPStore.sharedStore.appUserID == "1"
            {
                if let model = viewModel?.contractorProjectsArray {
                    controller.setProject(project: model[indexNumber])
                    self.navigationController?.pushViewController(controller, animated: true)
                }
            }
            else{
                if let model = viewModel?.employerProjectsArray {
                    controller.setProject(project: model[indexNumber])
                    self.navigationController?.pushViewController(controller, animated: true)
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        indexNumber = indexPath.row
        self.navigationItem.backBarButtonItem = UIBarButtonItem(
            title: "", style: .done, target: nil, action: nil)
        if projectTypeSegment.selectedSegmentIndex == 4{
            
        }
        else
        {
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProjectDetailVC") as? ProjectDetailVC {
                if APPStore.sharedStore.appUserID == "1"{
                    if let model = viewModel?.contractorProjectsArray {
                        controller.setProject(project: model[indexPath.row])
                        self.navigationController?.pushViewController(controller, animated: true)
                    }
                }
                else{
                    if let model = viewModel?.employerProjectsArray {
                        controller.setProject(project: model[indexPath.row])
                        self.navigationController?.pushViewController(controller, animated: true)
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        switch indexPath.section {
        case 0:
            switch APPStore.sharedStore.userIs {
            case USERTYPE.EMPLOYER:
                return true
            default:
                return false
            }
        default:
            return false
        }
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { action, index in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.001, execute: {
                if let controller = self.storyboard?.instantiateViewController(withIdentifier: "NewProjectVC") as? NewProjectVC {
                    let navControl = APPNavigationController(rootViewController: controller)
                    controller.delegate = self
                    controller.setProType(type: PROTYPE.NEW)
                    if APPStore.sharedStore.appUserID == "1"{
                        if let model = self.viewModel?.contractorProjectsArray {
                            controller.setProject(project: model[index.row])
                            controller.showClose(bool: true)
                            self.present(navControl, animated: true, completion: nil)
                        }
                    }else
                    {
                        if let model = self.viewModel?.employerProjectsArray {
                            controller.setProject(project: model[index.row])
                            controller.showClose(bool: true)
                            self.present(navControl, animated: true, completion: nil)
                        }
                    }
                }
            })
        }
        edit.backgroundColor = UIColor.lightGray
        return [edit]
    }
}

extension ProjectsViewController: NewProjectVCDelegate, AddProjectCellDelegate {
    func addProjectClicked() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "NewProjectVC") as? NewProjectVC {
            controller.showClose(bool: false)
            controller.delegate = self
            let navController = APPNavigationController(rootViewController: controller)
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    func inviteMemberClicked() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "InviteTeamMember") as? InviteTeamMember {
            controller.showBackButton(show: true, hideProject: false)
            self.navigationItem.backBarButtonItem = UIBarButtonItem(
                title: "", style: .done, target: nil, action: nil)
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func newProjectAdded(project: Project) {
        //self.projectsArray.append(project)
        self.getProjects()
        //self.projectTableview.reloadData()
    }
    
}
extension UISegmentedControl {
    
    func makeTitleMultiline(){
        for i in 0...self.numberOfSegments - 1 {
            let label = UILabel(frame: CGRect(x: 0, y: -4, width: (self.frame.width-18)/CGFloat(self.numberOfSegments), height: self.frame.height))
            label.textColor = UIColor.black
            label.text = self.titleForSegment(at: i)
            label.numberOfLines = 0
            label.textAlignment = .center
            label.font = UIFont.systemFont(ofSize: 12)
            label.adjustsFontSizeToFitWidth = true
            label.tag = i
            self.setTitle("", forSegmentAt: i)
            self.subviews[i].addSubview(label)
        }
    }
    
    func setSelectedTitleColor() {
        for i in 0...self.numberOfSegments - 1 {
            let label = self.subviews[self.numberOfSegments - 1 - i].subviews[1] as? UILabel
            label?.textColor = label?.tag == self.selectedSegmentIndex ? UIColor.red : UIColor.blue
        }
    }
}

extension ProjectsViewController{
    @objc func acceptButton(sender: UIButton) {
        print(sender.tag)
        print(estimatedProjectsArray![sender.tag].pname)
        callAPIForEstimateStatus(qStatus: 1, number: sender.tag)
    }
    
    @objc func declineButton(sender: UIButton) {
        callAPIForEstimateStatus(qStatus: 2, number: sender.tag)
        
    }
    func callAPIForEstimateStatus(qStatus: Int, number: Int)
    {
        if let model = viewModel {
            var parms = [String: Any]()
            parms = ["user_id": APPStore.sharedStore.user.userId ?? "", "q_status": qStatus, "pid": estimatedProjectsArray![number].pid ?? ""] as [String : Any]
            print(parms)
            self.addActivityIndicatior()
            self.showHUD(title: "Loading...")
            DispatchQueue.global(qos: .background).async {
                model.estimationStatus(parameters: parms, onSuccess: { [weak self] (dataSource) in
                    self?.getEstimatedProjects()
                    self?.removeActivityIndicator()
                    // }
                    self?.hideHud()
                    }, onError: { (error) in
                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
                        DispatchQueue.main.async {
                            //  self.showAlert(title: Message.Alert, message: error.statusMessage)
                            self.removeActivityIndicator()
                            self.hideHud()
                        }
                })
            }
        }
    }
}
