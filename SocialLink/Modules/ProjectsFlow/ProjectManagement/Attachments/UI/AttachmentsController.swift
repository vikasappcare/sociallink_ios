//
//  AttachmentsController.swift
//  SocialLink
//
//  Created by Harsha on 03/04/19.
//  Copyright © 2019 Santhosh Marripelli. All rights reserved.
//

import UIKit
import QuickLook
import Alamofire

class AttachmentsController: AppBaseVC {
    
    @IBOutlet weak var attachmentsTableView: UITableView!
    var files = [String]()
    var viewModel: AttachmentsViewModel?
    var selectedProject: Project?
    var path: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = AttachmentsViewModel()
        
        self.navigationItem.title = "ATTACHMENTS"
        setTableViewDatasource()
        callApiForAttachementsList()
        
    }
    func setProject(project: Project) {
        self.selectedProject = project
    }
    func callApiForAttachementsList()
    {
        if let model = viewModel {
            let param = ["project_id": selectedProject?.project_id ?? 0]
            print(param)
            self.showHUD(title: "Loading...")
            model.getAttachmentsList(parameters: param as [String : Any], onSuccess: { [weak self] (dataSource) in
                self?.hideHud()
                if dataSource != nil
                {
                    self?.files = (dataSource?.pupload)!
                    self?.attachmentsTableView.reloadData()
                }
                },  onError: { (error) in
                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
                    // self.showAlert(title: Message.Alert, message: error.statusMessage)
                    self.hideHud()
            })
            
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        if  self.navigationController?.navigationBar.backItem == nil {
            self.createNavigationItems()
        }
    }
    
}
extension AttachmentsController {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
}
extension AttachmentsController: UITableViewDelegate, UITableViewDataSource
{
    func setTableViewDatasource() {
        self.attachmentsTableView.delegate = self
        self.attachmentsTableView.dataSource = self
        self.attachmentsTableView.estimatedRowHeight = 100
        self.attachmentsTableView.rowHeight = UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return files.count
        //  return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AttachmentsCell", for: indexPath) as! AttachmentsCell
        cell.delegate = self
        cell.fileNameLabel.text = files[indexPath.row]
        path = files[indexPath.row]
        
        return cell
    }
}
extension AttachmentsController: AttachmentsCellDelegate{
    func previewClicked(sender: UIButton) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "PreviewController") as? PreviewController {
            
            controller.setProject(url: path!)
            // }
            let navController = UINavigationController(rootViewController: controller) // Creating a navigation controller with VC1 at the root of the navigation stack.
            self.navigationItem.backBarButtonItem = UIBarButtonItem(
                title: "", style: .done, target: nil, action: nil)
            self.present(navController, animated: true, completion: nil)
            // self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }
    
    func downloadClicked() {
        downloadFile(url: "https://app.sociallink.com/uploads/projects/" + path!, name: path!)
    }
    
}
extension AttachmentsController
{
    func downloadFile(url: String, name: String){
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            documentsURL.appendPathComponent(name)
            return (documentsURL, [.removePreviousFile])
        }
        
        Alamofire.download(url, to: destination).responseData { response in
            if let destinationUrl = response.destinationURL {
                print("destinationUrl \(destinationUrl.absoluteURL)")
            }
        }
    }
}
