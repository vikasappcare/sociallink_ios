//
//  UserContactsModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 09/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserContactsModel: AppBaseModel {
    
    var service: String?
    var data: [UserContact]?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    
    private func parseResponse(with response: JSON) {
        self.service = response["service"].stringValue
        for item in response["data"].arrayValue {
            if let model = self.createObject(with: item) {
                if self.data == nil {
                    self.data = [UserContact]()
                }
                self.data?.append(model)
            }
        }
    }
    
    private func createObject(with jsonObject: JSON) -> UserContact? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return UserContact.init(jsonObject)
        }
    }
    
}

class UserContact: AppBaseModel {
    
    var id: Int64?
    var name: String?
    var image: String?
    var designation: String?
    var emailid: String?

    
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    
    private func parseResponse(with response: JSON) {
        self.id = response["id"].int64Value
        self.name = response["name"].stringValue
        self.image = response["image"].stringValue
        self.designation = response["designation"].stringValue
        self.emailid = response["emailid"].stringValue
    }
    
}



