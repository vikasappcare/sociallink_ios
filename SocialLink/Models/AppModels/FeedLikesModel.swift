//
//  FeedLikesModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 07/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON


class FeedLikesModel: AppBaseModel {
    
    
    var feed_id = [Int64?]()
    var id = [Int64?]()
    var member_id = [Int64?]()
    var like_unlike = [Int64?]()
    var date = [String?]()
    var ufullname = [String?]()
    var uphoto = [String?]()
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    
    private func parseResponse(with response: JSON) {
        
         for i in 0..<response.count{
        self.date.append(response[i]["date"].stringValue)
        self.feed_id.append(response[i]["feed_id"].int64Value)
        self.id.append(response[i]["id"].int64Value)
        self.member_id.append(response[i]["member_id"].int64Value)
        self.like_unlike.append(response[i]["like_unlike"].int64Value)
        self.ufullname.append(response[i]["ufullname"].stringValue)
        self.uphoto.append(response[i]["uphoto"].stringValue)
        }
//        for item in response["Likes"].arrayValue {
//            if let model = self.createObject(with: item) {
//                if self.likes == nil {
//                    self.likes = [LikedUsers]()
//                }
//                self.likes?.append(model)
//            }
//        }
    }
    
    private func createObject(with jsonObject: JSON) -> LikedUsers? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return LikedUsers.init(jsonObject)
        }
    }
    
}


class LikedUsers: AppBaseModel {
    
    var user_id: Int64?
    var name: String?
    var image: String?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        self.user_id = response["user_id"].int64Value
        self.name = response["name"].stringValue
        self.image = response["image"].stringValue
    }
}
