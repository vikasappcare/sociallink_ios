//
//  AddViewController.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 12/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit
import CoreLocation

class AddViewController: UIViewController {
    
    @IBOutlet weak var plusView: UIView!
    @IBOutlet weak var newProjectsButton: UIButton!
    @IBOutlet weak var creativesButton: UIButton!
    @IBOutlet weak var conversationButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
         plusView.frame =  CGRect(x: 0, y: (UIScreen.main.bounds.height + 250), width: UIScreen.main.bounds.width, height: 200)
        self.addNotification()
        self.showProjectsList()
         self.showView()
        

    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addProjectsClicked(_ sender: Any) {
       
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "NewProjectVC") as? NewProjectVC {
            controller.showClose(bool: true)
            controller.showServiceAgreement(show: true)
             let navController = APPNavigationController(rootViewController: controller)
            self.present(navController, animated: true, completion: nil)
        }
        
        
    }
    
    func closeView() {
        UIView.animate(withDuration: 2.0, animations: {
            self.hideView()
        }, completion: {
            (value: Bool) in
            self.dismiss(animated: false, completion: nil)
        })
    }
    
    @IBAction func creativerClicked(_ sender: Any) {
        
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "LandingPageViewController") as? LandingPageViewController {
            let navController = APPNavigationController(rootViewController: controller)
            self.present(navController, animated: true, completion: nil)
        }
    }
    @IBAction func conversationClicked(_ sender: Any) {
        
        ////ContactsViewController
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactsViewController") as? ContactsViewController {
            let navController = APPNavigationController(rootViewController: controller)
            controller.showCloseButton(show: true, forScreen: ContactsShowScreen.AddController)
            self.present(navController, animated: true, completion: nil)
        }
        
     /*   var inputTextField: UITextField?
        
        //Create the AlertController
        let actionSheetController: UIAlertController = UIAlertController(title: "", message: "Enter Invite Code", preferredStyle: .alert)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            //Do some stuff
             self.dismiss(animated: true, completion: nil)
        }
        actionSheetController.addAction(cancelAction)
        //Create and an option action
        let nextAction: UIAlertAction = UIAlertAction(title: "OK", style: .default) { action -> Void in
            //Do some other stuff
            self.dismiss(animated: true, completion: nil)
        }
        actionSheetController.addAction(nextAction)
        //Add a text field
        actionSheetController.addTextField { textField -> Void in
            // you can use this text field
            inputTextField = textField
        }
        
        //Present the AlertController
        self.present(actionSheetController, animated: true, completion: nil)*/
        
    }
    @IBAction func closeClicked(_ sender: Any) {
        UIView.animate(withDuration: 2.0, animations: {
            self.hideView()
        }, completion: {
            (value: Bool) in
            self.dismiss(animated: false, completion: nil)
        })
    }
    
    
    @objc func showView() {
        UIView.animate(withDuration: 0.25) { () -> Void in
            self.plusView.frame =  CGRect(x: 0, y: (UIScreen.main.bounds.height - 200), width: UIScreen.main.bounds.width, height: 200)
        }
    }
    
    func hideView() {
        UIView.animate(withDuration: 0.25) { () -> Void in
            self.plusView.frame =  CGRect(x: 0, y: (UIScreen.main.bounds.height + 200), width: UIScreen.main.bounds.width, height: 200)
        }
    }
    
}

extension AddViewController {
    
    func addNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(dismissView),
                                               name:
            NSNotification.Name(rawValue: APPNotifications.dismissAddView),
                                               object: nil)
    }
    
    @objc func  dismissView(info: NSNotification) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func showProjectsList() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(showProjectList),
                                               name:
            NSNotification.Name(rawValue: APPNotifications.showProjectList),
                                               object: nil)
    }
    
    
    @objc func  showProjectList(info: NSNotification) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProjectListController") as? ProjectListController {
            let navController = APPNavigationController(rootViewController: controller)
            self.present(navController, animated: true, completion: nil)
        }
    }
    
}


