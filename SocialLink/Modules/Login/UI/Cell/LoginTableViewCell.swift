//
//  LoginTableViewCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 26/02/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.


import UIKit

protocol LoginTableViewCellDelegate: class {
    func forgetPasswordSelected()
    func createNewAccountSelected()
    func submitClicked()
    func gmailLogin()
   
}

class LoginTableViewCell: UITableViewCell {

    weak var delegate: LoginTableViewCellDelegate?
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var passwordText: AppTextField!
    
    @IBOutlet weak var checkBox: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       /// self.emailText.text = "santhosh@katalysttechnologies.com"
    }
    
    func getData() -> [String: Any]? {
        let dataIs = ["email": emailText.text ?? "", "password": passwordText.text ?? ""]
        return dataIs
    }

    @IBAction func submitClicked(_ sender: Any) {
        
        self.delegate?.submitClicked()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func checkBoxClicked(_ sender: Any) {
    }
    
    @IBAction func gmailLogin(_ sender: Any) {
        self.delegate?.gmailLogin()
    }
    @IBAction func createNewAccountClicked(_ sender: Any) {
        self.delegate?.createNewAccountSelected()
    }
    @IBAction func forgetUserNameClciked(_ sender: Any) {
        self.delegate?.forgetPasswordSelected()
    }
}
