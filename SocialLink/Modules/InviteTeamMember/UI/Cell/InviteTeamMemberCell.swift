//
//  InviteTeamMemberCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 28/08/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit
protocol InviteTeamMemberCellDelegate: class {
    
    func showScreen(screen: String)
    func sendData(data: [String: Any])
}

class InviteTeamMemberCell: UITableViewCell {

    weak var delegate: InviteTeamMemberCellDelegate?
    @IBOutlet weak var roleDropButton: UIButton!
    @IBOutlet weak var dropButton: UIButton!
    @IBOutlet weak var roleStack: UIStackView!
    @IBOutlet weak var projectStack: UIStackView!
    @IBOutlet weak var firstNameText: AppTextField!
    @IBOutlet weak var lastNameText: AppTextField!
    @IBOutlet weak var emailText: AppTextField!
    @IBOutlet weak var selectProjectText: AppTextField!
    @IBOutlet weak var selectRoleText: AppTextField!
    
    @IBOutlet weak var projectStackHeight: NSLayoutConstraint!
    @IBOutlet weak var inviteButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectProjectText.roundCorners(corners: [.topLeft, .bottomLeft], radius: 5.0)
        selectRoleText.roundCorners(corners: [.topLeft, .bottomLeft], radius: 5.0)
        dropButton.roundCorners(corners: [.topRight, .bottomRight], radius: 5.0)
        roleDropButton.roundCorners(corners: [.topRight, .bottomRight], radius: 5.0)
        
        firstNameText.delegate = self
        lastNameText.delegate = self
        emailText.delegate = self
        selectProjectText.delegate = self
        selectRoleText.delegate = self

        
    }
    
    func hideProjectStack(hide: Bool) {
        if hide {
            self.projectStackHeight.constant = 0
        } else {
            self.projectStackHeight.constant = 50
        }
    }
    
    @IBAction func inviteClicked(_ sender: Any) {
        let dataIs = ["fName": firstNameText.text ?? "",
                      "lName": lastNameText.text ?? "",
                      "email": emailText.text ?? "",
                      "project": selectProjectText.text ?? "",
                      "role": selectRoleText.text ?? ""]
            self.delegate?.sendData(data: dataIs)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    func updateProject(project: String) {
        self.selectProjectText.text = project
    }
    
    func updateRole(role: String) {
        self.selectRoleText.text = role
    }
    
    

}

extension InviteTeamMemberCell: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField.tag {
        case 4:
            self.delegate?.showScreen(screen: "Project")
            return false
        case 5:
            self.delegate?.showScreen(screen: "Role")
            return false
        default:
            return true
        }
    }
}
