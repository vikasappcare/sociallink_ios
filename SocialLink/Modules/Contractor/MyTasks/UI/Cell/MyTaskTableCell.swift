//
//  MyTaskTableCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 15/06/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class MyTaskTableCell: UITableViewCell {

    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var activeImage: UIImageView!
    @IBOutlet weak var projectName: UILabel!
    @IBOutlet weak var taskDescription: UILabel!
    @IBOutlet weak var taskName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(data: Task) {
        print(data)
        projectName.text = data.pname ?? ""
        taskDescription.text = data.tdetails ?? ""
        taskName.text = data.tname ?? ""
        
        startDateLabel.text = data.tsdate ?? ""
        endDateLabel.text = data.tedate ?? ""

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
