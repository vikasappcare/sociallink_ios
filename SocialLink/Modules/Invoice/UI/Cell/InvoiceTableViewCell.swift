//
//  InvoiceTableViewCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

protocol InvoiceTableViewCellDelegate: class {
    func editSelected()
}

class InvoiceTableViewCell: UITableViewCell {

    weak var delegate: InvoiceTableViewCellDelegate?
    @IBOutlet weak var invoiceTitle: UILabel!
    @IBOutlet weak var invoiceStatus: UILabel!
    @IBOutlet weak var invoiceNUmber: UILabel!
    @IBOutlet weak var invoiceAmount: UILabel!
    @IBOutlet weak var editButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func editClicked(_ sender: Any) {
        self.delegate?.editSelected()
    }
    
    func setData(data: [String: Any]?) {
        
        invoiceStatus.text = " \(String(describing: data!["type"] as? String ?? ""))     "
        invoiceNUmber.text = data!["data"] as? String
        invoiceStatus.backgroundColor = data!["color"] as? UIColor
        
        invoiceStatus.sizeToFit()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
