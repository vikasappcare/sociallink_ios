//
//  ProfileImageTableCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 28/02/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

protocol ProfileImageTableCellDelegate: class {
    
    func nameChanged(name: String)
    func changeImageClicked()
}

class ProfileImageTableCell: UITableViewCell {
    weak var delegate: ProfileImageTableCellDelegate?
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameText: AppTextField!
    @IBOutlet weak var selectProfileButton: UIButton!
    @IBOutlet weak var changeNameButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        nameText.isEnabled = false
        self.nameText.delegate = self
        self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2
        self.profileImage.layer.masksToBounds = true
        changeNameButton.isHidden = true
    }
    
    func setData(data: ProfileModel, tag: Int) {
        nameText.tag = tag
        let userProfileImage = APPURL.feedUserProfileImage + data.image!
        print(userProfileImage)
        
        profileImage.sd_setImage(with: URL(string: userProfileImage), placeholderImage: UIImage(named: ""))
       // profileImage.sd_setImage(with: URL(string: data.image ?? ""), placeholderImage: #imageLiteral(resourceName: "SampleProfile"))
        nameText.text = data.full_name ?? ""
      //  profileImage.setupForImageViewer()
    }
    
    func setImage(image: UIImage) {
        profileImage.image = image
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    @IBAction func selectImageClicked(_ sender: Any) {
        self.delegate?.changeImageClicked()
    }
    
    @IBAction func selectNameClicked(_ sender: Any) {
        if changeNameButton.isSelected {
            changeNameButton.isSelected = false
            nameText.isEnabled = false
        } else {
            changeNameButton.isSelected = true
            nameText.isEnabled = true
            nameText.becomeFirstResponder()
        }
    }
}

extension ProfileImageTableCell: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.delegate?.nameChanged(name: textField.text ?? "")
    }
}
