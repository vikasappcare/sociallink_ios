//
//  OverViewController.swift
//  SocialLink
//
//  Created by Harsha on 29/04/19.
//

import UIKit

class OverViewController: AppBaseVC {
    
    @IBOutlet weak var projectName: UILabel!
    @IBOutlet weak var projectType: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var endDate: UILabel!
    @IBOutlet weak var budget: UILabel!
    @IBOutlet weak var estimatedHours: UILabel!
    @IBOutlet weak var skills: UILabel!
    @IBOutlet weak var scope: UILabel!
    var viewModel: OverViewViewModel?
    var selectedProject: Project?
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    var proType: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "OVERVIEW"
        viewModel = OverViewViewModel()
        projectName.text = selectedProject?.project_name
        if selectedProject?.project_Type == 0
        {
            projectType.text = "Creatives nearby"
        }
        else
        {
            projectType.text = "SocialLink"
        }
        
        startDate.text = selectedProject?.start_date
        endDate.text = selectedProject?.endDate
        budget.text = selectedProject?.budget
       // estimatedHours.text = selectedProject?.estimatedTime
        scope.text = selectedProject?.description
        skills.text = selectedProject?.skills
        if APPStore.sharedStore.appUserID == "2"{
            if selectedProject?.statusIs == 0 || selectedProject?.statusIs == 1{
                cancelButton.isHidden = false
                editButton.isHidden = false
            }
            else
            {
                cancelButton.isHidden = true
                editButton.isHidden = true
            }
        }
        else
        {
            cancelButton.isHidden = true
            editButton.isHidden = true
        }
    }
    func setProject(project: Project) {
        self.selectedProject = project
        
    }
    
    @IBAction func cancelProjectAction(_ sender: Any) {
        if let model = viewModel {
            let param = ["project_id": selectedProject?.project_id]
            self.showHUD(title: "Loading...")
            model.cancelProject(parameters: param as [String : Any], onSuccess: { [weak self] (dataSource) in
                self?.hideHud()
                
                },  onError: { (error) in
                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
                   // self.showAlert(title: Message.Alert, message: error.statusMessage)
                    self.hideHud()
            })
            
        }
    }
    @IBAction func editProjectAction(_ sender: Any) {
        editClicked()
    }
    
}
extension OverViewController: NewProjectVCDelegate{
    func newProjectAdded(project: Project) {
        
    }
    
     func editClicked() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.001, execute: {
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "NewProjectVC") as? NewProjectVC {
                let navControl = APPNavigationController(rootViewController: controller)
                controller.setProType(type: PROTYPE.EDIT)
                controller.setProject(project: self.selectedProject!)
                controller.delegate = self
                
                self.present(navControl, animated: true, completion: nil)
            }
        })
    }
}
