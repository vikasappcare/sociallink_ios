//
//  NewProjectCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit
import iOSDropDown

protocol NewProjectCellDelegate: class {
    func submitClicked(data: [String: Any])
    func showAlert()
    func clickedStartDate()
    func clickedEndDate()
    func showDateAlert()
    func showServices()
    func chooseFileClicked()
    
}

class NewProjectCell: UITableViewCell {
    
    var skills = ""
   //var projectType: Int?
    
    @IBOutlet weak var servicesTextHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var servicesHeightConstraint: NSLayoutConstraint!
    
    weak var delegate: NewProjectCellDelegate?
    
    @IBOutlet weak var budgetTextField: AppTextField!
    @IBOutlet weak var estimationTextField: AppTextField!
    @IBOutlet weak var projectName: AppTextField!
    @IBOutlet weak var services: DropDown!
    @IBOutlet weak var startDate: AppTextField!
    @IBOutlet weak var endDate: AppTextField!
    @IBOutlet weak var uploadLabel: UILabel!
    @IBOutlet weak var chooseButton: UIButton!
    @IBOutlet weak var descriptionView: UITextView!
    @IBOutlet weak var startDateButton: UIButton!
    @IBOutlet weak var endDateButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var servicesStackHeight: NSLayoutConstraint!
    var isEdit: Bool?
    
    var skillsTextFieldText = ""
    var skillsIdText = ""
    var skillsIdArray = [String]()
    var showServices: Bool?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //self.services.delegate = self
        self.services.tag = 1810
        budgetTextField.isUserInteractionEnabled = true
        //estimationTextField.isUserInteractionEnabled = true
        self.services.didSelect{(selectedText , index ,id) in
            self.skillsTextFieldText = "\(self.skillsTextFieldText), \(selectedText)"
            self.skillsIdArray.append(skillsDic.someKey(forValue: selectedText)!)
            
        }
        
        self.services.listDidDisappear{
            if self.skillsTextFieldText.first == ","
            {
                self.skillsTextFieldText.remove(at: self.skillsTextFieldText.startIndex)
            }
            self.services.text = self.skillsTextFieldText
            
        }
    }
   
    func setData(data: Project) {
        isEdit = true
        self.projectName.text = data.project_name ?? ""
        self.startDate.text = data.start_date ?? ""
        self.endDate.text = data.endDate ?? ""
        self.services.text = data.skills ?? ""
        self.descriptionView.text = data.description ?? ""
        self.budgetTextField.text = data.budget
       // self.estimationTextField.text = data.estimatedTime
        self.uploadLabel.text = data.pupload
        self.submitButton.setTitle("Update", for: .normal)
        skills = data.skills!
    }
    
    func setFileName(name: String) {
        uploadLabel.text = name
    }
    
    func setStartDate(date: String) {
        startDate.text = date
    }
    
    func setService(data: String) {
        services.text = data
    }
    
    func setEndDate(date: String) {
        endDate.text = date
    }
    
    func showService(bool: Bool) {
        showServices = bool
    }
    
    
    //    override func layoutSubviews() {
    //        if showServices ?? false {
    //            //servicesTextHeightConstraint.constant = 50
    //            //servicesHeightConstraint.constant = 25
    //            servicesStackHeight.constant = 50
    //
    //        } else {
    //          //  servicesTextHeightConstraint.constant = 0
    //            //servicesHeightConstraint.constant = 0
    //            servicesStackHeight.constant = 0
    //        }
    //
    //    }
    
    @IBAction func startDateClicked(_ sender: Any) {
        self.delegate?.clickedStartDate()
    }
    
    @IBAction func endDateClciked(_ sender: Any) {
        self.delegate?.clickedEndDate()
        
    }
    @IBAction func chooseFileClicked(_ sender: Any) {
        self.delegate?.chooseFileClicked()
    }
    @IBAction func submitClicked(_ sender: Any) {
        
        let name = self.projectName.text ?? ""
        let services = self.services.text ?? ""
        let startDate = self.startDate.text ?? ""
        let endDate = self.endDate.text ?? ""
        let description = self.descriptionView.text ?? ""
        let budget = self.budgetTextField.text ?? ""
       // let estimatedTime = self.estimationTextField.text ?? ""
        
        
        if !(name.isEmpty) && !(services.isEmpty) && !(startDate.isEmpty) && !(endDate.isEmpty) && !(description.isEmpty) {
          print(startDate)
            print(endDate)
                if !APPUtility.validateDatesWithoutT(start: startDate, end: endDate) {
                    self.delegate?.showDateAlert()
                    return
                }
            var skillsString = skillsIdArray.joined(separator:",") // "1-2-3"

            print(skillsTextFieldText)
            print(skillsIdArray)
            if skills != ""{
                skillsString = skills + skillsString
            }
            
            print(skillsString)
            let dataIs = ["pname": name.trimmingCharacters(in: .whitespacesAndNewlines),
                          "pskills": skillsString,
                          "psdate": startDate.trimmingCharacters(in: .whitespacesAndNewlines),
                          "pdeadline": endDate.trimmingCharacters(in: .whitespacesAndNewlines),
                          "pscope": description.trimmingCharacters(in: .whitespacesAndNewlines),
                          "ebudget": budget.trimmingCharacters(in: .whitespacesAndNewlines)
                ] as [String : Any]
            print(dataIs)
            self.delegate?.submitClicked(data: dataIs)
        } else {
            self.delegate?.showAlert()
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

//extension NewProjectCell: UITextFieldDelegate {
//
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
////        if textField.tag == 1810 {
////           // self.delegate?.showServices()
////            return false
////        }
//
//        return true
//    }
//
//
//}
