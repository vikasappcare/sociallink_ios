//
//  InviteViewModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 29/08/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation

class InviteViewModel {
    
    init() {
    }
    
    func inviteMember(parameters: [String: Any],
                 onSuccess: @escaping (InviteTeamMemberModel?) -> Void,
                 onError: @escaping APIErrorHandler) {
        AppDataManager.inviteTeamMember(parametes: parameters, onSuccess: { (dataSource) in
            print(dataSource ?? "")
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    

    
}
