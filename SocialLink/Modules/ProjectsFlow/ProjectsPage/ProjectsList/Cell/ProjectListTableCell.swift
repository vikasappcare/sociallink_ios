//
//  ProjectListTableCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 29/05/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ProjectListTableCell: UITableViewCell {

    @IBOutlet weak var projectNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(data: Project) {
        
        if !(data.project_name?.checkForEmpty() ?? false) {
            projectNameLabel.text = data.project_name ?? ""
        } else {
            projectNameLabel.text = "No Project Name"
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
