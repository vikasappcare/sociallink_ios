//
//  ChoosePlanVC.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 28/02/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ChoosePlanVC: UIViewController {

    @IBOutlet weak var choosePlanPageControl: UIPageControl!
    @IBOutlet weak var planCollectionView: UICollectionView!
    
    let colorArray = [UIColor.planBlue, UIColor.planOrange, UIColor.planGray, UIColor.planBlue]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "CHOOSE PLAN"


        self.setCollectionView()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        planCollectionView.reloadData()
//    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ChoosePlanVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func setCollectionView() {
        self.planCollectionView.dataSource = self
        self.planCollectionView.delegate = self
        
//        if let flow = planCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
//            let itemSpacing: CGFloat = 1
//            let itemsInOneLine: CGFloat = 1
//            flow.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
//            let width = UIScreen.main.bounds.size.width - itemSpacing * CGFloat(itemsInOneLine - 1)
//            flow.itemSize = CGSize(width: floor(width/itemsInOneLine), height: width/itemsInOneLine)
//            flow.minimumInteritemSpacing = 1
//            flow.minimumLineSpacing = 1
//        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.choosePlanPageControl.currentPage = indexPath.row
    }
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        print(UIScreen.main.bounds.width)
        return CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - 104)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.PlanCollectionCellIdentifier, for: indexPath)
        cell.tag = indexPath.row
        if let cellIs = cell as? PlanCollectionCell {
            cellIs.delegate = self
           /// cellIs.setData(data: viewModel?.photoArray![indexPath.row])
            
//            cellIs.layer.borderColor = UIColor.gray.cgColor
//            cellIs.layer.borderWidth = 1.0
//            cellIs.layer.masksToBounds = true
            
            cellIs.setViewBackGroundColor(color: colorArray[indexPath.row])
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}

extension ChoosePlanVC: PlanCollectionCellDelegate {
    func choosePlanClicked() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as? PaymentViewController {
            /// let navController = APPNavigationController(rootViewController: controller)
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    
    
}
