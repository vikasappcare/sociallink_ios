//
//  FeedViewModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 18/08/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation

class FeedViewModel {
    
    init() {
    }
    
    var feedArray: [Feed]?
    var projectId: Int64?

    func updateFeeds(withProjectId id: Int64) {
        APPUtility.getFeed(withProjectId: id)
        feedArray = APPStore.sharedStore.feeds
        
        var dateFormatter: DateFormatter?
        if dateFormatter == nil {
            dateFormatter = DateFormatter()
        }
        dateFormatter?.dateFormat = Constant.globalDateFormat
        
        if feedArray?.count ?? 0 > 0 {
           // feedArray = feedArray?.sorted { (dateFormatter?.date(from: $0.feed_time ?? "")!)! > (dateFormatter?.date(from: $1.feed_time ?? "")!)! }
        }
       
        
        
    }

    func getFeed(parameters: [String: Any],
                   onSuccess: @escaping (FeedModel?) -> Void,
                   onError: @escaping APIErrorHandler) {
        AppDataManager.getFeed(parametes: parameters, onSuccess: { (dataSource) in
            self.updateFeeds(withProjectId: 0)
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
    func postFeed(parameters: [String: Any],
                 onSuccess: @escaping (OTPModule?) -> Void,
                 onError: @escaping APIErrorHandler) {
        AppDataManager.postFeed(parametes: parameters, onSuccess: { (dataSource) in
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
    func sendLikeUnLike(parameters: [String: Any],
                 onSuccess: @escaping (FeedLikeUnlikeModel?) -> Void,
                 onError: @escaping APIErrorHandler) {
        AppDataManager.sendLikeUnlike(parametes: parameters, onSuccess: { (dataSource) in
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
    func constructFeedModel(forTable: UITableView) -> [String: Any]? {
        let indexPath = IndexPath(row: 0, section: 0)
        let cellIs = forTable.cellForRow(at: indexPath)
        if let cell = cellIs as? ContentTableViewCell {
          var text = cell.getData()
            if text == "Share an article, photo, task or idea" {
                text = ""
            }
            if let imageCell = cellIs as? ImagePostCell{
                if let  data = UIImageJPEGRepresentation(imageCell.postImage.image!, 0.5) {
            //if let idIs = self.projectId {
                let dataIs = ["fuser": APPStore.sharedStore.user.userId ?? 0,
                              "project_id": "1",
                              "fdetails": text,
                              "ftime": APPUtility.getPresentDate(),
                              "cdate": "15-03-2019",
                              "document": data] as [String : Any]
                
                print(dataIs)
                return dataIs
            //}
                    
                }
            }
            
        }
        return nil
    }
    
}
