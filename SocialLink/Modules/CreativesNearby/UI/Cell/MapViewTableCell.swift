//
//  MapViewTableCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 11/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit
import GoogleMaps


protocol MapViewTableCellDelegate: class {
    
    func locationSearchClicked()
    func markerClicked(details: [String])
}

class MapViewTableCell: UITableViewCell,GMSMapViewDelegate {
    
    weak var delegate: MapViewTableCellDelegate?
    @IBOutlet weak var locationSearchBar: UISearchBar!
    @IBOutlet weak var mapView: GMSMapView!
    
    let lat = 36.5863
    let long = -119.3141
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mapView.mapType = .normal
        locationSearchBar.clearBackgroundColor()
        locationSearchBar.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.delegate = self
    }
    
    func setData(data: [String: Array<String>]) {
        
        var bounds = GMSCoordinateBounds()
        var name = [String]()
        var address = [String]()
        let latitude = data["lat"]
        let longitude = data["lng"]
        
        name = data["ufullname"]!
        address = data["address"]!
        let phone = data["uphone"]
        let email = data["uemailid"]
        let distance = data["distance"]
        let skills = data["uskills"]

        let iconPath = "https://app.sociallink.com/uploads/map_icons/"
        let iconsArr = data["map_img"]
        // let place = data["address"] as? String ?? ""
        
        //locationSearchBar.text = place
        //creating a marker view
        mapView.clear()
        print(latitude)
        for i in 0..<latitude!.count{
            
            let marker = GMSMarker()
        
            if latitude![i] != "" || longitude![i] != ""{
                let lat =  Double(latitude![i])
                let lng =  Double(longitude![i])
                marker.position = CLLocationCoordinate2D(latitude: lat!, longitude: lng!)
                marker.groundAnchor = CGPoint(x: 0.5, y: 1)//new line add
                marker.title = "\(name[i])"
                marker.snippet = "\(name[i]),\(address[i]),\(email![i]),\(phone![i]),\(distance![i] ),\(skills![i] )"
                
                let url = URL(string: iconPath + iconsArr![i])
                 //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                if url != nil{
                    let data = try? Data(contentsOf: url!)
                    if data != nil{
                    let image = UIImage(data: data!)
                        if image != nil{
                        let markerImage = image!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
                        let markerView = UIImageView(image: markerImage)
                        marker.iconView = markerView
                        }
                        }
                }
                else{
                    let markerImage = UIImage(named: "Designerpoint")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
                    let markerView = UIImageView(image: markerImage)
                    marker.iconView = markerView
                }
                
                marker.map = self.mapView
                bounds = bounds.includingCoordinate(marker.position)
            }
        }
        let update = GMSCameraUpdate.fit(bounds, withPadding: 100)
        self.mapView.animate(with: update)
        
        
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let pointsArr = marker.snippet!.components(separatedBy: ",")
        print(pointsArr)
        self.delegate?.markerClicked(details: pointsArr)
            return true
    }
        func setDataForNoData(data: [String : Any])
        {
            let latitude = data["lat"]
            let longitude = data["long"]
            let camera = GMSCameraPosition.camera(withLatitude: latitude as! Double, longitude:  longitude as! Double, zoom: 14.0)
            mapView.camera = camera
            mapView.clear()
            let marker = GMSMarker()
            let markerImage = UIImage(named: "Designerpoint")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
            let markerView = UIImageView(image: markerImage)
            marker.iconView = markerView
            marker.map = self.mapView
        }
        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)
            
            // Configure the view for the selected state
        }
    }
    
    extension MapViewTableCell: UISearchBarDelegate {
        func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
            self.delegate?.locationSearchClicked()
            return false
        }
}
