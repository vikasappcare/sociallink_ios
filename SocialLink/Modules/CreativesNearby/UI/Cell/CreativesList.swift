//
//  CreativesList.swift
//  SocialLink
//
//  Created by Harsha on 09/04/19.
//

import UIKit
import iOSDropDown

class CreativesListCell: UITableViewCell {
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var projectsList: DropDown!
    @IBOutlet weak var miles: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var milesImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let projectsListArray = ["Demo","Demo 1", "Demo 2", "Demo 3", "Demo 4", "Demo 5"]
        projectsList.optionArray = projectsListArray
        bgView.layer.cornerRadius = 10
    }
    
    @IBAction func viewProfileButton(_ sender: Any) {
    }
    @IBAction func sendInvitation(_ sender: Any) {
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
