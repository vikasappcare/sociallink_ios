//
//  TimesheetTableCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 16/06/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class TimesheetTableCell: UITableViewCell {

    @IBOutlet weak var hoursLabel: UILabel!
    @IBOutlet weak var endDate: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var taskName: UILabel!
    @IBOutlet weak var projectName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(data: TimeSheet) {
        projectName.text = data.project_name ?? ""
        taskName.text = data.task_title ?? ""
        startDate.text = APPUtility.convertUTCToClaenderFormat(date: data.start_date ?? "")
        endDate.text = APPUtility.convertUTCToClaenderFormat(date: data.end_date ?? "")
        hoursLabel.text = APPUtility.timeToHoursMinutesSeconds(time: data.totaltime ?? "")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
