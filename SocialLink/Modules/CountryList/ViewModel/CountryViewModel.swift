//
//  CountryViewMOdel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 16/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation


class CountryViewModel {
    init() {
    }
    
    var countryArray: [Country]?
    var stateArray: [State]?
    var cityArray: [City]?
    
    var tempCountryArray: [Country]?
    var tempStateArray: [State]?
    var tempCityArray: [City]?
    
    
    func getCountryList(parameters: [String: Any],
                        onSuccess: @escaping (CountryModel?) -> Void,
                        onError: @escaping APIErrorHandler) {
        AppDataManager.getCountryList(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: "Success")
            if self.countryArray == nil {
                self.countryArray = [Country]()
            }
            if self.tempCountryArray == nil {
                self.tempCountryArray = [Country]()
            }
            self.countryArray = dataSource?.data
            self.tempCountryArray = dataSource?.data
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
    func getStateList(parameters: [String: Any],
                        onSuccess: @escaping (StateModel?) -> Void,
                        onError: @escaping APIErrorHandler) {
        AppDataManager.getStateList(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: "Success")
            if self.stateArray == nil {
                self.stateArray = [State]()
            }
            if self.tempStateArray == nil {
                self.tempStateArray = [State]()
            }
            self.stateArray = dataSource?.data
            self.tempStateArray = dataSource?.data
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
    func getCityList(parameters: [String: Any],
                      onSuccess: @escaping (CityModel?) -> Void,
                      onError: @escaping APIErrorHandler) {
        AppDataManager.getCityList(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: "Success")
            if self.cityArray == nil {
                self.cityArray = [City]()
            }
            self.cityArray = dataSource?.data
            if self.tempCityArray == nil {
                self.tempCityArray = [City]()
            }
            self.tempCityArray = dataSource?.data
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
    
}
