//
//  Attachmentscell.swift
//  SocialLink
//
//  Created by Harsha on 03/04/19.
//  Copyright © 2019 Santhosh Marripelli. All rights reserved.
//

import UIKit
protocol AttachmentsCellDelegate: class {
    func previewClicked(sender:UIButton)
    func downloadClicked()
}
class AttachmentsCell: UITableViewCell {
    
    weak var delegate: AttachmentsCellDelegate?
    @IBOutlet weak var fileNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
    @IBAction func previewAction(_ sender: Any) {
        self.delegate?.previewClicked(sender: sender as! UIButton)
    }
    @IBAction func downloadAction(_ sender: Any) {
        self.delegate?.downloadClicked()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
