//
//  NewTaskViewMOdel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 01/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation

class NewTaskViewModel {
    
    init() {
    }
    
    var newTaskModel: [String: Any]?
    var tempEditTask: [String: Any]?

    
    func addTask(parameters: [String: Any],
                        onSuccess: @escaping (OTPModule?) -> Void,
                        onError: @escaping APIErrorHandler) {
        AppDataManager.addTask(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: dataSource)
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
    func editTask(parameters: [String: Any],
                 onSuccess: @escaping (Task) -> Void,
                 onError: @escaping APIErrorHandler) {
        AppDataManager.editTask(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: dataSource)
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
    func getFileName(path: String) -> String {
        let nameArray = path.components(separatedBy: "/")
        if nameArray.count > 0 {
            return nameArray.last ?? ""
        }
        return ""
    }
    
    func createNewTaskObject() {
        print("fdsfsf")
        print(newTaskModel)
    }
    
}
