//
//  CountryModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 16/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON

class CountryModel: AppBaseModel {
    
    var service: String?
    var data: [Country]?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    
    private func parseResponse(with response: JSON) {
        self.service = response["service"].stringValue
        for item in response["data"].arrayValue {
            if let model = self.createObject(with: item) {
                if self.data == nil {
                    self.data = [Country]()
                }
                self.data?.append(model)
            }
        }
    }
    
    private func createObject(with jsonObject: JSON) -> Country? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return Country.init(jsonObject)
        }
    }
    
}

class Country: AppBaseModel {
    
    var id: Int64?
    var name: String?
    
    
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    
    private func parseResponse(with response: JSON) {
        self.id = response["id"].int64Value
        self.name = response["name"].stringValue
    }
    
}



