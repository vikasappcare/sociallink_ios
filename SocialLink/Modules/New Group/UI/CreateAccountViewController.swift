//
//  CreateAccountViewController.swift
//  SocialLink
//
//  Created by verizonone on 2/7/19.
//  Copyright © 2019 Santhosh Marripelli. All rights reserved.
//

import UIKit

class CreateAccountViewController: UITableViewController, ProfileCreateAccountCellDelegate {
    
    var viewModel: RegisterViewModel?

    func updateProfileCicked() {
        return;
    }
    

    let contentPlaceHolder = ["First Name",
                              "Last Name",
                              "Email",
                              "Password",
                              "Phone Number",
                              "Zip code",
                              "Hire For"]
    
    let keyValue = ["First Name",
                    "Last Name",
                    "Email",
                    "Password",
                    "Phone Number",
                    "Zip code",
                    "Hire For"]

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = RegisterViewModel()

    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.contentPlaceHolder.count + 1
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row < self.contentPlaceHolder.count) {
            return 90
        }
        return 222
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var rect = tableView.frame
        rect.size.height = 100
        let headerView = UIView(frame: rect)
        headerView.backgroundColor = UIColor.clear
        let logo = UIImage(named: "logo3.png")
        let imageView = UIImageView(image:logo)
        imageView.layer.masksToBounds = true
        imageView.sizeToFit()
        var imageRect = rect
        imageRect.size.height = 40
        imageView.frame = imageRect
        headerView.addSubview(imageView)
        
        let labelView: UILabel = UILabel(frame: CGRect(x:imageRect.origin.x, y:imageRect.size.height + 10, width:imageRect.size.width, height:25))
        labelView.textAlignment = .center
        labelView.text = "Create Your Account"
        labelView.font = UIFont.boldSystemFont(ofSize: 18)
        headerView.addSubview(labelView)

        return headerView;
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row < self.contentPlaceHolder.count - 1) {
            let cell:ProfileContentCell = (tableView.dequeueReusableCell(withIdentifier: "ProfileContentCellIdentifier", for: indexPath) as? ProfileContentCell)!
            cell.headerLabel.text = self.contentPlaceHolder[indexPath.row]
            cell.contentText.placeholder = self.contentPlaceHolder[indexPath.row]
            cell.contentText.backgroundColor = UIColor(displayP3Red: 245/255.0, green: 248/255.0, blue: 250/255.0, alpha: 1.0)
            return cell
        } else if(indexPath.row  < self.contentPlaceHolder.count ) {
            print("indexPath.row:: ",indexPath.row)
            print("count: ",self.contentPlaceHolder.count)
            print("value: ",self.contentPlaceHolder[indexPath.row])

            let cell:ProfileContentCell = (tableView.dequeueReusableCell(withIdentifier: "ProfileContentCellIdentifier", for: indexPath) as? ProfileContentCell)!
            cell.headerLabel.text = self.contentPlaceHolder[indexPath.row]
            cell.optForLabel.isHidden = false
            cell.contentText.placeholder = "Skill (ex: Web Design)"
            cell.contentText.backgroundColor = UIColor(displayP3Red: 245/255.0, green: 248/255.0, blue: 250/255.0, alpha: 1.0)
            return cell
        }
        else {
            let cell:ProfileCreateAccountCell = (tableView.dequeueReusableCell(withIdentifier: "ProfileCreateAccountCellIdentifier", for: indexPath) as? ProfileCreateAccountCell)!
            cell.delegate = self;
            return cell
        }
    }

    func showServiceAgreement() {
        
    }
    func submitClicked() {
        var registrationArray = [Any]()
        var sgNumber : Int!
        var email : String!
        var name : String!
        var password : String!
        var phone : String!
        var zipCode : String!
        var skills : String!

        for i in 0..<contentPlaceHolder.count {
            let indexPath = IndexPath(row: i, section: 0)
            let cell = tableView.cellForRow(at: indexPath) as? ProfileContentCell
            if let data = cell?.getData() {
              registrationArray.append(data)
            sgNumber =  cell?.optForLabel.selectedSegmentIndex ?? Int()
            }
            
        }
        if registrationArray.count == 7 {
             email = (registrationArray[2] as? String)?.trimmingCharacters(in: .whitespaces)
             name = ("\(registrationArray[0]) \(registrationArray[1])" as? String)?.trimmingCharacters(in: .whitespaces)
             password = (registrationArray[3] as? String)?.trimmingCharacters(in: .whitespaces)
             phone = (registrationArray[4] as? String)?.trimmingCharacters(in: .whitespaces)
             zipCode = (registrationArray[5] as? String)?.trimmingCharacters(in: .whitespaces)
             skills = (registrationArray[6] as? String)?.trimmingCharacters(in: .whitespaces)


        }
        else
        {
             email = (registrationArray[1] as? String)?.trimmingCharacters(in: .whitespaces)
             name = ("\(registrationArray[0])" as? String)?.trimmingCharacters(in: .whitespaces)
             password = (registrationArray[2] as? String)?.trimmingCharacters(in: .whitespaces)
             phone = (registrationArray[3] as? String)?.trimmingCharacters(in: .whitespaces)
             zipCode = (registrationArray[4] as? String)?.trimmingCharacters(in: .whitespaces)
             skills = (registrationArray[5] as? String)?.trimmingCharacters(in: .whitespaces)

        }
        if (email?.isEmpty ?? false) || (email == "") {
            self.showAlert(title: "Alert!", message: "Please enter email to continue")
            return
        } else if !APPUtility.validateEmail(enteredEmail: email ?? "") {
            self.showAlert(title: "Alert!", message: "Please enter a valid email to continue")
            return
        }
        if (name?.isEmpty ?? false) || (email == "") {
            self.showAlert(title: "Alert!", message: "Please enter name to continue")
            return
        }
        if (password?.isEmpty ?? false) || (email == "") {
            self.showAlert(title: "Alert!", message: "Please enter password to continue")
            return
            
        }
        
        if (phone?.isEmpty ?? false) || (email == "") {
            self.showAlert(title: "Alert!", message: "Please enter phone number to continue")
            return
            
        }
        if (zipCode?.isEmpty ?? false) || (email == "") {
            self.showAlert(title: "Alert!", message: "Please enter zipcode to continue")
            return
        }
        if (skills?.isEmpty ?? false) || (email == "") {
            self.showAlert(title: "Alert!", message: "Please enter skills to continue")
            return
        }
        
        if let model = viewModel {

        let parms = ["ufullname": name, "uemailid": email, "uphone": phone, "upassword": password, "uzipcode": zipCode, "utype": sgNumber, "uskills": skills] as [String : Any]
            
           // self.showHUD(title: "Loading...")
            model.registerUser(parameters: parms, onSuccess: { [weak self] (dataSource) in
                print("Success")
                print(dataSource ?? "")
                APPUtility.getUserData()
                APPUtility.getToken()
                self?.navigationController?.popViewController(animated: true)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue:
                    APPNotifications.dismissLoginView),
                                                object: nil, userInfo: nil)
               // self?.hideHud()
                }, onError: { (error) in
                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
                  //  self.showAlert(title: Message.Alert, message: error.statusMessage)
                   // self.hideHud()
            })        }
        
        
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
