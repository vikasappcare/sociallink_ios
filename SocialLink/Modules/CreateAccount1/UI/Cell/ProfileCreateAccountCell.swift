//
//  ProfileCreateAccountCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 27/02/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

protocol ProfileCreateAccountCellDelegate: class {
    func updateProfileCicked()
   
}

class ProfileCreateAccountCell: UITableViewCell {

    weak var delegate: ProfileCreateAccountCellDelegate?
    @IBOutlet weak var checkBox: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func checkBoxClicked(_ sender: Any) {
        if checkBox.isSelected {
            checkBox.isSelected = false
        } else {
            checkBox.isSelected = true
        }
    }
    @IBAction func createAccountClicked(_ sender: Any) {
        self.delegate?.updateProfileCicked()
    }
}
