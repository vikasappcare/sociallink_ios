//
//  NewTaskUploadCell.swift
//  SocialLink
//
//  Created by Santhosh on 09/05/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

protocol NewTaskUploadCellDelegate: class {
    func chooseClicked()
}

class NewTaskUploadCell: UITableViewCell {

    weak var delegate: NewTaskUploadCellDelegate?
    @IBOutlet weak var chooseButton: UIButton!
     @IBOutlet weak var fileNameText: UILabel!
    @IBOutlet weak var backView: UIView!
    var fileUploadData: Data?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        backView.layer.borderWidth = 1.0
//        backView.layer.borderColor = UIColor.appDarkGrayColor.cgColor
//        backView.layer.masksToBounds = true
        
    }
    
    func setFileName(name: String) {
        fileNameText.text = name
    }
   
    @IBAction func chooseButtonClicked(_ sender: Any) {
        self.delegate?.chooseClicked()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
