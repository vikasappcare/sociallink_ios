//
//  UserProfileModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON



class UserProfileModel: AppBaseModel {
    var service: String?
    var data: UserProfile?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    
    private func parseResponse(with response: JSON) {
        self.service = response["service"].stringValue
        self.data = self.createObject(with: response["data"])
    }
    
    private func createObject(with jsonObject: JSON) -> UserProfile? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return UserProfile.init(jsonObject)
        }
    }
    
}


class UserProfile: AppBaseModel {
    var uid: Int64?
    var ufullname: String?
    var ulname: String?
    var uemailid: String?
    var uphone: String?
    var ugender: String?
    var uphoto: String?
    var uwebsite: String?
    var uaboutu: String?
    var uportfolio: String?
    var education: String?
    var uskills: String?
    var business: String?
    var industry: String?
    var department: String?
    var job: String?
    var resume: String?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        self.uid = response["uid"].int64Value
        self.ufullname = response["ufullname"].stringValue
        self.ulname = response["ulname"].stringValue
        self.uemailid = response["uemailid"].stringValue
        self.uphone = response["uphone"].stringValue
        self.ugender = response["ugender"].stringValue
        self.uphoto = response["uphoto"].stringValue
        self.uwebsite = response["uwebsite"].stringValue
        self.uaboutu = response["uaboutu"].stringValue
        self.uportfolio = response["uportfolio"].stringValue
        self.education = response["education"].stringValue
        self.uskills = response["uskills"].stringValue
        self.business = response["business"].stringValue
        self.industry = response["industry"].stringValue
        self.department = response["department"].stringValue
        self.job = response["job"].stringValue
        self.resume = response["resume"].stringValue

        
    }
}
