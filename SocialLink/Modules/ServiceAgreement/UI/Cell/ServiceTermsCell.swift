//
//  ServiceTermsCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 11/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ServiceTermsCell: UITableViewCell {

    @IBOutlet weak var checkBoxButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func checkBoxClicked(_ sender: Any) {
        
        
        if checkBoxButton.isSelected {
            checkBoxButton.isSelected = false
        } else {
            checkBoxButton.isSelected = true
        }
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
