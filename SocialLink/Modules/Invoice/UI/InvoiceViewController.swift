//
//  InvoiceViewController.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class InvoiceViewController: UIViewController {
    
    @IBOutlet weak var invoicesegment: AppSegmentControl!
    @IBOutlet weak var invoiceSearchBar: UISearchBar!
    @IBOutlet weak var invoiceTableview: UITableView!
    let invoiceData = ["invoice 1", "invoice 2", "invoice 3", "invoice 4"]
    let colorArray = [UIColor.appRedColor, UIColor.appOrangeColor, UIColor.appBlueColor, UIColor.appGreenColor]
    let unpaid = ["Overdue", "Partial", "Sent"]
    let paid = ["Paid", "Paid"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "INVOICE"
        self.setTableViewDatasource()
        // Do any additional setup after loading the view.
        
        invoiceSearchBar.barTintColor = UIColor.clear
        invoiceSearchBar.backgroundColor = UIColor.clear
        invoiceSearchBar.isTranslucent = true
        invoiceSearchBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        self.addNotification()
        
        UISegmentedControl.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for: .selected)
        let attr = NSDictionary(object: UIFont.boldFont(16), forKey: NSAttributedStringKey.font as NSCopying)
        UISegmentedControl.appearance().setTitleTextAttributes(attr as [NSObject: AnyObject], for: .normal)
        
        self.invoicesegment.selectedSegmentIndex = 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func invoiceSegmentClicked(_ sender: Any) {
        
        self.invoiceTableview.reloadData()
    }
    
}

extension InvoiceViewController {
    
    func addNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(showAddScreen),
                                               name:
            NSNotification.Name(rawValue: APPNotifications.plusPayment),
                                               object: nil)
    }
    @objc func  showAddScreen(info: NSNotification) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = mainStoryboard.instantiateViewController(withIdentifier: "AddViewController")
        if let addController = controller as? AddViewController {
            addController.modalPresentationStyle = .overCurrentContext
            addController.view.alpha = 0.0
            self.tabBarController?.present(addController, animated: true) { () -> Void in
                UIView.animate(withDuration: 0.025) { () -> Void in
                    addController.view.alpha = 1.0
                }
            }
        }
        
    }
}
extension InvoiceViewController: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.invoiceTableview.delegate = self
        self.invoiceTableview.dataSource = self
        self.invoiceTableview.estimatedRowHeight = 300
        self.invoiceTableview.rowHeight = UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        switch self.invoicesegment.selectedSegmentIndex {
        case 0:
            return paid.count
        case 1:
            return unpaid.count
        default:
            return 0
        }
    }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.InvoiceTableViewCellIdentifier, for: indexPath)
            if let cellIs = cell as? InvoiceTableViewCell {
                cellIs.selectionStyle = .none
                cellIs.delegate = self
                var dataIs: [String: Any]?

                switch self.invoicesegment.selectedSegmentIndex {
                case 0:
                    dataIs = ["type": paid[indexPath.row],
                              "data": invoiceData[indexPath.row],
                              "color": UIColor.appGreenColor] as [String : Any]
                case 1:
                    dataIs = ["type": unpaid[indexPath.row],
                              "data": invoiceData[indexPath.row],
                              "color": colorArray[indexPath.row]] as [String : Any]
                default: break
                }
                
                cellIs.setData(data: dataIs)
                
            }
            return cell
            
        }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
        }
        
        
    
}
    
    extension InvoiceViewController: InvoiceTableViewCellDelegate {
        func editSelected() {
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "InvoiceDetailVC") as? InvoiceDetailVC {
                /// let navController = APPNavigationController(rootViewController: controller)
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
        
        
        
}
