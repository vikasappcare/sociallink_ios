//
//  TaskDetailViewModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 16/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation

class TaskDetailViewModel {
    
    init() {
    }
    
    func taskAcceptDecline(parameters: [String: Any],
                  onSuccess: @escaping (Task?) -> Void,
                  onError: @escaping APIErrorHandler) {
        AppDataManager.taskAcceptDecline(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: "Success")
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
    func taskStatusUpdate(parameters: [String: Any],
                           onSuccess: @escaping (OTPModule?) -> Void,
                           onError: @escaping APIErrorHandler) {
        AppDataManager.taskStatusUpdate(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: "Success")
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
    
    func updateTaskLog(parameters: [String: Any],
                       onSuccess: @escaping (TaskLog?) -> Void,
                       onError: @escaping APIErrorHandler) {
        AppDataManager.updateTaskLog(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: "Success")
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    func timeUpdate(parameters: [String: Any],
                       onSuccess: @escaping (OTPModule?) -> Void,
                       onError: @escaping APIErrorHandler) {
        AppDataManager.timeUpdate(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: "Success")
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    func stopTimeUpdate(parameters: [String: Any],
                    onSuccess: @escaping (OTPModule?) -> Void,
                    onError: @escaping APIErrorHandler) {
        AppDataManager.stopTimeUpdate(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: "Success")
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
    func filterLogTime(logs: [TimeLog]) -> TimeLog? {
        let timeLog = logs.filter{ $0.time_started == true }
        if timeLog.count > 0 {
            return timeLog[0]
        }
        return nil
    }
    
}
