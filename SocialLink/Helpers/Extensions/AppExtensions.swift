//
//  AppExtensions.swift
//  BaseProject
//
//  Created by Santhosh Marripelli on 23/02/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import UIKit
extension String {
    public func getRestAPIName(_ baseURL: String) -> String {
        let filter = getFilters(self, baseURL) ?? ""
        let quoteBaseURL = baseURL + filter
        if let startPoint = self.range(of: quoteBaseURL)?.upperBound {
            let text = String(self[startPoint...])
            return getNameByFilters(text) ?? self
        }
        return self
    }
    private func getFilters(_ url: String, _ baseURL: String) -> String? {
        let string = url.replacingOccurrences(of: baseURL, with: "")
        let array = string.components(separatedBy: "/")
        guard let filter = array.first else {
            return nil
        }
        return filter + "/"
    }
    private func getNameByFilters(_ text: String) -> String? {
        if text.contains("/") {
            return text.components(separatedBy: "/").first
        } else if text.contains("?") {
            return text.components(separatedBy: "?").first
        } else if text.contains("&") {
            return  text.components(separatedBy: "&").first
        }
        return text
    }
}


//////////Fonts ///////////////
extension UILabel {
    
    public func setReguralFont(size: CGFloat) {
        self.font = UIFont.regularFont(size)
    }
    public func setBoldFontFont(size: CGFloat) {
        self.font = UIFont.boldFont(size)
    }
    public func setMediumFont(size: CGFloat) {
        self.font = UIFont.mediumFont(size)
    }
    public func setLightFont(size: CGFloat) {
        self.font = UIFont.lightFont(size)
    }
    public func setSemiBoldFont(size: CGFloat) {
        self.font = UIFont.semiBoldFont(size)
    }
    public func setThinFont(size: CGFloat) {
        self.font = UIFont.thinFont(size)
    }
    
}

extension UITextField {
    public func setReguralFont(size: CGFloat) {
        self.font = UIFont.regularFont(size)
    }
    public func setBoldFontFont(size: CGFloat) {
        self.font = UIFont.boldFont(size)
    }
    public func setMediumFont(size: CGFloat) {
        self.font = UIFont.mediumFont(size)
    }
    public func setLightFont(size: CGFloat) {
        self.font = UIFont.lightFont(size)
    }
    public func setSBFont(size: CGFloat) {
        self.font = UIFont.semiBoldFont(size)
    }
    public func setTFont(size: CGFloat) {
        self.font = UIFont.thinFont(size)
    }
}

extension UIButton {
    public func setReguralFont(size: CGFloat) {
        self.titleLabel?.font = UIFont.regularFont(size)
    }
    public func setBoldFontFont(size: CGFloat) {
        self.titleLabel?.font = UIFont.boldFont(size)
    }
    public func setMediumFont(size: CGFloat) {
        self.titleLabel?.font = UIFont.mediumFont(size)
    }
    public func setLightFont(size: CGFloat) {
        self.titleLabel?.font = UIFont.lightFont(size)
    }
    public func setSemiBoldFont(size: CGFloat) {
        self.titleLabel?.font = UIFont.semiBoldFont(size)
    }
    public func setThinFont(size: CGFloat) {
        self.titleLabel?.font = UIFont.thinFont(size)
    }
}

extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}

extension UIStackView {
    func addBackground(color: UIColor) {
        let subView = UIView(frame: bounds)
        subView.backgroundColor = color
        subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(subView, at: 0)
    }
}

extension String {
    func checkForEmpty() -> Bool {
        guard self.isEmpty, self.trimmingCharacters(in: .whitespaces).isEmpty
            else {
                return false
        }
        return true
    }
}

extension Date {
    // Convert local time to UTC (or GMT)
    func toGlobalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = -TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    // Convert UTC (or GMT) to local time
    func toLocalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    
    
}

extension UISegmentedControl {
    func replaceSegments(segments: Array<String>) {
        self.removeAllSegments()
        for segment in segments {
            self.insertSegment(withTitle: segment, at: self.numberOfSegments, animated: false)
        }
    }
}

extension UIImage {
    
    convenience init?(withContentsOfUrl url: URL) throws {
        let imageData = try Data(contentsOf: url)
        
        self.init(data: imageData)
    }
    
}

extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
                if let data = try? Data(contentsOf: url) {
                    if let image = UIImage(data: data) {
                        DispatchQueue.main.async {
                            self?.image = image
                        }
                    }
                }
        }
    }
}


