//
//  ServiceSpecializeCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ServiceSpecializeCell: UITableViewCell {

    @IBOutlet weak var cellTitleLabel: UILabel!
    @IBOutlet weak var specializedTableview: UITableView!
    var listOfSpecialization: [String]?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if listOfSpecialization == nil {
            listOfSpecialization = []
        }
        
        let height = listOfSpecialization?.count ?? 0 * 40
        
        specializedTableview.frame = CGRect(x: specializedTableview.frame.origin.x, y: specializedTableview.frame.origin.y, width: specializedTableview.frame.size.width, height: (CGFloat(height)))
        
        self.setTableViewDatasource()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setListOfData(data: [String]) {
        listOfSpecialization = data
    }

}

extension ServiceSpecializeCell: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.specializedTableview.delegate = self
        self.specializedTableview.dataSource = self
        self.specializedTableview.estimatedRowHeight = 300
        self.specializedTableview.rowHeight = UITableViewAutomaticDimension
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  listOfSpecialization?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.SpecializationListCellIdentifier, for: indexPath)
            if let cellIs = cell as? SpecializationListCell {
                cellIs.selectionStyle = .none
                cellIs.setData(data: listOfSpecialization?[indexPath.row] ?? "")
            }
            cell.selectionStyle = .none
            return cell
      
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
