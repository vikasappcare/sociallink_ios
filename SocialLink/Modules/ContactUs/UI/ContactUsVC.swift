//
//  ContactUsVC.swift
//  SocialLink
//
//  Created by Santhosh on 10/04/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ContactUsVC: UIViewController {

    @IBOutlet weak var contactUsTable: UITableView!
    
    let placeHolderArray = ["name", "email", "phone number", "comment"]

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "CONTACT US"
        // Do any additional setup after loading the view.
        
        self.setTableViewDatasource()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ContactUsVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.contactUsTable.delegate = self
        self.contactUsTable.dataSource = self
        self.contactUsTable.estimatedRowHeight = 300
        self.contactUsTable.rowHeight = UITableViewAutomaticDimension
        self.contactUsTable.tableFooterView = UIView()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
           return 3
        case 1, 2:
            return 1
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.ContactUsTextFieldCellIdentifier, for: indexPath)
            if let cellIs = cell as? ContactUsTextFieldCell {
                cellIs.selectionStyle = .none
                cellIs.setData(data: placeHolderArray[indexPath.row])
                
            }
            cell.selectionStyle = .none
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.ContactUsDecriptionCellIdentifier, for: indexPath)
            if let cellIs = cell as? ContactUsDecriptionCell {
                cellIs.selectionStyle = .none
                cellIs.setData(data: "comment")
            }
            cell.selectionStyle = .none
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.ContactUsSubmitCellIdentifier, for: indexPath)
            if let cellIs = cell as? ContactUsSubmitCell {
                cellIs.selectionStyle = .none
                
            }
            cell.selectionStyle = .none
            return cell
        default:
            return UITableViewCell()
            
        }
        
    }
    
    
    
    
    
   
    
    
}
