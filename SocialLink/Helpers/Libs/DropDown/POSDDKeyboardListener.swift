//
//  POSDDKeyboardListener.swift
//  POSSalesSheet
//
//  Created by Marripelli, Santosh Kumar on 05/07/17.
//  Copyright © 2017 Verizon. All rights reserved.
//


import UIKit

internal final class POSDDKeyboardListener {
    
    static let sharedInstance: POSDDKeyboardListener = POSDDKeyboardListener()
    
    fileprivate(set) var isVisible: Bool = false
    fileprivate(set) var keyboardFrame: CGRect = CGRect.zero
    fileprivate var isListening: Bool = false
    
    deinit {
        stopListeningToKeyboard()
    }
    
}

// MARK: - Notifications

extension POSDDKeyboardListener {
    
    func startListeningToKeyboard() {
        if isListening {
            return
        }
        
        isListening = true
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow(_:)),
            name: NSNotification.Name.UIKeyboardWillShow,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(_:)),
            name: NSNotification.Name.UIKeyboardWillHide,
            object: nil)
    }
    
    func stopListeningToKeyboard() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc
    fileprivate func keyboardWillShow(_ notification: Notification) {
        isVisible = true
        keyboardFrame = keyboardFrame(fromNotification: notification)
    }
    
    @objc
    fileprivate func keyboardWillHide(_ notification: Notification) {
        isVisible = false
        keyboardFrame = keyboardFrame(fromNotification: notification)
    }
    
    fileprivate func keyboardFrame(fromNotification notification: Notification) -> CGRect {
        return ((notification as NSNotification).userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue ?? CGRect.zero
    }
    
}
