//
//  ProjectTextTableCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 24/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

protocol ProjectTextTableCellDelegate: class {
    func likeSelected(feedID: Int64, likeUnlike: Int, forRow: Int)
    func showLikesClicked(feedId: Int64)
    func showCommentsClicked(feedId: Int64)
    func dispalyComments(feedId: Int64)
    
}
var commentsCount : Int64?
class ProjectTextTableCell: UITableViewCell {
    
    weak var delegate: ProjectTextTableCellDelegate?
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var designation: UILabel!
    @IBOutlet weak var moreBUtton: UIButton!
    @IBOutlet weak var descriptionText: UILabel!
    @IBOutlet weak var likesLabel: UIButton!
    @IBOutlet weak var commentsLabel: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    var feedId: Int64?
    var likeUnlike: String?
    var likesCount : Int64?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        profileImage.layer.cornerRadius = profileImage.frame.size.width / 2
        profileImage.layer.masksToBounds = true
        //  moreBUtton.isHidden = true
        shareButton.isHidden = true
    }
    
    
    func setData(data: Feed , tag: Int, profile: ProfileModel) {
        feedId = data.feed_id
        //  lineUnlike = data.lik
        likeUnlike = data.userliked
        print(likeUnlike)
        if likeUnlike == "1"{
            likeButton.setImage(UIImage(named: "PostLike_Selected"), for: .normal)
            
        }
            
        else{
            likeUnlike = "0"
            likeButton.setImage(UIImage(named: "PostLike"), for: .normal)//PostLike
            
        }
        
        let userProfileImage = APPURL.feedUserProfileImage  + data.userimage!
        profileImage.sd_setImage(with: URL(string: userProfileImage), placeholderImage: UIImage(named: ""))
        profileName.text = data.username ?? ""
        designation.text = data.userdesignation ?? ""
        descriptionText.text = data.feed_details ?? ""
        likeButton.tag = tag
        likesCount = data.likesCount
        commentsCount = data.commentsCount
        likesLabel.setTitle("\(String(describing: data.likesCount ?? 0)) Likes", for: .normal)
        commentsLabel.tag = tag
        commentButton.tag = tag
        commentsCount = data.commentsCount
        commentsLabel.setTitle("\(String(describing: data.commentsCount ?? 0)) Comments", for: .normal)      
        
    }
    @IBAction func moreClicked(_ sender: Any) {
    }
    @IBAction func commentClicked(_ sender: Any) {
        if let idIs = feedId {
            self.delegate?.showCommentsClicked(feedId: idIs)
        }
    }
    @IBAction func shareClicked(_ sender: Any) {
        
        
    }
    
    @IBAction func showLikes(_ sender: Any) {
        if let idIs = feedId {
            self.delegate?.showLikesClicked(feedId: idIs)
        }
    }
    
    @IBAction func showComments(_ sender: UIButton) {
        if let idIs = feedId {
            self.delegate?.dispalyComments(feedId: idIs)
        }
    }
    
    func updateComments()
    {
        self.commentsLabel.setTitle("\(commentsCount)) Comments", for: .normal)
    }
    
    
    func updateLikeCell(likeData: Feed) {
        switch likeData.userliked {
        case "0":
            likeButton.isSelected = false
            likeUnlike = "1"
            likeButton.setImage(UIImage(named: "PostLike_Selected"), for: .normal)
        case "1":
            likeButton.isSelected = true
            likeUnlike = "0"
            likeButton.setImage(UIImage(named: "PostLike"), for: .selected)
        default:
            break
        }
        likesLabel.setTitle("\(String(describing: likesCount)) Likes", for: .normal)
    }
    
    @IBAction func likeClicked(_ sender: UIButton) {
        if let id = feedId, let liked = likeUnlike {
            var likeUnlikeIs = 0
            switch liked {
            case "0":
                likeUnlikeIs = 1
                likeUnlike = "1"
                likeButton.isSelected = true
                likeButton.setImage(UIImage(named: "PostLike_Selected"), for: .selected)
                likesLabel.setTitle("\(String(describing: likesCount! + 1)) Likes", for: .normal)
                
            case "1":
                likeButton.isSelected = false
                likeButton.setImage(UIImage(named: "PostLike"), for: .normal)
                likeUnlikeIs = 0
                likeUnlike = "0"
                if likesCount == 0{
                    likesLabel.setTitle("\(String(describing: likesCount!)) Likes", for: .normal)
                }
                else
                {
                    likesLabel.setTitle("\(String(describing: likesCount! - 1)) Likes", for: .normal)
                }
            default:
                break
            }
            self.delegate?.likeSelected(feedID: id, likeUnlike: likeUnlikeIs, forRow: sender.tag)
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
