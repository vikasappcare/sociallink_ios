//
//  PaymentTableViewCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 04/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

protocol SelectPaymentCellDelegate: class {
    func selectPayment()
}
class SelectPaymentCell: UITableViewCell {

    weak var delegate: SelectPaymentCellDelegate?

    
    
    @IBAction func selectPaymentClicked(_ sender: Any) {
        
        self.delegate?.selectPayment()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
