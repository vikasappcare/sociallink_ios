//
//  ProjectContactsVC.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 09/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

protocol ProjectContactsVCDelegate: class {
    func selectedContact(contact: ProjectContact)
}

class ProjectContactsVC: AppBaseVC {
    
    weak var delegate: ProjectContactsVCDelegate?
    @IBOutlet weak var contactsTable: UITableView!
    var viewModel: ContactsProjectViewModel?
    var selectedProject: Project?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = ContactsProjectViewModel()
        self.setTableViewDatasource()
        self.createNavigationItems()
        self.title = "Contacts"
        self.getContacts()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setProject(project: Project) {
        self.selectedProject = project
    }
    
    func getContacts() {
        if let model = viewModel {
            self.showHUD(title: "")
            if let idIs = self.selectedProject?.project_id {
                let data = ["token": APPStore.sharedStore.token,
                            "project_id": idIs] as [String : Any]
                model.getProjectContacts(parameters: data, onSuccess: { [weak self] (dataSource) in
                    print("Success")
                    self?.contactsTable.reloadData()
                    self?.hideHud()
                    if dataSource?.list?.count == 0 {
                        self?.showAlertForZeroContacts()
                    }
                    }, onError: { (error) in
                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
                      //  self.showAlert(title: Message.Alert, message: error.statusMessage)
                        self.hideHud()
                })
            }
        }
    }

    func showAlertForZeroContacts() {
                let alert = UIAlertController(title: "No Contacts Found Invite a Member to Join", message: "", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Invite", style: .default, handler: {  (_) in
                    if let controller = self.storyboard?.instantiateViewController(withIdentifier: "InviteTeamMember") as? InviteTeamMember {
                        let navController = APPNavigationController(rootViewController: controller)
                        controller.showBackButton(show: false, hideProject: true)
                        if let project = self.selectedProject {
                            controller.setSelectedProject(project: project)
                        }
                        self.present(navController, animated: true, completion: nil)
                    }
                    
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (_) in
        
                }))
        
                // 4. Present the alert.
                self.present(alert, animated: true, completion: nil)
        
            }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension ProjectContactsVC {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
        
        
        let addButton = UIBarButtonItem(image: UIImage(named: "addPlus"), style: .done, target: self, action: #selector(addClicked))
        self.navigationItem.rightBarButtonItem = addButton
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func addClicked() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "InviteTeamMember") as? InviteTeamMember {
            let navController = APPNavigationController(rootViewController: controller)
            controller.showBackButton(show: false, hideProject: true)
            if let project = self.selectedProject {
                controller.setSelectedProject(project: project)
            }
            self.present(navController, animated: true, completion: nil)
        }
//        let alert = UIAlertController(title: "Enter Email Id", message: "", preferredStyle: .alert)
//        alert.addTextField { (textField) in
//            textField.placeholder = "Enter Email Id"
//            textField.keyboardType = UIKeyboardType.emailAddress
//            textField.clearButtonMode = UITextFieldViewMode.whileEditing
//        }
//        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { [weak alert] (_) in
//            let textField = alert?.textFields![0]
//            print("Text field: \(String(describing: textField?.text) )")
//
//            if !APPUtility.validateEmail(enteredEmail: textField?.text ?? "") {
//                self.showAlert(title: "Alert", message: "Enter a valid email")
//                return
//            }
//
//            let userContact = UserContact()
//            userContact.emailid = textField?.text ?? ""
//            self.delegate?.selectedContact(contact: userContact)
//            self.dismiss(animated: true, completion: nil)
//
//        }))
//        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (_) in
//
//        }))
//
//        // 4. Present the alert.
//        self.present(alert, animated: true, completion: nil)

    }
    
    
    
    
}
extension ProjectContactsVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.contactsTable.delegate = self
        self.contactsTable.dataSource = self
        self.contactsTable.estimatedRowHeight = 300
        self.contactsTable.rowHeight = UITableViewAutomaticDimension
        self.contactsTable.tableFooterView = UIView()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let model = viewModel {
         return model.contactsArray?.count ?? 0
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:
            CellIdentifiers.ProjectsContactsCellIdentifier, for: indexPath)
        if let cellIs = cell as? ProjectsContactsCell {
            cellIs.selectionStyle = .none
            if let model = viewModel {
                print(model.contactsArray![indexPath.row])
                cellIs.setData(data: model.contactsArray![indexPath.row])
            }
            
        }
        cell.selectionStyle = .none
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let model = viewModel {
            self.delegate?.selectedContact(contact: model.contactsArray![indexPath.row])
        }
        self.dismiss(animated: true, completion: nil)
    }
}
