//
//  PaymentController.swift
//  SocialLink
//
//  Created by Harsha on 09/05/19.
//

import UIKit
import Stripe

class PaymentController: AppBaseVC {
    
    var selectedTask: Task?
    var selectedProject: Project?
    var sum = 0
    var detailViewModel: TaskDetailViewModel?
    
    var totalAmount: Double?
    
    @IBOutlet weak var taskNameLabel: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var dueDate: UILabel!
    @IBOutlet weak var timeWorked: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var amountBilled: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if  self.navigationController?.navigationBar.backItem == nil {
            self.createNavigationItems()
        }
        detailViewModel = TaskDetailViewModel()
        taskNameLabel.text = selectedTask?.tname
        startDate.text = selectedTask?.tsdate
        dueDate.text = selectedTask?.tedate
        status.text = "Completed"
        self.navigationItem.title = "PAYMENT"
        taskLog()

    }
    func setProject(task: Task, project: Project) {
        self.selectedTask = task
        self.selectedProject = project
    }
    
    @IBAction func payButtonAction(_ sender: Any) {
        print(Int(totalAmount!))
       // showPayNowAlert()
        if Int(totalAmount!) != 0{
        showPayNowAlert()
        }
        else
        {
            self.showAlert(title: "Alert!", message: "There is no amount to pay.")

        }
    }
    func showPayNowAlert() {
        let alert = UIAlertController(title: "Alert!", message: "Are you sure? you want to pay \(Int(totalAmount!))$.",         preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: { _ in
            self.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Pay",
                                      style: UIAlertActionStyle.default,
                                      handler: {(_: UIAlertAction!) in
                                        let addCardViewController = STPAddCardViewController()
                                        addCardViewController.delegate = self
                                        self.navigationController?.pushViewController(addCardViewController, animated: true)        }))
        self.present(alert, animated: true, completion: nil)
    }
}
extension PaymentController {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
}
extension PaymentController{
    
    func taskLog()
    {
        if let model = detailViewModel {
            
            let parms = ["task_id": selectedTask?.task_id ?? "",
                         "uid": APPStore.sharedStore.user.userId!,
                         "pid": selectedProject?.project_id! ?? ""] as [String : Any]
            print(parms)
            self.showHUD(title: "")
            
            model.updateTaskLog(parameters: parms, onSuccess: { [weak self] (dataSource) in
                print("Print Success")
                // print(dataSource?.tassignto)
                let time =  dataSource?.cumulate_time ?? [""]
                print(dataSource?.cumulate_time ?? "")
                let time1 = time.map { Int($0) }
                var timeSum = 0
                
                for element in time1 {
                    if element != nil{
                    timeSum += element!
                    }
                }
                self?.sum = timeSum
                self?.timeWorked.text = self?.minutesToHoursMinutes(minutes: (self?.sum)!) ?? ""
                //self?.paymentTable.reloadData()
                let amount = (self?.caliculateBudget(minutes: Double((self?.sum)!)))
                self?.amountBilled.text = "\(amount ?? 0.0)$"
                self?.totalAmount = amount! * 100
                self?.hideHud()
                }, onError: { (error) in
                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
//                    self.showAlert(title: Message.Alert, message: error.statusMessage)
                    self.hideHud()
            })
            
        }
    }
    func minutesToHoursMinutes (minutes : Int) -> String {
        
        let hours = minutes / 60
        let minutes = (minutes % 60)
        //let second = (seconds % 3600) % 60
        
        return String(format: "%02d:%02d", hours, minutes)
    }
    func caliculateBudget(minutes: Double) -> Double
    {
        return (minutes*1.416).roundTo(places: 2)
    }
}
extension Double {
    func roundTo(places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

extension PaymentController: STPAddCardViewControllerDelegate {
    
    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreatePaymentMethod paymentMethod: STPPaymentMethod, completion: @escaping STPErrorBlock) {
        
        totalAmount = (self.caliculateBudget(minutes: Double((self.sum)))) * 100
        print(Int(totalAmount!))
        let params: [String: Any] = [
            //"token": paymentMethod.stripeId,//token.tokenId,
            "amount":Int(totalAmount!),
            "currency": StripeConstants.defaultCurrency,
            "description": StripeConstants.defaultDescription,
            "cust_email": APPStore.sharedStore.user.email ?? "",
            "user_id": APPStore.sharedStore.user.userId ?? 0,
            "task_id": selectedTask?.task_id ?? 0,
            "project_id": selectedProject?.project_id ?? 0]
        print(params)
        StripeClient.shared.completeCharge(amount: Int(totalAmount!), params: params) { result in
            print(result)
            switch result {
            // 1
            case .success:
                completion(nil)
                
                let alertController = UIAlertController(title: "Congrats",
                                                        message: "Your payment was successful!",
                                                        preferredStyle: .alert)
                let alertAction = UIAlertAction(title: "OK", style: .default, handler: { _ in
                    self.navigationController?.popViewController(animated: true)
                })
                alertController.addAction(alertAction)
                self.present(alertController, animated: true)
            // 2
            case .failure(let error):
                completion(error)
            }
        }
    }
    
    
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        navigationController?.popViewController(animated: true)
    }
    
    //Vikas change
    /*
     completion: @escaping STPErrorBlock
     completion(error as NSError)
     */
    /*
    func addCardViewController(_ addCardViewController: STPAddCardViewController,
                               didCreateToken token: STPToken,
                               completion: (NSError?) -> Void) {
        totalAmount = (self.caliculateBudget(minutes: Double((self.sum)))) * 100
        print(Int(totalAmount!))
        let params: [String: Any] = [
            "token": token.tokenId,
            "amount":Int(totalAmount!),
            "currency": StripeConstants.defaultCurrency,
            "description": StripeConstants.defaultDescription,
            "cust_email": APPStore.sharedStore.user.email ?? "",
            "user_id": APPStore.sharedStore.user.userId ?? 0,
            "task_id": selectedTask?.task_id ?? 0,
            "project_id": selectedProject?.project_id ?? 0]
        print(params)
        StripeClient.shared.completeCharge(with: token, amount: Int(totalAmount!), params: params) { result in
            print(result)
            switch result {
            // 1
            case .success:
                completion(nil)
                
                let alertController = UIAlertController(title: "Congrats",
                                                        message: "Your payment was successful!",
                                                        preferredStyle: .alert)
                let alertAction = UIAlertAction(title: "OK", style: .default, handler: { _ in
                    self.navigationController?.popViewController(animated: true)
                })
                alertController.addAction(alertAction)
                self.present(alertController, animated: true)
            // 2
            case .failure(let error):
                completion(error as NSError)
            }
        }
    }
 */
}
