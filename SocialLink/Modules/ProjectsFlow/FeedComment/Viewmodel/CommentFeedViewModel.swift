//
//  CommentFeedViewModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 11/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation

class CommentFeedViewModel {
    
    init() {
    }
    
    var commentsArray: [CommentedUsers]?
    
    func getComments(parameters: [String: Any],
                 onSuccess: @escaping (FeedCommentModel?) -> Void,
                 onError: @escaping APIErrorHandler) {
        AppDataManager.getComments(parametes: parameters, onSuccess: { (dataSource) in
            self.commentsArray = dataSource?.comments
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
    func postComment(parameters: [String: Any],
                     onSuccess: @escaping (AddCommentModel?) -> Void,
                     onError: @escaping APIErrorHandler) {
        AppDataManager.postComment(parametes: parameters, onSuccess: { (dataSource) in
            if let data = dataSource?.comment {
                self.commentsArray?.insert(data, at: 0)
            }
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
}
