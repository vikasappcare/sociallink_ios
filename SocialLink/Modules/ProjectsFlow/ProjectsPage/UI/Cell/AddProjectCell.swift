//
//  AddProjectCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

protocol AddProjectCellDelegate: class {
    
    func addProjectClicked()
    func inviteMemberClicked()
}

class AddProjectCell: UITableViewCell {

    weak var delegate: AddProjectCellDelegate?
    @IBOutlet weak var inviteMember: UIButton!
    @IBOutlet weak var addNewProject: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func inviteMemberCLicked(_ sender: Any) {
        self.delegate?.inviteMemberClicked()
    }
    @IBAction func addNewProjectClicked(_ sender: Any) {
        self.delegate?.addProjectClicked()
    }
    
}
