//
//  OTPModule.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 09/08/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON

class OTPModule: AppBaseModel {
    var service: String?
    var otp: Int64?
    var data: String?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        self.service = response["service"].stringValue
        self.otp = response["otp"].int64Value
        self.data = response["data"].stringValue
    }
}
