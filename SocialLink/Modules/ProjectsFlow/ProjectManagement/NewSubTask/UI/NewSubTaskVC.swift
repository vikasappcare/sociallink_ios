//
//  NewSubTaskVC.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/05/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class NewSubTaskVC: UIViewController {

    @IBOutlet weak var newSubTaskTable: UITableView!
    var subTaskType: String?
    var subTaskStatus: String?
    var count: Int?
    override func viewDidLoad() {
        super.viewDidLoad()

        self.createNavigationItems()
        self.setTableViewDatasource()
        // Do any additional setup after loading the view.
        
        switch subTaskType {
        case SUBTASKTYPE.NEW:
            self.navigationItem.title = "New Sub Task"
            count = 0
            self.subTaskStatus = SubTaskStatus.yetToStart
        default:
            self.navigationItem.title = "Edit Sub Task"
            count = 2
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setSubTaskType(type: String) {
        subTaskType = type
    }
    
    func setSubTaskStatus(status: String) {
        subTaskStatus = status
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension NewSubTaskVC {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension NewSubTaskVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.newSubTaskTable.delegate = self
        self.newSubTaskTable.dataSource = self
        self.newSubTaskTable.estimatedRowHeight = 300
        self.newSubTaskTable.rowHeight = UITableViewAutomaticDimension
        self.newSubTaskTable.tableFooterView = UIView()
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        case 1:
            return 1
        case 2:
            return 1
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
      
        
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier:
                    CellIdentifiers.NewSubTaskDataCellIdentifier, for: indexPath)
                if let cellIs = cell as? NewSubTaskDataCell {
                    cellIs.selectionStyle = .none
                    let dataIs = ["count": count ?? 0] as [String: Any]
                    cellIs.setData(data: dataIs)
                }
                cell.selectionStyle = .none
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier:
                    CellIdentifiers.NewSubTaskUploadCellIdentifier, for: indexPath)
                if let cellIs = cell as? NewSubTaskUploadCell {
                    cellIs.selectionStyle = .none
                }
                cell.selectionStyle = .none
                return cell
            default:
                return UITableViewCell()
            }
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.NewSubTaskDescriptionCellIdentifier, for: indexPath)
            if let cellIs = cell as? NewSubTaskDescriptionCell {
                cellIs.selectionStyle = .none
            }
            cell.selectionStyle = .none
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.NewSubTaskSaveDeleteCellIdentifier, for: indexPath)
            if let cellIs = cell as? NewSubTaskSaveDeleteCell {
                cellIs.selectionStyle = .none
                var dataIs: [String: Any]?
                switch self.subTaskStatus {
                case SubTaskStatus.yetToStart:
                    dataIs = ["completeTitle": "Start",
                              "deleteTitle": "Delete",
                              "backColor": UIColor.appGreenColor]
                    cellIs.setType(type: SubTaskStatus.yetToStart)
                case SubTaskStatus.inProgress:
                    dataIs = ["completeTitle": "Complete",
                              "deleteTitle": "Delete",
                    "backColor": UIColor.appOrangeColor]
                    cellIs.setType(type: SubTaskStatus.inProgress)
                case SubTaskStatus.completed:
                    dataIs = ["completeTitle": "Reopen",
                              "deleteTitle": "Delete",
                    "backColor": UIColor.appBlueColor]
                    cellIs.setType(type: SubTaskStatus.completed)
                case SubTaskStatus.confirmed:
                    dataIs = ["completeTitle": "Delete",
                              "deleteTitle": "",
                    "backColor": UIColor.appRedColor]
                    cellIs.setType(type: SubTaskStatus.confirmed)
                default:
                    break
                }
                
                cellIs.setData(data: dataIs!)
                
            }
            cell.selectionStyle = .none
            return cell
        default:
            return UITableViewCell()
            
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 1:
            return 30
        default:
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.appLightGrayColor
        let label = UILabel(frame: CGRect(x: 15, y: 0, width: 300, height: 30))
        label.font = UIFont.semiBoldFont(16)
        headerView.addSubview(label)
        label.text = "Task Description"
        return headerView
    }
    
    /*func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     switch indexPath.section {
     case 0:
     return true
     default:
     return false
     }
     
     }
     func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
     
     
     let edit = UITableViewRowAction(style: .normal, title: "Edit") { action, index in
     
     }
     edit.backgroundColor = UIColor.lightGray
     
     let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
     print("more button tapped")
     }
     delete.backgroundColor = UIColor.darkGray
     
     return [delete, edit]
     
     }*/
    
    
    
    
}
