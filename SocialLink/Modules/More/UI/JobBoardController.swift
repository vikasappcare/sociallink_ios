//
//  JobBoardController.swift
//  SocialLink
//
//  Created by Harsha on 08/05/19.
//

import UIKit

class JobBoardController: AppBaseVC {
    
    var viewModel: ProjectsControllerViewModel?

    @IBOutlet weak var jobboardTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        getCurrentProjects()
        setTableViewDatasource()
        self.createNavigationCloseItems()
        navigationItem.title = "JobBoard"

    }
    func getCurrentProjects() {
        if let model = viewModel {
            var parms = [String: Any]()
            if APPStore.sharedStore.appUserID == "2"{
                parms = ["user_id": APPStore.sharedStore.user.userId ?? ""] as [String : Any]
            }
            else{
                parms = ["emailid": APPStore.sharedStore.user.email ?? ""] as [String : Any]
            }
            print(parms)
            self.addActivityIndicatior()
            DispatchQueue.global(qos: .background).async {
                model.getCurrentProjectList(parameters: parms, onSuccess: { [weak self] (dataSource) in
                    DispatchQueue.main.async {
                        model.updateProjects()
                        model.updateProjectList(state: Int64(0))
                        self?.jobboardTableView.reloadData()
                        self?.removeActivityIndicator()
                    }
                    }, onError: { (error) in
                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
                        DispatchQueue.main.async {
                          //  self.showAlert(title: Message.Alert, message: error.statusMessage)
                            self.removeActivityIndicator()
                        }
                })
            }
        }
    }
}
extension JobBoardController: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.jobboardTableView.delegate = self
        self.jobboardTableView.dataSource = self
        self.jobboardTableView.estimatedRowHeight = 300
        self.jobboardTableView.rowHeight = UITableViewAutomaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let model = viewModel?.contractorProjectsArray {
            return model.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:
            CellIdentifiers.ProjectTableViewCellIdentifier, for: indexPath)
        if let cellIs = cell as? ProjectTableViewCell {
            cellIs.selectionStyle = .none
            cellIs.statusLabel.isHidden = true
            cellIs.quotationButton.isHidden = true
            var color: UIColor?
             color = UIColor.appBlueColor
            if let model = viewModel, let colorIs = color {
                if APPStore.sharedStore.appUserID == "1"{
                    cellIs.setData(data: model.contractorProjectsArray![indexPath.row], color: colorIs, index: 2)
                }
                else{
                    cellIs.setData(data: model.employerProjectsArray![indexPath.row], color: colorIs, index: 2)
                    
                }
            }
        }
        cell.selectionStyle = .none
        return cell
        }
}
extension JobBoardController
{
    func createNavigationCloseItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
}
