//
//  ProjectSearchContactCell.swift
//  InternalMembers
//
//  Created by Santhosh Marripelli on 23/04/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ProjectSearchContactCell: UITableViewCell {

    @IBOutlet weak var selectImage: UIImageView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profileLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        nameLabel.setReguralFont(size: 16)
        profileLabel.setReguralFont(size: 13)
    }
    
    
    func setData(data: [String: Any]) {
       
        profileImage.image = UIImage(named: data["image"] as? String ?? "")
       nameLabel.text = data["name"] as? String ?? ""
        profileLabel.text = "    \(data["profile"] as? String ?? "")    "
        self.profileLabel.sizeToFit()
        profileLabel.backgroundColor = data["color"] as? UIColor ?? UIColor.clear

        let selected =  data["selected"] as? Bool ?? false

        if selected {
            selectImage.image = UIImage(named: "checkedBlue")
        } else {
            selectImage.image = UIImage(named: "uncheckedBlue")

        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
