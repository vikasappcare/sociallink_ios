//
//  ContactProfileVC.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ContactProfileVC: AppBaseVC {
    
    ///let profileKeyData = ["Portolio", "Email", "Phone", "Resume", "Website"]
    let profileKeyData = ["Email",
                          "Phone",
                          "Gender",
                          "Website",
                          "Education",
                          "Skills",
                          "Business",
                          "Industry",
                          "Department",
                          "Job",
                          "Resume",
                          "Designation"]
    var profileValueData: [String: String]?
   //// var profileValueData = ["portolio.jpg", "resume.jpg", "www.bobross.com"]
    ///let colorArray = [UIColor.appBlueColor, UIColor.appMediumGrayColor, UIColor.appMediumGrayColor, UIColor.appBlueColor, UIColor.appMediumGrayColor]
    let colorArray = [UIColor.appBlueColor, UIColor.appBlueColor]
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var contactProfileTable: UITableView!
    var viewModel: ContactProfileViewModel?
    var memberId: Int64?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = ContactProfileViewModel()
        self.navigationItem.title = "PROFILE"
        self.setTableViewDatasource()
        self.getContactInfo()
        // Do any additional setup after loading the view.
    }
    @IBAction func closeClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setMemberId(id: Int64) {
        self.memberId = id
    }
    
    func getContactInfo() {
        if let model = viewModel {
            if let idIs = self.memberId {
                let parms = ["token": APPStore.sharedStore.token,
                             "member_id": idIs] as [String : Any]
                self.showHUD(title: "")
                model.getContactProfile(parameters: parms, onSuccess: { [weak self] (dataSource) in
                    print("Success")
                    self?.hideHud()
                    self?.profileValueData = model.constructValueArray()
                    self?.contactProfileTable.reloadData()
                    }, onError: { (error) in
                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
                      //  self.showAlert(title: Message.Alert, message: error.statusMessage)
                        self.hideHud()
                })
            }
    
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ContactProfileVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.contactProfileTable.delegate = self
        self.contactProfileTable.dataSource = self
        self.contactProfileTable.estimatedRowHeight = 300
        self.contactProfileTable.rowHeight = UITableViewAutomaticDimension
        self.contactProfileTable.tableFooterView = UIView()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0, 1, 3:
            return 1
        case 2:
            return profileKeyData.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.ContactProfileCellIdentifier, for: indexPath)
            if let cellIs = cell as? ContactProfileCell {
                cellIs.selectionStyle = .none
                cellIs.delegate = self
                if let profile = viewModel?.userProfile {
                    cellIs.setData(profile: profile)
                }
            }
            cell.selectionStyle = .none
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.ContactAboutCellIdentifier, for: indexPath)
            if let cellIs = cell as? ContactAboutCell {
                cellIs.selectionStyle = .none
                if let profile = viewModel?.userProfile {
                    cellIs.setData(profile: profile)
                }
            }
            cell.selectionStyle = .none
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.ContactDetailCellIdentifier, for: indexPath)
            if let cellIs = cell as? ContactDetailCell {
                cellIs.selectionStyle = .none
                if let valueIs = self.profileValueData {
                    let dataIs = ["key": profileKeyData[indexPath.row],
                                  "value": valueIs[profileKeyData[indexPath.row]] ?? "",
                                  "color": colorArray[0]] as [String: Any]
                    cellIs.setData(data: dataIs)
                }
               
                
            }
            cell.selectionStyle = .none
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.ContactInviteCellIdentifier, for: indexPath)
            if let cellIs = cell as? ContactInviteCell {
                cellIs.selectionStyle = .none
            }
            cell.selectionStyle = .none
            return cell
        default:
            return UITableViewCell()
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 2:
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ImageViewerVC") as? ImageViewerVC {
                controller.setImage(image: "SampleProfile")
                self.present(controller, animated: true, completion: nil)
            }
        default:
            break
        }
        
    }
}

extension ContactProfileVC: ContactProfileCellDelegate {
    func messageClicked() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProjectMesagesListVC") as? ProjectMesagesListVC {
            let navigationController = APPNavigationController.init(rootViewController: controller)
            self.present(navigationController, animated: true, completion: nil)
        }
    }
    
    
    
    
}
