//
//  ProjectsContactsCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 09/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ProjectsContactsCell: UITableViewCell {
@IBOutlet weak var contactImage: UIImageView!
    @IBOutlet weak var contactName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(data: ProjectContact) {
       // contactImage.sd_setImage(with: URL(string: data.image ?? ""), placeholderImage: #imageLiteral(resourceName: "SampleProfile"))
        contactName.text = data.member_name ?? ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
