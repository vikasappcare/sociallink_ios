//
//  ContactUsDecriptionCell.swift
//  SocialLink
//
//  Created by Santhosh on 10/04/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ContactUsDecriptionCell: UITableViewCell {

    @IBOutlet weak var descriptionText: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        descriptionText.delegate = self
    }
    
    func setData(data: String) {
        ////
        
        descriptionText.text = data
        descriptionText.textColor = UIColor.lightGray
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension ContactUsDecriptionCell: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if descriptionText.textColor == UIColor.lightGray {
            descriptionText.text = ""
            descriptionText.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if descriptionText.text == "" {
            descriptionText.text = "comment"
            descriptionText.textColor = UIColor.lightGray
        }
    }

    
}
