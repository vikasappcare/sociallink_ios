//
//  TaskStateSegmentCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 01/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

protocol TaskStateSegmentCellDelegate: class {
    
    func stateSelected(state: Int64)
}

class TaskStateSegmentCell: UITableViewCell {

    weak var delegate: TaskStateSegmentCellDelegate?
    @IBOutlet weak var taskSegment: AppSegmentControl!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(data: Int64) {
        self.taskSegment.selectedSegmentIndex = Int(data)
    }

    @IBAction func segmentSelected(_ sender: Any) {
        self.delegate?.stateSelected(state: Int64(taskSegment.selectedSegmentIndex))
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
