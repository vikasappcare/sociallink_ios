//
//  InvoiceDetailVC.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 11/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class InvoiceDetailVC: UIViewController {

    @IBOutlet weak var projectName: UILabel!
    @IBOutlet weak var invoiceName: UILabel!
    @IBOutlet weak var invoiceAmount: UILabel!
    @IBOutlet weak var invoiceDetailTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
self.navigationItem.title = "PAY INVOICE"
        self.setTableViewDatasource()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension InvoiceDetailVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.invoiceDetailTable.delegate = self
        self.invoiceDetailTable.dataSource = self
        self.invoiceDetailTable.estimatedRowHeight = 300
        self.invoiceDetailTable.rowHeight = UITableViewAutomaticDimension
        
        self.invoiceDetailTable.tableFooterView = UIView()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 3
        case 2, 3:
            return 1
        case 1:
            return 3
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier:
                    CellIdentifiers.InvoiceDateCellIdentifier, for: indexPath)
                if let cellIs = cell as? InvoiceDateCell {
                    cellIs.selectionStyle = .none
                }
                cell.selectionStyle = .none
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier:
                    CellIdentifiers.PaymentDueCellIdentifier, for: indexPath)
                if let cellIs = cell as? PaymentDueCell {
                    cellIs.selectionStyle = .none
                }
                cell.selectionStyle = .none
                return cell
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier:
                    CellIdentifiers.ProfileTableViewCellIdentifier, for: indexPath)
                if let cellIs = cell as? ProfileTableViewCell {
                    cellIs.selectionStyle = .none
                }
                cell.selectionStyle = .none
                return cell
            default:
                return UITableViewCell()
            }
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.DetailProjectCellIdentifier, for: indexPath)
            if let cellIs = cell as? DetailProjectCell {
                cellIs.selectionStyle = .none
                
            }
            cell.selectionStyle = .none
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.SendPaymentCellIdentifier, for: indexPath)
            if let cellIs = cell as? SendPaymentCell {
                cellIs.selectionStyle = .none
                cellIs.delegate = self
                
            }
            cell.selectionStyle = .none
            return cell
        default:
            return UITableViewCell()
        }
        
        
      
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 2:
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as? PaymentViewController {
                /// let navController = APPNavigationController(rootViewController: controller)
                self.navigationController?.pushViewController(controller, animated: true)
            }
        default:
            break
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 1:
            return 60
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        let titleLabel = UILabel(frame: CGRect(x: 20, y: 20, width: UIScreen.main.bounds.width - 27, height: 30))
        
        titleLabel.text = "Services"
        headerView.addSubview(titleLabel)
        titleLabel.textColor = UIColor.init(red: 171/255.0, green: 171/255.0, blue: 171/255.0, alpha: 1.0)
        titleLabel.font = UIFont.boldFont(13)
        headerView.backgroundColor = UIColor.appLightGrayColor
        return headerView
    }
    
}
extension InvoiceDetailVC: SendPaymentCellDelegate {
    func sendPaymentSelected() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as? PaymentViewController {
            /// let navController = APPNavigationController(rootViewController: controller)
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    
    
}
