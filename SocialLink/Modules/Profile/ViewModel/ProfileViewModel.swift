
//
//  ProfileViewMOdel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 15/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation


class ProfileViewModel {
    init() {
    }
    var editContactProfile: ProfileModel?
    var dataIs: [String: Any]?
   

    
    func constructUpdateProfile(profile: ProfileModel) -> [String: Any]? {
        let updateData = ["token": APPStore.sharedStore.token,
                          "ufullname": profile.full_name,
                          "uphone": profile.phone_number,
                          "ucity": profile.city,
                          "ustate": profile.state,
                          "ucountry": profile.country,
                          "uaboutu": profile.aboutme,
                          "ugender": profile.gender,
                          "ueducation": profile.education,
                          "uskills": profile.skills,
                          "business": profile.business,
                          "industry": profile.industry,
                          "department": profile.department,
                          "job": profile.job,
                          "resume": profile.resume,
                          "uphoto": profile.image,
                          "uwebsite": profile.website,
                          "designation": profile.designation,
                          "uzipcode": ""]
        return updateData as [String : Any]
    }
    
    
    func updateProfile(parameters: [String: Any],
                           onSuccess: @escaping (SignUpPasswordModel?) -> Void,
                           onError: @escaping APIErrorHandler) {
        AppDataManager.updateProfile(parametes: parameters, onSuccess: { (dataSource) in
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
    
}
