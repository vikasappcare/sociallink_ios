//
//  InviteTeamMemberVC.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 28/08/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class InviteTeamMember: AppBaseVC {
    
    @IBOutlet weak var projectStack: UIStackView!
    @IBOutlet weak var projectHeight: NSLayoutConstraint!
    @IBOutlet weak var inviteTable: UITableView!
    var selectedProjectId: Int64?
    var viewModel: InviteViewModel?
    var showBack: Bool?
    var selectedProject: Project?
    var hideProjectStack: Bool?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = InviteViewModel()
        
        self.selectedProjectId = 0
        self.setTableview()
        
        if let show = showBack {
            if !show {
                self.createNavigationItems()
            }
        }
       
        // Do any additional setup after loading the view.
    }
    
    func showBackButton(show: Bool, hideProject: Bool) {
        showBack = show
        hideProjectStack = hideProject
    }
    
    func setSelectedProject(project: Project) {
        self.selectedProject = project
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension InviteTeamMember {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
 
}


extension InviteTeamMember: UITableViewDelegate, UITableViewDataSource {
    
    func setTableview() {
        self.inviteTable.tableFooterView = UIView()
        self.inviteTable.delegate = self
        self.inviteTable.dataSource = self
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.InviteTeamMemberCellIdentifier, for: indexPath) as UITableViewCell
        if let cellIs = cell as? InviteTeamMemberCell {
            cellIs.selectionStyle = .none
            cellIs.delegate = self
            
            if let show = hideProjectStack {
               cellIs.hideProjectStack(hide: show)
            }
            
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
}

extension InviteTeamMember: InviteTeamMemberCellDelegate {
    func showScreen(screen: String) {
        print(screen)
        
        switch screen {
        case "Project":
            print("Project")
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProjectListController") as? ProjectListController {
                controller.delegate = self
                let navController = APPNavigationController(rootViewController: controller)
                self.present(navController, animated: true, completion: nil)
            }
        case "Role":
            print("Role")
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "RolesViewController") as? RolesViewController {
                controller.delegate = self
                controller.setServiceType(type: ScreenType.ROLE)
                let navController = APPNavigationController(rootViewController: controller)
                self.present(navController, animated: true, completion: nil)
            }
        default:
            break
        }
        
        
        
    }
    
    func sendData(data: [String : Any]) {
        print(data)
        var projectIs = ""
        if let project = selectedProject {
            self.selectedProjectId = project.project_id
            projectIs = project.project_name ?? ""
        } else {
            projectIs = data["project"] as? String ?? ""
        }
        
        
        let fName = data["fName"] as? String
        let lName = data["lName"] as? String
        let email = data["email"] as? String
        
        let role = data["role"] as? String
        
        if fName?.checkForEmpty() ?? false {
            self.showAlert(title: "Alert!", message: "Please enter First Name to continue")
            return
        } else if lName?.checkForEmpty() ?? false {
            self.showAlert(title: "Alert!", message: "Please enter Last Name to continue")
            return
        } else if email?.checkForEmpty() ?? false {
            self.showAlert(title: "Alert!", message: "Please enter Email to continue")
            return
        } else if !APPUtility.validateEmail(enteredEmail: email ?? "")  {
            self.showAlert(title: "Alert!", message: "Please enter a valid email to continue")
            return
        } else if projectIs.checkForEmpty() ?? false {
            self.showAlert(title: "Alert!", message: "Please select project to continue")
            return
        } else if role?.checkForEmpty() ?? false {
            self.showAlert(title: "Alert!", message: "Please select role to continue")
            return
        }
        
        let data = ["token": APPStore.sharedStore.token ,
                    "fname": fName ?? "" ,
                    "lname": lName ?? "",
                    "memailid": email ?? "",
                    "mrole": role ?? "",
                    "user_id": APPStore.sharedStore.user.userId ?? 0,
                    "project_id": self.selectedProjectId ?? 0,
                    ] as [String : Any]
        
        print(data)
        
        self.inviteTeamMember(data: data)
        
        
        
        
    }
    
    func inviteTeamMember(data: [String: Any]) {
        if let model = viewModel {
            self.showHUD(title: "")
            model.inviteMember(parameters: data, onSuccess: { [weak self] (dataSource) in
                print("Success")
                let alertController = UIAlertController(title: "Success", message: "Member has been successfully invited", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                    if let show = self?.showBack {
                        if !show {
                            self?.dismiss(animated: true, completion: nil)
                        } else {
                            self?.navigationController?.popViewController(animated: true)
                        }
                    }
                }))
                self?.present(alertController, animated: true, completion: nil)
                self?.hideHud()
                }, onError: { (error) in
                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
                   // self.showAlert(title: Message.Alert, message: error.statusMessage)
                    self.hideHud()
            })
        }
    }
    
}

extension InviteTeamMember: ProjectListControllerDelegate,RolesViewControllerDelegate {
    func serviceSelectted(service: Service) {
        ///
    }
    
    func roleSelectted(role: Role) {
        print(role)
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = self.inviteTable.cellForRow(at: indexPath)
        if let cellIs = cell as? InviteTeamMemberCell {
            cellIs.updateRole(role: role.name ?? "")
        }
    }
    
    func projectSelectted(project: Project) {
        print(project)
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = self.inviteTable.cellForRow(at: indexPath)
        if let cellIs = cell as? InviteTeamMemberCell {
            cellIs.updateProject(project: project.project_name ?? "")
            self.selectedProjectId = project.project_id ?? 0
        }
    }
    
    
    
    
    
}
