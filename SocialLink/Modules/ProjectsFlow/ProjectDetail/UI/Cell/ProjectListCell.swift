//
//  ProjectListCell.swift
//  SocialLink
//
//  Created by Santhosh on 21/04/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ProjectListCell: UICollectionViewCell {
    
    @IBOutlet weak var listImage: UIImageView!
    @IBOutlet weak var listText: UILabel!
    
    
    
    func setData(data: [String: Any]) {
        
        listImage.image = UIImage(named: data["image"] as? String ?? "")
        listText.text = data["text"] as? String ?? ""
        
        
    }
    
     
}
