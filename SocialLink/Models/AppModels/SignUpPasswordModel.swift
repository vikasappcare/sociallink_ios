//
//  SignUpPasswordModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 15/08/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON



class SignUpPasswordModel: AppBaseModel {
    
    var service: String?
    var token: String?
    var profile: ProfileModel?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    
    private func parseResponse(with response: JSON) {
        
        self.service = response["service"].stringValue
        self.token = response["token"].stringValue
        self.profile = createObject(with: response["profile"])
        
    }
    
    private func createObject(with jsonObject: JSON) -> ProfileModel? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return ProfileModel.init(jsonObject)
        }
    }
    
}



