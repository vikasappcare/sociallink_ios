//
//  TimeSheetsVC.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 07/06/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class TimeSheetsVC: AppBaseVC {
    
    @IBOutlet weak var timeSheetTable: UITableView!
    var viewModel: TimeSheetViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = TimeSheetViewModel()
        self.setTableViewDatasource()
        self.navigationItem.title = "My Timesheets"
        // Do any additional setup after loading the view.
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            self.getTimesheets()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getTimesheets() {
        if let model = viewModel {
                let dataIs = ["token": APPStore.sharedStore.token,
                              "user_id": APPStore.sharedStore.user.userId ?? 0,
                              "start_time": Constant.defaultDate,
                              "start": 0,
                              "limit": Constant.ProjectFeedLimit] as [String : Any]
                self.addActivityIndicatior()
                DispatchQueue.global(qos: .background).async {
                    model.getContractorTimesheets(parameters: dataIs, onSuccess: { [weak self] (object) in
                        dprint(object: "Success")
                        DispatchQueue.main.async {
                            if model.timesheetArray == nil {
                                model.timesheetArray = [TimeSheet]()
                            }
                            self?.timeSheetTable.reloadData()
                            self?.hideHud()
                            self?.removeActivityIndicator()
                        }
                        }, onError: { (error) in
                            Logger.log(message: "Error \(error.statusMessage)", event: .error)
                          //  self.showAlert(title: Message.Alert, message: error.statusMessage)
                            self.hideHud()
                            self.removeActivityIndicator()
                    })
                }
                
            
        }
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension TimeSheetsVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.timeSheetTable.delegate = self
        self.timeSheetTable.dataSource = self
        self.timeSheetTable.estimatedRowHeight = 300
        self.timeSheetTable.rowHeight = UITableViewAutomaticDimension
        self.timeSheetTable.tableFooterView = UIView()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let model = viewModel {
            return model.timesheetArray?.count ?? 0
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:
            CellIdentifiers.TimesheetTableCellIdentifier, for: indexPath)
        if let cellIs = cell as? TimesheetTableCell {
            cellIs.selectionStyle = .none
            if let model = viewModel?.timesheetArray {
              cellIs.setData(data: model[indexPath.row])
            }
            
        }
        cell.selectionStyle = .none
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    
}
