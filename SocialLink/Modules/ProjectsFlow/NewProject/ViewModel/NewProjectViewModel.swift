//
//  NewProjectViewModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 16/08/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation

class NewProjectViewModel {
    
    init() {
    }
    
    var selectedEditProject: Project?
    
    func addProject(parameters: [String: Any],
                   onSuccess: @escaping (Project?) -> Void,
                   onError: @escaping APIErrorHandler) {
        AppDataManager.addProject(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: "Success")
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
    func editProject(parameters: [String: Any],
                    onSuccess: @escaping (Project?) -> Void,
                    onError: @escaping APIErrorHandler) {
        AppDataManager.editProject(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: "Success")
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
    func getFileName(path: String) -> String {
        let nameArray = path.components(separatedBy: "/")
        if nameArray.count > 0 {
            return nameArray.last ?? ""
        }
        return ""
    }
    
}
