//
//  OverViewViewModel.swift
//  SocialLink
//
//  Created by Harsha on 08/05/19.
//

import Foundation
class OverViewViewModel {
    init() {
        
    }
    func cancelProject(parameters: [String: Any],
                            onSuccess: @escaping (OTPModule?) -> Void,
                            onError: @escaping APIErrorHandler) {
        AppDataManager.cancelProject(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: "Success")
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
}
