//
//  CancelledProjectsCell.swift
//  SocialLink
//
//  Created by Harsha on 29/04/19.
//

import UIKit

class CancelledProjectsCell: UITableViewCell {
    @IBOutlet weak var projectNameLabel: UILabel!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var estimatedTimeLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    func setData(data: Project) {
        projectNameLabel.text = "\(String(describing: data.project_name ?? ""))"
        self.startDateLabel.text = data.start_date
        self.endDateLabel.text = data.endDate
       // estimatedTimeLabel.text = data.estimatedTime
        descriptionLabel.text = data.description
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
