//
//  ProjectNewTaskVC.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 09/05/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

protocol ProjectNewTaskVCDelegate: class {
    func reloadUpdatedRow(row: Int)
    func reloadRows()
    
}

class ProjectNewTaskVC: AppBaseVC {
    
    weak var delegate: ProjectNewTaskVCDelegate?
    @IBOutlet weak var newTaskTableView: UITableView!
    var taskType: String?
    var editTitleButton: UIBarButtonItem?
    var viewModel: NewTaskViewModel?
    var selectedTask: Task?
    var selectedRow: Int?
    var assignedTo: String?
    var uploadModel: FileUploadViewModel?
    var fileUploadData: Data?
    var fileExtension: String?
    var fileName: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = NewTaskViewModel()
         uploadModel = FileUploadViewModel()
        self.setTableViewDatasource()
        ////  self.createNavigationCloseItems()
        // Do any additional setup after loading the view.
        self.navigationItem.title = "New Task"
        self.createNavigationCloseItems()
        taskType = TASKTYPE.NEW
        
    }
 
    func addNewTask(data: [String: Any]) {
        
        if let model = viewModel {
            self.showHUD(title: "Loading...")
            model.addTask(parameters: data, onSuccess: { [weak self] (object) in
                dprint(object: "Success")
                print(object)
                self?.hideHud()
                
                }, onError: { (error) in
                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
                   // self.showAlert(title: Message.Alert, message: error.statusMessage)
                    self.hideHud()
            })
        }
    }

}

extension ProjectNewTaskVC {
    
    func createNavigationCloseItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension ProjectNewTaskVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.newTaskTableView.delegate = self
        self.newTaskTableView.dataSource = self
        self.newTaskTableView.estimatedRowHeight = 300
        self.newTaskTableView.rowHeight = UITableViewAutomaticDimension
        self.newTaskTableView.tableFooterView = UIView()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        case 1:
            return 1
        case 2:
            switch taskType {
            case TASKTYPE.NEW, TASKTYPE.EDIT:
                return 1
            default:
                return 0
            }
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        switch indexPath.section {
        case 0:
            switch taskType {
            case TASKTYPE.NEW:
                switch indexPath.row {
                case 0:
                    let cell = tableView.dequeueReusableCell(withIdentifier:
                        CellIdentifiers.ProjectNewTaskDateCellIdentifier, for: indexPath)
                    if let cellIs = cell as? ProjectNewTaskDateCell {
                        cellIs.selectionStyle = .none
                        cellIs.delegate = self
                    }
                    cell.selectionStyle = .none
                    return cell
                default:
                    return UITableViewCell()
                }
            case TASKTYPE.EDIT:
                switch indexPath.row {
                case 0:
                    let cell = tableView.dequeueReusableCell(withIdentifier:
                        CellIdentifiers.ProjectNewTaskDateCellIdentifier, for: indexPath)
                    if let cellIs = cell as? ProjectNewTaskDateCell {
                        cellIs.selectionStyle = .none
                        cellIs.delegate = self
                        switch taskType {
                        case TASKTYPE.EDIT:
                            if let task = selectedTask {
                                cellIs.setData(taskData: task)
                                self.assignedTo = task.assigned_to?.emailid ?? ""
                            }
                            if let assigned = selectedTask?.assigned_to {
                                cellIs.updateContact(contact: APPUtility.convertCreatedByToProjectContact(createdBy: assigned))
                            }
                            
                        default:
                            break
                        }
                    }
                    cell.selectionStyle = .none
                    return cell
                case 1:
                    let cell = tableView.dequeueReusableCell(withIdentifier:
                        CellIdentifiers.NewTaskUploadCellIdentifier, for: indexPath)
                    if let cellIs = cell as? NewTaskUploadCell {
                        cellIs.selectionStyle = .none
                        cellIs.delegate = self

                    }
                    cell.selectionStyle = .none
                    return cell
                default:
                    return UITableViewCell()
                }
            default:
                return UITableViewCell()
            }
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.TaskDescriptionCellIdentifier, for: indexPath)
            if let cellIs = cell as? TaskDescriptionCell {
                cellIs.selectionStyle = .none
                cellIs.delegate = self
                
                switch taskType {
                case TASKTYPE.EDIT:
                    if let task = selectedTask {
                        cellIs.setData(data: task.tdetails ?? "")
                    }
                default:
                    break
                }
                
                
            }
            cell.selectionStyle = .none
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.SaveDeleteNewTaskCellIdentifier, for: indexPath)
            if let cellIs = cell as? SaveDeleteNewTaskCell {
                cellIs.selectionStyle = .none
                cellIs.delegate = self
            }
            cell.selectionStyle = .none
            return cell
        default:
            return UITableViewCell()
            
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 3{
            
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "NewSubTaskVC") as? NewSubTaskVC {
                controller.setSubTaskType(type: SUBTASKTYPE.NEW)
                let navControl = APPNavigationController(rootViewController: controller)
                self.present(navControl, animated: true, completion: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 1:
           return 0
        case 2:
            return 0
        default:
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.appLightGrayColor
        let label = UILabel(frame: CGRect(x: 15, y: 0, width: 300, height: 30))
        label.font = UIFont.semiBoldFont(16)
        headerView.addSubview(label)
        switch section {
        case 1:
            label.text = "Task Description"
        case 2:
            label.text = "Sub Tasks"
        default:
            break
        }
        return headerView
    }
    
    /*func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     switch indexPath.section {
     case 0:
     return true
     default:
     return false
     }
     
     }
     func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
     
     
     let edit = UITableViewRowAction(style: .normal, title: "Edit") { action, index in
     
     }
     edit.backgroundColor = UIColor.lightGray
     
     let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
     print("more button tapped")
     }
     delete.backgroundColor = UIColor.darkGray
     
     return [delete, edit]
     
     }*/
    
    
    
    
}

extension ProjectNewTaskVC: ProjectNewTaskDateCellDelegate {
    func addParticipantClicked() {
        switch taskType {
        case TASKTYPE.NEW:
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProjectContactsVC") as? ProjectContactsVC {
                controller.delegate = self
                let navController = APPNavigationController(rootViewController: controller)
                
                self.present(navController, animated: true, completion: nil)
            }
        default:
            break
        }
    }
    
    func updateTextField(tag: Int, data: String) {
        if let model = viewModel {
            switch taskType {
            case TASKTYPE.NEW:
                if model.newTaskModel == nil {
                    model.newTaskModel = [String: Any]()
                }
                 model.newTaskModel!["taskName"] = data
            case TASKTYPE.EDIT:
                if model.tempEditTask == nil {
                    model.tempEditTask = [String: Any]()
                }
                model.tempEditTask!["taskName"] = data
            default:
                break
            }
        }
    }
    
    func startDateClicked(withTag: Int) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProjectCalenderVC") as? ProjectCalenderVC {
            let navController = APPNavigationController(rootViewController: controller)
            controller.delegate = self
            controller.controllerTag = 2
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    func endDateClicked(withTag: Int) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProjectCalenderVC") as? ProjectCalenderVC {
            let navController = APPNavigationController(rootViewController: controller)
            controller.delegate = self
            controller.controllerTag = 3
            self.present(navController, animated: true, completion: nil)
        }
    }

}
extension ProjectNewTaskVC: ProjectCalenderVCDelegate {
    func dateSelected(date: String, tag: Int, dateIs: Date) {
        print(date)
        print(tag)
        let utcDateString = APPUtility.zeroTimeString(date: date)
//        let utcDateString = APPUtility.convertDateToString(date: zeroDate.toGlobalTime())
//        print(utcDateString)
        var indexPath: IndexPath?
         indexPath = IndexPath(row: 0, section: 0)
        if let index = indexPath {
            let cell = self.newTaskTableView.cellForRow(at: index)
            if let cellIs = cell as? ProjectNewTaskDateCell {
                
                if let model = viewModel {
                    switch taskType {
                    case TASKTYPE.NEW:
                        if model.newTaskModel == nil {
                            model.newTaskModel = [String: Any]()
                        }
                        if tag == 2 {
                            cellIs.startDate(date: date)
                            model.newTaskModel!["startDate"] = utcDateString
                        } else if tag == 3 {
                            cellIs.endDate(date: date)
                            model.newTaskModel!["endDate"] = utcDateString
                        }
                    case TASKTYPE.EDIT:
                        if model.tempEditTask == nil {
                            model.tempEditTask = [String: Any]()
                        }
                        if tag == 2 {
                            cellIs.startDate(date: date)
                            model.tempEditTask!["startDate"] = utcDateString
                        } else if tag == 3 {
                            cellIs.endDate(date: date)
                            model.tempEditTask!["endDate"] = utcDateString
                        }
                    default:
                        break
                    }
                }
            }
        }
    }

}

extension ProjectNewTaskVC: TaskDescriptionCellDelegate {
    func descriptionSelected(description: String) {
        print(description)
        if let model = viewModel {
            switch taskType {
            case TASKTYPE.NEW:
                if model.newTaskModel == nil {
                    model.newTaskModel = [String: Any]()
                }
                model.newTaskModel!["description"] = description
            case TASKTYPE.EDIT:
                if model.tempEditTask == nil {
                    model.tempEditTask = [String: Any]()
                }
                model.tempEditTask!["description"] = description
            default:
                break
            }
        }
    }

}
extension ProjectNewTaskVC: SaveDeleteNewTaskCellDelegate {
    func saveClicked() {
        print("Save Clciked")
        if let model = viewModel {
            switch taskType {
            case TASKTYPE.NEW:
                if let taskModel = model.newTaskModel {
                    let taskName = taskModel["taskName"] as? String ?? ""
                    let startDate = taskModel["startDate"] as? String ?? ""
                    let endDate = taskModel["endDate"] as? String ?? ""
                    let description = taskModel["description"] as? String ?? ""
             
                    if taskName.checkForEmpty() {
                        self.showAlert(title: "Alert!", message: "Task name can't be empty")
                        return
                    } else if startDate.checkForEmpty() {
                        self.showAlert(title: "Alert!", message: "Start date can't be empty")
                        return
                    } else if endDate.checkForEmpty() {
                        self.showAlert(title: "Alert!", message: "End date can't be empty")
                        return
                    } else if !APPUtility.validateDates(start: startDate, end: endDate) {
                        self.showAlert(title: "Alert!", message: "End Date should not be Smaller than Start Date")
                        return
                    } else if (assignedTo ?? "").checkForEmpty() {
                        self.showAlert(title: "Alert!", message: "Assign Task to a Person")
                        return
                    } else if !APPUtility.validateEmail(enteredEmail: (assignedTo ?? "")) {
                        self.showAlert(title: "Alert!", message: "Enter a valid Email")
                        return
                    } else if description.checkForEmpty() {
                        self.showAlert(title: "Alert!", message: "Description can't be empty")
                        return
                    }
                    if let assigned = self.assignedTo {
                        let dataIs = ["token": APPStore.sharedStore.token,
                                      "tname": taskName,
                                      "tdetails": description,
                                      "tsdate": startDate,
                                      "tedate": endDate,
                                      "tassignto": assigned,
                                      "accept_state": ""] as [String : Any]
                        print(dataIs)
                        self.addNewTask(data: dataIs)
                    }
                
                    
                }
            case TASKTYPE.EDIT:
                if let taskModel = model.tempEditTask {
                    let taskName = taskModel["taskName"] as? String ?? ""
                    let startDate = taskModel["startDate"] as? String ?? ""
                    let endDate = taskModel["endDate"] as? String ?? ""
                    let description = taskModel["description"] as? String ?? ""
                    let state = taskModel["task_state"] as? Int64 ?? 0
                    

                    if let task = self.selectedTask {
                        let taskId = task.task_id
                    if taskName.checkForEmpty() {
                        self.showAlert(title: "Alert!", message: "Task name can't be empty")
                        return
                    } else if startDate.checkForEmpty() {
                        self.showAlert(title: "Alert!", message: "Start date can't be empty")
                        return
                    } else if endDate.checkForEmpty() {
                        self.showAlert(title: "Alert!", message: "End date can't be empty")
                        return
                    } else if !APPUtility.validateDates(start: startDate, end: endDate) {
                        self.showAlert(title: "Alert!", message: "End Date should not be Smaller than Start Date")
                        return
                    } else if (assignedTo ?? "").checkForEmpty() {
                        self.showAlert(title: "Alert!", message: "Assign Task to a Person")
                        return
                    } else if !APPUtility.validateEmail(enteredEmail: (assignedTo ?? "")) {
                        self.showAlert(title: "Alert!", message: "Enter a valid Email")
                        return
                    } else if description.checkForEmpty() {
                        self.showAlert(title: "Alert!", message: "Description can't be empty")
                        return
                    }
                        if let assigned = self.assignedTo {
                            let dataIs = ["token": APPStore.sharedStore.token,
                                          "task_id": taskId ?? 0,
                                          "tname": taskName,
                                          "tdetails": description,
                                          "tsdate": startDate,
                                          "tedate": endDate,
                                          "tassignto": assigned,
                                          "task_state": state] as [String : Any]
                            print(dataIs)
                           // self.editTask(data: dataIs)
                        }
                    }
                }
            default:
                break
            }

        }
        
    }
    
    func deleteClicked() {
        print("Delete Clciked")
    }
    
    
    
}
extension ProjectNewTaskVC: TaskStateSegmentCellDelegate {
    func stateSelected(state: Int64) {
        print(state)
        if let model = viewModel {
            if model.tempEditTask == nil {
                model.tempEditTask = [String: Any]()
            }
            model.tempEditTask!["state"] = state
        }
    }

    
}

extension ProjectNewTaskVC: ProjectContactsVCDelegate {
   
    
    func selectedContact(contact: ProjectContact) {
        let index = IndexPath(row: 0, section: 0)
        let cell = self.newTaskTableView.cellForRow(at: index)
        if let cellIs = cell as? ProjectNewTaskDateCell {
            cellIs.updateContact(contact: contact)
            self.assignedTo = contact.email_id ?? ""
        }
    }
    
}

extension ProjectNewTaskVC: NewTaskUploadCellDelegate, UIDocumentMenuDelegate,UIDocumentPickerDelegate {
    func updateTaskFile(taskId: Int64, withExtension: String) {
        if let model = uploadModel {
            self.showHUD(title: "")
            if let data = self.fileUploadData {
                let uploadData = ["token": APPStore.sharedStore.token,
                                  "upload_type": "task_file",
                                  "file_extension": withExtension,
                                  "id": taskId,
                                  "upload_time": APPUtility.getPresentDate(),
                                  "data": data,
                                  "fileName": self.fileName ?? ""] as [String : Any]
                model.requestWith(imageData: data, parameters: uploadData, endURL: "", onCompletion: { [weak self] (dataSource) in
                    self?.hideHud()
                    if let data = dataSource {
                        self?.dismiss(animated: true, completion: {
                            self?.delegate?.reloadRows()
                        })
                    }
                    }, onError: { (error) in
                        self.showAlert(title: "Alert", message: error?.localizedDescription ?? "")
                        self.hideHud()
                })
            }
        }
    }
    func chooseClicked() {
        let documentPicker: UIDocumentPickerViewController = UIDocumentPickerViewController(documentTypes: ["public.text", "com.apple.iwork.pages.pages", "public.data"], in: UIDocumentPickerMode.import)
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        self.present(documentPicker, animated: true, completion: nil)
    }
    
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let myURL = url as URL
        print("import result : \(myURL)")
        print(myURL.pathExtension)
        let fileData = try! Data(contentsOf: myURL)
        self.fileUploadData = fileData
        self.fileExtension = myURL.pathExtension
        if let model = viewModel {
            let fileNameIs = model.getFileName(path: myURL.absoluteString)
            self.fileName = fileNameIs
            let indexPath = IndexPath(row: 1, section: 0)
            let cell = self.newTaskTableView.cellForRow(at: indexPath)
            if let cellIs = cell as? NewTaskUploadCell {
                cellIs.setFileName(name: fileNameIs)
            }
        }
    }
    
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    
    
    
    
}
