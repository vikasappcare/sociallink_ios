//
//  MyTaskDetailNameCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 15/06/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class MyTaskDetailNameCell: UITableViewCell {

    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(data: [String: Any]) {
        
        headerLabel.text = data["header"] as? String
        descriptionLabel.text = data["description"] as? String
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
