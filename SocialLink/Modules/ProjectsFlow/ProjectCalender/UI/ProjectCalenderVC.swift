//
//  ProjectCalenderVC.swift
//  SocialLink
//
//  Created by Santhosh on 23/04/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit
import FSCalendar

struct Constraints {
    static let dateFormatString: String = "MMMM dd, yyyy"
    static let selectedDateFormat: String = Constant.globalDateFormatWithoutT
    static let EventTableViewCellIdentifier: String = "EventTableViewCellIdentifier"
    static let CalenderTodayCellIdentifier: String = "CalenderTodayCellIdentifier"
    static let CalenderViewCellIdentifier: String = "CalenderViewCellIdentifier"
}

protocol ProjectCalenderVCDelegate: class {
    func dateSelected(date: String, tag: Int, dateIs: Date)
}

class ProjectCalenderVC: UIViewController {
    
    weak var delegate: ProjectCalenderVCDelegate?
    @IBOutlet weak var eventAdd: UIButton!
    @IBOutlet weak var animationLabel: UILabel!
    @IBOutlet weak var calenderHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var monthButton: UIButton!
    @IBOutlet weak var weekButton: UIButton!
    @IBOutlet weak var todayView: UIView!
    @IBOutlet weak var eventsTableView: UITableView!
    var controllerTag = 0
    
    let chooseArticleDropDown: POSDropDown = POSDropDown()
    
    @IBOutlet weak var monthSelected: UIButton!
    @IBOutlet weak var dateSelected: UIButton!
    @IBOutlet weak var yearSelected: UIButton!
    
    
    @IBOutlet weak var calenderView: FSCalendar!
    fileprivate let gregorian: NSCalendar! = NSCalendar(calendarIdentifier:NSCalendar.Identifier.gregorian)
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        let origImage = closeButton.backgroundImage(for: .normal)
        //        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        //        closeButton.setImage(tintedImage, for: .normal)
        //        closeButton.tintColor = .red
        
        calenderView.delegate = self
        calenderView.dataSource =  self
        self.calenderView.appearance.borderRadius = 0
        
        self.view.addGestureRecognizer(self.scopeGesture)
        self.eventsTableView.panGestureRecognizer.require(toFail: self.scopeGesture)
        self.calenderView.scope = .week
        self.populateDateFromSelectedDate(date: Date())
        self.setAnimationLabel()
        self.setTableview()
        
       self.navigationItem.title = "CALENDER"
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if  self.navigationController?.navigationBar.backItem == nil {
            self.createNavigationItems()
        }
    }
    
    
    func setAnimationLabel() {
        let scope = self.calenderView.scope
        
        
        switch scope {
        case .month:
            UIView.animate(withDuration: 0.2, animations: {
                self.animationLabel.frame = CGRect(x: self.monthButton.frame.origin.x, y: (self.monthButton.frame.origin.y + self.monthButton.frame.size.height) - 5, width: self.monthButton.frame.size.width, height: 2)
            }, completion:{ _ in
            })
            
        case .week:
            UIView.animate(withDuration: 0.2, animations: {
                self.animationLabel.frame = CGRect(x: self.weekButton.frame.origin.x, y: (self.weekButton.frame.origin.y + self.weekButton.frame.size.height) - 5, width: self.weekButton.frame.size.width, height: 2)
            }, completion:{ _ in
            })
        }
        
        
        
        
        
        
    }
    
    @IBAction func eventAddClicked(_ sender: Any) {
        
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "NewEventVC") as? NewEventVC {
            let navigationController = APPNavigationController.init(rootViewController: controller)
            self.present(navigationController, animated: true, completion: nil)
        }
        
        
    }
    
    
    
    func setupChooseArticleDropDown(button: UIButton, data: [String], tag: Int) {
        chooseArticleDropDown.anchorView = button
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: button.bounds.height)
        chooseArticleDropDown.dataSource = data
        chooseArticleDropDown.selectionAction = { (index, item) in
            button.setTitle(item, for: .normal)
            self.setDateFromString()
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = Constraints.dateFormatString
        return formatter
    }()
    
    fileprivate let selectedFormat: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = Constraints.selectedDateFormat
        return formatter
    }()
    
    fileprivate lazy var scopeGesture: UIPanGestureRecognizer = {
        [unowned self] in
        let panGesture = UIPanGestureRecognizer(target: self.calenderView, action: #selector(self.calenderView.handleScopeGesture(_:)))
        panGesture.delegate = self
        panGesture.minimumNumberOfTouches = 1
        panGesture.maximumNumberOfTouches = 2
        return panGesture
        }()
    
    @IBAction func changeMonthClicked(_ sender: Any) {
        self.setupChooseArticleDropDown(button: monthSelected, data: ["January",
                                                                      "February",
                                                                      "March",
                                                                      "April",
                                                                      "May",
                                                                      "June",
                                                                      "July",
                                                                      "August",
                                                                      "September",
                                                                      "October",
                                                                      "November",
                                                                      "December"], tag: 1)
        self.chooseArticleDropDown.show()
    }
    @IBAction func changeDateClicked(_ sender: Any) {
    }
    @IBAction func changeYearClicked(_ sender: Any) {
        var yearsArray: [String] = []
        
        let currentYear = self.gregorian.component(.year, from: Date())
        
        
        for i in (1970 ..< currentYear + 100) {
            yearsArray.append("\(i)")
        }
        
        
        
        
        self.setupChooseArticleDropDown(button: yearSelected, data: yearsArray, tag: 3)
        self.chooseArticleDropDown.show()
        
        let indexOfYear = yearsArray.index{$0 == String(currentYear)} // 0
        let indexPath = IndexPath(item: indexOfYear ?? 0, section: 0)
        self.chooseArticleDropDown.scrollToRow(index: indexPath)
    }
    
}

extension ProjectCalenderVC {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
 
}

extension ProjectCalenderVC {
    
    func populateDateFromSelectedDate(date: Date) {
        let month = self.gregorian.component(.month, from: date)
        let selectedMonth = DateFormatter().monthSymbols[month - 1]
        let selectedDate = self.gregorian.component(.day, from: date)
        let selectedYear = self.gregorian.component(.year, from: date)
        
        monthSelected.setTitle(selectedMonth, for: .normal)
        dateSelected.setTitle("\(selectedDate),", for: .normal)
        yearSelected.setTitle("\(selectedYear)", for: .normal)
    }
    
    @IBAction func todayClicked(_ sender: Any) {
        self.populateDateFromSelectedDate(date: Date())
        self.calenderView.setCurrentPage(Date(), animated: true)
        for date in self.calenderView.selectedDates {
            calenderView.deselect(date)
        }
    }
    @IBAction func weekClicked(_ sender: Any) {
        let  scope = FSCalendarScope.week
        self.calenderView.setScope(scope, animated: true)
        self.setAnimationLabel()
    }
    @IBAction func monthClicked(_ sender: Any) {
        let  scope = FSCalendarScope.month
        self.calenderView.setScope(scope, animated: true)
        self.setAnimationLabel()
    }
    
    func getTodayDate() -> String {
        let date = Date()
        let result = self.formatter.string(from: date)
        return result
    }
    
    func convertDateToString(date: Date) -> String {
        let result = self.formatter.string(from: date)
        return result
    }
    
}


extension ProjectCalenderVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableview() {
        self.eventsTableView.tableFooterView = UIView()
        self.eventsTableView.delegate = self
        self.eventsTableView.dataSource = self
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Constraints.EventTableViewCellIdentifier, for: indexPath) as UITableViewCell
        if let cellIs = cell as? EventTableViewCell {
            cellIs.selectionStyle = .none
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    
    
    
    
}
extension ProjectCalenderVC: FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance, UIGestureRecognizerDelegate {
    
    
    
    // MARK:- FSCalendarDelegate
    
    
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        print("change page to \(self.formatter.string(from: calendar.currentPage))")
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        let shouldBegin = self.eventsTableView.contentOffset.y <= -self.eventsTableView.contentInset.top
        if shouldBegin {
            let velocity = self.scopeGesture.velocity(in: self.view)
            switch self.calenderView.scope {
            case .month:
                return velocity.y < 0
            case .week:
                return velocity.y > 0
            }
        }
        return shouldBegin
    }
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calenderHeightConstraint.constant = bounds.height
        self.view.layoutIfNeeded()
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print("calendar did select date \(self.formatter.string(from: date))")
        self.populateDateFromSelectedDate(date: date)
        
        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: true)
        }
        self.delegate?.dateSelected(date: "\(self.selectedFormat.string(from: date))", tag: controllerTag, dateIs: date)
        print(self.selectedFormat.string(from: date))
        self.dismiss(animated: true, completion: nil)
        ////////////// Return Delegate
        
    }
    //    func maximumDate(for calendar: FSCalendar) -> Date {
    //        return self.formatter.date(from: "October 30, 2018")!
    //    }
    
    //    func minimumDate(for calendar: FSCalendar) -> Date {
    //        return Date()
    //    }
    
    func calendar(_ calendar: FSCalendar, imageFor date: Date) -> UIImage? {
        let day: Int! = self.gregorian.component(.day, from: date)
        return [10,11,12].contains(day) ? UIImage(named: "Event") : nil
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, imageOffsetFor date: Date) -> CGPoint {
        return CGPoint(x: 13, y: -33)
    }
    
}




extension ProjectCalenderVC {
    
    func setDateFromString() {
        let day = dateSelected.currentTitle ?? ""
        let month = monthSelected.currentTitle ?? ""
        let year = yearSelected.currentTitle ?? ""
        let dateString = "\(month) \(day) \(year)"
        let dateIs = self.formatter.date(from: dateString) ?? Date()
        self.calenderView.setCurrentPage(dateIs, animated: true)
        self.calenderView.select(dateIs)
    }
    
}



