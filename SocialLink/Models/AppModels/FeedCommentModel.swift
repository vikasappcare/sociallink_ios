//
//  FeedCommentModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 11/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON


class FeedCommentModel: AppBaseModel {
    
    var service: String?
    var count: Int64?
    var comments: [CommentedUsers]?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    
    private func parseResponse(with response: JSON) {
        self.service = response["service"].stringValue
        self.count = response["count"].int64Value
        for item in response.arrayValue {
            if let model = self.createObject(with: item) {
                if self.comments == nil {
                    self.comments = [CommentedUsers]()
                }
                self.comments?.append(model)
            }
        }
    }
    
    private func createObject(with jsonObject: JSON) -> CommentedUsers? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return CommentedUsers.init(jsonObject)
        }
    }
    
}


class CommentedUsers: AppBaseModel {
    
    var comment_id: Int64?
    var parent_comment_id: Int64?
    var user_id: Int64?
    var name: String?
    var image: String?
    var feed_id: Int64?
    var comment: String?
    var comment_time: String?
    var date: String?

    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        
        self.comment_id = response["comment_id"].int64Value
        self.user_id = response["user_id"].int64Value
        self.name = response["name"].stringValue
        self.image = response["uphoto"].stringValue
        self.feed_id = response["feed_id"].int64Value
        self.comment = response["comment"].stringValue
        self.comment_time = response["comment_time"].stringValue
        self.parent_comment_id = response["parent_comment_id"].int64Value
        self.date = response["date"].stringValue

    }
}
