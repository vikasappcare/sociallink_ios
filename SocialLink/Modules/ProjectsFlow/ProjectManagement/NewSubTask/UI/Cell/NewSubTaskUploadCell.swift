//
//  NewSubTaskUploadCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/05/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class NewSubTaskUploadCell: UITableViewCell {

    @IBOutlet weak var backView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        backView.layer.borderWidth = 1.0
        backView.layer.borderColor = UIColor.appDarkGrayColor.cgColor
        backView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
