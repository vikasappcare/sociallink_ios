//
//  ProfileCompanyCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 27/02/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit
protocol ProfileCompanyCellDelegate: class {
    func textChanged(data: String, tag: Int)
}
class ProfileCompanyCell: UITableViewCell {
weak var delegate: ProfileCompanyCellDelegate?
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var companyText: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        companyText.delegate = self
    }
    
    func setData(data: String?, tag: Int) {
        companyText.tag = tag
        self.headerLabel.text = data ?? ""
         let lowercase = (data ?? "").lowercased()
        companyText.text = "Enter \(lowercase)"
        companyText.textColor = UIColor.lightGray
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension ProfileCompanyCell: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "about your company"
            textView.textColor = UIColor.lightGray
            self.delegate?.textChanged(data: textView.text ?? "", tag: textView.tag)
        }
    }
    
    
    
}
