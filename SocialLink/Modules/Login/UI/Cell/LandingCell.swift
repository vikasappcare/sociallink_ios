//
//  LandingCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 26/02/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

protocol LandingCellDelegate: class {
    func newProjectClicked()
    func signSelected()
    func inviteSelected()
    func joinUsClicked()
}

class LandingCell: UITableViewCell {

    weak var delegate: LandingCellDelegate?
    @IBOutlet weak var newProjectButton: UIButton!
    @IBOutlet weak var joinTeamButton: UIButton!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var inviteCodeButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func createNewProjectAction(_ sender: Any) {
        self.delegate?.newProjectClicked()
    }
    @IBAction func joinTeamButton(_ sender: Any) {
        self.delegate?.joinUsClicked()
    }
    @IBAction func signinClicked(_ sender: Any) {
        self.delegate?.signSelected()
    }
    @IBAction func inviteAction(_ sender: Any) {
        self.delegate?.inviteSelected()
    }
}

extension LandingCell {
    
    
    
    
    
}
