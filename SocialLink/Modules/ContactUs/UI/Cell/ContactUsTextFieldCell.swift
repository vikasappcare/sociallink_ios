//
//  ContactUsTextFieldCell.swift
//  SocialLink
//
//  Created by Santhosh on 10/04/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ContactUsTextFieldCell: UITableViewCell {

    @IBOutlet weak var dataTextField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(data: String) {
        ////
        
        dataTextField.placeholder = data 
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
