//
//  APPTabbarController.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 12/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class APPTabbarController: UITabBarController {
    


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        UITabBar.appearance().tintColor = UIColor.appBlueColor
        
        self.view.backgroundColor = UIColor.white
        
        
       /// self.tabBarController?.delegate = self
        
        
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(switchTabbars),
                                                   name:
                NSNotification.Name(rawValue: APPNotifications.switchTabbar),
                                                   object: nil)
       

        
        
        self.createTabbarItems()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
     @objc func  switchTabbars(info: NSNotification) {
         self.createTabbarItems()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
//        if  let arrayOfTabBarItems = self.tabBar.items as AnyObject as? NSArray, let tabBarItem = arrayOfTabBarItems[2] as? UITabBarItem {
//            tabBarItem.isEnabled = false
//        }
    }
    
//    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
//        if viewController.title == "AddViewController" {
//            return false
//        } else {
//            return true
//        }
//    }

    
    func createTabbarItems() {
         let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        /////////PROJECTS FEED
        var projectFeedNav: APPNavigationController?
        let prjFeedController = mainStoryboard.instantiateViewController(withIdentifier: "ProjectFeedVC")
        if let projectFeedController = prjFeedController as? ProjectFeedVC {
            projectFeedController.tabBarItem = UITabBarItem(title: "Home", image: UIImage(named: "TabProjectFeed"), tag: 1)
            projectFeedController.setProjectId(projectId: 0)
            projectFeedController.setProjectFeedType(type: ProjectFeedType.Common)
          //  projectFeedController.tabBarItem.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: -6, right: 0)
            projectFeedNav = APPNavigationController(rootViewController: projectFeedController)
        }
        
        /////////PROJECTS
        var projectNav: APPNavigationController?
        let prjController = mainStoryboard.instantiateViewController(withIdentifier: "ProjectsViewController")
        if let projectController = prjController as? ProjectsViewController {
            projectController.tabBarItem = UITabBarItem(title: "Projects", image: UIImage(named: "TabProject"), tag: 2)
            //projectController.tabBarItem.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: -6, right: 0)
            projectNav = APPNavigationController(rootViewController: projectController)
        }
        
        /////////CONTACTS
        var contactsNav: APPNavigationController?
        let cntController = mainStoryboard.instantiateViewController(withIdentifier: "ContactsViewController")
        if let contactController = cntController as? ContactsViewController {
            contactController.showCloseButton(show: false, forScreen: "")
            contactController.tabBarItem = UITabBarItem(title: "Messenger", image: UIImage(named: "TabContact"), tag: 4)
            //contactController.tabBarItem.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: -6, right: 0)
            contactsNav = APPNavigationController(rootViewController: contactController)
        }
        
        var addNav: APPNavigationController?
        print("User is :",APPStore.sharedStore.appUserID)
        print(APPStore.sharedStore.user.user_type)
        switch APPStore.sharedStore.appUserID {
        case "2":
            /////////ADD Creatives NearBy
            let addCntrller = mainStoryboard.instantiateViewController(withIdentifier: "LandingPageViewController")
            if let addController = addCntrller as? LandingPageViewController {
                addController.tabBarItem = UITabBarItem(title: "Creatives Nearby", image: UIImage(named: "tabCreatives"), tag: 3)
                // addController.tabBarItem.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: -6, right: 0)
                addNav = APPNavigationController(rootViewController: addController)
            }
        case "1":
            /////////ADD Timesheets
            let addCntrller = mainStoryboard.instantiateViewController(withIdentifier: "MyTaskContoller")
            if let addController = addCntrller as? MyTasksViewController {
                addController.tabBarItem = UITabBarItem(title: "My Tasks", image: UIImage(named: "TabTasks"), tag: 3)
                // addController.tabBarItem.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: -6, right: 0)
                addNav = APPNavigationController(rootViewController: addController)
            }
        default:
            break
        }

        
        /////////MORE
        var moreNav: APPNavigationController?
        let moreCntrl = mainStoryboard.instantiateViewController(withIdentifier: "MoreViewController")
        if let moreController = moreCntrl as? MoreViewController {
           moreController.tabBarItem = UITabBarItem(title: "More", image: UIImage(named: "TabMore"), tag: 5)
           // moreController.tabBarItem.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: -6, right: 0)
            moreNav = APPNavigationController(rootViewController: moreController)
        }

        self.viewControllers = [projectFeedNav!, projectNav!, addNav!, contactsNav!, moreNav!]
        
    }
    
    func setupAddButton() {
        let menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        ///var menuButtonFrame = menuButton.frame
//        if (DeviceInfo.sharedStore.device == .simulator(.iPhoneX)) || (DeviceInfo.sharedStore.device == .iPhoneX) {
//            menuButtonFrame.origin.y = self.view.bounds.height - 82
//        } else {
//             menuButtonFrame.origin.y = self.view.bounds.height - menuButtonFrame.height
//        }
//
//
//        menuButtonFrame.origin.x = self.view.bounds.width / 2 - menuButtonFrame.size.width / 2
//        menuButton.frame = menuButtonFrame
        //menuButton.layer.cornerRadius = menuButtonFrame.height/2

        
        menuButton.center = self.tabBar.center
        
        let device = DeviceInfo.sharedStore.device.name
        print(device)
        
        print(menuButton.frame)
        
        menuButton.backgroundColor = UIColor.clear
        self.view.addSubview(menuButton)
        
        menuButton.setImage(UIImage(named: "TabAdd"), for: UIControlState.normal)
        menuButton.addTarget(self, action: #selector(addProjectButtonAction), for: UIControlEvents.touchUpInside)
        
        self.view.layoutIfNeeded()
    }
    
    @objc func addProjectButtonAction(sender: UIButton) {
        switch self.selectedIndex {
        case 0:
            NotificationCenter.default.post(name: Notification.Name(APPNotifications.plusNewsFeed), object: nil)
        case 1:
            NotificationCenter.default.post(name: Notification.Name(APPNotifications.plusProjects), object: nil)
        case 3:
            NotificationCenter.default.post(name: Notification.Name(APPNotifications.plusContacts), object: nil)
        case 4:
            NotificationCenter.default.post(name: Notification.Name(APPNotifications.plusMore), object: nil)
        default:
            break
        }

    }

}
