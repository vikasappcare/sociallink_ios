//
//  LoginPasswordCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 13/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit
protocol LoginPasswordCellDelegate: class {
   
    func submitClicked()
    func passwordForgetClicked()
    func passwordRegisterClicked()
    
}


class LoginPasswordCell: UITableViewCell {
    weak var delegate: LoginPasswordCellDelegate?
    @IBOutlet weak var profileImage: AppRoundImage!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var designation: UILabel!
    @IBOutlet weak var passwordText: AppTextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var checkBox: UIButton!
    @IBOutlet weak var forgetPassword: UIButton!
    @IBOutlet weak var createAccount: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        ///self.passwordText.text = "123456"
        
    }
    
    func getData() -> [String: Any]? {
        let dataIs = ["password": passwordText.text ?? ""]
        return dataIs
    }
    
    func setData(data: SignUpModel) {
        
        if let profileIs = data.profile {
            profileImage.sd_setImage(with: URL(string: profileIs[0].image ?? ""), placeholderImage: #imageLiteral(resourceName: "SampleProfile"))

            profileName.text = profileIs[0].full_name
            designation.text = profileIs[0].designation
        }
        
        
        

    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func submitClicked(_ sender: Any) {
        self.delegate?.submitClicked()
    }
    @IBAction func forgetPasswordClicked(_ sender: Any) {
        self.delegate?.passwordForgetClicked()
    }
    @IBAction func checkBoxClicked(_ sender: Any) {
    }
    
    @IBAction func createAccountClciked(_ sender: Any) {
        self.delegate?.passwordRegisterClicked()
    }
}
