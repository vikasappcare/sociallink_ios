//
//  NewMileStoneCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 09/05/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

protocol NewMileStoneCellDelegate: class {
    func startDateClicked(withTag: Int)
    func endDateClicked(withTag: Int)
}
class NewMileStoneCell: UITableViewCell {
    weak var delegate: NewMileStoneCellDelegate?
    @IBOutlet weak var milestoneProgress: UIProgressView!
    @IBOutlet weak var mileStoneText: AppRoundBorderTextField!
    @IBOutlet weak var endDateButton: UIButton!
    @IBOutlet weak var startDateButton: UIButton!
    @IBOutlet weak var startDateText: AppRoundBorderTextField!
    @IBOutlet weak var endDateText: AppRoundBorderTextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        milestoneProgress.layer.cornerRadius = 5.0
        milestoneProgress.layer.masksToBounds = true
        
        
    }
    @IBAction func startDateClicked(_ sender: UIButton) {
        self.delegate?.startDateClicked(withTag: sender.tag)
    }
    
    @IBAction func endDateClicked(_ sender: UIButton) {
        self.delegate?.endDateClicked(withTag: sender.tag)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
