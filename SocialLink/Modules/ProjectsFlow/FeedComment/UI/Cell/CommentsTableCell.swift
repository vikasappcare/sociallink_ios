//
//  CommentsTableCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 11/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class CommentsTableCell: UITableViewCell {

    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2
        profileImage.layer.masksToBounds = true
    }
    
    func setData(data: CommentedUsers) {
        comment.text = data.comment ??  ""
        profileName.text = data.name ?? ""
        let userProfileImage = APPURL.feedUserProfileImage  + data.image!
        profileImage.sd_setImage(with: URL(string: userProfileImage), placeholderImage: UIImage(named: ""))
       // timeLabel.text = timeAgoSince(APPUtility.convertUTCToClaenderFormatDate(date: data.comment_time ?? ""))

        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
