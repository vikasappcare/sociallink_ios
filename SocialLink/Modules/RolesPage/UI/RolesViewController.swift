//
//  RolesViewController.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 29/08/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit
protocol RolesViewControllerDelegate: class {
    func roleSelectted(role: Role)
    func serviceSelectted(service: Service)
}
class RolesViewController: AppBaseVC {

    weak var delegate: RolesViewControllerDelegate?
    @IBOutlet weak var rolesListTable: UITableView!
    var rolesArray: [Role]?
    var viewModel: RolesViewModel?
    var servicesArray: [Service]?
    var serviceType: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = RolesViewModel()
        switch serviceType {
        case ScreenType.ROLE:
            if rolesArray == nil {
                rolesArray = [Role]()
            }
            self.navigationItem.title = "Select Role"
            self.updateRoleData()
        case ScreenType.SERVICES:
            if servicesArray == nil {
                servicesArray = [Service]()
            }
            self.navigationItem.title = "Select Services"
            self.updateServiceData()
        default:
            break
        }
        
        self.setTableViewDatasource()
        self.createNavigationItems()
        // Do any additional setup after loading the view.
       /// self.getRoles()
        
    }
    
    func setServiceType(type: String) {
        self.serviceType = type
    }
    
    func updateRoleData() {
        APPUtility.getRoles()
        rolesArray = APPStore.sharedStore.roles
    }
    
    func updateServiceData() {
        APPUtility.getServices()
        servicesArray = APPStore.sharedStore.services
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        switch serviceType {
        case ScreenType.ROLE:
             self.getRoles()
        case ScreenType.SERVICES:
            self.getServices()
        default:
            break
        }
        
        
       
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func getRoles() {
        if let model = viewModel {
            let parms = ["token": APPStore.sharedStore.token] as [String : Any]
            self.showHUD(title: "")
            model.getRolesList(parameters: parms, onSuccess: { [weak self] (dataSource) in
                print(dataSource ?? "")
                self?.rolesArray?.removeAll()
                self?.updateRoleData()
                self?.rolesListTable.reloadData()
                self?.hideHud()
                }, onError: { (error) in
                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
                    self.hideHud()
            })
        }
        
        
    }
    
    func getServices() {
        if let model = viewModel {
            let parms = ["token": APPStore.sharedStore.token] as [String : Any]
            self.showHUD(title: "")
            model.getServiceList(parameters: parms, onSuccess: { [weak self] (dataSource) in
                print(dataSource ?? "")
                self?.servicesArray?.removeAll()
                self?.updateServiceData()
                self?.rolesListTable.reloadData()
                self?.hideHud()
                }, onError: { (error) in
                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
                    self.hideHud()
            })
        }
        
        
    }

}

extension RolesViewController {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
    
}


extension RolesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.rolesListTable.delegate = self
        self.rolesListTable.dataSource = self
        self.rolesListTable.estimatedRowHeight = 300
        self.rolesListTable.rowHeight = UITableViewAutomaticDimension
        self.rolesListTable.tableFooterView = UIView()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch serviceType {
        case ScreenType.ROLE:
            return self.rolesArray?.count ?? 0
        case ScreenType.SERVICES:
            return self.servicesArray?.count ?? 0
        default:
            return 0
        }
        
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:
            CellIdentifiers.RoleTableViewCellIdentifier, for: indexPath)
        if let cellIs = cell as? RoleTableViewCell {
            cellIs.selectionStyle = .none
            switch serviceType {
            case ScreenType.ROLE:
                let role = self.rolesArray![indexPath.row]
                cellIs.setData(data: role)
            case ScreenType.SERVICES:
                let service = self.servicesArray![indexPath.row]
                cellIs.setServiceData(data: service)
            default:
                break
            }
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.001, execute: {
            switch self.serviceType {
            case ScreenType.ROLE:
                let selectedRole = self.rolesArray![indexPath.row]
                self.delegate?.roleSelectted(role: selectedRole)
            case ScreenType.SERVICES:
                let selectedService = self.servicesArray![indexPath.row]
                self.delegate?.serviceSelectted(service: selectedService)
            default:
                break
            }
            
            self.dismiss(animated: true, completion: nil)
        })
    }
    
}

