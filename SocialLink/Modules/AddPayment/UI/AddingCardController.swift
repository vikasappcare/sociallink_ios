//
//  AddingCardController.swift
//  SocialLink
//
//  Created by Banda Anil Reddy on 26/02/19.
//  Copyright © 2019 Santhosh Marripelli. All rights reserved.
//

import UIKit
import FormTextField

class AddingCardController: AppBaseVC {

    var viewModel: AddingPaymentViewModel?

    @IBOutlet weak var nameOnCardTextField: UITextField!
    
    @IBOutlet weak var cardnumberTextField: FormTextField!
    @IBOutlet weak var expdatetextField: FormTextField!
    
    @IBOutlet weak var cvvTextField: FormTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        addNotification()
        validationsForTextFields()
        viewModel = AddingPaymentViewModel()

    }

    func validationsForTextFields()
    {
        //card number validation
        cardnumberTextField.inputType = .integer
        cardnumberTextField.formatter = CardNumberFormatter()

        var cardNumberValidation = Validation()
        cardNumberValidation.maximumLength = "1234 5678 1234 5678".count
        cardNumberValidation.minimumLength = "1234 5678 1234 5678".count
        let characterSet = NSMutableCharacterSet.decimalDigit()
        characterSet.addCharacters(in: " ")
        cardNumberValidation.characterSet = characterSet as CharacterSet
        let inputValidatorForCardNumber = InputValidator(validation: cardNumberValidation)
        cardnumberTextField.inputValidator = inputValidatorForCardNumber


        expdatetextField.inputType = .integer
        expdatetextField.formatter = CardExpirationDateFormatter()

        var expiryDateValidatore = Validation()
        expiryDateValidatore.minimumLength = 1
        let inputValidatorForExpDate = CardExpirationDateInputValidator(validation: expiryDateValidatore)
        expdatetextField.inputValidator = inputValidatorForExpDate

        cvvTextField.inputType = .integer

        var cvvValidation = Validation()
        cvvValidation.maximumLength = "CVC".count
        cvvValidation.minimumLength = "CVC".count
        cvvValidation.characterSet = NSCharacterSet.decimalDigits
        let inputValidatorForCVV = InputValidator(validation: cvvValidation)
        cvvTextField.inputValidator = inputValidatorForCVV
    }
    @IBAction func authoriseButtonClicked(_ sender: Any) {
        if (nameOnCardTextField.text?.isEmpty ?? true) || (nameOnCardTextField.text == "") {
            self.showAlert(title: "Alert!", message: "Please enter Name on card to continue")
            return
        }
        if (cardnumberTextField.text?.isEmpty ?? true) || (cardnumberTextField.text == "") {
            self.showAlert(title: "Alert!", message: "Please enter Card Number to continue")
            return
        }
        if (expdatetextField.text?.isEmpty ?? true) || (expdatetextField.text == "") {
            self.showAlert(title: "Alert!", message: "Please enter Expiry Date to continue")
            return
        }
        if (cvvTextField.text?.isEmpty ?? true) || (cvvTextField.text == "") {
            self.showAlert(title: "Alert!", message: "Please enter Expiry date to continue")
            return
        }

        let validCardNumber = cardnumberTextField.validate()
        let validCardExpirationDate = expdatetextField.validate()
        let validCVC = cvvTextField.validate()

        if validCardNumber && validCardExpirationDate && validCVC {

            if let userID = APPStore.sharedStore.user.userId{
                if let model = viewModel {
                    let cardMonth = expdatetextField.text?.prefix(2)
                    let cardYear = expdatetextField.text?.suffix(2)

                    let param = ["card_number": cardnumberTextField.text!, "card_month": cardMonth!, "card_year": cardYear!, "card_cvv": cvvTextField.text!, "user_id": userID] as [String : Any]
                    print(param)
                    self.showHUD(title: "Loading...")
                    model.addingCardDetails(parameters: param as [String : Any], onSuccess: { [weak self] (dataSource) in
                        self?.hideHud()
                        self?.moveToHome()
                        },  onError: { (error) in
                            Logger.log(message: "Error \(error.statusMessage)", event: .error)
                           // self.showAlert(title: Message.Alert, message: error.statusMessage)
                            self.hideHud()
                    })
                }
            }
        }
        else
        {
            self.showAlert(title: Message.Alert, message: "Entered details are not valid")
        }

    }

    func moveToHome()
    {
        navigationController?.popViewController(animated: true)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue:
            APPNotifications.dismissLoginView),
                                        object: nil, userInfo: nil)

    }

    @IBAction func saveForLater(_ sender: Any) {
        
       moveToHome()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddingCardController {
    
    func addNotification() {
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(dismissView),
                                               name:
            NSNotification.Name(rawValue: APPNotifications.dismissLoginView),
                                               object: nil)
    }
    
    @objc func  dismissView(info: NSNotification) {
        self.dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue:
            APPNotifications.changeRootView),
                                        object: nil, userInfo: nil)
    }
}
