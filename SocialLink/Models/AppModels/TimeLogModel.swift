//
//  TimeLogModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 17/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON

class TimeLogModel: AppBaseModel {
    
    var service: String?
    var data: [TimeLog]?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    
    private func parseResponse(with response: JSON) {
        self.service = response["service"].stringValue
        for item in response["Data"].arrayValue {
            if let model = self.createObject(with: item) {
                if self.data == nil {
                    self.data = [TimeLog]()
                }
                self.data?.append(model)
            }
        }
    }
    
    private func createObject(with jsonObject: JSON) -> TimeLog? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return TimeLog.init(jsonObject)
        }
    }
    
}




