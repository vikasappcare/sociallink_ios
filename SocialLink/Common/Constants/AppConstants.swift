//
//  AppConstants.swift
//  BaseProject
//
//  Created by Santhosh Marripelli on 22/02/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation

// MARK: - Project Environment Type
struct Environment {
    static let Local: String = "Local"
    static let Production: String = "Production"
}
enum StripeConstants {
    static let publishableKey = "pk_test_rqpMo5hgfk4hxQsGKDLAlwB2"
    static let baseURLString = "https://app.sociallink.com/stripe_pay.php"
    static let defaultCurrency = "usd"
    static let defaultDescription = "Paying"
}
struct LoggedInUser {
    static let Contractor: String = "Contractor"
    static let Employer: String = "Employer"
}
struct Slack {
    static let Client_ID: String = "660557450150.666938267717"
    static let Client_Secret: String = "a83d2b9772282979215a37ea69037db6"
    static var AuthToken: String = ""
    static var IsSlack: Bool = false
    static var ChannelId: String = ""
    static var UserId: String = ""

}

struct NetworkMessage {
    static let ErrorTitle: String = "No Network..!"
    static let ErrorMessage: String = "Please check your internet connection"
    
    static let SuccessTitle: String = "Connected to Network..!"
    static let SuccessMessage: String = "You have connected to active network"
}

// MARK: - App URL's
struct APPURL {
    static let SampleURL: String = "mralexgray/repos"
    static let NetworkURL: String = "www.apple.com"
    static let LoginURL: String = "Register/login"
    static let SkillsURL: String = "Register/skills"
    static let RegisterURL: String = "Register/register"
    static let gmailURL: String = "Register/social_login"
    static let BanksListURL: String = "Register/banks"
    static let BanksDetailsURL: String = "Register/bank_details"
    static let CardDetailsURL: String = "Register/card_details"
    static let CreativesNearByURL: String = "Create_near_by/Create_near_by"
    static let validateEmail: String = "Register/Forget_password"
    static let invite: String = "Project_management/create_near_by_invite"

    static let postComment: String = "Feed/comments"
    static let getComment: String = "Feed/get_comments"
    static let postLike: String = "Feed/like"
    static let getLike: String = "Feed/get_likes"
    static let updatePassword: String = "New-Password"
    static let addProject: String = "Project_create/create_project"

    static let projectAttachments: String = "Project_management/projects_attachment_upload"
    static let editProject: String = "Project_management/edit_project"
    static let getProject: String = "Get-Projects"
    static let getPendingProject: String = "Project_management/pending_projects"
    static let getCurrentProject: String = "Project_management/current_projects"
    static let getCompletedProject: String = "Project_management/completed_projects"
    static let getEstimatedProject: String = "Project_estimates/Project_estimates"
    static let getCancelledProject: String = "Project_management/cancel_projects"
    static let cancelledProject: String = "Project_management/cancel_project"

    static let getFeed: String = "Feed/get_feed"
    static let getProjectFeed: String = "Project_management/get_projects_feed"
    static let postFeed: String = "Feed/feed"
    static let profilePicUpdate: String = "Register/profile_photo_update"
    static let profileUpdate: String = "Register/profile_update"
    static let getChannels: String = "Chat_slack/channel_list"
    static let getAttachments: String = "Project_management/projects_attachments"
    static let getRoles: String = "Get-Roles"
    static let getServices: String = "Get-Services"
    static let inviteTeamMenber: String = "Invite-Team-Member"
    static let projectContacts: String = "Project_management/get_projects_people"
    static let addTask: String = "Add-Task"
    static let likeUnLike: String = "Feed-Likeunlike"
    static let projectTasks: String = "Project_management/get_projects_tasks"
    static let projectList: String = "Project_management/project_list"
    static let editTask: String = "Edit-Task"
    static let joinTeam: String = "Join-Team"
    static let switchRole: String = "User-Toggle"
    static let feedLikes: String = "Get-Feed-Likes"
    static let userContacts: String = "User-Contacts"
    static let contactProfile: String = "View-Pdetails"
    static let profile: String = "Profile"
    static let country: String = "Country"
    static let state: String = "State"
    static let city: String = "City"
    static let taskAcceptDecline: String = "Task-Accept-Decline"
    static let taskStatusUpdate: String = "Project_management/mark_as_complete"
    static let startTimeURL: String = "Project_management/start_timer"
    static let endTimeURL: String = "Project_management/stop_timer"
    static let updateTaskLog: String = "Project_management/task_log"
    static let contractorActiveTasks: String = "Contractor_mytasks/active_tasks"
    static let contractorInProgressTasks: String = "Contractor_mytasks/completed_tasks"
    static let contractorPaymentsTasks: String = "Contractor_mytasks/payment_done"
    static let savingCardDetails: String = "/Register/card_details"
    static let savingBankDetails: String = "Register/bank_details"
    //Register/card_details

    static let timesheetTasks: String = "User-Timesheets"
    static let uploadFile: String = "Upload-Document"
    static let feedUserProfileImage: String = "https://app.sociallink.com/uploads/profiles/"
    static let feedImages: String = "https://app.sociallink.com/uploads/feed/"
    static let attachments: String = "https://app.sociallink.com/uploads/files/"
    static let slackauthURL: String = "https://slack.com/api/oauth.access?"
    static let slackUsersListURL: String = "Chat_slack/user_list"

    static let slackChatHistoryURL: String = "Chat_slack/channels_history"
    static let postChatMessageURL: String = "Chat_slack/postmessage"
    
    
    //Contractor
    static let getContractorAwaitingProject: String = "Contractor_project_management/awaiting_projects"
    static let getContractorQuotationProject: String = "Contractor_project_management/quotation_sent"
    static let getContractorInProgressProject: String = "Contractor_project_management/in_progress"
    static let getContractorCompletedProject: String = "Contractor_project_management/completed_projects"
    static let sendQuotaion: String = "Contractor_project_management/Project_estimates_status_update"
    static let estimationStatus: String = "Project_estimates/Project_estimates_status_update"

    //http://204.48.22.81/Sociallink_Api/index.php/Contractor_project_management/create_task
    static let createTask: String = "Contractor_project_management/create_task"
    
    static let contractorPostFeed: String = "Contractor_feed/feed"
    static let contractorPostComment: String = "Contractor_feed/comments"
    static let contractorGetComment: String = "Contractor_feed/get_comments"
    static let contractorPostLike: String = "Contractor_feed/like"
    static let ContractorGetLike: String = "Contractor_feed/get_likes"
    
    
    static func getBaseURL() -> String {
        let env = APPStore.sharedStore.environment
        if env == Environment.Local {
            //return "http://159.65.145.170/sociallinks/webservice/"
            return "http://204.48.22.81/Sociallink_Api/index.php/"
        } else if env == Environment.Production {
            return "http://204.48.22.81/Sociallink_Api/index.php/"
        } else {
            return ""
        }
    }
    
    
    static func imageBaseURL() -> String {
        let env = APPStore.sharedStore.environment
        if env == Environment.Local {
            //return "http://159.65.145.170/sociallinks/webservice/"
            return "http://159.65.145.170//social//uploads//profiles//"
        } else if env == Environment.Production {
            return "http://159.65.145.170//social//uploads//profiles//"
        } else {
            return ""
        }
    }
    
}

// MARK: - Database

struct APPDBConstants {
    static let USER: String = "UserDB"
    static let TOKEN: String = "TokenDB"
    static let PROJECT: String = "ProjectsDB"
    static let ROLE: String = "RolesDB"
    static let FEED: String = "FeedsDB"
    static let TASK: String = "TasksDB"
    static let TIMELOGS: String = "TimeLogsDB"
    static let SERVICE: String = "ServicesDB"
    static let LIKES: String = "LikesDB"
    
}

struct EMPLOYERTASKSTATES {
    static let ASSIGNED: Int64 = 0
    static let ACTIVE: Int64 = 1
    static let PAYMENT: Int64 = 2
}
struct CONTRACTORTASKSTATES {
    static let ASSIGNED: Int64 = 0
    static let INPROGRESS: Int64 = 1
    static let COMPLETED: Int64 = 2
}

struct ACCEPTEDSTATES {
    static let REJECT: Int64 = 0
    static let ACCEPT: Int64 = 1
    static let COMPLETED: Int64 = 2
    static let PAID: Int64 = 3
}


struct USERTYPE {
    static let EMPLOYER: String = "Employer"
    static let CONTRACTOR: String = "Contractor"
    static let ONLYCONTRACTOR: String = "Onyl Contractor"
}

struct Constant {
    static let ProjectFeedLimit: Int = 10
    static let dateFormat: String = "dd-MM-yy hh:mm:ss"
    static let defaultDate: String = "0000-00-00T00:00:00"
    static let globalDateFormat: String = "yyyy-MM-dd'T'HH:mm:ss"
    static let globalDateFormatWithoutT: String = "yyyy-MM-dd HH:mm:ss"
    static let calenderDateFormat: String = "yyyy-MM-dd"
    static let calenderDateFormatDateAndTime: String = "yyyy-MM-dd||HH:mm:ss"
    ////static let TempglobalDateFormat: String = "yyyy-MM-dd'T'HH:mm:ss"
}

struct MemberType {
    static let SocialLink: Int = 0
    static let Internal: Int = 1
}


struct ServiceResponse {
    static let SUCCESS: String = "true"
    static let FAIL: String = "false"
}

struct COREDATAErrorConstants {
    static let CoreDataErrorDomian: String = "CORE_DATA_ERROR_DOMAIN"
    static let CoreDataErrorCode: Int = 1001
}


// MARK: - App Notifications's
struct APPNotifications {
    static let removeForgetPassword: String = "removeForgetPassword"
    static let createNewAccountNotification: String = "createNewAccountNotification"
    static let changeRootView: String = "changeRootView"
    static let dismissLoginView: String = "dismissLoginView"
    static let dismissMoreView: String = "dismissMoreView"
    static let dismissRegistrationView: String = "dismissRegistrationView"
    
    static let dismissAddView: String = "dismissAddView"
    
    //////PlusNotification
    static let plusNewsFeed: String = "plusNewsFeed"
    static let plusProjects: String = "plusProjects"
    static let plusContacts: String = "plusContacts"
    static let plusPayment: String = "plusPayment"
    static let plusMore: String = "plusMore"
    static let switchTabbar: String = "SwitchTabbar"
    static let userSwitched: String = "UserSwitched"
    
    /////////////////App Signin Notificaton
    static let googleSignInNotification: String = "googleSignInNotification"

    
    static let showProjectList: String = "showProjectList"
    static let showLoginAfterLogout: String = "showLoginAfterLogout"

}

struct AppLoggedInFrom {
    static var Google: String = "google"
    static var Direct: String = "direct"
}

struct ProfileSelect {
    static var Country: String = "Country"
    static var State: String = "State"
    static var City: String = "City"

}


struct FileType {
    static var Text: String = "Text"
    static var PDF: String = "pdf"
    static var DOC: String = "doc"
    static var DOCX: String = "docx"
    static var JPEG: String = "jpeg"
    static var PPT: String = "ppt"
    static var PPTX: String = "pptx"
    static var XLS: String = "xls"
    static var XLSX: String = "xlsx"
    
}
struct Document {
    static var notExist: String = ""

}

struct ScreenType {
    static var ROLE: String = "Role"
    static var SERVICES: String = "Services"
}

struct ContactsShowScreen {
    static var PostViewController: String = "PostViewController"
    static var ProjectFeedVC: String = "ProjectFeedVC"
    static var AddController: String = "AddController"
}

struct ProjectFeedType {
    static var Common: String = "Common"
    static var Individual: String = "Individual"
}

struct Message {
    static var Alert: String = "Alert..!"
}


// MARK: - All Tableview / Collectionview Identifiers will go here
struct CellIdentifiers {
    static let LandingCellIdentifier: String = "LandingCellIdentifier"
    static let LoginTableViewCellIdentifier: String = "LoginTableViewCellIdentifier"
    static let LoginPasswordCellIdentifier: String = "LoginPasswordCellIdentifier"
    static let RegisterTableCellIdentifier: String = "RegisterTableCellIdentifier"
    static let ProfileContentCellIdentifier: String = "ProfileContentCellIdentifier"
    static let ProfileCompanyCellIdentifier: String = "ProfileCompanyCellIdentifier"
    static let ProfileGenderCellIdentifier: String = "ProfileGenderCellIdentifier"
    static let ProfileCreateAccountCellIdentifier: String = "ProfileCreateAccountCellIdentifier"
    static let ProfileImageTableCellIdentifier: String = "ProfileImageTableCellIdentifier"
    static let PlanCollectionCellIdentifier: String = "PlanCollectionCellIdentifier"
    static let PlanDetailsIdentifier: String = "PlanDetailsIdentifier"
    static let PlanTableViewCellIdentifier: String = "PlanTableViewCellIdentifier"
    static let PaymentMethodCellIdentifier: String = "PaymentMethodCellIdentifier"
    static let AddPaymentCellIdentifier: String = "AddPaymentCellIdentifier"
    static let SelectPaymentCellIdentifier: String = "SelectPaymentCellIdentifier"
    static let NewCardCellIdentifier: String = "NewCardCellIdentifier"
    static let CardNumberCellIdentifier: String = "CardNumberCellIdentifier"
    static let ExpiryDateCellIdentifier: String = "ExpiryDateCellIdentifier"
    static let ContinueCellIdentifier: String = "ContinueCellIdentifier"
    static let CardCollectionViewCellIdentifier: String = "CardCollectionViewCellIdentifier"
    static let ForgetPasswordCellIdentifier: String = "ForgetPasswordCellIdentifier"
    static let CheckEmailTableCellIdentifier: String = "CheckEmailTableCellIdentifier"
    static let NewPasswordCellIdentifier: String = "NewPasswordCellIdentifier"
    static let InviteTableViewCellIdentifier: String = "InviteTableViewCellIdentifier"
    static let ServiceSpecializeCellIdentifier: String = "ServiceSpecializeCellIdentifier"
    static let ServiceContactsCellIdentifier: String = "ServiceContactsCellIdentifier"
    static let SpecializationListCellIdentifier: String = "SpecializationListCellIdentifier"
    static let ContactProfileCellIdentifier: String = "ContactProfileCellIdentifier"
    static let ProjectTableViewCellIdentifier: String = "ProjectTableViewCellIdentifier"
    static let AddProjectCellIdentifier: String = "AddProjectCellIdentifier"
    static let NewProjectCellIdentifier: String = "NewProjectCellIdentifier"
    static let ContactTableViewCellIdentifier: String = "ContactTableViewCellIdentifier"
    static let SearchCollectionCellIdentifier: String = "SearchCollectionCellIdentifier"
    static let InvoiceTableViewCellIdentifier: String = "InvoiceTableViewCellIdentifier"
    static let InvoiceDateCellIdentifier: String = "InvoiceDateCellIdentifier"
    static let PaymentDueCellIdentifier: String = "PaymentDueCellIdentifier"
    static let ProfileTableViewCellIdentifier: String = "ProfileTableViewCellIdentifier"
    static let DetailProjectCellIdentifier: String = "DetailProjectCellIdentifier"
    static let SendPaymentCellIdentifier: String = "SendPaymentCellIdentifier"
    static let MoreProfileCellIdentifier: String = "MoreProfileCellIdentifier"
    static let MoreTableViewCellIdentifier: String = "MoreTableViewCellIdentifier"
    static let ServiceAgreementCellIdentifier: String = "ServiceAgreementCellIdentifier"
    static let ServiceTermsCellIdentifier: String = "ServiceTermsCellIdentifier"
    static let ServiceAcceptCellIdentifier: String = "ServiceAcceptCellIdentifier"
    static let QuestionTableCellIdentifier: String = "QuestionTableCellIdentifier"
    static let AnswerTableCellIdentifier: String = "AnswerTableCellIdentifier"
    static let ContactInviteCellIdentifier: String = "ContactInviteCellIdentifier"
    static let ContactDetailCellIdentifier: String = "ContactDetailCellIdentifier"
    static let ContactAboutCellIdentifier: String = "ContactAboutCellIdentifier"
    static let MapViewTableCellIdentifier: String = "MapViewTableCellIdentifier"
    static let ChooseServiceCellIdentifier: String = "ChooseServiceCellIdentifier"
    static let DistanceTableViewCellIdentifier: String = "DistanceTableViewCellIdentifier"
    static let ServiceTableViewCellIdentifier: String = "ServiceTableViewCellIdentifier"
    static let FindMemberCellIdentifier: String = "FindMemberCellIdentifier"
    static let CreativesCellIdentifier: String = "CreativesListCell"

    static let PostTableViewCellIdentifier: String = "PostTableViewCellIdentifier"
    static let ProjectTextTableCellIdentifier: String = "ProjectTextTableCellIdentifier"
    static let ProjectImageTableCellIdentifier: String = "ProjectImageTableCellIdentifier"
    static let ProjectCurrentCellIdentifier: String = "ProjectCurrentCellIdentifier"
    static let ProjectEstimationCellIdentifier: String = "ProjectEstimationCellIdentifier"
    static let CancelledProjectsCellIdentifier: String = "CancelledProjectsCell"

    //CancelledProjectsCell
    static let ContactUsTextFieldCellIdentifier: String = "ContactUsTextFieldCellIdentifier"
    static let ContactUsDecriptionCellIdentifier: String = "ContactUsDecriptionCellIdentifier"
    static let ContactUsSubmitCellIdentifier: String = "ContactUsSubmitCellIdentifier"
    
    static let NotificationsCellIdentifier: String = "NotificationsCellIdentifier"
    
    static let  ContentTableViewCellIdentifier = "ContentTableViewCellIdentifier"
    static let  PostTypeTableViewCellIdentifier = "PostTypeTableViewCellIdentifier"
    static let  ProjectListCellIdentifier = "ProjectListCellIdentifier"
    static let  TeamInfoCellIdentifier = "TeamInfoCellIdentifier"
    
    
    static let  ProjectMessageListCellIdentifier = "ProjectMessageListCellIdentifier"
    
    static let  ProjectPaymentCellIdentifier = "ProjectPaymentCellIdentifier"
    
    static let  ProjectUnpaidCellIdentifier = "ProjectUnpaidCellIdentifier"
    static let  EventTableViewCellIdentifier = "EventTableViewCellIdentifier"
    
    static let  EventTimeCellIdentifier = "EventTimeCellIdentifier"
    static let ProjectSearchContactCellIdentifier = "ProjectSearchContactCellIdentifier"
    static let ProjectTimeLineCellIdentifier = "ProjectTimeLineCellIdentifier"
    static let SearchContactCellIdentifier = "SearchContactCellIdentifier"

    static let MilestoneTableCellIdentifier = "MilestoneTableCellIdentifier"
    static let AddMIlestoneCellIdentifier = "AddMIlestoneCellIdentifier"
    static let NewMileStoneCellIdentifier = "NewMileStoneCellIdentifier"
    static let NewMilestoneAddCellIdentifier = "NewMilestoneAddCellIdentifier"

    static let NewMilestoneTaskCellIdentifier = "NewMilestoneTaskCellIdentifier"
    static let ParticipantsCellIdentifier = "ParticipantsCellIdentifier"

    static let SaveDeleteCellIdentifier = "SaveDeleteCellIdentifier"
    
    
    static let ProjectTaskCellIdentifier = "ProjectTaskCellIdentifier"
    static let ProjectAddTaskCellIdentifier = "ProjectAddTaskCellIdentifier"
    static let ProjectNewTaskDateCellIdentifier = "ProjectNewTaskDateCellIdentifier"

    static let TaskAssigneeCellIdentifier = "TaskAssigneeCellIdentifier"
    static let NewTaskUploadCellIdentifier = "NewTaskUploadCellIdentifier"
    static let TaskDescriptionCellIdentifier = "TaskDescriptionCellIdentifier"
    static let NewTaskSubTaskCellIdentifier = "NewTaskSubTaskCellIdentifier"
    static let SubTaskAssigneeCellIdentifier = "SubTaskAssigneeCellIdentifier"
    static let AddNewSubTaskCellIdentifier = "AddNewSubTaskCellIdentifier"
    static let SaveDeleteNewTaskCellIdentifier = "SaveDeleteNewTaskCellIdentifier"

    static let SubTaskListCellIdentifier = "SubTaskListCellIdentifier"
    static let SubTaskParticipantsCellIdentifier = "SubTaskParticipantsCellIdentifier"
    static let SubTaskAddSubTaskCellIdentifier = "SubTaskAddSubTaskCellIdentifier"

    
    
    static let NewSubTaskDataCellIdentifier = "NewSubTaskDataCellIdentifier"
    static let NewSubTaskUploadCellIdentifier = "NewSubTaskUploadCellIdentifier"
    static let NewSubTaskDescriptionCellIdentifier = "NewSubTaskDescriptionCellIdentifier"
    static let NewSubTaskSaveDeleteCellIdentifier = "NewSubTaskSaveDeleteCellIdentifier"

    static let NewSubTaskAsigneeCellIdentifier = "NewSubTaskAsigneeCellIdentifier"
    static let ProjectListTableCellIdentifier = "ProjectListTableCellIdentifier"

    
    static let MyTaskTableCellIdentifier = "MyTaskTableCellIdentifier"
    static let MyTaskDetailNameCellIdentifier = "MyTaskDetailNameCellIdentifier"
    static let MyTaskUploadCellIdentifier = "MyTaskUploadCellIdentifier"
    static let MyTaskLogCellIdentifier = "MyTaskLogCellIdentifier"
    static let TimesheetTableCellIdentifier = "TimesheetTableCellIdentifier"
    static let TaskStartStopCellIdentifier = "TaskStartStopCellIdentifier"
    static let InviteTeamMemberCellIdentifier = "InviteTeamMemberCellIdentifier"
    static let RoleTableViewCellIdentifier = "RoleTableViewCellIdentifier"
    static let TaskDetailCellIdentifier = "TaskDetailCellIdentifier"
    static let TaskStateSegmentCellIdentifier = "TaskStateSegmentCellIdentifier"
    static let ProjectLikeCellIdentifier = "ProjectLikeCellIdentifier"
    static let ProjectsContactsCellIdentifier = "ProjectsContactsCellIdentifier"
    static let CommentsTableCellIdentifier = "CommentsTableCellIdentifier"
    static let ProfileResumeCellIdentifier = "ProfileResumeCellIdentifier"
    static let CountryCellIdentifier = "CountryCellIdentifier"
    static let TaskDetailAsigneeCellIdentifier = "TaskDetailAsigneeCellIdentifier"
    static let TaskDetailLogCellIdentifier = "TaskDetailLogCellIdentifier"
    static let ImagePostCellIdentifier = "ImagePostCellIdentifier"

    

}

struct Google {
    static let googleLoginClientId: String = "352861569671-n1fc6mqer4o4ce42to3sgc6tgbl4hnml.apps.googleusercontent.com"
   static let googlePlacesKey: String = "AIzaSyDazsiTR1Vx2ei6HvWxwnEaJG4ito3fSbc"
    
}

struct APIKEYS {
    static let GOOGLEMAPS: String = "AIzaSyCLBhGqk5c0bxNwjGbcK9wlSp5F5LqHsi8"
}

struct APPLoggerKey {
    static let restErrorCode: String = "qRestErrorCode"
    static let restErrorMsg: String = "qRestErrorMsg"
    static let restStatusCode: String = "qRestStatusCode"
    static let restStatusMsg: String = "qRestStatusMsg"
    static let restAPIName: String = "qRestAPIName"
    static let restExecTime: String = "qRestExecTime"
    static let httpError: String = "qHTTPError"
    static let restError: String = "qRestError"
}

struct SubTaskStatus {
    static let yetToStart: String = "yetToStart"
    static let inProgress: String = "inProgress"
    static let completed: String = "completed"
    static let confirmed: String = "confirmed"
}

struct ALERTMESSAGE {
    
    static let ALERT: String = "Alert..!"
    static let NOINTERNET: String = "Please check your internet connectivity..."
    
}

struct MILESTONETYPE {
    static let NEW: String = "NEW"
    static let EDIT: String = "EDIT"
}

struct TASKTYPE {
    static let NEW: String = "NEW"
    static let EDIT: String = "EDIT"
     static let DETAIL: String = "DETAIL"
}
struct PROTYPE {
    static let NEW: String = "NEW"
    static let EDIT: String = "EDIT"
}
struct SUBTASKTYPE {
    static let NEW: String = "NEW"
    static let EDIT: String = "EDIT"
}

//struct NOTIFICATION {
//    static let NETWORK: String = "NETWORK"
//
//}


struct APPUSERDEFAULTS {
    
    static let NETWORK: String = "NETWORK"
    static let GeneralFeedTimeStamp: String = "GeneralFeedTimeStamp"
    
}


