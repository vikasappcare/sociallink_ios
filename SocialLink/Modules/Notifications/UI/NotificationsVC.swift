//
//  NotificationsVC.swift
//  SocialLink
//
//  Created by Santhosh on 10/04/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class NotificationsVC: UIViewController {

    var alertIsHidden = true
    
    @IBOutlet weak var notificationsTable: UITableView!
    @IBOutlet weak var alertViewHideButton: UIButton!
    @IBOutlet weak var alertViewTitleBG: UIView!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var alertViewHeight: NSLayoutConstraint!
    @IBOutlet weak var alertViewTitleHeight: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "NOTIFICATIONS"
        
        self.setTableViewDatasource()
        roundedCorners()
        alertView.isHidden = true
        alertViewTitleBG.isHidden = true
        alertViewHideButton.isHidden = true
        alertViewHideButton.addTarget(self, action: #selector(hideButtonClicked), for: .touchUpInside)
    }
    
    func roundedCorners()
    {
        alertView.clipsToBounds = true
        alertView.layer.cornerRadius = 30
        alertView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        alertViewHideButton.layer.cornerRadius = 6
        
        
    }
   @objc func hideButtonClicked()
   {
    if alertIsHidden == false{
        alertView.isHidden = true
        alertViewTitleBG.isHidden = true
        alertViewHideButton.isHidden = true
        alertIsHidden = true
    }
    }
}

extension NotificationsVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.notificationsTable.delegate = self
        self.notificationsTable.dataSource = self
        self.notificationsTable.estimatedRowHeight = 300
        self.notificationsTable.rowHeight = UITableViewAutomaticDimension
        self.notificationsTable.tableFooterView = UIView()
        self.notificationsTable.separatorStyle = .singleLine
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return 2
        case 1:
            return 6
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.NotificationsCellIdentifier, for: indexPath)
            if let cellIs = cell as? NotificationsCell {
                cellIs.selectionStyle = .none
                
            }
            cell.selectionStyle = .none
            return cell
     
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 36
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if alertIsHidden == true{
            alertView.isHidden = false
            alertViewTitleBG.isHidden = false
            alertViewHideButton.isHidden = false
            alertIsHidden = false
            
        }
    }
    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        switch section {
//        case 0:
//            return "New Notices"
//        case 0:
//            return "Old Notices"
//        default:
//            return ""
//        }
//    }
    
//    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
//        view.tintColor = UIColor.red
//        let header = view as! UITableViewHeaderFooterView
//        header.textLabel?.textColor = UIColor.white
//    }
//
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
            headerView.backgroundColor = UIColor.clear
        let label = UILabel(frame: CGRect(x: 20, y: 6, width: tableView.bounds.size.width - 20, height: 30))
        label.font = UIFont.regularFont(15)
        label.textColor = UIColor.lightGray
        headerView.addSubview(label)
        switch section {
        case 0:
            label.text = "New"
        case 1:
            label.text = "Earlier"
        default:
            label.text = ""
        }
        return headerView
    }
    
}
