 //
 //  ProfileViewController.swift
 //  SocialLink
 //
 //  Created by Santhosh Marripelli on 27/02/18.
 //  Copyright © 2018 Santhosh Marripelli. All rights reserved.
 //
 
 import UIKit
 import MobileCoreServices
 class ProfileViewController: AppBaseVC {
    
    var paramDict = [String: Any]()
    let contentPlaceHolder = ["",
                              "Email",
                              "Phone Number",
                              "",
                              "About You",
                              "Website",
                              "Education",
                              "Skills",
                              "Business",
                              "Industry",
                              "Department",
                              "Job",
                              "Country",
                              "State",
                              "City",
                              "Designation",
                              ""]
    
    let keyValue = ["",
                    "Email",
                    "Phone Number",
                    "",
                    "About You",
                    "Website",
                    "Education",
                    "Skills",
                    "Business",
                    "Industry",
                    "Department",
                    "Job",
                    "Country",
                    "State",
                    "City",
                    "Designation",
                    ""]
    
    var uploadModel: FileUploadViewModel?
    var viewModel: ProfileViewModel?
    @IBOutlet weak var profileTableView: UITableView!
    var skip: Bool?
    var contactProfile: ProfileModel?
    
    //    var selectedCountry: Country?
    //    var selectedState: State?
    //    var selectedCity: City?
    var imageUpdated: Bool?
    var newImage: UIImage?
    // var resumeData: Data?

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = ProfileViewModel()
        self.navigationItem.title = "PROFILE"
        
        self.getusersFromDB()
        // Do any additional setup after loading the view.
        
        self.setTableViewDatasource()
        
        if skip ?? false {
            self.createNavigationItems()
            
        }
        uploadModel = FileUploadViewModel()
    }
    func getusersFromDB() {
        let users = APPDBUtility.getUserData(mocType: .main)
        if users?.count ?? 0 > 0 {
            if let user = users {
                if viewModel != nil {
                    contactProfile = APPUtility.convertDBUserToUser(dbUser: user[0])
                }
                
            }
        }
    }
    
    func showSkip(bool: Bool) {
        skip = bool
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateContact(data: [String: Any]) {
        if let model = viewModel {
            self.showHUD(title: "")
            model.updateProfile(parameters: data, onSuccess: { [weak self] (dataSource) in
                print("Success")
                self?.hideHud()
                self?.updateContactImage()
                APPUtility.getUserData()
                }, onError: { (error) in
                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
                   // self.showAlert(title: Message.Alert, message: error.statusMessage)
                    self.hideHud()
            })
        }
    }
    
    func updateprofile() {
        var text = ""
        if let model = uploadModel {
            if let image = newImage {
                self.showHUD(title: "")
                let indexPath = IndexPath(row: 0, section: 0)
                let cellIs = self.profileTableView.cellForRow(at: indexPath)
                if let cell = cellIs as? ProfileImageTableCell {
                    text = cell.nameText.text!
                }
                print(text)
                print(paramDict)
                if let  data = UIImageJPEGRepresentation(image, 0.1) {
                    //var random = arc4random_uniform(10)
                    let randomName = APPUtility.getPresentDate()
                    let fileNameString = randomName
                        .components(separatedBy:CharacterSet.decimalDigits.inverted)
                        .joined(separator: "")
                    print(fileNameString)
                    
                    let uploadData = ["uid": APPStore.sharedStore.user.userId ?? 0,
                                      "file_extension": "jpeg",
                                      "uphoto": "\(fileNameString).jpeg"] as [String : Any]
                    
                    // uploadData = paramDict
                    //print(uploadData)
                    model.requestWith(imageData: data, parameters: uploadData, endURL: APPURL.profilePicUpdate, onCompletion: { [weak self] (dataSource) in
                        self?.hideHud()
                        if let data = dataSource {
                            print(data)
                        }
                        
                        //if let data = dataSource {
                        //APPDBModel.updateFeedImage(data)
                        DispatchQueue.main.async(execute: {
                            // if let feedIS = dataSource?.data?.id {
                            // self?.delegate?.postedFeed(feedId: feedIS)
                            self?.dismiss(animated: true, completion: nil)
                        })
                        //}
                        }, onError: { (error) in
                            self.showAlert(title: "Alert", message: error?.localizedDescription ?? "")
                            self.hideHud()
                    })
                }
                print(image)
            }
            else
            {
                if let model = viewModel {
                    self.showHUD(title: "Loading...")
                    paramDict["uid"] = APPStore.sharedStore.user.userId
                    print(paramDict)
                    model.updateProfile(parameters: paramDict, onSuccess: { [weak self] (dataSource) in
                        self?.hideHud()
                        }, onError: { (error) in
                            Logger.log(message: "Error \(error.statusMessage)", event: .error)
                         //   self.showAlert(title: Message.Alert, message: error.statusMessage)
                            self.hideHud()
                    })
                }
            }
        }
    }
    func updateContactImage() {
        if let model = uploadModel {
            if let image = newImage {
                self.showHUD(title: "")
                if let  data = UIImageJPEGRepresentation(image, 0.5) {
                    let uploadData = ["token": APPStore.sharedStore.token,
                                      "upload_type": "profile_picture",
                                      "file_extension": "jpeg",
                                      "id": APPStore.sharedStore.user.userId ?? 0,
                                      "upload_time": APPUtility.getPresentDate(),
                                      "data": data] as [String : Any]
                    model.requestWith(imageData: data, parameters: uploadData, endURL: "", onCompletion: { [weak self] (dataSource) in
                        self?.hideHud()
                        if let data = dataSource {
                            print(data)
                            APPDBModel.updateUserPictureInDB(data.data?.dfile ?? "")
                            //self?.getusersFromDB()
                            APPUtility.getUserData()
                            self?.profileTableView.reloadData()
                        }
                        
                        }, onError: { (error) in
                            self.showAlert(title: "Alert", message: error?.localizedDescription ?? "")
                            self.hideHud()
                    })
                }
            }
        }
    }
    
    //    func updateFeedFile(feedId: Int64, withExtension: String) {
    //        if let model = uploadModel {
    //            self.showHUD(title: "")
    //            if let data = self.resumeData {
    //                let uploadData = ["token": APPStore.sharedStore.token,
    //                                  "upload_type": "feed_file",
    //                                  "file_extension": withExtension,
    //                                  "id": feedId,
    //                                  "upload_time": APPUtility.getPresentDate(),
    //                                  "data": data] as [String : Any]
    //                model.requestWith(imageData: data, parameters: uploadData, endURL: "", onCompletion: { [weak self] (dataSource) in
    //                    self?.hideHud()
    //                    if let data = dataSource {
    ////                        APPDBModel.updateFeedImage(data)
    ////                        DispatchQueue.main.async(execute: {
    ////                            if let feedIS = dataSource?.data?.id {
    ////                                self?.delegate?.postedFeed(feedId: feedIS)
    ////                            }
    ////                            self?.dismiss(animated: true, completion: nil)
    ////                        })
    //                        print(data)
    //
    //                    }
    //                    }, onError: { (error) in
    //                        self.showAlert(title: "Alert", message: error?.localizedDescription ?? "")
    //                        self.hideHud()
    //                })
    //            }
    //        }
    //    }
    //
    //
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
 }
 
 extension ProfileViewController {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
        let skipItem = UIBarButtonItem(title: "Skip", style: .done, target: self, action: #selector(skipClicked))
        self.navigationItem.rightBarButtonItem = skipItem
    }
    
    @objc func skipClicked() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ChoosePlanVC") as? ChoosePlanVC {
            /// let navController = APPNavigationController(rootViewController: controller)
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
 }
 
 extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.profileTableView.delegate = self
        self.profileTableView.dataSource = self
        ///self.profileTableView.estimatedRowHeight = 300
        ///self.profileTableView.rowHeight = UITableViewAutomaticDimension
    }
    
    //    func numberOfSections(in tableView: UITableView) -> Int {
    //        return 3
    //    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contentPlaceHolder.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.ProfileImageTableCellIdentifier, for: indexPath)
            if let cellIs = cell as? ProfileImageTableCell {
                cellIs.selectionStyle = .none
                cellIs.delegate = self
                if let profile = self.contactProfile {
                    // print(profile.full_name)
                    cellIs.setData(data: profile, tag: indexPath.row)
                }
            }
            cell.selectionStyle = .none
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.ProfileGenderCellIdentifier, for: indexPath)
            if let cellIs = cell as? ProfileGenderCell {
                cellIs.selectionStyle = .none
                cellIs.delegate = self
                print(cellIs.genderSegment.selectedSegmentIndex)
                if let profile = self.contactProfile {
                    cellIs.setData(gender: profile.gender ?? "")
                }
                
            }
            cell.selectionStyle = .none
            return cell
            //        case 4:
            //            let cell = tableView.dequeueReusableCell(withIdentifier:
            //                CellIdentifiers.ProfileCompanyCellIdentifier, for: indexPath)
            //            if let cellIs = cell as? ProfileCompanyCell {
            //                cellIs.selectionStyle = .none
            //                cellIs.delegate = self
            //                cellIs.setData(data: contentPlaceHolder[indexPath.row], tag: indexPath.row)
            //            }
            //            cell.selectionStyle = .none
            //            return cell
            //        case 12:
            //            let cell = tableView.dequeueReusableCell(withIdentifier:
            //                CellIdentifiers.ProfileResumeCellIdentifier, for: indexPath)
            //            if let cellIs = cell as? ProfileResumeCell {
            //                cellIs.selectionStyle = .none
            //                cellIs.delegate = self
            //                if let profile = contactProfile {
            //                    cellIs.setData(data: contentPlaceHolder[indexPath.row], tag: indexPath.row, profile: profile)
            //                }
            //            }
            //            cell.selectionStyle = .none
        //            return cell
        case 16:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.ProfileCreateAccountCellIdentifier, for: indexPath)
            if let cellIs = cell as? ProfileCreateAccountCell {
                cellIs.selectionStyle = .none
                cellIs.delegate = self
            }
            cell.selectionStyle = .none
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.ProfileContentCellIdentifier, for: indexPath)
            if let cellIs = cell as? ProfileContentCell {
                cellIs.selectionStyle = .none
                cellIs.delegate = self
                
                if let profile = contactProfile {
                    cellIs.setData(data: contentPlaceHolder[indexPath.row], tag: indexPath.row, profile: profile)
                }
            }
            cell.selectionStyle = .none
            return cell
        }
    }
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //
    //        return UIScreen.main.bounds.height
    //    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
 }
 
 extension ProfileViewController: ProfileCreateAccountCellDelegate, ProfileContentCellDelegate, ProfileCompanyCellDelegate, ProfileGenderCellDelegate {
    func textfieldData(data: String, tag: Int) {
        switch tag {
        case 0:
            paramDict["uName"] = data
        case 1:
            paramDict["uEmail"] = data
            
        case 2:
            paramDict["uphone"] = data
        case 3:
            paramDict["ugender"] = data
        case 4:
            paramDict["uaboutu"] = data
        case 5:
            paramDict["uwebsite"] = data
        case 6:
            paramDict["education"] = data
        case 7:
            paramDict["uskills"] = data
        case 8:
            paramDict["business"] = data
        case 9:
            paramDict["industry"] = data
        case 10:
            paramDict["department"] = data
        case 11:
            paramDict["job"] = data
        case 12:
            paramDict["ucountry"] = data
        case 13:
            paramDict["ustate"] = data
        case 14:
            paramDict["ucity"] = data
        case 15:
            paramDict["designation"] = data
        default:
            break
        }
    }
    
    func showPickerView(forTag: Int) {
        
    }
    
    func submitClicked() {
        updateprofile()
        
    }
    
    func showServiceAgreement() {
        return
    }
    
    //    func showPickerView(forTag: Int) {
    //        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "CountryListVC") as? CountryListVC {
    //            switch forTag {
    //            case 13:
    //                controller.setControllerType(type: ProfileSelect.Country)
    //            case 14:
    //                if let countryIs = self.selectedCountry {
    //                    controller.setControllerType(type: ProfileSelect.State)
    //                    controller.setCountryId(id: countryIs.id ?? 0)
    //                }
    //            case 15:
    //                if let countryIs = self.selectedCountry, let stateIs = self.selectedState {
    //                    controller.setControllerType(type: ProfileSelect.City)
    //                    controller.setCountryId(id: countryIs.id ?? 0)
    //                    controller.setStateId(id: stateIs.id ?? 0)
    //                }
    //
    //
    //            default:
    //                break
    //            }
    //            controller.delegate = self
    //            let navController = APPNavigationController(rootViewController: controller)
    //            self.present(navController, animated: true, completion: nil)
    //        }
    //
    //
    //    }
    
    func genderChanged(gender: String, tag: Int) {
        self.updateModel(text: gender, tag: tag)
    }
 
    func dataChanged(data: String, tag: Int) {
        self.updateModel(text: data, tag: tag)
    }
    
    func textChanged(data: String, tag: Int) {
        self.updateModel(text: data, tag: tag)
    }
    
    func updateProfileCicked() {
        if let model = viewModel {
            if let profile = self.contactProfile {
                if let data =  model.constructUpdateProfile(profile: profile) {
                    self.updateContact(data: data)
                }
            }
        }
    }
    
    func updateModel(text: String, tag: Int) {
        switch tag {
        case 0:
            if !text.checkForEmpty() {
                self.contactProfile?.full_name = text
            } else {
                self.showAlert(title: "Alert", message: "Name can't be empty")
            }
        case 1:
            if !APPUtility.validateEmail(enteredEmail: text) {
                self.showAlert(title: "Alert", message: "Enter a valid Email")
            } else {
                self.contactProfile?.email = text
            }
        case 2:
            self.contactProfile?.phone_number = text
        case 3:
            self.contactProfile?.gender = text
        case 4:
            self.contactProfile?.aboutme = text
        case 5:
            //            if !text.checkForEmpty() {
            //                if APPUtility.verifyUrl(urlString: text) {
            //                   self.contactProfile?.website = text
            //                } else {
            //                  self.showAlert(title: "Alert", message: "Enter a valid URL")
            //                }
            //            }
            self.contactProfile?.website = text
        case 6:
            self.contactProfile?.education = text
        case 7:
            self.contactProfile?.skills = text
        case 8:
            self.contactProfile?.business = text
        case 9:
            self.contactProfile?.industry = text
        case 10:
            self.contactProfile?.department = text
        case 11:
            self.contactProfile?.job = text
            //        case 12:
            //            //////Resume
        //            self.contactProfile?.resume = text
        case 12:
            self.contactProfile?.country = text
        case 13:
            self.contactProfile?.state = text
        case 14:
            self.contactProfile?.city = text
        case 15:
            self.contactProfile?.designation = text
        default:
            break
        }
    }
 }
 //extension ProfileViewController: CountryListVCDelegate {
 //    func selectedCountry(country: Country) {
 //
 //        self.selectedState = nil
 //        self.selectedCity = nil
 //        self.setStateEmpty()
 //        self.setCityEmpty()
 //        self.selectedCountry = country
 //        let indexPath = IndexPath(row: 13, section: 0)
 //        let cell = self.profileTableView.cellForRow(at: indexPath)
 //        if let cellIs = cell as? ProfileContentCell {
 //            cellIs.setText(text: country.name ?? "", tag: 13)
 //            self.contactProfile?.country = country.name ?? ""
 //        }
 //    }
 //    func selectedState(state: State) {
 //        self.selectedCity = nil
 //        self.setCityEmpty()
 //        self.selectedState = state
 //        let indexPath = IndexPath(row: 14, section: 0)
 //        let cell = self.profileTableView.cellForRow(at: indexPath)
 //        if let cellIs = cell as? ProfileContentCell {
 //            cellIs.setText(text: state.name ?? "", tag: 14)
 //            self.contactProfile?.state = state.name ?? ""
 //        }
 //    }
 //    func selectedCity(city: City) {
 //        self.selectedCity = city
 //        let indexPath = IndexPath(row: 15, section: 0)
 //        let cell = self.profileTableView.cellForRow(at: indexPath)
 //        if let cellIs = cell as? ProfileContentCell {
 //            cellIs.setText(text: city.name ?? "", tag: 15)
 //            self.contactProfile?.city = city.name ?? ""
 //        }
 //    }
 //    //////// Empty
 //    func setCountryEmpty() {
 //        let indexPath = IndexPath(row: 13, section: 0)
 //        let cell = self.profileTableView.cellForRow(at: indexPath)
 //        if let cellIs = cell as? ProfileContentCell {
 //            cellIs.setText(text: "", tag: 13)
 //        }
 //    }
 //    func setStateEmpty() {
 //        let indexPath = IndexPath(row: 14, section: 0)
 //        let cell = self.profileTableView.cellForRow(at: indexPath)
 //        if let cellIs = cell as? ProfileContentCell {
 //            cellIs.setText(text: "", tag: 14)
 //        }
 //    }
 //    func setCityEmpty() {
 //        let indexPath = IndexPath(row: 15, section: 0)
 //        let cell = self.profileTableView.cellForRow(at: indexPath)
 //        if let cellIs = cell as? ProfileContentCell {
 //            cellIs.setText(text: "", tag: 15)
 //        }
 //    }
 //
 //}
 
 extension ProfileViewController: ProfileImageTableCellDelegate {
    func changeImageClicked() {
        
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetController: UIAlertController = UIAlertController(title: "Please select", message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "Camera", style: .default)
        { _ in
            self.showPicker(with: "Camera")
        }
        actionSheetController.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Gallery", style: .default)
        { _ in
            self.showPicker(with: "Gallery")
        }
        actionSheetController.addAction(deleteActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
   
    }
    func nameChanged(name: String) {
        self.updateModel(text: name, tag: 0)
    }
 }
 
 extension ProfileViewController: ProfileResumeCellDelegate {
    func chooseResumeClicked() {
        self.showDocumentPicker()
    }
    
 }
 
 extension ProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func showPicker(with type: String) {
        
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        switch type {
        case "Camera":
            picker.sourceType = .camera
        case "Gallery":
            picker.sourceType = .photoLibrary
        default:
            break
        }
        present(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true)
        
        guard let image = info[UIImagePickerControllerEditedImage] as? UIImage else {
            print("No image found")
            return
        }
        
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = self.profileTableView.cellForRow(at: indexPath)
        if let cellIs = cell as? ProfileImageTableCell {
            cellIs.setImage(image: image)
            self.imageUpdated = true
            self.newImage = image
        }
        print(image.size)
    }
 }
 
 extension ProfileViewController: UIDocumentMenuDelegate,UIDocumentPickerDelegate {
    
    func showDocumentPicker() {
        let documentPicker: UIDocumentPickerViewController = UIDocumentPickerViewController(documentTypes: ["com.microsoft.word.doc","org.openxmlformats.wordprocessingml.document", kUTTypePDF as String], in: UIDocumentPickerMode.import)
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        self.present(documentPicker, animated: true, completion: nil)
    }
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let myURL = url as URL
        print("import result : \(myURL)")
        print(myURL.pathExtension)
        
    }
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
   
 }
