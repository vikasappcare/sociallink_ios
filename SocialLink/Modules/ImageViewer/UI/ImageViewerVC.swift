//
//  ImageViewerVC.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 24/04/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ImageViewerVC: UIViewController {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var closeButton: UIButton!
    var imageIs: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileImage.image = UIImage(named: imageIs ?? "")

        // Do any additional setup after loading the view.
    }
    func setImage(image: String) {
        imageIs = image
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func closeClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
