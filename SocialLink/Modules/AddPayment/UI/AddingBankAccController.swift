//
//  AddingBankAccController.swift
//  SocialLink
//
//  Created by Banda Anil Reddy on 26/02/19.
//  Copyright © 2019 Santhosh Marripelli. All rights reserved.
//

import UIKit
import iOSDropDown

class AddingBankAccController: AppBaseVC {

    var viewModel: AddingPaymentViewModel?

    @IBOutlet weak var bankNameTextField: DropDown!
    @IBOutlet weak var fullnameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var banknumberTextField: UITextField!

    var banksArray = [String]()


    @IBOutlet weak var payPalSignIn: UIButton!
    @IBOutlet weak var routingNumberTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        addNotification()
        viewModel = AddingPaymentViewModel()

        callServiceForBanksList()
    }
    func callServiceForBanksList()
    {
        if let model = viewModel {

            let param = ["": ""]
            self.showHUD(title: "Loading...")
            model.banksList(parameters: param as [String : Any], onSuccess: { [weak self] (dataSource) in
                self?.hideHud()
                self?.banksArray = (dataSource?.bName)!
                self?.banksTextFieldDataLoading()
                },  onError: { (error) in
                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
                  //  self.showAlert(title: Message.Alert, message: error.statusMessage)
                    self.hideHud()
            })

        }
    }
    func banksTextFieldDataLoading()
    {
        bankNameTextField.optionArray = banksArray
        bankNameTextField.didSelect{(selectedText , index ,id) in
            self.bankNameTextField.text = selectedText

        }
    }
    
    @IBAction func setUpBankAccButtonclicked(_ sender: Any) {

        if (bankNameTextField.text?.isEmpty ?? true) || (bankNameTextField.text == "") {
            self.showAlert(title: "Alert!", message: "Please enter Bank name to continue")
            return
        }
        if (fullnameTextField.text?.isEmpty ?? true) || (fullnameTextField.text == "") {
            self.showAlert(title: "Alert!", message: "Please enter Name to continue")
            return
        }
        if (addressTextField.text?.isEmpty ?? true) || (addressTextField.text == "") {
            self.showAlert(title: "Alert!", message: "Please enter address to continue")
            return
        }
        if (banknumberTextField.text?.isEmpty ?? true) || (banknumberTextField.text == "") {
            self.showAlert(title: "Alert!", message: "Please enter Bank Number to continue")
            return
        }
        if (routingNumberTextField.text?.isEmpty ?? true) || (routingNumberTextField.text == "") {
            self.showAlert(title: "Alert!", message: "Please enter Routing Number to continue")
            return
        }

        if let userID = APPStore.sharedStore.user.userId{
            if let model = viewModel {
                let param = ["bank_name": bankNameTextField.text!, "bank_accountholder_name": fullnameTextField.text!, "bank_account_number": banknumberTextField.text!, "bank_route_number": routingNumberTextField.text!, "user_id": userID] as [String : Any]
                print(param)
                self.showHUD(title: "Loading...")
                model.addingBankDetails(parameters: param as [String : Any], onSuccess: { [weak self] (dataSource) in
                    self?.hideHud()
                    if APPStore.sharedStore.appUserID == "2"{

                        self?.moveToNextController()
                    }
                    else{
                        self?.moveToHome()

                    }

                    },  onError: { (error) in
                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
                      //  self.showAlert(title: Message.Alert, message: error.statusMessage)
                        self.hideHud()
                })

            }
        }

    }
    func moveToNextController() {
        
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddingCardController") as? AddingCardController {

                self.present(controller, animated: true, completion: nil)
                /// self.navigationController?.pushViewController(controller, animated: true)
            }
    }
    func moveToHome()
    {
        navigationController?.popViewController(animated: true)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue:
            APPNotifications.dismissLoginView),
                                        object: nil, userInfo: nil)

    }
    @IBAction func setUpLaterClicked(_ sender: Any) {
        if APPStore.sharedStore.userIs == "Onyl Contractor"{
            
            moveToNextController()
        }
        else{
            navigationController?.popViewController(animated: true)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue:
                APPNotifications.dismissLoginView),
                                            object: nil, userInfo: nil)
            
        }
    }
    @IBAction func paypalSigninClicked(_ sender: Any) {
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddingBankAccController {
    
    func addNotification() {
       
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(dismissView),
                                               name:
            NSNotification.Name(rawValue: APPNotifications.dismissLoginView),
                                               object: nil)
    }
    
    @objc func  dismissView(info: NSNotification) {
        self.dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue:
            APPNotifications.changeRootView),
                                        object: nil, userInfo: nil)

    }
}
