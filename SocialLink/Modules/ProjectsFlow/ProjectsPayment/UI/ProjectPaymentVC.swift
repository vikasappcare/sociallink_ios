//
//  ProjectPaymentVC.swift
//  SocialLink
//
//  Created by Santhosh on 22/04/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ProjectPaymentVC: AppBaseVC {
    
    var viewModel: TaskListViewModel?
    var detailViewModel: TaskDetailViewModel?
    var sum = [0]
    
    
    var selectedProject: Project?
    @IBOutlet weak var paymentTable: UITableView!
    
    
    var filteredProjectsArray = [Task]()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = TaskListViewModel()
        detailViewModel = TaskDetailViewModel()
        // Do any additional setup after loading the view.
    }
    func setProject(project: Project) {
        self.selectedProject = project
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.setTableViewDatasource()
        self.navigationItem.title = "MY PAYMENTS"
        filteredProjectsArray = []
        getTasks(background: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getTasks(background: Bool) {
        if let model = viewModel {
            if let project = selectedProject {
                if !background {
                    self.showHUD(title: "Loading...")
                }
                let dataIs = ["project_id": project.project_id ?? 0] as [String : Any]
                self.addActivityIndicatior()
                DispatchQueue.global(qos: .background).async {
                    model.getProjectTasks(parameters: dataIs, onSuccess: { [weak self] (object) in
                        dprint(object: "Success")
                        DispatchQueue.main.async {
                            model.projectTaskListArray = object?.list
                            if model.projectTaskListArray == nil {
                                model.projectTaskListArray = [Task]()
                            }
                            for i in 0..<model.projectTaskListArray!.count{
                                if model.projectTaskListArray![i].status == "2"
                                {
                                    self?.filteredProjectsArray.append(model.projectTaskListArray![i])
                                    
                                }
                            }
                            
                            
                            //self?.updateTasks()
                            self?.paymentTable.reloadData()
                            self?.hideHud()
                            self?.removeActivityIndicator()
                        }
                        }, onError: { (error) in
                            Logger.log(message: "Error \(error.statusMessage)", event: .error)
                            //  self.showAlert(title: Message.Alert, message: error.statusMessage)
                            self.hideHud()
                            self.removeActivityIndicator()
                            
                    })
                }
                
            }
            
        }
        
    }
    
    
    
}
extension ProjectPaymentVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.paymentTable.delegate = self
        self.paymentTable.dataSource = self
        self.paymentTable.estimatedRowHeight = 300
        self.paymentTable.rowHeight = UITableViewAutomaticDimension
        self.paymentTable.tableFooterView = UIView()
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        return view
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return filteredProjectsArray.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:
            CellIdentifiers.ProjectPaymentCellIdentifier, for: indexPath)
        if let cellIs = cell as? ProjectPaymentCell {
            cellIs.selectionStyle = .none
            cellIs.bgView.backgroundColor = .white
            if APPStore.sharedStore.appUserID == "1"{
                
                cellIs.paynowButton.isHidden = true
            }
            else{
                cellIs.paynowButton.isHidden = false
            }
            let taskId = filteredProjectsArray[indexPath.row].task_id
            cellIs.setData(data: filteredProjectsArray[indexPath.row])
            
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 220
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if APPStore.sharedStore.appUserID == "2"{
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "PaymentController") as? PaymentController {
                controller.setProject(task: filteredProjectsArray[indexPath.row], project: selectedProject!)
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
        
    }
    
}
