//
//  NewEventVC.swift
//  SocialLink
//
//  Created by Santhosh on 22/04/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class NewEventVC: UIViewController {

    let titleData = ["Add Guests", "", "Does not repeat", "10 minutes before", "Add note"]
    let iconData = ["EventGuest", "EventTimer", "EventRepeat", "EventAlrm", "EventNotes"]

    @IBOutlet weak var newEventTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTableViewDatasource()
self.createNavigationItems()
        
        self.navigationItem.title = "NEW EVENT"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension NewEventVC {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
        
        let postButton = UIBarButtonItem(title: "Post", style: .done, target: self, action: #selector(postClicked))
         self.navigationItem.rightBarButtonItem = postButton
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
    @objc func postClicked() {
        
    }
    
    
    
}
extension NewEventVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.newEventTable.delegate = self
        self.newEventTable.dataSource = self
        self.newEventTable.estimatedRowHeight = 300
        self.newEventTable.rowHeight = UITableViewAutomaticDimension
        self.newEventTable.tableFooterView = UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.EventTimeCellIdentifier, for: indexPath)
            if let cellIs = cell as? EventTimeCell {
                cellIs.selectionStyle = .none
                let data = ["icon": iconData[indexPath.row]] as [String: Any]
                cellIs.setData(data: data)
            }
            cell.selectionStyle = .none
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.EventTableViewCellIdentifier, for: indexPath)
            if let cellIs = cell as? EventTableViewCell {
                cellIs.selectionStyle = .none
                let data = ["title": titleData[indexPath.row],
                            "icon": iconData[indexPath.row]] as [String: Any]
                cellIs.setData(data: data)
            }
            cell.selectionStyle = .none
            return cell
        }
        
        
        
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    
}
