//
//  ListOfBanksModel.swift
//  SocialLink
//
//  Created by admin on 3/6/19.
//  Copyright © 2019 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON

class AttachmentsModel: AppBaseModel {
    
    var doc_id = [String]()
    var project_id = [String]()
    var dname = [String]()
    var description = [String]()
    var pupload = [String]()

    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    private func parseResponse(with response: JSON) {
        
        for i in 0..<response.count{
            self.doc_id.append(response[i]["doc_id"].stringValue)
            self.project_id .append(response[i]["project_id"].stringValue)
            self.dname .append(response[i]["dname"].stringValue)
            self.description .append(response[i]["description"].stringValue)
            self.pupload .append(response[i]["pupload"].stringValue)
        }
        
    }
    
}
