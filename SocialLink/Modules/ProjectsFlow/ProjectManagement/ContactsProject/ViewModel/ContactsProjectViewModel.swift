//
//  ProjectContactsViewModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 09/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation

class ContactsProjectViewModel {
    
    init() {
    }
    var contactsArray: [ProjectContact]?
    
//    func getUserContacts(parameters: [String: Any],
//                      onSuccess: @escaping (UserContactsModel?) -> Void,
//                      onError: @escaping APIErrorHandler) {
//        AppDataManager.getUserContacts(parametes: parameters, onSuccess: { (dataSource) in
//            dprint(object: "Success")
//            if self.contactsArray == nil {
//                self.contactsArray = [UserContact]()
//            }
//            self.contactsArray = dataSource?.data
//            onSuccess(dataSource)
//        }, onFailure: { (error) in
//            onError(error)
//            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
//        })
//    }
    
    func getProjectContacts(parameters: [String: Any],
                            onSuccess: @escaping (ProjectContactsModel?) -> Void,
                            onError: @escaping APIErrorHandler) {
        AppDataManager.getProjectContacts(parametes: parameters, onSuccess: { (dataSource) in
            if self.contactsArray == nil {
                self.contactsArray = [ProjectContact]()
            }
            self.contactsArray = dataSource?.list
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
    
    
}
