//
//  CityModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 16/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON

class CityModel: AppBaseModel {
    
    var service: String?
    var data: [City]?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    
    private func parseResponse(with response: JSON) {
        self.service = response["service"].stringValue
        for item in response["data"].arrayValue {
            if let model = self.createObject(with: item) {
                if self.data == nil {
                    self.data = [City]()
                }
                self.data?.append(model)
            }
        }
    }
    
    private func createObject(with jsonObject: JSON) -> City? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return City.init(jsonObject)
        }
    }
    
}

class City: AppBaseModel {
    
    var id: Int64?
    var name: String?
    var country_id: Int64?
    var state_id: Int64?

    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    
    private func parseResponse(with response: JSON) {
        self.id = response["id"].int64Value
        self.name = response["name"].stringValue
        self.country_id = response["country_id"].int64Value
        self.state_id = response["state_id"].int64Value

    }
    
}



