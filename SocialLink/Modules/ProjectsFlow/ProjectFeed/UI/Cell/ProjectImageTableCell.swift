
//
//  ProjectImageTableCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 24/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

protocol ProjectImageTableCellDelegate: class {
    func imageLikeSelected(feedID: Int64, likeUnlike: Int, forRow: Int)
    func imageShowLikesClicked(feedId: Int64)
    func imageShowCommentsClicked(feedId: Int64)
    func imageDispalyComments(feedId: Int64)
}

class ProjectImageTableCell: UITableViewCell {

    weak var delegate: ProjectImageTableCellDelegate?
    @IBOutlet weak var contentImage: UIImageView!
    @IBOutlet weak var contentText: UILabel!
    @IBOutlet weak var designation: UILabel!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var displayLikesButton: UIButton!
    @IBOutlet weak var displayCommentsButton: UIButton!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    
    var feedId: Int64?
    var likeUnlike: String?
    var likesCount : Int64?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        profileImage.layer.cornerRadius = profileImage.frame.size.width / 2
        profileImage.layer.masksToBounds = true
        //moreButton.isHidden = true
        shareButton.isHidden = true
    }
    
    
    func updateLikeCell(likeData: Feed) {
        switch likeData.userliked {
        case "0":
            likeButton.isSelected = false
            likeUnlike = "0"
         likeButton.setImage(UIImage(named: "PostLike"), for: .normal)
        case "1":
            likeButton.isSelected = true
            likeUnlike = "1"
        likeButton.setImage(UIImage(named: "PostLike_Selected"), for: .selected)
        default:
            break
        }
        displayLikesButton.setTitle("\(String(describing: likesCount)) Likes", for: .normal)
    }
    
    func setData(feed: Feed, tag: Int, profile: ProfileModel) {
        feedId = feed.feed_id
        likeUnlike = feed.userliked
        if likeUnlike == "1"{
            likeButton.setImage(UIImage(named: "PostLike_Selected"), for: .normal)

        }
        else{
            likeUnlike = "0"
            likeButton.setImage(UIImage(named: "PostLike"), for: .normal)
        }
        
        let userProfileImage = APPURL.feedUserProfileImage  + feed.userimage!
        profileImage.sd_setImage(with: URL(string: userProfileImage), placeholderImage: UIImage(named: ""))
        profileName.text = feed.username ?? ""
        designation.text = feed.userdesignation ?? ""
        contentText.text = feed.feed_details ?? ""
        let imageString = APPURL.feedImages + feed.feed_document!
        print("Feed image is \(imageString)")
        contentImage?.sd_setImage(with: URL(string: imageString), placeholderImage: UIImage(named: ""))
        likeButton.tag = tag
        likesCount = feed.likesCount
        commentsCount = feed.commentsCount
        displayLikesButton.setTitle("\(String(describing: feed.likesCount ?? 0)) Likes", for: .normal)
        displayLikesButton.tag = tag
        commentButton.tag = tag
        displayCommentsButton.setTitle("\(String(describing: feed.commentsCount ?? 0)) Comments", for: .normal)
        

        switch feed.feed_extension {
        case FileType.JPEG:
            contentImage.sd_setImage(with: URL(string: imageString), placeholderImage: #imageLiteral(resourceName: "ImagePlaceHolder"), options: .refreshCached, completed: nil)            
            if let image = contentImage.image {
                let aspect = image.size.width / image.size.height
                self.contentImage.frame = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
                let constraint = NSLayoutConstraint(item: self.contentImage, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: self.contentImage, attribute: NSLayoutAttribute.height, multiplier: aspect, constant: 0.0)
                constraint.priority = UILayoutPriority(rawValue: 999)
                self.aspectConstraint = constraint
                self.contentImage.image = image
                
            }
        case FileType.PDF:
            contentImage.image = UIImage(named: "PdfPlaceHolder")
        case FileType.DOC , FileType.DOCX:
            contentImage.image = UIImage(named: "WordPlaceHolder")
        case FileType.PPT , FileType.PPTX:
            contentImage.image = UIImage(named: "PPTPlaceHolder")
        case FileType.XLS , FileType.XLSX:
            contentImage.image = UIImage(named: "EXCELPlaceHolder")
        default:
            break
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    func updateComments()
    {
        displayCommentsButton.setTitle("\(String(describing: commentsCount)) Comments", for: .normal)
        
    }
    @IBAction func commentClicked(_ sender: Any) {
        if let idIs = feedId {
            self.delegate?.imageShowCommentsClicked(feedId: idIs)
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func likeClicked(_ sender: UIButton) {
        if let id = feedId, let liked = likeUnlike {
            var likeUnlikeIs = 0
            switch liked {
            case "0":
                likeUnlikeIs = 1
                likeUnlike = "1"
                likeButton.isSelected = true
                likeButton.setImage(UIImage(named: "PostLike_Selected"), for: .selected)//PostLike
                displayLikesButton.setTitle("\(String(describing: likesCount! + 1)) Likes", for: .normal)
                
            case "1":
                likeButton.isSelected = false
                likeButton.setImage(UIImage(named: "PostLike"), for: .normal)
                likeUnlikeIs = 0
                likeUnlike = "0"

                if likesCount == 0{
                    displayLikesButton.setTitle("\(String(describing: likesCount!)) Likes", for: .normal)
                }
                else
                {
                    displayLikesButton.setTitle("\(String(describing: likesCount! - 1)) Likes", for: .normal)
                }
            default:
                break
            }
            self.delegate?.imageLikeSelected(feedID: id, likeUnlike: likeUnlikeIs, forRow: sender.tag)
        }
    }
    
    @IBAction func shareClicked(_ sender: Any) {
    }
    
    @IBAction func displayLikeClicked(_ sender: Any) {
        if let idIs = feedId {
            self.delegate?.imageShowLikesClicked(feedId: idIs)
        }
    }
    @IBAction func displayCommentClicked(_ sender: Any) {
        if let idIs = feedId {
            self.delegate?.imageDispalyComments(feedId: idIs)
        }
    }
    
    internal var aspectConstraint : NSLayoutConstraint? {
        didSet {
            if oldValue != nil {
                contentImage.removeConstraint(oldValue!)
            }
            if aspectConstraint != nil {
                contentImage.addConstraint(aspectConstraint!)
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        aspectConstraint = nil
    }
    
}
