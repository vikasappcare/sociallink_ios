//
//  TaskDescriptionCell.swift
//  SocialLink
//
//  Created by Santhosh on 09/05/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

protocol TaskDescriptionCellDelegate: class {
    
    func descriptionSelected(description: String)
}

class TaskDescriptionCell: UITableViewCell {
    weak var delegate: TaskDescriptionCellDelegate?
    @IBOutlet weak var taskDescription: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        taskDescription.delegate = self
    }
    
    
    func setData(data: String) {
        taskDescription.text = data
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension TaskDescriptionCell: UITextViewDelegate {
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.delegate?.descriptionSelected(description: textView.text)
    }
    
}
