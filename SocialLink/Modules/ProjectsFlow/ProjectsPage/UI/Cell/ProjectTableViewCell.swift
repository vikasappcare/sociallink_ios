//
//  ProjectTableViewCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ProjectTableViewCell: UITableViewCell {

    @IBOutlet weak var projectId: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var endDate: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var cornerImage: UIImageView!
    @IBOutlet weak var estimatedtime: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var quotationButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(data: Project, color: UIColor, index: Int) {
        projectId.text = "\(String(describing: data.project_name ?? ""))"
        self.startDate.text = data.start_date
        self.endDate.text = data.endDate
        ///statusLabel.text = "\(String(describing: data.statusIs))"
        statusLabel.backgroundColor = color
        statusLabel.text = APPUtility.getProjectStatus(value: data.statusIs ?? 5)
        //estimatedtime.text = data.estimatedTime
        descriptionLabel.text = data.description
        
        
        if APPStore.sharedStore.appUserID == "1"{
            quotationButton.isHidden = false
        }
        else
        {
            quotationButton.isHidden = true
        }
        if index == 0{
            if APPStore.sharedStore.appUserID == "1"{
                quotationButton.isHidden = false
            }
            else
            {
                quotationButton.isHidden = true
            }
        }
        else{
            quotationButton.isHidden = true
        }
    }
//    func setData(data: [String: Any]?) {
//         statusLabel.text = data!["projectStatus"] as? String
//        statusLabel.backgroundColor = data!["color"] as? UIColor
//
//
//    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
