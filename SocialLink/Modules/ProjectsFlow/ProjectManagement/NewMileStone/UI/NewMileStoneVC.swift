//
//  NewMileStoneVC.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 09/05/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class NewMileStoneVC: UIViewController {
    
    @IBOutlet weak var milestoneTable: UITableView!
    var milestoneType: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTableViewDatasource()
        self.createNavigationItems()
        
     ///   self.navigationController?.navigationItem.title = "New MileStone"
        // Do any additional setup after loading the view.
        
        switch milestoneType {
        case MILESTONETYPE.NEW:
            self.navigationItem.title = "New Milestone"
        default:
            self.navigationItem.title = "Edit Milestone"
        }
    }
    
    func milestoneType(type: String) {
        milestoneType = type
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NewMileStoneVC {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension NewMileStoneVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.milestoneTable.delegate = self
        self.milestoneTable.dataSource = self
        self.milestoneTable.estimatedRowHeight = 300
        self.milestoneTable.rowHeight = UITableViewAutomaticDimension
        self.milestoneTable.tableFooterView = UIView()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            switch milestoneType {
            case MILESTONETYPE.NEW:
                return 0
            default:
                return 2
            }
        
        default:
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.NewMileStoneCellIdentifier, for: indexPath)
            if let cellIs = cell as? NewMileStoneCell {
                cellIs.selectionStyle = .none
                cellIs.delegate = self
            }
            cell.selectionStyle = .none
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.NewMilestoneTaskCellIdentifier, for: indexPath)
            if let cellIs = cell as? NewMilestoneTaskCell {
                cellIs.selectionStyle = .none
            }
            cell.selectionStyle = .none
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.NewMilestoneAddCellIdentifier, for: indexPath)
            if let cellIs = cell as? NewMilestoneAddCell {
                cellIs.selectionStyle = .none
            }
            cell.selectionStyle = .none
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.SaveDeleteCellIdentifier, for: indexPath)
            if let cellIs = cell as? SaveDeleteCell {
                cellIs.selectionStyle = .none
            }
            cell.selectionStyle = .none
            return cell
            
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 2:
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProjectNewTaskVC") as? ProjectNewTaskVC {
                let navControl = APPNavigationController(rootViewController: controller)
                self.present(navControl, animated: true, completion: nil)
            }
            print("Clicked")
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 1:
            switch milestoneType {
            case MILESTONETYPE.NEW:
                return 0
            default:
                return 30
            }
            
        default:
            return 0
        }
    }
    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        switch section {
//        case 1:
//            return "Tasks"
//        default:
//            return ""
//        }
//    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.appLightGrayColor
        let label = UILabel(frame: CGRect(x: 15, y: 0, width: 300, height: 30))
        label.text = "Tasks"
        label.font = UIFont.semiBoldFont(16)
        headerView.addSubview(label)
        return headerView
    }
    
    /*func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        switch indexPath.section {
        case 0:
            return true
        default:
            return false
        }
        
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        
        
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { action, index in
            
        }
        edit.backgroundColor = UIColor.lightGray
        
        let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
            print("more button tapped")
        }
        delete.backgroundColor = UIColor.darkGray
        
        return [delete, edit]
        
    }*/
    
    
    
    
}

extension NewMileStoneVC: NewMileStoneCellDelegate {
    func startDateClicked(withTag: Int) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProjectCalenderVC") as? ProjectCalenderVC {
            let navController = APPNavigationController(rootViewController: controller)
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    func endDateClicked(withTag: Int) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProjectCalenderVC") as? ProjectCalenderVC {
            let navController = APPNavigationController(rootViewController: controller)
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    
    
}
