//
//  POSDropDownCell.swift
//  POSSalesSheet
//
//  Created by Marripelli, Santosh Kumar on 05/07/17.
//  Copyright © 2017 Verizon. All rights reserved.
//


import UIKit

open class POSDropDownCell: UITableViewCell {
    
    //UI
    @IBOutlet weak private var optionLabel: UILabel!
    @IBOutlet weak fileprivate var separatorView: UIView!

    var selectedBackgroundColor: UIColor?
    
    func updateOptionalLabel(textColor: UIColor, textFont: UIFont, textValue: String) {
       // setupAccessibilityIdentifier()
        optionLabel.textColor = textColor
        optionLabel.font = textFont
        optionLabel.text = textValue
    }
    
//    func setupAccessibilityIdentifier() {
//        self.optionLabel.isAccessibilityElement = true
//        self.optionLabel.accessibilityIdentifier = POSAutomationConstants.DeviceGrid.dropDownTitle
//    }
    
}

// MARK: - UI

extension POSDropDownCell {
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = .clear
    }
    
    override open var isSelected: Bool {
        willSet {
            setSelected(newValue, animated: false)
        }
    }
    
    override open var isHighlighted: Bool {
        willSet {
            setSelected(newValue, animated: false)
        }
    }
    
    override open func setHighlighted(_ highlighted: Bool, animated: Bool) {
        setSelected(highlighted, animated: animated)
    }
    
    override open func setSelected(_ selected: Bool, animated: Bool) {
        let executeSelection: () -> Void = { [unowned self] in
            if let selectedBackgroundColor = self.selectedBackgroundColor {
                if selected {
                    self.backgroundColor = selectedBackgroundColor
                } else {
                    self.backgroundColor = .clear
                }
            }
        }
        
        if animated {
            UIView.animate(withDuration: 0.3, animations: {
                executeSelection()
            })
        } else {
            executeSelection()
        }
    }
    
    
}
