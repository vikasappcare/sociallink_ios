//
//  ForgetPasswordViewModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 25/06/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation

class ForgetPasswordViewModel {
    
    init() {
    }
    
    func validateEmail(parameters: [String: Any],
                      onSuccess: @escaping (OTPModule?) -> Void,
                      onError: @escaping APIErrorHandler) {
        AppDataManager.validateEmail(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: dataSource)
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
    func confirmOTP(parameters: [String: Any],
                       onSuccess: @escaping (Bool?) -> Void,
                       onError: @escaping APIErrorHandler) {
        AppDataManager.confirmOTP(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: dataSource)
            // onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
    
    func updatePassword(parameters: [String: Any],
                    onSuccess: @escaping (Bool?) -> Void,
                    onError: @escaping APIErrorHandler) {
        AppDataManager.updatePassword(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: dataSource)
             onSuccess(true)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
}
