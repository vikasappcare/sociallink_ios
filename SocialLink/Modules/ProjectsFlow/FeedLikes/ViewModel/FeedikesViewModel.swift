//
//  FeedikesModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 07/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation


class FeedikesViewModel {
    
    init() {
    }
    
    func getLikes(parameters: [String: Any],
                        onSuccess: @escaping (FeedLikesModel?) -> Void,
                        onError: @escaping APIErrorHandler) {
        AppDataManager.getFeedLikes(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: "Success")
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
    
    
}
