//
//  SlackAuthModel.swift
//  SocialLink
//
//  Created by Harsha on 23/04/19.
//

import Foundation
import SwiftyJSON

class SlackAuthModel: AppBaseModel {
    
    var ok: String?
    var access_token: String?
    var scope: String?
    var user_id: String?
    var team_name: String?
    var team_id: String?
    var channel: String?
    var channel_id: String?
    var configuration_url: String?
    var url: String?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    
    private func parseResponse(with response: JSON) {
        self.ok = response["ok"].stringValue
        self.access_token = response["access_token"].stringValue
        self.scope = response["scope"].stringValue
        self.user_id = response["user_id"].stringValue
        self.team_name = response["team_name"].stringValue
        self.team_id = response["team_id"].stringValue
        self.channel = response["incoming_webhook"]["channel"].stringValue
        self.channel_id = response["incoming_webhook"]["channel_id"].stringValue
        self.configuration_url = response["incoming_webhook"]["configuration_url"].stringValue
        self.url = response["incoming_webhook"]["url"].stringValue

        
    }
    
   
}

//class WebhookModel: AppBaseModel {
//    var channel: String?
//    var channel_id: String?
//    var configuration_url: String?
//    var url: String?
//
//    override init() {
//        super.init()
//    }
//
//    init?(_ response: JSON) {
//        if response.isEmpty {
//            return nil
//        }
//        super.init(with: response)
//
//        if response.exists() {
//            self.parseResponse(with: response)
//        }    }
//
//    private func parseResponse(with response: JSON) {
//        print("parseResponse : ", response)
//        self.channel = response["channel"].stringValue
//        self.channel_id = response["channel_id"].stringValue
//        self.configuration_url = response["configuration_url"].stringValue
//        self.url = response["url"].stringValue
//
//    }
//}
