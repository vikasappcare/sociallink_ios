//
//  ContactDetailCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 11/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ContactDetailCell: UITableViewCell {

    @IBOutlet weak var keyLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(data: [String: Any]?) {
        keyLabel.text = data!["key"] as? String
        valueLabel.text = data!["value"] as? String
        valueLabel.textColor = data!["color"] as? UIColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
