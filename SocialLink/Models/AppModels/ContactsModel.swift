//
//  ContactsModel.swift
//  SocialLink
//
//  Created by Harsha on 02/04/19.
//  Copyright © 2019 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON

class ChannelsList: AppBaseModel {
    var list: [ContactsModel]?
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    
    private func parseResponse(with response: JSON) {
        
       
        for item in response.arrayValue {
            if let channelModel = self.createObject(with: item) {
                if self.list == nil {
                    self.list = [ContactsModel]()
                }
                self.list?.append(channelModel)
            }
        }
        
    }
    
    private func createObject(with jsonObject: JSON) -> ContactsModel? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return ContactsModel.init(jsonObject)
        }
    }
    
}
class ContactsModel: AppBaseModel {

    var id: String?
    var name: String?
    var is_channel: Bool?
    var created: Int64?
    var is_archived: Bool?
    var is_general: Bool?
    var unlinked: Int64?
    var creator: String?
    var name_normalized: String?
    var is_shared: Bool?
    var is_org_shared: Bool?
    var is_member: Bool?
    var is_private: Bool?
    var is_mpim: Bool?
    
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        
        print("Response for channels:", response)
        self.id = response["id"].stringValue
        self.name = response["name"].stringValue
        self.is_channel = response["is_channel"].bool
        self.created = response["created"].int64Value
        self.is_archived = response["is_archived"].bool
        self.is_general = response["is_general"].bool
        self.unlinked = response["unlinked"].int64Value
        self.creator = response["creator"].stringValue
        self.name_normalized = response["name_normalized"].stringValue
        self.is_shared = response["is_shared"].bool
        self.is_org_shared = response["is_org_shared"].bool
        self.is_member = response["is_member"].bool
        self.is_private = response["is_private"].bool
        self.is_mpim = response["is_mpim"].bool
        
    }
}
