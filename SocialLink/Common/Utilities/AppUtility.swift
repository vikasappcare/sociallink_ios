//
//  AppUtility.swift
//  BaseProject
//
//  Created by Santhosh Marripelli on 23/02/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON

struct APPUtility {
    
    static public func setUserDefaultsForBool(key: String?, bool: Bool?) {
        let defaults = UserDefaults.standard
        defaults.set(bool, forKey: key ?? "")
    }
    
    static public func getUserDefaultsForBool(key: String?) -> Bool? {
        let defaults = UserDefaults.standard
        return defaults.value(forKey: key ?? "") as? Bool
    }
    
    static public func setUserDefaultsForKey(key: String?, value: Any?) {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key ?? "")
    }
    
    static public func getUserDefaultsForKey(key: String?) -> Any? {
        let defaults = UserDefaults.standard
        return defaults.value(forKey: key ?? "")
    }
 
    
    
}

extension APPUtility {
    
    static func validateEmail(enteredEmail: String) -> Bool {
        print(enteredEmail)
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format: "SELF MATCHES %@", emailFormat)
        print(emailPredicate)
        return emailPredicate.evaluate(with: enteredEmail)
    }
    
   static func verifyUrl (urlString: String?) -> Bool {
        if let urlString = urlString {
            var finalString = ""
            if !urlString.contains("http") {
                finalString = "http://" + urlString
            }
            if let url = URL(string: finalString) {
                print(url)
                return UIApplication.shared.canOpenURL(url)
            }
        }
        return false
    }
    
    
    static func getProjectStatus(value: Int64) -> String {
        var returnValue = ""
        switch value {
        case 0:
            returnValue = "Open"
        case 1:
            returnValue = "Active"
        case 2:
            returnValue = "Completed"
        case 3:
            returnValue = "Cancelled"
        default:
            break
        }
        return returnValue
    }


    

    
    
}
// MARK: DATE LOGICS
extension APPUtility {
    
    private static var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = Constant.dateFormat
        formatter.locale = Locale.current
        formatter.timeZone = TimeZone.current
        return formatter
    }
    
    
    
    static func getPresentDate() -> String {
        let dateNow = Date().toGlobalTime()
        return self.convertDateToString(date: dateNow)
    }
    
    
    static func convertDateFormat(from: String, toFromat: String, date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = from
        let dateIs = dateFormatter.date(from: date)
        dateFormatter.dateFormat = toFromat
        if let dateIs = dateIs {
            return  dateFormatter.string(from: dateIs)
        }
        return ""
    }
    
    
    static func convertUTCToClaenderFormat(date: String) -> String {
        let localDate = self.convertUTCDateToLocalDate(utcString: date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constant.globalDateFormat
        let dateIs = dateFormatter.date(from: localDate)
        dateFormatter.dateFormat = Constant.calenderDateFormat
        if let dateIss = dateIs {
            return  dateFormatter.string(from: dateIss)
        }
        return ""
    }
    
    static func convertUTCToDateAndTime(date: String) -> String {
        let localDate = self.convertUTCDateToLocalDate(utcString: date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constant.globalDateFormat
        let dateIs = dateFormatter.date(from: localDate)
        dateFormatter.dateFormat = Constant.calenderDateFormatDateAndTime
        if let dateIss = dateIs {
            return  dateFormatter.string(from: dateIss)
        }
        return ""
    }
    static func convertUnixToTime(unix: Double) -> String
    {
//        let timeInterval  = 1415639000.67457
//        print("time interval is \(timeInterval)")
//
        //Convert to Date
        let date = NSDate(timeIntervalSince1970: unix)
        
        //Date formatting
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:a"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let dateString = dateFormatter.string(from: date as Date)
        print("formatted date is =  \(dateString)")
        return dateString
    }
    static func convertUTCToClaenderFormatDate(date: String) -> Date {
        let localDate = self.convertUTCDateToLocalDate(utcString: date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constant.globalDateFormat
        let dateIs = dateFormatter.date(from: localDate)
        return dateIs ?? Date()
    }
    
    static func getDateFromString(dateString: String, dateFormat: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        let date = dateFormatter.date(from: dateString)
        return date
    }
    
    
    static func convertDateToString(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constant.globalDateFormat
        let returnString = dateFormatter.string(from: date)
        return returnString
    }
    
    
    static func zeroTimeString(date: String) -> String {
        let zeroDate = date.appending("T00:00:00")
        return zeroDate
    }
    
    
    
    static func convertUTCDateToLocalDate(utcString: String) -> String {
        let utcDate = self.getDateFromString(dateString: utcString, dateFormat: Constant.globalDateFormat) ///// UTC DATE
        if let localDate = utcDate?.toLocalTime() { //// Local Date
            let localDateString = self.convertDateToString(date: localDate)  /// Local Date String
            return localDateString
        }
        return ""
    }
    
    
    
    ////////// TIme Difference
    
  
    static func differenceFromDateToDate(from: String, toDate: String) -> String {
        let fromDateStr = APPUtility.convertUTCToClaenderFormatDate(date: from)
        var toDateStr: Date?
        var difference = ""
        if !toDate.isEmpty {
            toDateStr = APPUtility.convertUTCToClaenderFormatDate(date: toDate)
             difference = APPUtility.differcnceBetweenDatesinHours(fromDate: fromDateStr, toDate: toDateStr ?? Date())
        } else {
            toDateStr = APPUtility.convertUTCToClaenderFormatDate(date: APPUtility.getPresentDate())
            difference = APPUtility.differcnceBetweenDatesinMinutes(fromDate: fromDateStr, toDate: toDateStr ?? Date())
        }
        
        return difference
    }
    
   static func differcnceBetweenDatesinHours(fromDate : Date, toDate: Date) -> String {
        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: fromDate, to: toDate);
  
        let seconds = "\(difference.second ?? 0)"
        let minutes = "\(difference.minute ?? 0)" + "||" + seconds
        let hours = "\(difference.hour ?? 0)" + "||" + minutes
        let days = "\(difference.day ?? 0)" + "||" + hours
        if let day = difference.day, day          > 0 { return days }
        if let hour = difference.hour, hour       > 0 { return hours }
        if let minute = difference.minute, minute > 0 { return minutes }
        if let second = difference.second, second > 0 { return seconds }
        return ""
    }
    
    static func differcnceBetweenDatesinHoursToDisplay(fromDate : Date, toDate: Date) -> String {
        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: fromDate, to: toDate);
        
        let seconds = "\(difference.second ?? 0)s"
        let minutes = "\(difference.minute ?? 0)m" + " " + seconds
        let hours = "\(difference.hour ?? 0)h" + " " + minutes
        let days = "\(difference.day ?? 0)d" + " " + hours
        if let day = difference.day, day          > 0 { return days }
        if let hour = difference.hour, hour       > 0 { return hours }
        if let minute = difference.minute, minute > 0 { return minutes }
        if let second = difference.second, second > 0 { return seconds }
        return ""
    }
    
    static func differcnceBetweenDatesinMinutes(fromDate : Date, toDate: Date) -> String {
        let minuteDifference = NSCalendar.current.dateComponents([.minute], from: fromDate, to: toDate)
        print("\(String(describing: minuteDifference.minute))")
        let minutes = "\(minuteDifference.minute ?? 0)"
        if let minute = minuteDifference.minute, minute > 0 { return minutes }
        return ""
    }
    
    
   static func validateDates(start: String, end: String) -> Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constant.globalDateFormat
        let startDate = dateFormatter.date(from: start)
        let endDate = dateFormatter.date(from: end)
    if endDate != nil{
        if startDate?.compare(endDate!) == .orderedAscending {
            print("First Date is smaller then second date")
            return true
        } else {
            print("First Date is greater then second date")
            return false
        }
    }
    return false
    }
    

static func validateDatesWithoutT(start: String, end: String) -> Bool {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = Constant.globalDateFormatWithoutT
    let startDate = dateFormatter.date(from: start)
    let endDate = dateFormatter.date(from: end)
    if endDate != nil{
        if startDate?.compare(endDate!) == .orderedAscending || startDate?.compare(endDate!) == .orderedSame{
            print("First Date is smaller then second date")
            return true
        } else {
            print("First Date is greater then second date")
            return false
        }
    }
    return false
}

}

extension APPUtility {

    // MARK: DB to Model Conversions
    static func convertDBUserToUser(dbUser: UserDB) -> ProfileModel {
        let userModule = ProfileModel()
        userModule.userId = dbUser.userId
        userModule.email = dbUser.email
        userModule.full_name = dbUser.name
      //  userModule.zipcode = dbUser.pincode
        userModule.phone_number = dbUser.phone_number
        userModule.city = dbUser.city
        userModule.state = dbUser.state
        userModule.country = dbUser.country
        userModule.pincode = dbUser.pincode
        userModule.image = dbUser.image
        userModule.user_type = dbUser.user_type
        userModule.website = dbUser.website
        userModule.created_time = dbUser.created_time
        userModule.gender = dbUser.gender
        userModule.education = dbUser.education
        userModule.skills = dbUser.skills
        userModule.business = dbUser.business
        userModule.industry = dbUser.industry
        userModule.department = dbUser.department
        userModule.job = dbUser.job
        userModule.resume = dbUser.resume
        userModule.designation = dbUser.designation
        return userModule
    }
    
    static func convertDBProjectToProject(dbProject: ProjectsDB) -> Project {
        let projectModule = Project()
        projectModule.project_id = dbProject.projectId
        projectModule.project_name = dbProject.projectName
        projectModule.start_date = dbProject.startDate
        projectModule.endDate = dbProject.endDate
        projectModule.statusIs = dbProject.status
        projectModule.progress = dbProject.progress
        projectModule.skills = dbProject.service
        projectModule.description = dbProject.descriptionIs
        projectModule.estimatedTime = dbProject.estimatedTime
        projectModule.budget = dbProject.budget
        projectModule.cdate = dbProject.cDate
        projectModule.project_Type = dbProject.p_type
        projectModule.user_id = dbProject.createdId
        /////// CreatedBy
        let  createdBy = CreatedBy()
        createdBy.id = dbProject.createdId
        createdBy.name = dbProject.createdName
        createdBy.image = dbProject.createdImage
        createdBy.emailid = dbProject.createdEmail
        ///////
        projectModule.created_by = createdBy
        return projectModule
    }
    
    static func convertDBRoleToRole(object: RolesDB) -> Role {
        let objectModule = Role()
        objectModule.rid = object.rid
        objectModule.name = object.name
        return objectModule
    }
    
    static func convertDBServiceToRole(object: ServicesDB) -> Service {
        let objectModule = Service()
        objectModule.service_id = object.service_id
        objectModule.service_name = object.service_name
        return objectModule
    }
    
    static func convertDBFeedToFeed(dbFeed: FeedsDB) -> Feed {
        let feedModule = Feed()
        feedModule.feed_id = dbFeed.feed_id
        feedModule.project_id = dbFeed.project_id
        feedModule.feed_type = dbFeed.feed_type
        feedModule.feed_type_id = dbFeed.feed_type_id
        feedModule.feed_document = dbFeed.document
        feedModule.feed_details = dbFeed.feed_details
        feedModule.feed_extension = dbFeed.feed_extension
        feedModule.feed_title = dbFeed.feed_title
        /////// FeedLikes
        feedModule.likesCount = dbFeed.likes_count
        feedModule.flikes = dbFeed.flikes
        feedModule.feed_time = dbFeed.feed_time
        ////////////////////////////
        feedModule.comments = dbFeed.comments
        feedModule.commentsCount = dbFeed.commentsCount
        /////// CreatedBy
        feedModule.userid = dbFeed.created_by_ID
        feedModule.username = dbFeed.created_by_Name
        feedModule.userimage = dbFeed.created_by_Image
        feedModule.userdesignation = dbFeed.created_by_Designation
        feedModule.useremailid = dbFeed.created_by_Email
        ///////
        feedModule.created_Date = dbFeed.created_Date
        return feedModule
    }
    
    
    
    static func convertDBTaskToTask(dbTask: TasksDB, timeLogs: [TimeLogsDB]) -> Task {
        print(timeLogs.count)
        let taskModule = Task()
        taskModule.task_id = dbTask.task_id
        taskModule.project_id = dbTask.project_id
        taskModule.project_name = dbTask.project_name
        taskModule.tname = dbTask.task_title
        taskModule.tdetails = dbTask.task_details
        taskModule.tsdate = dbTask.start_date
        taskModule.tedate = dbTask.end_date
        taskModule.task_state = dbTask.task_state
        taskModule.accepted_state = dbTask.accepted_state
       
        /////// CreatedBy
        let  createdBy = CreatedBy()
        createdBy.id = dbTask.created_by_id
        createdBy.name = dbTask.created_by_name
        createdBy.image = dbTask.created_by_image
        createdBy.designation = dbTask.created_by_designation
        createdBy.emailid = dbTask.created_by_email
        ///////
        taskModule.created_by = createdBy
        /////// Assigned To
        let  assignedTo = CreatedBy()
        assignedTo.id = dbTask.assigned_to_id
        assignedTo.name = dbTask.assigned_to_name
        assignedTo.image = dbTask.assigned_to_image
        assignedTo.designation = dbTask.assigned_to_designation
        assignedTo.emailid = dbTask.assigned_to_email
        ///////
        taskModule.assigned_to = assignedTo
        
        /////////////////// TIMELOGS ////
        var timeLogArray = [TimeLog]()
        for log in timeLogs {
            let logIs = TimeLog()
            logIs.task_id = log.task_id
            logIs.start_time = log.start_time
            logIs.time_started = log.time_started
            logIs.end_time = log.end_time
            timeLogArray.append(logIs)
        }
        /////////
        taskModule.timeLog = timeLogArray
        
        return taskModule
    }
    
  static func convertCreatedByToProjectContact(createdBy: CreatedBy) -> ProjectContact {
        let user = ProjectContact()
        user.user_id = createdBy.id
        user.member_name = createdBy.name
        user.phoneNumber = createdBy.phoneNumber
        user.designation = createdBy.designation
        user.email_id = createdBy.emailid
        return user
    }
    
    
    
    
    
    static func converturlToImage(urlString: String) -> UIImage? {
        var returnImage: UIImage?
        if let url = URL(string: urlString) {
            if let data = try? Data(contentsOf: url) {
                if let image: UIImage = UIImage(data: data) {
                    returnImage = image
                }
            }
        }
        
        return returnImage
    }
    
    
    static func timeToHoursMinutesSeconds(time: String) -> String {
        var timeArray = time.components(separatedBy: ":")
        if timeArray.count == 3 {
            let hoursString = timeArray[0] + "hours "
            let minutesString = timeArray[1] + "minutes "
            let secondsString = timeArray[2] + "seconds"
            return hoursString + minutesString + secondsString
        }
        return ""
    }
    
    
    
    
    static func constructTokenObject(data: [String: Any]) -> JSON {
        let token = data["token"]
        let userId = data["uid"]
        let returnData = ["token": token,
                          "userId": userId]
        return JSON(returnData)
    }
    
    
    static func getToken() {
        let tokenData = APPDBUtility.getToken(mocType: .main)
        if let tokenIs = tokenData?.token {
            APPStore.sharedStore.token = tokenIs
        }
    }
    
    
    static func getUserData() {
        let users = APPDBUtility.getUserData(mocType: .main)
        print(users as Any)
        if users?.count ?? 0 > 0 {
            if let user = users {
                APPStore.sharedStore.user = APPUtility.convertDBUserToUser(dbUser: user[0])
                print(user)
                self.setUserType()
            }
        }
    }
    
    static func setUserType() {
        let userType = APPStore.sharedStore.user.user_type
        switch userType {
        case 1:
            APPStore.sharedStore.userIs = USERTYPE.ONLYCONTRACTOR
            UserDefaults.standard.set("1", forKey: "appUserType")
            APPStore.sharedStore.appUserID = "2"
        case 2:
            APPStore.sharedStore.userIs = USERTYPE.EMPLOYER
            UserDefaults.standard.set("2", forKey: "appUserType")
            APPStore.sharedStore.appUserID = "1"
        case 3:
            APPStore.sharedStore.userIs = USERTYPE.CONTRACTOR
            UserDefaults.standard.set("1", forKey: "appUserType")
            APPStore.sharedStore.appUserID = "2"
        default:
            APPStore.sharedStore.userIs = USERTYPE.CONTRACTOR
            UserDefaults.standard.set("1", forKey: "appUserType")
            APPStore.sharedStore.appUserID = "2"
            break
        }
    }
    
    static func getProjects() {
        let projects = APPDBUtility.getProjects(mocType: .main)
        APPStore.sharedStore.projects.removeAll()
        APPStore.sharedStore.contractorProjects.removeAll()
        APPStore.sharedStore.employerProjects.removeAll()
        if projects?.count ?? 0 > 0 {
            if let projectsIs = projects {
                for projectObject in projectsIs {
                    let projectIs = APPUtility.convertDBProjectToProject(dbProject: projectObject)
                    if APPStore.sharedStore.appUserID == "1"{
                        APPStore.sharedStore.contractorProjects.append(projectIs)
                    }
                    else{
                        APPStore.sharedStore.employerProjects.append(projectIs)
                    }
                }
            }
        }
    }
    static func getRoles() {
        let objects = APPDBUtility.getRoles(mocType: .main)
        APPStore.sharedStore.roles.removeAll()
        if objects?.count ?? 0 > 0 {
            if let objectIs = objects {
                for object in objectIs {
                    let convertObject = APPUtility.convertDBRoleToRole(object: object)
                    APPStore.sharedStore.roles.append(convertObject)
                    print(APPStore.sharedStore.roles.count)
                }
            }
        }
    }
    
    static func getServices() {
        let objects = APPDBUtility.getServices(mocType: .main)
        APPStore.sharedStore.services.removeAll()
        if objects?.count ?? 0 > 0 {
            if let objectIs = objects {
                for object in objectIs {
                    let convertObject = APPUtility.convertDBServiceToRole(object: object)
                    APPStore.sharedStore.services.append(convertObject)
                }
            }
        }
    }
    
    static func getFeed(withProjectId id: Int64) {
        let objects = APPDBUtility.getFeeds(mocType: .main, id: id)
        APPStore.sharedStore.feeds.removeAll()
        if objects?.count ?? 0 > 0 {
            if let objectIs = objects {
                for object in objectIs {
                    let convertObject = APPUtility.convertDBFeedToFeed(dbFeed: object)
                    APPStore.sharedStore.feeds.append(convertObject)
                    print(APPStore.sharedStore.feeds.count)
                }
            }
        }
    }
    
    static func getFeedWithFeedId(withFeedId id: Int64) -> Feed? {
        let objects = APPDBUtility.getFeedWithFeedId(mocType: .main, id: id)
        if objects?.count ?? 0 > 0 {
            if let objectIs = objects {
                let convertObject = APPUtility.convertDBFeedToFeed(dbFeed: objectIs[0])
                return convertObject
            }
        }
        return nil
    }
    
    
    static func getTasks(forProjectId: Int64) {
        print(forProjectId)
        let objects = APPDBUtility.getTasks(forId: forProjectId, mocType: .main)
         APPStore.sharedStore.tasks.removeAll()
        if objects?.count ?? 0 > 0 {
            if let objectIs = objects {
                for object in objectIs {
                    if let timeLogs = APPDBUtility.getTimeLogs(forId: object.task_id, mocType: .main) {
                        let convertObject = APPUtility.convertDBTaskToTask(dbTask: object, timeLogs: timeLogs)
                        APPStore.sharedStore.tasks.append(convertObject)
                        print(APPStore.sharedStore.tasks.count)
                    }
                    
                }
            }
        }
    }
    
    
    static func getTaskWithId(forTaskId: Int64) -> Task? {
        if let taskObject = APPDBUtility.getTaskWithTaskId(forId: forTaskId, mocType: .main) {
            if let timeLogs = APPDBUtility.getTimeLogs(forId: forTaskId, mocType: .main) {
                let convertObject = APPUtility.convertDBTaskToTask(dbTask: taskObject, timeLogs: timeLogs)
                return convertObject
            }
        }
        return nil
    }
    
    
    static func getContractorTasks(forUserType: Int64) {
        let objects = APPDBUtility.getContractorTasks(mocType: .main)
        APPStore.sharedStore.contractorTasks.removeAll()
        if objects?.count ?? 0 > 0 {
            if let objectIs = objects {
                for object in objectIs {
                    if let timeLogs = APPDBUtility.getTimeLogs(forId: object.task_id, mocType: .main) {
                        let convertObject = APPUtility.convertDBTaskToTask(dbTask: object, timeLogs: timeLogs)
//                        APPStore.sharedStore.contractorTasks.append(convertObject)
//                        print(APPStore.sharedStore.contractorTasks.count)
                    }
                    
                }
            }
        }
    }
    
    
    static func convertPDFTOData(path: String) -> Data {
        let documentPath = path
        let pdfData = try! Data(contentsOf: documentPath.asURL())
        let data : Data = pdfData
        print(data)
    return data
    }
    
   static func getFileName(path: String) -> String {
        let nameArray = path.components(separatedBy: "/")
        if nameArray.count > 0 {
            return nameArray.last ?? ""
        }
        return ""
    }
    
    
    
    
}

extension APPUtility {
    
    static func userLoggedOut() {
    
        self.deleteAllRecordsFromDB(entity: APPDBConstants.FEED)
        self.deleteAllRecordsFromDB(entity: APPDBConstants.LIKES)
        self.deleteAllRecordsFromDB(entity: APPDBConstants.PROJECT)
        self.deleteAllRecordsFromDB(entity: APPDBConstants.ROLE)
        self.deleteAllRecordsFromDB(entity: APPDBConstants.TASK)
        self.deleteAllRecordsFromDB(entity: APPDBConstants.TOKEN)
        self.deleteAllRecordsFromDB(entity: APPDBConstants.USER)
        self.deleteAllRecordsFromDB(entity: APPDBConstants.TIMELOGS)
        self.deleteAllRecordsFromDB(entity: APPDBConstants.SERVICE)
        
        self.resetDefaults()
        
        NotificationCenter.default.post(name: Notification.Name(APPNotifications.showLoginAfterLogout), object: nil)
        
    }
    
    
    static func deleteAllRecordsFromDB(entity: String) {
        let context = APPDBManager.shared.getMoc(for: .main)
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print ("There was an error")
        }
    }
    
   static func resetDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }

}


