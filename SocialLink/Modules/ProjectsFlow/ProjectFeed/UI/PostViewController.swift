//
//  PostViewController.swift
//  SampleSliding
//
//  Created by Santhosh Marripelli on 20/04/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit
import CTSlidingUpPanel
import MobileCoreServices
protocol PostViewControllerDelegate: class {
    func postedFeed(feedId: Int64)
}


class PostViewController: AppBaseVC {
    weak var delegate: PostViewControllerDelegate?
    let listItems: [String] = ["Attach Photo"]
    let imageItems: [String] = ["addAttachment"]
    var slideUpController: CTBottomSlideController?;
    
    @IBOutlet weak var parentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var postTableView: UITableView!
    @IBOutlet weak var contentTableView: UITableView!
    var selectedImage: UIImage?
    @IBOutlet weak var viewTopConstraint: NSLayoutConstraint!
    var textViewHeight = 30.0
    var imagePicker = UIImagePickerController()
    var viewModel: FeedViewModel?
    var imageSelected: Bool?
    var uploadModel: FileUploadViewModel?
    var fileUploadData: Data?
    var fileExtension: String?
    var fileName: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        uploadModel = FileUploadViewModel()
        
        if viewModel == nil {
            viewModel = FeedViewModel()
        }
        slideUpController = CTBottomSlideController(topConstraint: viewTopConstraint, heightConstraint: parentViewHeight, parent: view, bottomView: parentView, tabController: nil, navController: self.navigationController, visibleHeight: 250)
        
        slideUpController?.set(table: postTableView)
        
        self.setTableViewDatasource()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //// self.contentTableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setProjectId(id: Int64) {
        if viewModel == nil {
            viewModel = FeedViewModel()
        }
        viewModel?.projectId = id
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    @IBAction func cancelClciked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func postClicked(_ sender: Any) {
        updateFeedImage()
    }
    
    func updateFeedImage() {
        var text = ""
        if let model = uploadModel {
            let indexPath = IndexPath(row: 0, section: 0)
            let cellIs = self.contentTableView.cellForRow(at: indexPath)
            if let cell = cellIs as? ContentTableViewCell {
                text = cell.getData()
                if text == "Share an article, photo, task or idea" {
                    text = ""
                }
            }
            if let image = self.selectedImage {
                self.showHUD(title: "")
                if let  data = UIImageJPEGRepresentation(image, 0.1) {
                    //var random = arc4random_uniform(10)
                    let randomName = APPUtility.getPresentDate()
                    let fileNameString = randomName
                        .components(separatedBy:CharacterSet.decimalDigits.inverted)
                        .joined(separator: "")
                    print(fileNameString)
                    
                    var endURL = ""
                    var uploadData = ["fuser": APPStore.sharedStore.user.userId ?? 0,
                                      "file_extension": "jpeg",
                                      "fdetails": text,
                                      "ftime": APPUtility.getPresentDate(),
                                      "cdate": "15-03-2019",
                                      "document": "\(fileNameString).jpeg"] as [String : Any]
                    if viewModel!.projectId != 0 {
                        uploadData["project_id"] =  viewModel!.projectId
                        endURL = APPURL.contractorPostFeed
                    }
                    else{
                        uploadData["project_id"] =  "0"
                        endURL = APPURL.postFeed
                    }
                    print(uploadData)
                    model.requestWith(imageData: data, parameters: uploadData, endURL: endURL, onCompletion: { [weak self] (dataSource) in
                        self?.hideHud()
                               self?.dismiss(animated: true, completion: nil)
                        }, onError: { (error) in
                            self.dismiss(animated: true, completion: nil)
                            self.hideHud()
                    })
                }
                print(image)
            }
            else
            {
            if let model = viewModel {
                var uploadData = ["fuser": APPStore.sharedStore.user.userId ?? 0,
                                  "fdetails": text,
                                  "ftime": APPUtility.getPresentDate(),
                                  "cdate": "15-03-2019"] as [String : Any]
                if viewModel!.projectId != 0 {
                    uploadData["project_id"] =  viewModel!.projectId
                }
                else{
                    uploadData["project_id"] =  "0"
                }
                print(uploadData)
                self.showHUD(title: "Loading...")
                model.postFeed(parameters: uploadData as [String : Any], onSuccess: { [weak self] (dataSource) in
                    self?.hideHud()
                  
                    self?.dismiss(animated: true, completion: nil)

                    },  onError: { (error) in
                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
                       // self.showAlert(title: Message.Alert, message: error.statusMessage)
                      
                        self.dismiss(animated: true, completion: nil)

                        self.hideHud()
                })
            }
        }
        }
    }
    
    func updateFeedFile(feedId: Int64, withExtension: String) {
        if let model = uploadModel {
            self.showHUD(title: "")
            if let data = self.fileUploadData {
                let uploadData = ["token": APPStore.sharedStore.token,
                                  "upload_type": "feed_file",
                                  "file_extension": withExtension,
                                  "id": feedId,
                                  "upload_time": APPUtility.getPresentDate(),
                                  "data": data,
                                  "fileName": self.fileName ?? ""] as [String : Any]
                model.requestWith(imageData: data, parameters: uploadData, endURL: "", onCompletion: { [weak self] (dataSource) in
                    self?.hideHud()
                    if let data = dataSource {
                        //APPDBModel.updateFeedImage(data)
                        DispatchQueue.main.async(execute: {
//                            if let feedIS = dataSource?.data?.id {
//                                self?.delegate?.postedFeed(feedId: feedIS)
//                            }
                            self?.dismiss(animated: true, completion: nil)
                        })
                    }
                    }, onError: { (error) in
                        self.showAlert(title: "Alert", message: error?.localizedDescription ?? "")
                        self.hideHud()
                })
            }
        }
    }
    
}

extension PostViewController: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.contentTableView.delegate = self
        self.contentTableView.dataSource = self
        self.contentTableView.estimatedRowHeight = 300
        self.contentTableView.rowHeight = UITableViewAutomaticDimension
        self.contentTableView.tableFooterView = UIView()
        
        
        self.postTableView.delegate = self
        self.postTableView.dataSource = self
        self.postTableView.estimatedRowHeight = 300
        self.postTableView.rowHeight = UITableViewAutomaticDimension
        self.postTableView.tableFooterView = UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case contentTableView:
            if selectedImage != nil {
                return 2
            }
            return 1
        default:
            return listItems.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
        case contentTableView:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier:
                    CellIdentifiers.ContentTableViewCellIdentifier, for: indexPath)
                if let cellIs = cell as? ContentTableViewCell {
                    cellIs.selectionStyle = .none
                    cellIs.delgate = self
                }
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier:
                    CellIdentifiers.ImagePostCellIdentifier, for: indexPath)
                if let cellIs = cell as? ImagePostCell {
                    cellIs.selectionStyle = .none
                    cellIs.setImage(image: self.selectedImage ?? UIImage())
                }
                return cell
            default:
                return UITableViewCell()
            }
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.PostTypeTableViewCellIdentifier, for: indexPath)
            if let cellIs = cell as? PostTypeTableViewCell {
                cellIs.selectionStyle = .none
                let data = ["image": imageItems[indexPath.row],
                            "text": listItems[indexPath.row]]
                cellIs.setData(data: data)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == contentTableView {
            if indexPath.row == 0 {
                return CGFloat(textViewHeight + 20)
            } else {
                return UITableViewAutomaticDimension
            }
        }
        return UITableViewAutomaticDimension
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        
        if tableView == postTableView {
            switch indexPath.row {
            case 0:
                if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                    imagePicker.delegate = self
                    imagePicker.allowsEditing = false
                    
                    let alertView = UIAlertController(title: "", message: "Choose", preferredStyle: .actionSheet)
                    let camera = UIAlertAction(title: "Camera", style: .default, handler: { (alert) in
                        self.imagePicker.sourceType = .camera
                        self.present(self.imagePicker, animated: true, completion: nil)
                    })
                    alertView.addAction(camera)
                    let gallery = UIAlertAction(title: "Gallery", style: .default, handler: { (alert) in
                        self.imagePicker.sourceType = .photoLibrary
                        self.present(self.imagePicker, animated: true, completion: nil)
                    })
                    alertView.addAction(gallery)
                    let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert) in
                        
                    })
                    alertView.addAction(cancel)
                    
                    self.present(alertView, animated: true, completion: nil)
                }
            case 1:
                //                //////////////////////PDF
                //                let alertView = UIAlertController(title: "", message: "Choose", preferredStyle: .actionSheet)
                //                let pdfAction = UIAlertAction(title: "Add PDF", style: .default, handler: { (alert) in
                ////                    let pdfIs = Bundle.main.url(forResource: "SaplePDF", withExtension: "pdf", subdirectory: nil, localization: nil)
                ////                    let pdfData = APPUtility.convertPDFTOData(path: pdfIs?.absoluteString ?? "")
                ////                    self.pdfData = pdfData
                ////                    self.fileExtension = FileType.PDF
                ////                    self.selectedImage = nil
                ////                    self.setSelectedFileImage(image: "PdfPlaceHolder")
                //
                //                    let documentPicker: UIDocumentPickerViewController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF)], in: UIDocumentPickerMode.import)
                //                    documentPicker.delegate = self
                //                    documentPicker.modalPresentationStyle = UIModalPresentationStyle.fullScreen
                //                    self.present(documentPicker, animated: true, completion: nil)
                //
                //
                //                })
                //                alertView.addAction(pdfAction)
                //                //////////////////////DOC
                //                let wordAction = UIAlertAction(title: "Add Doc", style: .default, handler: { (alert) in
                //                    let pdfIs = Bundle.main.url(forResource: "sample", withExtension: "doc", subdirectory: nil, localization: nil)
                //                    let pdfData = APPUtility.convertPDFTOData(path: pdfIs?.absoluteString ?? "")
                //                    self.fileUploadData = pdfData
                //                    self.fileExtension = FileType.DOC
                //                    self.selectedImage = nil
                //                    self.setSelectedFileImage(image: "WordPlaceHolder")
                //                })
                //                alertView.addAction(wordAction)
                //                //////////////////////DOCX
                //                let wordDocxAction = UIAlertAction(title: "Add Docx", style: .default, handler: { (alert) in
                //                    let pdfIs = Bundle.main.url(forResource: "demo", withExtension: "docx", subdirectory: nil, localization: nil)
                //                    let pdfData = APPUtility.convertPDFTOData(path: pdfIs?.absoluteString ?? "")
                //                    self.fileUploadData = pdfData
                //                    self.fileExtension = FileType.DOCX
                //                    self.selectedImage = nil
                //                    self.setSelectedFileImage(image: "WordPlaceHolder")
                //                })
                //                alertView.addAction(wordDocxAction)
                //                //////////////////////PPT
                //                let pptAction = UIAlertAction(title: "Add PPT", style: .default, handler: { (alert) in
                //                    let pdfIs = Bundle.main.url(forResource: "sample", withExtension: "ppt", subdirectory: nil, localization: nil)
                //                    let pdfData = APPUtility.convertPDFTOData(path: pdfIs?.absoluteString ?? "")
                //                    self.fileUploadData = pdfData
                //                    self.fileExtension = FileType.PPT
                //                    self.selectedImage = nil
                //                    self.setSelectedFileImage(image: "PPTPlaceHolder")
                //                })
                //                alertView.addAction(pptAction)
                //                //////////////////////PPTX
                //                let pptXAction = UIAlertAction(title: "Add PPTX", style: .default, handler: { (alert) in
                //                    let pdfIs = Bundle.main.url(forResource: "sample", withExtension: "pptx", subdirectory: nil, localization: nil)
                //                    let pdfData = APPUtility.convertPDFTOData(path: pdfIs?.absoluteString ?? "")
                //                    self.fileUploadData = pdfData
                //                    self.fileExtension = FileType.PPTX
                //                    self.selectedImage = nil
                //                    self.setSelectedFileImage(image: "PPTPlaceHolder")
                //                })
                //                alertView.addAction(pptXAction)
                //
                //                //////////////////////XLS
                //                let excelAction = UIAlertAction(title: "Add XLS", style: .default, handler: { (alert) in
                //                    let pdfIs = Bundle.main.url(forResource: "sample", withExtension: "xls", subdirectory: nil, localization: nil)
                //                    let pdfData = APPUtility.convertPDFTOData(path: pdfIs?.absoluteString ?? "")
                //                    self.fileUploadData = pdfData
                //                    self.fileExtension = FileType.XLS
                //                    self.selectedImage = nil
                //                    self.setSelectedFileImage(image: "EXCELPlaceHolder")
                //                })
                //                alertView.addAction(excelAction)
                //
                //                //////////////////////XLSX
                //                let excelXAction = UIAlertAction(title: "Add XLSX", style: .default, handler: { (alert) in
                //                    let pdfIs = Bundle.main.url(forResource: "sample", withExtension: "xlsx", subdirectory: nil, localization: nil)
                //                    let pdfData = APPUtility.convertPDFTOData(path: pdfIs?.absoluteString ?? "")
                //                    self.fileUploadData = pdfData
                //                    self.fileExtension = FileType.XLSX
                //                    self.selectedImage = nil
                //                    self.setSelectedFileImage(image: "EXCELPlaceHolder")
                //                })
                //                alertView.addAction(excelXAction)
                //
                //
                //                let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert) in
                //
                //                })
                //                alertView.addAction(cancel)
                //
                //                self.present(alertView, animated: true, completion: nil)
                
                let documentPicker: UIDocumentPickerViewController = UIDocumentPickerViewController(documentTypes: ["public.text", "com.apple.iwork.pages.pages", "public.data"], in: UIDocumentPickerMode.import)
                documentPicker.delegate = self
                documentPicker.modalPresentationStyle = UIModalPresentationStyle.fullScreen
                self.present(documentPicker, animated: true, completion: nil)
                
            case 2:
                if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProjectCalenderVC") as? ProjectCalenderVC {
                    let navController = APPNavigationController(rootViewController: controller)
                    self.present(navController, animated: true, completion: nil)
                }
            case 3:
                if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactsViewController") as? ContactsViewController {
                    controller.showCloseButton(show: true, forScreen: "")
                    let navController = APPNavigationController(rootViewController: controller)
                    self.present(navController, animated: true, completion: nil)
                }
            default:
                break
            }
            
        }
    }
    
}

extension PostViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        self.imageSelected = true
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            print("No image found")
            return
        }
        picker.dismiss(animated: true, completion: {
            self.selectedImage = image
            self.fileUploadData = nil
            let indexPath = IndexPath(row: 0, section: 0)
            let cell = self.contentTableView.cellForRow(at: indexPath)
            if let cellIs = cell as? ImagePostCell {
                cellIs.setImage(image: image)
            }
            self.contentTableView.reloadData()
        })
    }
    
}

extension PostViewController: ContentTableViewCellDelegate {
    func heightOfTextView(height: CGFloat) {
        if height > 300 {
            return
        }
        textViewHeight = Double(height)
        self.contentTableView.beginUpdates()
        self.contentTableView.endUpdates()
    }
    func setSelectedFileImage(image: String) {
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = self.contentTableView.cellForRow(at: indexPath)
        if let cellIs = cell as? ImagePostCell {
            if let image = UIImage(named: image) {
                cellIs.setImage(image: image)
            }
        }
        self.contentTableView.reloadRows(at: [indexPath], with: .automatic)
    }
}

extension PostViewController: UIDocumentMenuDelegate,UIDocumentPickerDelegate {
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let myURL = url as URL
        print("import result : \(myURL)")
        print(myURL.pathExtension)
        let fileData = try! Data(contentsOf: myURL)
        let fileNameIs = APPUtility.getFileName(path: myURL.absoluteString)
        self.fileName = fileNameIs
        print(fileName)
        self.fileUploadData = fileData
        self.selectedImage = nil
        switch myURL.pathExtension {
        case FileType.PDF:
            self.fileExtension = FileType.PDF
            self.setSelectedFileImage(image: "PdfPlaceHolder")
        case FileType.DOC:
            self.fileExtension = FileType.DOC
            self.setSelectedFileImage(image: "WordPlaceHolder")
        case FileType.DOCX:
            self.fileExtension = FileType.DOCX
            self.setSelectedFileImage(image: "WordPlaceHolder")
        case FileType.PPT:
            self.fileExtension = FileType.PPT
            self.setSelectedFileImage(image: "PPTPlaceHolder")
        case FileType.PPTX:
            self.fileExtension = FileType.PPTX
            self.setSelectedFileImage(image: "PPTPlaceHolder")
        case FileType.XLS:
            self.fileExtension = FileType.XLS
            self.setSelectedFileImage(image: "EXCELPlaceHolder")
        case FileType.XLSX:
            self.fileExtension = FileType.XLSX
            self.setSelectedFileImage(image: "EXCELPlaceHolder")
            
        default:
            break
        }
        
    }
    
    
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    
    
    
    
}
