//
//  ProjectPaymentCell.swift
//  SocialLink
//
//  Created by Santhosh on 22/04/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ProjectPaymentCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var taskNameLabel: UILabel!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var dueDateLabel: UILabel!
    @IBOutlet weak var timeWorkedLabel: UILabel!
    @IBOutlet weak var amountBilledLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    @IBOutlet weak var paynowButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setData(data: Task) {
        taskNameLabel.text = data.tname
        startDateLabel.text = data.tsdate
        dueDateLabel.text = data.tedate
        statusLabel.text = "Completed"
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
