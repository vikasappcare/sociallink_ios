//
//  POSDDown+Appearance.swift
//  POSSalesSheet
//
//  Created by Marripelli, Santosh Kumar on 05/07/17.
//  Copyright © 2017 Verizon. All rights reserved.
//

import UIKit

extension POSDropDown {
    
    public class func setupDefaultAppearance() {
        let appearance = POSDropDown.appearance()
        
        appearance.cellHeight = POSDDConstants.UI.RowHeight
        appearance.backgroundColor = POSDDConstants.UI.BackgroundColor
        appearance.selectionBackgroundColor = POSDDConstants.UI.SelectionBackgroundColor
        appearance.separatorColor = POSDDConstants.UI.SeparatorColor
        appearance.cornerRadius = POSDDConstants.UI.CornerRadius
        appearance.shadowColor = POSDDConstants.UI.Shadow.Color
        appearance.shadowOffset = POSDDConstants.UI.Shadow.Offset
        appearance.shadowOpacity = POSDDConstants.UI.Shadow.Opacity
        appearance.shadowRadius = POSDDConstants.UI.Shadow.Radius
        appearance.animationduration = POSDDConstants.Animation.Duration
        appearance.textColor = POSDDConstants.UI.TextColor
        appearance.textFont = POSDDConstants.UI.TextFont
    }
    
}
