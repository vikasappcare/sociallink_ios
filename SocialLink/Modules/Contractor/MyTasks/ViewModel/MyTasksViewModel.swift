//
//  MyTasksViewModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 17/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation

class MyTasksViewModel {
    
    init() {
    }
     var taskListArray: [Task]?
    var projectTaskListArray: [TaskModel]?
    func getContractorTasks(parameters: [String: Any],
                         onSuccess: @escaping (ProjectListModel) -> Void,
                         onError: @escaping APIErrorHandler) {
        AppDataManager.getContractorTasks(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: dataSource)
            onSuccess((dataSource)!)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    func getContractorAssignedTasks(parameters: [String: Any],
                            onSuccess: @escaping (TaskModel?) -> Void,
                            onError: @escaping APIErrorHandler) {
        AppDataManager.getContractorAssignedTasks(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: dataSource)
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    func getContractorInProgressTasks(parameters: [String: Any],
                                    onSuccess: @escaping (TaskModel?) -> Void,
                                    onError: @escaping APIErrorHandler) {
        AppDataManager.getContractorInProgressTasks(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: dataSource)
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    func getContractorPaymentTasks(parameters: [String: Any],
                                      onSuccess: @escaping (TaskModel?) -> Void,
                                      onError: @escaping APIErrorHandler) {
        AppDataManager.getContractorPaymentsTasks(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: dataSource)
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
    static func getTasks() {
        let objects = APPDBUtility.getContractorTasks(mocType: .main)
        APPStore.sharedStore.contractorTasks.removeAll()
        if objects?.count ?? 0 > 0 {
            if let objectIs = objects {
                for object in objectIs {
                    if let timeLogs = APPDBUtility.getTimeLogs(forId: object.task_id, mocType: .main) {
                        let convertObject = APPUtility.convertDBTaskToTask(dbTask: object, timeLogs: timeLogs)
//                        APPStore.sharedStore.contractorTasks.append(convertObject)
//                        print(APPStore.sharedStore.contractorTasks.count)
                    }
                    
                }
            }
        }
    }
    
    
    func updateTasksOnSelection(index: Int) {
        var taskState: Int64?
        var acceptedState: String?
        switch APPStore.sharedStore.userIs {
        case USERTYPE.CONTRACTOR, USERTYPE.ONLYCONTRACTOR:
            switch index {
            case 0:
                print("Contractor Asigned")
                taskState = CONTRACTORTASKSTATES.ASSIGNED
                acceptedState = "" //// Empty First
            case 1:
                print("Contractor In Progress")
                taskState = CONTRACTORTASKSTATES.INPROGRESS
                acceptedState = "\(ACCEPTEDSTATES.ACCEPT)"
            case 2:
                print("Contractor Completed")
                taskState = CONTRACTORTASKSTATES.COMPLETED
                acceptedState = "\(ACCEPTEDSTATES.COMPLETED)"
            default:
                break
            }
        default:
            break
        }
        print("TaskState: \(String(describing: taskState ?? 0))")
        print("AcceptedState: \(acceptedState ?? "")")
        
//        if let state = taskState {
//            if let accepted = acceptedState {
//                if self.taskListArray == nil {
//                    self.taskListArray = [ContractorTask]()
//                }
//
//
//                let taskArrayIs = APPStore.sharedStore.contractorTasks.filter {$0.pstatus == state && $0.accepted_state == accepted}
//                self.taskListArray = taskArrayIs
//
//
//
//                return
//            }
//            let taskArrayIs = self.taskListArray?.filter {$0.pstatus == state}
//            self.taskListArray = taskArrayIs
//        }
//    }
//
    
}
}
