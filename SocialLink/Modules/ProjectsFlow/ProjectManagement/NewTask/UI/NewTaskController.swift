//
//  NewTaskController.swift
//  SocialLink
//
//  Created by Harsha on 08/05/19.
//

import UIKit

class NewTaskController: AppBaseVC {
    
    @IBOutlet weak var startDateTextField: AppTextField!
    @IBOutlet weak var endDateTextField: AppTextField!
    @IBOutlet weak var taskNameTextField: AppTextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    var viewModel: NewTaskViewModel?
    var selectedProject: Project?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "New Task"
        viewModel = NewTaskViewModel()
        self.createNavigationCloseItems()
        
    }
    func setProject(project: Project) {
        self.selectedProject = project
    }
    @IBAction func startDateButtonAction(_ sender: Any) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProjectCalenderVC") as? ProjectCalenderVC {
            let navController = APPNavigationController(rootViewController: controller)
            controller.delegate = self
            controller.controllerTag = 2
            self.present(navController, animated: true, completion: nil)
        }
    }
    @IBAction func endDateButtonAction(_ sender: Any) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProjectCalenderVC") as? ProjectCalenderVC {
            let navController = APPNavigationController(rootViewController: controller)
            controller.delegate = self
            controller.controllerTag = 3
            self.present(navController, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        print(startDateTextField.text)
        print(endDateTextField.text)
        if !APPUtility.validateDatesWithoutT(start: startDateTextField.text!, end: endDateTextField.text!) {
            self.showDateAlert()
            return
        }
        addNewTask()
    }
    func addNewTask() {
        if taskNameTextField.text?.isEmpty == false || descriptionTextView.text.isEmpty == false || (startDateTextField.text?.isEmpty)! == false || endDateTextField.text?.isEmpty == false{
            if let model = viewModel {
                let description = descriptionTextView.text
                let data = ["project_id": selectedProject?.project_id ?? 0,
                            "user_id": APPStore.sharedStore.user.userId!,
                            "tname": taskNameTextField.text!,
                            "tdetails": description!,
                            "tsdate": startDateTextField.text!,
                            "tedate": endDateTextField.text!,
                            "tassignto": APPStore.sharedStore.user.email!,
                            "status": "1",
                            "accepted_state": "1"] as [String : Any]
                print(data)
                model.addTask(parameters: data, onSuccess: { [weak self] (object) in
                    dprint(object: "Success")
                    self?.moveToNextController()
                    }, onError: { (error) in
                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
                        self.dismiss(animated: true, completion: nil)
                        self.hideHud()
                })
            }
        }
    }
    func moveToNextController()
    {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProjectTaskkListVC") as? ProjectTaskkListVC {
            if let project = self.selectedProject {
                controller.setProject(project: project)
            }
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
extension NewTaskController {
    
    func createNavigationCloseItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension NewTaskController: ProjectCalenderVCDelegate {
    func dateSelected(date: String, tag: Int, dateIs: Date) {
        
        //  let utcDateString = APPUtility.zeroTimeString(date: date)
        
        if tag == 2 {
            startDateTextField.text = date
        } else if tag == 3 {
            endDateTextField.text = date
        }
    }
    func showDateAlert() {
        self.showAlert(title: "Alert!", message: "End Date should not be Smaller than Start Date")
    }
    
}
