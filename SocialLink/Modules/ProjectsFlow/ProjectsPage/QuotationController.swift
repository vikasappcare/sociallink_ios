//
//  QuotationController.swift
//  SocialLink
//
//  Created by Harsha on 08/05/19.
//

import UIKit

class QuotationController: AppBaseVC {
    
    @IBOutlet weak var estimatedTextField: UITextField!
    @IBOutlet weak var estimatedBudgetTextField: UITextField!
    @IBOutlet weak var descriptionText: UITextView!
    
    var viewModel: ProjectsControllerViewModel?
    
    var selectedProject: Project?
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.createNavigationItems()
        viewModel = ProjectsControllerViewModel()
        self.navigationItem.title = "Quotation"

    }
    func setProject(project: Project) {
        self.selectedProject = project
    }
    
    func getProjects() {
        if let model = viewModel {
            //let estimatedTime = estimatedTextField.text
            let estimatedBudget = estimatedBudgetTextField.text
            let description = descriptionText.text
            let parms = ["user_id": APPStore.sharedStore.user.userId ?? "",
                         "pid": selectedProject?.project_id ?? "",
                         "q_status": 0,
                         "quote_scope": description ?? "",
                         "ebudget": estimatedBudget ?? ""] as [String : Any]
            print(parms)
            
            self.addActivityIndicatior()
            self.showHUD(title: "Loading...")
            model.sendQuotation(parameters: parms, onSuccess: { [weak self] (dataSource) in
                self?.removeActivityIndicator()
                self?.hideHud()
                self?.dismiss(animated: true, completion: nil)
                }, onError: { (error) in
                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
                    DispatchQueue.main.async {
                        // self.showAlert(title: Message.Alert, message: error.statusMessage)
                        self.removeActivityIndicator()
                    }
            })
        }
        
        
    }
    @IBAction func sendQuotationButtonClicked(_ sender: Any) {
        getProjects()
    }
}

extension QuotationController
{
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
}
