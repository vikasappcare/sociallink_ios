//
//  ImagePostCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 20/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ImagePostCell: UITableViewCell {

    @IBOutlet weak var imageText: UITextView!
    @IBOutlet weak var postImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setImage(image: UIImage) {
        let aspect = image.size.width / image.size.height
        let constraint = NSLayoutConstraint(item: postImage, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: postImage, attribute: NSLayoutAttribute.height, multiplier: aspect, constant: 0.0)
        constraint.priority = UILayoutPriority(rawValue: 999)
        aspectConstraint = constraint
        postImage.image = image
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    
    internal var aspectConstraint : NSLayoutConstraint? {
        didSet {
            if oldValue != nil {
                postImage.removeConstraint(oldValue!)
            }
            if aspectConstraint != nil {
                postImage.addConstraint(aspectConstraint!)
            }
        }
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        aspectConstraint = nil
    }

}
