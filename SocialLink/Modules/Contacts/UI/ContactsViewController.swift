//
//  ContactsViewController.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit
import WebKit

class ContactsViewController: AppBaseVC, WKNavigationDelegate {
    
    var viewModel: ContactsViewModel?
    var webView: WKWebView!
    var channelId: String!
    var accessToken: String!
    var channelName: String!
    
    @IBOutlet weak var searchCollectionView: UICollectionView!
    @IBOutlet weak var contactsTableview: UITableView!
    @IBOutlet weak var contactsSearch: UISearchBar!
    
    let searchData = ["People", "Jobs", "Companies", "Profile"]
    let profileArray = ["Graphic Designer", "SEO", "Content Specialist", "Graphic Designer"]
    var nameArray = [String]()
    let colorArray = [UIColor.appGreenColor, UIColor.appOrangeColor, UIColor.appBlueColor, UIColor.appGreenColor]
    var showClose: Bool?
    var showCloseForScreen: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = ContactsViewModel()
        
        //        self.navigationItem.title = "CONTACTS"
        //        //self.setTableViewDatasource()
        //        //self.setCollectionView()
        //        self.addNotification()
        //        contactsSearch.barTintColor = UIColor.clear
        //        contactsSearch.backgroundColor = UIColor.clear
        //        contactsSearch.isTranslucent = true
        //        contactsSearch.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        //        self.callService()
        //        if showClose ?? false {
        //            self.createNavigationItems()
        //        }
        // Do any additional setup after loading the view.
    }
    override func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("finish to load")
        
        if let url = webView.url?.absoluteString{
            print(url)
            if url.contains("https://app.sociallink.com/"){
                let codeFirstPart = url.strstr(needle: "&", beforeNeedle: true)
                let finalCode = codeFirstPart?.strstr(needle: "=")
                print("url = \(url)")
                // print("code = \(finalCode ?? "")")
                webView.removeFromSuperview()
                if finalCode != nil{
                    callAPIForAuth(code: finalCode!)
                }
            }
        }
    }
    func callAPIForAuth(code: String)
    {
        if let model = viewModel {
            let param = ["": ""]
            self.showHUD(title: "Loading...")
           
            let url = APPURL.slackauthURL + "&client_id=\(Slack.Client_ID)" + "&client_secret=\(Slack.Client_Secret)" + "&code=\(code)" + "&single_channel=true"
            
            model.slackAuth(parameters: param as [String : Any], url: url, onSuccess: { [weak self] (dataSource) in
                self?.hideHud()
                self?.channelId = dataSource?.channel_id
                self?.channelName = dataSource?.channel
                self?.accessToken = dataSource?.access_token
                let userId = dataSource?.user_id
                Slack.AuthToken = (self?.accessToken)!
                Slack.ChannelId = (self?.channelId)!
                Slack.UserId = (userId!)
                print(Slack.AuthToken)
                self?.callAPIForUsersList()
                //self?.moveToChat()
                },  onError: { (error) in
                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
                    // Present the controller
                    self.hideHud()
            })
            
        }
    }
    func callAPIForUsersList()
    {
        if let model = viewModel {
            let param = ["token": Slack.AuthToken]
            self.showHUD(title: "Loading...")
           
            model.slackUsersList(parameters: param as [String : Any], onSuccess: { [weak self] (dataSource) in
                self?.hideHud()
                for i in 0..<dataSource!.id.count{
                    let id = dataSource?.id[i]
                    let name = dataSource?.real_name[i]
                    let image = dataSource?.image_48[i]
                   
                    nameDic["\(id ?? "")"] = name
                    imageDic["\(id ?? "")"] = image
                }
                print(nameDic)
                Slack.IsSlack = true

                self?.callServiceForChat()
                
                },  onError: { (error) in
                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
                    // Present the controller
                    self.hideHud()
            })
            
        }
    }
    func moveToChat()
    {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController {
            displayName = self.channelName
            let navController = UINavigationController(rootViewController: controller) // Creating a navigation controller with VC1 at the root of the navigation stack.
            self.navigationItem.backBarButtonItem = UIBarButtonItem(
                title: "", style: .done, target: nil, action: nil)
            self.present(navController, animated: true, completion: nil)
            // self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }
    func showCloseButton(show: Bool, forScreen: String) {
        showClose = show
        showCloseForScreen = forScreen
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showTabBar()
        let url = URL(string: "https://slack.com/oauth/authorize?client_id=\(Slack.Client_ID)&scope=incoming-webhook,users:read,channels:history,channels:read,channels:write,chat:write:user,groups:history,groups:read,groups:write,im:history,im:read,im:write,users:read.email,users:write")!
        webView.load(URLRequest(url: url))
        //        let refresh = UIBarButtonItem(barButtonSystemItem: .refresh, target: webView, action: #selector(webView.reload))
        //        toolbarItems = [refresh]
        //        navigationController?.isToolbarHidden = false
    }
    
    func showTabBar() {
        self.setTabBarHidden(false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func callServiceForChat()
    {
        if let model = viewModel {
            
            let param = ["channel": channelId!,
                         "token": accessToken!]
            print(param)
            self.showHUD(title: "Loading...")
            model.chatHistory(parameters: param as [String : Any], onSuccess: { [weak self] (dataSource) in
                self?.hideHud()
                print(dataSource?.subtype as Any)
                for i in 0..<dataSource!.subtype.count{
                    if dataSource?.subtype[i] == ""
                    {
                        persons.append((dataSource?.user[i])!)
                        text.append((dataSource?.text[i])!)
                        time.append((dataSource?.ts[i])!)
                    }
                }
                Slack.IsSlack = false
                
                self?.moveToChat()
                
                },  onError: { (error) in
                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
                    let alertController = UIAlertController(title: "Alert!", message: "Unable to load the chat.", preferredStyle: .alert)
                    
                    // Create the actions
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                        UIAlertAction in
                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let cntController = mainStoryboard.instantiateViewController(withIdentifier: "ContactsViewController")
                        if let contactController = cntController as? ContactsViewController {
                            contactController.showCloseButton(show: false, forScreen: "")
                            contactController.tabBarItem = UITabBarItem(title: "Messenger", image: UIImage(named: "TabContact"), tag: 4)
                            //contactController.tabBarItem.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: -6, right: 0)
                            self.present(contactController, animated: true, completion: nil)
                        }
                        
                    }
                    
                    // Add the actions
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil);

                    self.hideHud()
            })
        }
    }
}

extension ContactsViewController {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension ContactsViewController {
    
    func addNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(showAddScreen),
                                               name:
            NSNotification.Name(rawValue: APPNotifications.plusContacts),
                                               object: nil)
    }
    @objc func  showAddScreen(info: NSNotification) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = mainStoryboard.instantiateViewController(withIdentifier: "AddViewController")
        if let addController = controller as? AddViewController {
            addController.modalPresentationStyle = .overCurrentContext
            addController.view.alpha = 0.0
            self.tabBarController?.present(addController, animated: true) { () -> Void in
                UIView.animate(withDuration: 0.025) { () -> Void in
                    addController.view.alpha = 1.0
                }
            }
        }
        
    }
}

extension ContactsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.contactsTableview.delegate = self
        self.contactsTableview.dataSource = self
        self.contactsTableview.estimatedRowHeight = 300
        self.contactsTableview.rowHeight = UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return nameArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:
            CellIdentifiers.ContactTableViewCellIdentifier, for: indexPath)
        if let cellIs = cell as? ContactTableViewCell {
            cellIs.selectionStyle = .none
            let data = ["name": nameArray[indexPath.row]] as [String: Any]
            cellIs.setData(data: data)
        }
        cell.selectionStyle = .none
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //        if !(showClose ?? false) {
        //            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactProfileVC") as? ContactProfileVC {
        //                self.navigationController?.pushViewController(controller, animated: true)
        //            }
        //        } else if (showClose ?? false) && (showCloseForScreen == ContactsShowScreen.AddController) {
        //
        //            DispatchQueue.main.asyncAfter(deadline: .now() + 0.010, execute: {
        //                self.dismiss(animated: true, completion: {
        //                })
        //                NotificationCenter.default.post(name: NSNotification.Name(rawValue:
        //                    APPNotifications.showProjectList),
        //                                                object: nil, userInfo: nil)
        //
        //            })
        //
        //
        //        }
        
        
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController {
            displayName = nameArray[indexPath.row]
            self.navigationItem.backBarButtonItem = UIBarButtonItem(
                title: "", style: .done, target: nil, action: nil)
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    /* func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
     let headerView = UIView()
     let titleLabel = UILabel(frame: CGRect(x: 20, y: 0, width: UIScreen.main.bounds.width - 27, height: 30))
     
     titleLabel.text = "Your Contacts"
     headerView.addSubview(titleLabel)
     titleLabel.textColor = UIColor.init(red: 57/255.0, green: 57/255.0, blue: 57/255.0, alpha: 1.0)
     titleLabel.font = UIFont.boldFont(17)
     headerView.backgroundColor = UIColor.appLightGrayColor
     return headerView
     }*/
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        //        let call = UITableViewRowAction(style: .normal, title: "") { action, index in
        //            print("more button tapped")
        //        }
        //        call.backgroundColor = UIColor.red
        //        call.backgroundColor = UIColor(patternImage: UIImage(named: "contactCall")!)
        
        let message = UITableViewRowAction(style: .normal, title: "") { action, index in
            print("more button tapped")
        }
        message.backgroundColor = UIColor.green
        message.backgroundColor = UIColor(patternImage: UIImage(named: "contactMessage")!)
        
        //        let mail = UITableViewRowAction(style: .normal, title: "") { action, index in
        //            print("more button tapped")
        //        }
        //        mail.backgroundColor = UIColor.blue
        //        mail.backgroundColor = UIColor(patternImage: UIImage(named: "contactMail")!)
        return [message]
    }
}

/*extension ContactsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
 
 
 func setCollectionView() {
 self.searchCollectionView.dataSource = self
 self.searchCollectionView.delegate = self
 
 if let flow = searchCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
 flow.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
 flow.itemSize = CGSize(width: 80, height: 80)
 flow.minimumInteritemSpacing = 20
 flow.minimumLineSpacing = 20
 }
 
 
 }
 
 func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
 return searchData.count
 }
 func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
 let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.SearchCollectionCellIdentifier, for: indexPath)
 cell.tag = indexPath.row
 if let cellIs = cell as? SearchCollectionCell {
 
 let data = ["type": searchData[indexPath.row],
 "image": ""]
 
 cellIs.setData(data: data)
 }
 
 return cell
 }
 func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
 
 }
 }*/
extension String {
    
    func strstr(needle: String, beforeNeedle: Bool = false) -> String? {
        guard let range = self.range(of: needle) else { return nil }
        
        if beforeNeedle {
            return self.substring(to: range.lowerBound)
        }
        return self.substring(from: range.upperBound)
    }
    
}
