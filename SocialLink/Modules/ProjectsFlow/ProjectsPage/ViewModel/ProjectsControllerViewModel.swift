//
//  ProjectListViewModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 16/08/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation

class ProjectsControllerViewModel {
    
    init() {
    }
    
    var employerProjectsArray: [Project]?
    var contractorProjectsArray: [Project]?

    var estimatedProjectsArray: [EstimatedProject]?
    
    func updateProjects() {
        APPUtility.getProjects()
        if APPStore.sharedStore.appUserID == "1"{
            contractorProjectsArray = APPStore.sharedStore.contractorProjects
        }
        else{
        employerProjectsArray = APPStore.sharedStore.employerProjects
        }
       // estimatedProjectsArray = APPStore.sharedStore.estimatedProjects
    }
    
    func getProjectList(parameters: [String: Any],
                    onSuccess: @escaping (ProjectListModel?) -> Void,
                    onError: @escaping APIErrorHandler) {
        AppDataManager.getProjectList(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: "Success")
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    func getCurrentProjectList(parameters: [String: Any],
                        onSuccess: @escaping (ProjectListModel?) -> Void,
                        onError: @escaping APIErrorHandler) {
        AppDataManager.getCurrentProjectList(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: "Success")
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    func getCompletedProjectList(parameters: [String: Any],
                               onSuccess: @escaping (ProjectListModel?) -> Void,
                               onError: @escaping APIErrorHandler) {
        AppDataManager.getCompletedProjectList(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: "Success")
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    func getCancelledProjectList(parameters: [String: Any],
                                 onSuccess: @escaping (ProjectListModel?) -> Void,
                                 onError: @escaping APIErrorHandler) {
        AppDataManager.getCancelledProjectList(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: "Success")
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    func getEstimatedProjectList(parameters: [String: Any],
                                 onSuccess: @escaping (EstimatedProjectListModel?) -> Void,
                                 onError: @escaping APIErrorHandler) {
        AppDataManager.getEstimatedProjectList(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: "Success")
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
    func sendQuotation(parameters: [String: Any],
                        onSuccess: @escaping (OTPModule?) -> Void,
                        onError: @escaping APIErrorHandler) {
        AppDataManager.sendQuotation(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: "Success")
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    func estimationStatus(parameters: [String: Any],
                       onSuccess: @escaping (OTPModule?) -> Void,
                       onError: @escaping APIErrorHandler) {
        AppDataManager.estimationStatus(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: "Success")
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    func updateProjectList(state: Int64) {
        print(state)
        employerProjectsArray = []
        contractorProjectsArray = []

        if APPStore.sharedStore.appUserID == "1"{
            
            self.contractorProjectsArray = APPStore.sharedStore.contractorProjects.filter {$0.statusIs == state }
            
        }
        else
        {
            self.employerProjectsArray = APPStore.sharedStore.employerProjects.filter {$0.statusIs == state }
        }
        
     //   self.estimatedProjectsArray = APPStore.sharedStore.estimatedProjects.filter {$0.q_status == 0 }
    }
    
    
    
}
