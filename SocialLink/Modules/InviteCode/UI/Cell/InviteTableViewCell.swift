//
//  InviteTableViewCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

protocol InviteTableViewCellDelegate: class {
    func backClicked()
    func submitClicked(withCode: String)
}

class InviteTableViewCell: UITableViewCell {
    weak var delegate: InviteTableViewCellDelegate?
    @IBOutlet weak var emailText: AppTextField!
    @IBOutlet weak var iAgreeCheckboxButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func gobackClicked(_ sender: Any) {
        self.delegate?.backClicked()
    }
    @IBAction func submitClicked(_ sender: Any) {
        self.delegate?.submitClicked(withCode: emailText.text ?? "")
    }
    @IBAction func iAgreeCheckboxClicked(_ sender: Any) {
    }
}
