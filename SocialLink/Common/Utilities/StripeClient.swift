//
//  IntExtension.swift
//  TimerS
//
//  Created by Shaobai Li on 19/7/17.
//  Copyright © 2017 Shaobai. All rights reserved.
//
import Foundation
import Alamofire
import Stripe

enum Result {
  case success
  case failure(Error)
}

final class StripeClient {
  
  static let shared = StripeClient()
  
  private init() {
    // private
  }
  
  private lazy var baseURL: URL = {
    guard let url = URL(string: StripeConstants.baseURLString) else {
      fatalError("Invalid URL")
    }
    return url
  }()
    
    /*
     changes vikas add this in params
     with token: STPToken,
     */
    
    func completeCharge(amount: Int, params: [String: Any], completion: @escaping (Result) -> Void) {
        // 1
        let url = baseURL
        print(url)
        // 2
       
        print(params)
        // 3
        Alamofire.request(url, method: .post, parameters: params)
            .validate(statusCode: 200..<300)
            .responseString { response in
                switch response.result {
                case .success:
                    completion(Result.success)
                case .failure(let error):
                    completion(Result.failure(error))
                }
        }
    }
    
    
}

/*
final class MyAPIClient: NSObject, STPCustomerEphemeralKeyProvider {

    private lazy var baseURL: URL = {
      guard let url = URL(string: StripeConstants.baseURLString) else {
        fatalError("Invalid URL")
      }
      return url
    }()
    
    func createCustomerKey(withAPIVersion apiVersion: String, completion: @escaping STPJSONResponseCompletionBlock) {
        let url = self.baseURL.appendingPathComponent("ephemeral_keys")
        var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)!
        urlComponents.queryItems = [URLQueryItem(name: "api_version", value: apiVersion)]
        var request = URLRequest(url: urlComponents.url!)
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
            guard let response = response as? HTTPURLResponse,
                response.statusCode == 200,
                let data = data,
                let json = ((try? JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]) as [String : Any]??) else {
                completion(nil, error)
                return
            }
            completion(json, nil)
        })
        task.resume()
    }
}
*/
