//
//  FileUploadViewModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 20/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class FileUploadViewModel {
    
    init() {
    }
    
    func requestWith(imageData: Data?, parameters: [String : Any], endURL: String?, onCompletion: ((FileUploadModel?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        let url = APPURL.getBaseURL() + endURL!
        
        let headers: HTTPHeaders = ["Content-type": "multipart/form-data"]
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            let type = parameters["file_extension"] as? String
            //let name = parameters["document"] as? String ?? "Sample"
            
            var fileNameKey = ""
            if endURL == APPURL.addProject{
                fileNameKey = "pupload"
            }
            else if endURL == APPURL.postFeed || endURL == APPURL.contractorPostFeed{
                fileNameKey = "document"
            }
            else if endURL == APPURL.profilePicUpdate{
                fileNameKey = "uphoto"
            }
            let randomName = APPUtility.getPresentDate()
            let fileNameString = randomName
                .components(separatedBy:CharacterSet.decimalDigits.inverted)
                .joined(separator: "")
          
            switch type {
            case "jpeg":
                if let data = imageData {
                    multipartFormData.append(data, withName: fileNameKey, fileName: "\(fileNameString).jpeg", mimeType: "image/jpeg")
                }
            case "PNG":
                if let data = imageData {
                    multipartFormData.append(data, withName: fileNameKey, fileName: "\(fileNameString).jpeg", mimeType: "image/jpeg")
                }
            case "pdf":
                if let data = imageData {
                    multipartFormData.append(data, withName: fileNameKey, fileName: fileNameString, mimeType: "application/pdf")
                }
            case "doc":
                if let data = imageData {
                    multipartFormData.append(data, withName: fileNameKey, fileName: fileNameString, mimeType: "application/msword")
                }
            case "docx":
                if let data = imageData {
                    multipartFormData.append(data, withName: fileNameKey, fileName: fileNameString, mimeType: "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
                }
            case "ppt":
                if let data = imageData {
                    multipartFormData.append(data, withName: fileNameKey, fileName: fileNameString, mimeType: "application/vnd.ms-powerpoint")
                }
            case "pptx":
                if let data = imageData {
                    multipartFormData.append(data, withName: fileNameKey, fileName: fileNameString, mimeType: "application/vnd.openxmlformats-officedocument.presentationml.presentation")
                }
            case "xls":
                if let data = imageData {
                    multipartFormData.append(data, withName: fileNameKey, fileName: fileNameString, mimeType: "application/vnd.ms-excel")
                }
            case "xlsx":
                if let data = imageData {
                    multipartFormData.append(data, withName: fileNameKey, fileName: fileNameString, mimeType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                }
            default:
                break
            }
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    if let err = response.error{
                        onError?(err)
                        onCompletion?(nil)
                    }
                    if let json = response.result.value as? [String: Any] {
                        let jsonObject = JSON.init(json)
                        print(jsonObject)
                        let status = jsonObject["status"].stringValue
                        switch status {
                        case ServiceResponse.SUCCESS:
                            let uploadModel = FileUploadModel.init(jsonObject["records"])
                            print(uploadModel?.data?.dfile ?? "")
                            onCompletion?(uploadModel)
                            return
                        case ServiceResponse.FAIL:
                            if let err = response.error{
                                onError?(err)
                                return
                            }
                        default:
                            break
                        }
                        onCompletion?(nil)
                    }
    
                    onCompletion?(nil)
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                
                onError?(error)
            }
        }
    }
}
