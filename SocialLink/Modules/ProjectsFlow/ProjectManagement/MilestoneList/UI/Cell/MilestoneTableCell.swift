//
//  MilestoneTableCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 09/05/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class MilestoneTableCell: UITableViewCell {

    @IBOutlet weak var milestoneLabel: UILabel!
    @IBOutlet weak var milestoneProgress: UIProgressView!
    @IBOutlet weak var milestoneNameLabel: UILabel!
    @IBOutlet weak var startDatetext: UIButton!
    @IBOutlet weak var endDateText: UITextField!
    @IBOutlet weak var startDateButton: UIButton!
    @IBOutlet weak var endDateButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.milestoneProgress.layer.cornerRadius = 5.0
        self.milestoneProgress.layer.masksToBounds = true
    }

    @IBAction func endDateClicked(_ sender: Any) {
    }
    @IBAction func startDateCllicked(_ sender: Any) {
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
