//
//  ExpiryDateCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 04/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ExpiryDateCell: UITableViewCell {

    @IBOutlet weak var headertext1: UILabel!
    @IBOutlet weak var headerText2: UILabel!
    @IBOutlet weak var contentText1: UITextField!
    @IBOutlet weak var contextText2: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func setData(data: [String: Any]?) {
        headertext1.text = data!["header1"] as? String
        headerText2.text = data!["header2"] as? String
        contentText1.placeholder = data!["contentText1"] as? String
        contextText2.placeholder = data!["contextText2"] as? String
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
