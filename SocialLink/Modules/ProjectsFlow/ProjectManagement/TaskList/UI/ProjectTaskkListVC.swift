//
//  ProjectTaskkListVC.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 09/05/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ProjectTaskkListVC: AppBaseVC {
    
    @IBOutlet weak var segmentView: UIView!
    @IBOutlet weak var segmentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var taskTypeSegment: AppSegmentControl!
    @IBOutlet weak var projectTaskTable: UITableView!
    
    var viewModel: TaskListViewModel?
    var selectedProject: Project?
   // var taskArray: [Task] = [Task]()
    
//    var employerArray = ["Assigned",
//                         "Active",
//                         "Payment"]
//
//    var contractorArray = ["Active Tasks",
//
//                         "Completed"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UILabel.appearance(whenContainedInInstancesOf: [UISegmentedControl.self]).numberOfLines = 2
        
      //  taskTypeSegment.isHidden = true
        viewModel = TaskListViewModel()
        self.setTableViewDatasource()
        ///   self.createNavigationItems()
        // Do any additional setup after loading the view.
        self.updateTasks()
       // self.validateUser()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            self.getTasks(background: true)
        })
      //  self.taskTypeSegment.selectedSegmentIndex = 0
        
        self.title = "Project TaskList"
        self.addUserTypeSwitchNotification()
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UILabel.appearance(whenContainedInInstancesOf: [UISegmentedControl.self]).numberOfLines = 2
        
        viewModel = TaskListViewModel()
        self.setTableViewDatasource()
        ///   self.createNavigationItems()
        // Do any additional setup after loading the view.
        self.updateTasks()
       // self.validateUser()
        //DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            self.getTasks(background: true)
        //})
       // self.taskTypeSegment.selectedSegmentIndex = 0
        
        self.title = "Project TaskList"
        self.addUserTypeSwitchNotification()     }
    
//    func validateUser() {
//        switch APPStore.sharedStore.userIs {
//        case USERTYPE.EMPLOYER:
//            taskTypeSegment.replaceSegments(segments: employerArray)
//        case USERTYPE.CONTRACTOR:
//            taskTypeSegment.replaceSegments(segments: contractorArray)
//        case USERTYPE.ONLYCONTRACTOR:
//            taskTypeSegment.replaceSegments(segments: employerArray)
//
//        default:
//            break
//        }
//        self.taskTypeSegment.selectedSegmentIndex = 0
//        if let model = viewModel {
//            model.updateTasksOnSelection(index: self.taskTypeSegment.selectedSegmentIndex)
//            self.projectTaskTable.reloadData()
//        }
//    }
    
   
    
    func updateTasks() {
        if let model = viewModel {
            APPUtility.getTasks(forProjectId: selectedProject?.project_id ?? 0)
           //model.updateTasksOnSelection(index: self.taskTypeSegment.selectedSegmentIndex)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setProject(project: Project) {
        self.selectedProject = project
    }
    
    
    @IBAction func segmentChanged(_ sender: Any) {
        if let model = viewModel {
           // model.updateTasksOnSelection(index: self.taskTypeSegment.selectedSegmentIndex)
            self.projectTaskTable.reloadData()
        }
    }

    
    func getTasks(background: Bool) {
        if let model = viewModel {
            if let project = selectedProject {
                if !background {
                    self.showHUD(title: "Loading...")
                }
                let dataIs = ["project_id": project.project_id ?? 0] as [String : Any]
                print(dataIs)
                self.showHUD(title: "Loading...")
                DispatchQueue.global(qos: .background).async {
                    model.getProjectTasks(parameters: dataIs, onSuccess: { [weak self] (object) in
                        dprint(object: "Success")
                        DispatchQueue.main.async {
                            model.projectTaskListArray = object?.list
                            if model.projectTaskListArray == nil {
                                model.projectTaskListArray = [Task]()
                            }
                            //self?.updateTasks()
                            self?.projectTaskTable.reloadData()
                            self?.hideHud()
                        }
                        }, onError: { (error) in
                            Logger.log(message: "Error \(error.statusMessage)", event: .error)
                          //  self.showAlert(title: Message.Alert, message: error.statusMessage)
                            self.hideHud()
                            
                    })
                }
 
            }

        }
        
    }
    
}

extension ProjectTaskkListVC {
    
    func addUserTypeSwitchNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(userSwitched),
                                               name:
            NSNotification.Name(rawValue: APPNotifications.userSwitched),
                                               object: nil)
    }
    @objc func userSwitched(info: NSNotification) {
        self.getTasks(background: true)
    }
}


extension ProjectTaskkListVC {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension ProjectTaskkListVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.projectTaskTable.delegate = self
        self.projectTaskTable.dataSource = self
        self.projectTaskTable.estimatedRowHeight = 300
        self.projectTaskTable.rowHeight = UITableViewAutomaticDimension
        self.projectTaskTable.tableFooterView = UIView()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return viewModel?.projectTaskListArray?.count ?? 0
        case 1:
            if APPStore.sharedStore.appUserID == "1"{
                return 1
            }
            else
            {
                return 0
            }
//            return APPStore.sharedStore.userIs == USERTYPE.EMPLOYER ? 1 : 0
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.ProjectTaskCellIdentifier, for: indexPath)
            if let cellIs = cell as? ProjectTaskCell {
                cellIs.selectionStyle = .none
                cellIs.delegate = self
                if let model = viewModel {
                    if model.projectTaskListArray != nil{
                    cellIs.setData(data: model.projectTaskListArray![indexPath.row])
                    print("Data from cell:", model.projectTaskListArray![indexPath.row])
                }
                }
            }
            cell.selectionStyle = .none
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.ProjectAddTaskCellIdentifier, for: indexPath)
            if let cellIs = cell as? ProjectAddTaskCell {
                cellIs.selectionStyle = .none
            }
            cell.selectionStyle = .none
            return cell
        default:
            return UITableViewCell()
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.001, execute: {

        switch indexPath.section {
        case 0:
                if let controller = self.storyboard?.instantiateViewController(withIdentifier: "TaskDetailViewController") as? TaskDetailViewController {
                    self.navigationItem.backBarButtonItem = UIBarButtonItem(
                        title: "", style: .done, target: nil, action: nil)
                    if let model = self.viewModel {
                       // var taskState: Int?
//                        switch self.taskTypeSegment.selectedSegmentIndex {
//                        case 0:
//                            taskState = 0
//                        case 1:
//                            taskState = 1
//                        case 2:
//                            taskState = 2
//                        case 3:
//                            taskState = 3
//                        default:
//                            break
//                        }
//                        if let typeIs = taskState {
                            if model.projectTaskListArray != nil{
                            controller.setTask(task: model.projectTaskListArray![indexPath.row], row: indexPath.row, type: 0, projectId: self.selectedProject!.project_id!)
                            }
                        //}
                    }
                    controller.delegate = self
                   // let navControl = APPNavigationController(rootViewController: controller)
                    ///self.present(navControl, animated: true, completion: nil)
                    self.navigationController?.pushViewController(controller, animated: true)
                }

        case 1:
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "NewTaskController") as? NewTaskController {
                if let project = self.selectedProject {
                    controller.setProject(project: project)
                }
                let navControl = APPNavigationController(rootViewController: controller)
                self.present(navControl, animated: true, completion: nil)
            }
        default:
            break
        }
         })
        
    }
  
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        switch indexPath.section {
        case 0:
            switch APPStore.sharedStore.userIs {
            case USERTYPE.EMPLOYER:
                 return true
            default:
                return false
            }
        default:
            return false
        }
    }
     func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
     let edit = UITableViewRowAction(style: .normal, title: "Edit") { action, index in
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.001, execute: {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProjectNewTaskVC") as? ProjectNewTaskVC {
            let navControl = APPNavigationController(rootViewController: controller)
            controller.delegate = self
          
            self.present(navControl, animated: true, completion: nil)
        }
              })
     }
     edit.backgroundColor = UIColor.lightGray
     return [edit]
     
     }
    
    
    
}

extension ProjectTaskkListVC: ProjectNewTaskVCDelegate {
    func reloadUpdatedRow(row: Int) {
        self.updateTasks()
        if let model = viewModel {
            if row <= model.projectTaskListArray?.count ?? 0 {
                let index = IndexPath(row: row, section: 0)
                self.projectTaskTable.reloadRows(at: [index], with: .automatic)
            }
        }
       
    }
    func reloadRows() {
        self.getTasks(background: true)
        self.projectTaskTable.reloadData()
    }
}

extension ProjectTaskkListVC: TaskDetailViewControllerDelegate {
    func reloadChangedUpdatedRow(row: Int) {
        self.updateTasks()
        let index = IndexPath(row: row, section: 0)
        self.projectTaskTable.reloadRows(at: [index], with: .automatic)
    }
}

extension ProjectTaskkListVC: ProjectTaskCellDelegate {
    func contactProfileSelected(withId: Int64) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactProfileVC") as? ContactProfileVC {
            ///let navControl = APPNavigationController(rootViewController: controller)
            controller.setMemberId(id: withId)
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    
}
