//
//  SignUpModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 09/08/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON

class SignUpModel: AppBaseModel {
    
    var service: String?
    var token: String?
    var profile: [ProfileModel]?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    
    private func parseResponse(with response: JSON) {
        
        self.service = response["service"].stringValue
        self.token = response["token"].stringValue
        for item in response.arrayValue {
            if let profileModel = self.createObject(with: item) {
                print(profileModel)
                if self.profile == nil {
                    self.profile = [ProfileModel]()
                }
                self.profile?.append(profileModel)
            }
        }

    }
    
    private func createObject(with jsonObject: JSON) -> ProfileModel? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return ProfileModel.init(jsonObject)
        }
    }
    
}


class ProfileModel: AppBaseModel {
    var userId: Int64?
    var full_name: String?
    var last_name: String?
    var password: String?
    var email: String?
    var phone_number: String?
    var city: String?
    var state: String?
    var country: String?
    var pincode: Int64?
    var aboutme: String?
    var gender: String?
    var education: String?
    var skills: String?
    var business: String?
    var industry: String?
    var department: String?
    var job: String?
    var resume: String?
    var image: String?
    var user_type: Int64?
    var website: String?
    var created_time: String?
    var designation: String?
    var zipcode: String?
    var hereFor: String?
    var requiredSkills: String?



    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        print("parseResponse : ", response)
        self.userId = response["uid"].int64Value
        self.full_name = response["ufullname"].stringValue
        self.last_name = response["ulname"].stringValue
        self.email = response["uemailid"].stringValue
        self.phone_number = response["uphone"].stringValue
        self.city = response["ucity"].stringValue
        self.state = response["ustate"].stringValue
        self.country = response["ucountry"].stringValue
        self.pincode = response["uzipcode"].int64Value
        self.aboutme = response["uaboutu"].stringValue
        self.gender = response["ugender"].stringValue
        self.education = response["education"].stringValue
        self.skills = response["uskills"].stringValue
        self.business = response["business"].stringValue
        self.industry = response["industry"].stringValue
        self.department = response["department"].stringValue
        self.job = response["job"].stringValue
        self.resume = response["resume"].stringValue
        self.image = response["uphoto"].stringValue
        self.user_type = response["utype"].int64Value
        self.website = response["uwebsite"].stringValue
        self.created_time = response["created_time"].stringValue
        self.designation = response["designation"].stringValue
        self.zipcode = response["uzipcode"].stringValue
        self.hereFor = response["hereFor"].stringValue
        self.requiredSkills = response["required_skills"].stringValue

    }
}
