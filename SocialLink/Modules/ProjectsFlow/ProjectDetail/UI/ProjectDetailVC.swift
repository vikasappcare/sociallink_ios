//
//  ProjectDetailVC.swift
//  SocialLink
//
//  Created by Santhosh on 21/04/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ProjectDetailVC: AppBaseVC {
    
    @IBOutlet weak var roundIcon: UIImageView!
    @IBOutlet weak var projectNameLabel: UILabel!
    @IBOutlet weak var projectProgress: UIProgressView!
    @IBOutlet weak var projectCollectionview: UICollectionView!
    
    let detailText = ["OVERVIEW", "FEED", "PEOPLE", "TASKS", "ATTACHMENT", "BILLING"]
    let detailImages = ["overView", "ProjectsFeed", "ProjectContacts", "ProjectManagement",  "ProjectAttachments", "ProjectInvoice"]
    var selectedProject: Project?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setCollectionView()
        
        roundIcon.center = self.view.center
        roundIcon.center.y = projectCollectionview.frame.origin.y
        
        //self.createNavigationItems()
        self.navigationItem.title = "MENU"
        self.updateData()
    }
    
    override func viewDidLayoutSubviews() {
//        self.projectProgress.layer.cornerRadius = self.projectProgress.frame.height / 2
//        self.projectProgress.layer.masksToBounds = true
    }
    func setProject(project: Project) {
        self.selectedProject = project
    }
    
    func updateData() {
        
        if let projectIs = self.selectedProject {
            projectNameLabel.text = projectIs.project_name
            let progress = (projectIs.progress ?? 0.0) / 100
         //   projectProgress.progress = progress
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ProjectDetailVC {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension ProjectDetailVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    func setCollectionView() {
        self.projectCollectionview.dataSource = self
        self.projectCollectionview.delegate = self
        
        if let flow = projectCollectionview.collectionViewLayout as? UICollectionViewFlowLayout {
            let itemSpacing: CGFloat = 3
            let itemsInOneLine: CGFloat = 3
            let width = UIScreen.main.bounds.size.width - itemSpacing * CGFloat(itemsInOneLine - 1)
            flow.itemSize = CGSize(width: floor(width/itemsInOneLine), height: width/itemsInOneLine)
            flow.minimumInteritemSpacing = 3
            flow.minimumLineSpacing = 3
            let cellHeight = flow.itemSize.height + flow.minimumInteritemSpacing
            let totalCellHeight = cellHeight * 2
            let value = (self.projectCollectionview.frame.height - totalCellHeight) / 2
            flow.sectionInset = UIEdgeInsets(top: value, left: 0, bottom: 0, right: 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return detailText.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.layoutSubviews()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 120.0, height: 120.0)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.ProjectListCellIdentifier, for: indexPath)
        if let cellIs = cell as? ProjectListCell {
            
            let data = ["image": detailImages[indexPath.row],
                        "text": detailText[indexPath.row]]
            cellIs.setData(data: data)
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(
            title: "", style: .done, target: nil, action: nil)
        switch indexPath.row {
        case 0:
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "OverViewController") as? OverViewController {
                if let project = selectedProject {
                    controller.setProject(project: project)
                }
                self.navigationController?.pushViewController(controller, animated: true)
            }
        case 1:
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProjectFeedVC") as? ProjectFeedVC {
                controller.setProjectFeedType(type: ProjectFeedType.Individual)
                if let projectId = self.selectedProject?.project_id {
                    controller.setProjectId(projectId: projectId)
                }
                self.navigationController?.pushViewController(controller, animated: true)
            }
        case 2:
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "TeamInfoVC") as? TeamInfoVC {
                if let project = self.selectedProject {
                    controller.setProject(project: project)
                }
                self.navigationController?.pushViewController(controller, animated: true)
            }
            //        case 2:
            //            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProjectMesagesListVC") as? ProjectMesagesListVC {
            //                self.navigationController?.pushViewController(controller, animated: true)
        //            }
        case 3:
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProjectTaskkListVC") as? ProjectTaskkListVC {
                if let project = selectedProject {
                    controller.setProject(project: project)
                }
                self.navigationController?.pushViewController(controller, animated: true)
            }
            //        case 4:
            //            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "InviteTeamMember") as? InviteTeamMember {
            //                controller.showBackButton(show: true, hideProject: true)
            //                if let project = selectedProject {
            //                    controller.setSelectedProject(project: project)
            //                }
            //                self.navigationController?.pushViewController(controller, animated: true)
            //            }
            
        case 4:
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "AttachmentsController") as? AttachmentsController {
                if let project = selectedProject {
                    controller.setProject(project: project)
                }
                self.navigationController?.pushViewController(controller, animated: true)
            }
        case 5:
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProjectPaymentVC") as? ProjectPaymentVC {
                if let project = selectedProject {
                    controller.setProject(project: project)
                }
                self.navigationController?.pushViewController(controller, animated: true)
            }
        default:
            break
        }
    }
}
