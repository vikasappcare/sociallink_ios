//
//  AppBaseVC.swift
//  BaseProject
//
//  Created by Santhosh Marripelli on 23/02/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire
import SDWebImage
import CoreLocation
import ReachabilitySwift
import SwiftyDrop


class AppBaseVC: UIViewController {
    var loadingHud: MBProgressHUD?
    var isReachable: Bool?
    var activityIndicator: UIActivityIndicatorView?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startListiningLocaton()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.networkBanner()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}
extension AppBaseVC {
    
    func addActivityIndicatior() {
        DispatchQueue.main.async {
            self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            self.activityIndicator?.hidesWhenStopped = true
            self.activityIndicator?.startAnimating()
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.activityIndicator!)
        }
    }
    
    func removeActivityIndicator() {
        self.activityIndicator?.stopAnimating()
    }
}


// MARK: - Adding MBPogress HUD
extension AppBaseVC {
    func showHUD(title: String?) {
        resetLoadingView()
        loadingHud = MBProgressHUD.showAdded(to: view, animated: true)
        loadingHud?.mode = MBProgressHUDMode.indeterminate
        loadingHud?.label.text = title
    }
    private func resetLoadingView() {
        if loadingHud == nil {
            loadingHud = MBProgressHUD()
        }
        loadingHud?.hide(animated: true)
    }
    func hideHud() {
        self.resetLoadingView()
    }
}


// MARK: - Adding a Network Banner
extension AppBaseVC {
    func networkBanner() {
        networkObserver { (reachable) in
           /// self.view.backgroundColor = reachable == true ? UIColor.appGreenColor : UIColor.appRedColor
            self.isReachable = reachable
            if self.isReachable ?? false {
            } else {
                self.showNetworkAlert()
            }
        }
    }
    
    func networkObserver(networkReachable: @escaping (_ YES: Bool)->Void) {
        let net = NetworkReachabilityManager()
        net?.startListening()
        net?.listener = { status in
            print("\(status)")
            if net?.isReachable ?? false {
                networkReachable(true)
            } else {
                networkReachable(false)
            }
        }
    }
    
    func showNetworkAlert() {
        enum Custom: DropStatable {
            case errorAlert
            var backgroundColor: UIColor? {
                switch self {
                case .errorAlert: return UIColor.appRedColor
                }
            }
            var font: UIFont? {
                switch self {
                case .errorAlert: return UIFont.semiBoldFont(16.0)
                }
            }
            var textColor: UIColor? {
                switch self {
                case .errorAlert: return .white
                }
            }
            var textAlignment: NSTextAlignment? {
                switch self {
                case .errorAlert: return NSTextAlignment.center
                }
            }
            var blurEffect: UIBlurEffect? {
                switch self {
                case .errorAlert: return nil
                }
            }
        }
        Drop.down("No Internet Connection...!", state: Custom.errorAlert)
    }
}

extension AppBaseVC: LocationServiceDelegate {
    
    func startListiningLocaton() {
        LocationSingleton.sharedInstance.startUpdatingLocation()
        LocationSingleton.sharedInstance.delegate = self
    }
    
    func tracingLocation(currentLocation: CLLocation) {
        dprint(object: currentLocation.coordinate.latitude)
        dprint(object: currentLocation.coordinate.longitude)
    }
    
    func tracingLocationDidFailWithError(error: NSError) {
        dprint(object: error)
    }
}

extension AppBaseVC {
    
    func setTabBarHidden(_ hidden: Bool, animated: Bool = true, duration: TimeInterval = 0.09) {
        if animated {
            UIView.animate(withDuration: duration, animations: {
                self.tabBarController?.tabBar.layer.zPosition = hidden ? -1 : 0
            })
        }
        self.tabBarController?.tabBar.isHidden = hidden
    }
}
