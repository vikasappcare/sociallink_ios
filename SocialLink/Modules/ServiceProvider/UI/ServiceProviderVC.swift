//
//  ServiceProviderVC.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ServiceProviderVC: UIViewController {

    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var serviceProviderTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
self.navigationItem.title = "GRAPHIC DESIGNER"
        self.setTableViewDatasource()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ServiceProviderVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.serviceProviderTable.delegate = self
        self.serviceProviderTable.dataSource = self
        self.serviceProviderTable.estimatedRowHeight = 300
        self.serviceProviderTable.rowHeight = UITableViewAutomaticDimension
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        switch section {
//        case 0:
//            return "Refine Search:"
//        case 1:
//            return "Qualified Designers:"
//        default:
//            return ""
//        }
//    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        let titleLabel = UILabel(frame: CGRect(x: 27, y: 0, width: UIScreen.main.bounds.width - 27, height: 30))
        switch section {
        case 0:
            titleLabel.text = "Refine Search:"
        case 1:
            titleLabel.text =  "Qualified Designers:"
        default:
            titleLabel.text = ""
        }
        headerView.addSubview(titleLabel)
        titleLabel.textColor = UIColor.init(red: 148/255.0, green: 148/255.0, blue: 148/255.0, alpha: 1.0)
        titleLabel.font = UIFont.semiBoldFont(12)
        headerView.backgroundColor = UIColor.init(red: 235/255.0, green: 235/255.0, blue: 235/255.0, alpha: 1.0)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return 1
        default:
            return  5
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.ServiceSpecializeCellIdentifier, for: indexPath)
            if let cellIs = cell as? ServiceSpecializeCell {
                cellIs.selectionStyle = .none
                
                let spelizeData = ["Logo Design",
                            "Print Desigh",
                            "Website Design",
                            "Social Media Design",
                            "Ad Design"]
                
                cellIs.setListOfData(data: spelizeData)
                
            }
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.ServiceContactsCellIdentifier, for: indexPath)
            if let cellIs = cell as? ServiceContactsCell {
                cellIs.selectionStyle = .none
            }
            return cell
        default:
            return UITableViewCell()
        }
      
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        switch indexPath.section {
        case 1:
             return true
        default:
             return false
        }
        
       
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        let add = UITableViewRowAction(style: .normal, title: "") { action, index in
            print("more button tapped")
        }
        add.backgroundColor = .lightGray
        add.backgroundColor = UIColor(patternImage: UIImage(named: "AddContact")!)
        return [add]
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactProfileVC") as? ContactProfileVC {
            /// let navController = APPNavigationController(rootViewController: controller)
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
