//
//  ProjectLikeCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 07/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ProjectLikeCell: UITableViewCell {

    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        self.profileImage.layer.cornerRadius = self.profileImage.frame.width / 2
        self.profileImage.layer.masksToBounds = true
    }
    
    
    func setData(name: String, pic: String) {
        profileName.text = name
        let userProfileImage = APPURL.feedUserProfileImage  + pic
        profileImage.sd_setImage(with: URL(string: userProfileImage), placeholderImage: UIImage(named: ""))
        //profileImage.sd_setImage(with: URL(string: data.uphoto ?? ""), placeholderImage: #imageLiteral(resourceName: "SampleProfile"))

        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
