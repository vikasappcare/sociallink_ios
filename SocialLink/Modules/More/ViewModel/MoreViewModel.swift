//
//  MoreViewModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 04/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
class MoreViewModel {
    
    init() {
    }
    
    func switchRole(parameters: [String: Any],
                      onSuccess: @escaping (UserRoleModel?) -> Void,
                      onError: @escaping APIErrorHandler) {
        AppDataManager.switchRole(parametes: parameters, onSuccess: { (dataSource) in
            print(dataSource ?? "")
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
    
    
}
