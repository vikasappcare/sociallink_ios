//
//  FeedModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 17/08/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON


class FeedModel: AppBaseModel {
    
    var service: String?
    var count: Int64?
    var list: [Feed]?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
    }
    
    private func parseResponse(with response: JSON) {
        print(response)
        self.service = response["service"].stringValue
        self.count = response["count"].int64Value
        for item in response.arrayValue {
            if let feedModel = self.createObject(with: item) {
                if self.list == nil {
                    self.list = [Feed]()
                }
                self.list?.append(feedModel)
            }
        }
    }
    
    private func createObject(with jsonObject: JSON) -> Feed? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return Feed.init(jsonObject)
        }
    }
    
}


class Feed: AppBaseModel {
    var feed_id: Int64?
    var feed_title: String?
    var feed_type: Int64?
    var project_id: Int64?
    var feed_type_id: Int64?
    var feed_details: String?
    var feed_time: String?
    var feed_document: String?
    var feed_extension: String?
    var likesCount: Int64?
    var flikes: Int64?
    var created_Date: String?
    var comments: String?
    var commentsCount: Int64?
    var username: String?
    var userimage: String?
    var userid: Int64?
    var userdesignation: String?
    var useremailid: String?
    var userliked: String?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        print(response)
        self.feed_id = response["fid"].int64Value
        self.project_id = response["project_id"].int64Value
        self.feed_type = response["ftype"].int64Value
        self.feed_type_id = response["ftype_id"].int64Value
        self.feed_extension = response["file_extension"].stringValue
        self.feed_details = response["fdetails"].stringValue
        self.feed_title = response["f_title"].stringValue
        self.feed_time = response["ftime"].stringValue
        self.likesCount = response["like_unlike_count"].int64Value
        self.flikes = response["flikes"].int64Value
        self.feed_document = response["document"].stringValue
        self.userid = response["fuser"].int64Value
        self.username = response["ufullname"].stringValue
        self.userimage = response["uphoto"].stringValue
        self.useremailid = response["uemailid"].stringValue
        self.userdesignation = response["designation"].stringValue
        self.created_Date = response["created_time"].stringValue
        self.commentsCount = response["comment_count"].int64Value
        self.comments = response["comment"].stringValue
        self.userliked = response["userliked"].stringValue
        self.userdesignation = response["designation"].stringValue
    }
    
    private func createLikeObject(with jsonObject: JSON) -> FeedLikes? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return FeedLikes.init(jsonObject)
        }
    }
    
    
    private func createFeedObject(with jsonObject: JSON) -> FeedData? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return FeedData.init(jsonObject)
        }
    }
    
    private func createObject(with jsonObject: JSON) -> CreatedBy? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return CreatedBy.init(jsonObject)
        }
    }
}

class FeedData: AppBaseModel {
    
    var data: String?
    var thumbnail: String?
        override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        self.data = response["data"].stringValue
        self.thumbnail = response["thumbnail"].stringValue
    }
}

class FeedLikes: AppBaseModel {
    
    var count: Int64?
    var flikes: Int64?
   /// var list: [LikedUsers]?
  
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        self.count = response["count"].int64Value
        self.flikes = response["liked"].int64Value
//        for item in response["list"].arrayValue {
//            if let model = self.createObject(with: item) {
//                if self.list == nil {
//                    self.list = [LikedUsers]()
//                }
//                self.list?.append(model)
//            }
//        }
        
    }
    
//    private func createObject(with jsonObject: JSON) -> LikedUsers? {
//        if jsonObject.isEmpty {
//            return nil
//        } else {
//            return LikedUsers.init(jsonObject)
//        }
//    }
}

//class LikedUsers: AppBaseModel {
//    
//    var user_id: Int64?
//    var name: String?
//    var image: String?
//
//    override init() {
//        super.init()
//    }
//    
//    init?(_ response: JSON) {
//        if response.isEmpty {
//            return nil
//        }
//        super.init(with: response)
//        
//        if response.exists() {
//            self.parseResponse(with: response)
//        }    }
//    
//    private func parseResponse(with response: JSON) {
//        self.user_id = response["user_id"].int64Value
//        self.name = response["name"].stringValue
//        self.image = response["image"].stringValue
//    }
//}
