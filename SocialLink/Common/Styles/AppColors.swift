//
//  AppColors.swift
//  BaseProject
//
//  Created by Santhosh Marripelli on 24/02/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import UIKit

struct CustomColors {
    static let red: String  = "#e74c3c"
    static let blue: String  = "#57afe2"
    static let lightBlue: String = "#ABE0F9"
    static let greenColor: String = "#3d733b"
    static let lightgray: String = "#e4e4e2"
    static let orange: String = "#f5a63a"
    static let darkGray: String = "#3d3d3e"
    static let mediumGray: String = "#9c9d99"
    static let borderColor: String = "#9B9B97"
    static let navigationTitleColor: String = "#3D3E3C"
    
    
    static let planBlueColor: String = "#00a4d8"
    static let planGrayColor: String = "#3d3e3c"
   static let planOrangeColor: String = "#efa951"
    
    
    static let projectGreenColor: String = "#27ae60"
    static let projectRedColor: String = "#c0392b"
    

}

extension UIColor {
    
    static var appRedColor: UIColor {
        return UIColor(hexString: CustomColors.red)
    }
    
    static var appBlueColor: UIColor {
        return UIColor(hexString: CustomColors.blue)
    }
    
    static var applightBlueColor: UIColor {
        return UIColor(hexString: CustomColors.lightBlue)
    }
    
    static var appGreenColor: UIColor {
        return UIColor(hexString: CustomColors.greenColor)
    }
    
    static var appLightGrayColor: UIColor {
        return UIColor(hexString: CustomColors.lightgray)
    }
    static var appOrangeColor: UIColor {
        return UIColor(hexString: CustomColors.orange)
    }
    static var appDarkGrayColor: UIColor {
        return UIColor(hexString: CustomColors.darkGray)
    }
    static var appMediumGrayColor: UIColor {
        return UIColor(hexString: CustomColors.mediumGray)
    }
    static var appBorderColor: UIColor {
        return UIColor(hexString: CustomColors.borderColor)
    }
    static var appNavigationTitleColor: UIColor {
        return UIColor(hexString: CustomColors.navigationTitleColor)
    }
    
    static var appprojectGreenColor: UIColor {
        return UIColor(hexString: CustomColors.projectGreenColor)
    }
    
    static var appprojectRedColor: UIColor {
        return UIColor(hexString: CustomColors.projectRedColor)
    }
    
    
    
    static var planBlue: UIColor {
        return UIColor(hexString: CustomColors.planBlueColor)
    }
    
    static var planOrange: UIColor {
        return UIColor(hexString: CustomColors.planOrangeColor)
    }
    
    static var planGray: UIColor {
        return UIColor(hexString: CustomColors.planGrayColor)
    }
    
    
    
}

extension UIColor {
    private convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}
