//
//  ProjectMesagesListVC.swift
//  SocialLink
//
//  Created by Santhosh on 22/04/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ProjectMesagesListVC: UIViewController {

    @IBOutlet weak var projectMessageListTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
self.setTableViewDatasource()
        
        self.navigationItem.title = "MESSAGES"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func viewDidAppear(_ animated: Bool) {
        if  self.navigationController?.navigationBar.backItem == nil {
            self.createNavigationItems()
        }
    }
    

}

extension ProjectMesagesListVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.projectMessageListTable.delegate = self
        self.projectMessageListTable.dataSource = self
        self.projectMessageListTable.estimatedRowHeight = 300
        self.projectMessageListTable.rowHeight = UITableViewAutomaticDimension
        self.projectMessageListTable.tableFooterView = UIView()
    }
    
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.ProjectMessageListCellIdentifier, for: indexPath)
            if let cellIs = cell as? ProjectMessageListCell {
                cellIs.selectionStyle = .none
                //            let data = ["profile": profileArray[indexPath.row],
                //                        "color": colorArray[indexPath.row]] as [String: Any]
                //            cellIs.setData(data: data)
            }
            cell.selectionStyle = .none
            return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
  
    }
    
    
    
}

extension ProjectMesagesListVC {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
}
