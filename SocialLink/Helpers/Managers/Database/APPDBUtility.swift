//
//  APPDBUtility.swift
//  BaseProject
//
//  Created by Santhosh Marripelli on 26/02/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import CoreData


class APPDBUtility {

    ////////////////////////GET DATA FROM DB////////////////////////////
    
    class func getUserData(mocType: MOCType) -> [UserDB]? {
        let moc = APPDBManager.shared.getMoc(for: mocType)
        let fetchRequest = NSFetchRequest<UserDB>.init(entityName: APPDBConstants.USER)
        do {
//            let sort = NSSortDescriptor(key: #keyPath(UserDB.userId), ascending: true)
//            fetchRequest.sortDescriptors = [sort]
            let userList: [UserDB] = try moc.fetch(fetchRequest)
            print(userList)
            return userList.isEmpty ? nil : userList
        } catch let error as NSError {
            Logger.log(message: "getUserData Could not get getUserData from DB. Error - \(error), \(error.userInfo)", event: .error)
        }
        return nil
    }
    
    class func getToken(mocType: MOCType) -> TokenDB? {
        let moc = APPDBManager.shared.getMoc(for: mocType)
        let fetchRequest = NSFetchRequest<TokenDB>.init(entityName: APPDBConstants.TOKEN)
        do {
            let tokenList: [TokenDB] = try moc.fetch(fetchRequest)
            return tokenList.isEmpty ? nil : tokenList.first
        } catch let error as NSError {
            Logger.log(message: "getToken Could not get getToken from DB. Error - \(error), \(error.userInfo)", event: .error)
        }
        return nil
    }
    
    
    class func getProjects(mocType: MOCType) -> [ProjectsDB]? {
        let moc = APPDBManager.shared.getMoc(for: mocType)
        let fetchRequest = NSFetchRequest<ProjectsDB>.init(entityName: APPDBConstants.PROJECT)
        let userType = APPStore.sharedStore.user.user_type ?? 0
        
        fetchRequest.predicate = NSPredicate.init(format: "user_type == %d", userType)
        do {
            let projectList: [ProjectsDB] = try moc.fetch(fetchRequest)
            return projectList.isEmpty ? nil : projectList
        } catch let error as NSError {
            Logger.log(message: "getProjects Could not get getProjects from DB. Error - \(error), \(error.userInfo)", event: .error)
        }
        return nil
    }
    
    class func getRoles(mocType: MOCType) -> [RolesDB]? {
        let moc = APPDBManager.shared.getMoc(for: mocType)
        let fetchRequest = NSFetchRequest<RolesDB>.init(entityName: APPDBConstants.ROLE)
        do {
            let objectList: [RolesDB] = try moc.fetch(fetchRequest)
            return objectList.isEmpty ? nil : objectList
        } catch let error as NSError {
            Logger.log(message: "getRoles Could not get getProjects from DB. Error - \(error), \(error.userInfo)", event: .error)
        }
        return nil
    }
    
    class func getServices(mocType: MOCType) -> [ServicesDB]? {
        let moc = APPDBManager.shared.getMoc(for: mocType)
        let fetchRequest = NSFetchRequest<ServicesDB>.init(entityName: APPDBConstants.SERVICE)
        do {
            let objectList: [ServicesDB] = try moc.fetch(fetchRequest)
            return objectList.isEmpty ? nil : objectList
        } catch let error as NSError {
            Logger.log(message: "getRoles Could not get getProjects from DB. Error - \(error), \(error.userInfo)", event: .error)
        }
        return nil
    }
    
    
    class func getFeeds(mocType: MOCType, id: Int64) -> [FeedsDB]? {
        let moc = APPDBManager.shared.getMoc(for: mocType)
        let fetchRequest = NSFetchRequest<FeedsDB>.init(entityName: APPDBConstants.FEED)
        let userType = APPStore.sharedStore.user.user_type ?? 0
        fetchRequest.predicate = NSPredicate.init(format: "project_id == %d AND user_type == %d", id, userType)
        do {
            let objectList: [FeedsDB] = try moc.fetch(fetchRequest)
            return objectList.isEmpty ? nil : objectList
        } catch let error as NSError {
            Logger.log(message: "getFeeds Could not get getProjects from DB. Error - \(error), \(error.userInfo)", event: .error)
        }
        return nil
    }
    
    class func getFeedWithFeedId(mocType: MOCType, id: Int64) -> [FeedsDB]? {
        let moc = APPDBManager.shared.getMoc(for: mocType)
        let fetchRequest = NSFetchRequest<FeedsDB>.init(entityName: APPDBConstants.FEED)
        let userType = APPStore.sharedStore.user.user_type ?? 0
        fetchRequest.predicate = NSPredicate.init(format: "feed_id == %d AND user_type == %d", id, userType)
        do {
            let objectList: [FeedsDB] = try moc.fetch(fetchRequest)
            return objectList.isEmpty ? nil : objectList
        } catch let error as NSError {
            Logger.log(message: "getFeedWithFeedId Could not get getProjects from DB. Error - \(error), \(error.userInfo)", event: .error)
        }
        return nil
    }
    
    class func getTasks(forId: Int64, mocType: MOCType) -> [TasksDB]? {
        let moc = APPDBManager.shared.getMoc(for: mocType)
        let fetchRequest = NSFetchRequest<TasksDB>.init(entityName: APPDBConstants.TASK)
         let userType = APPStore.sharedStore.user.user_type ?? 0
        fetchRequest.predicate = NSPredicate.init(format: "project_id == %d AND user_type == %d", forId, userType)
        do {
            let objectList: [TasksDB] = try moc.fetch(fetchRequest)
            return objectList.isEmpty ? nil : objectList
        } catch let error as NSError {
            Logger.log(message: "getFeeds Could not get getProjects from DB. Error - \(error), \(error.userInfo)", event: .error)
        }
        return nil
    }
    
    class func getTaskWithTaskId(forId: Int64, mocType: MOCType) -> TasksDB? {
        let moc = APPDBManager.shared.getMoc(for: mocType)
        let fetchRequest = NSFetchRequest<TasksDB>.init(entityName: APPDBConstants.TASK)
        let userType = APPStore.sharedStore.user.user_type ?? 0
        fetchRequest.predicate = NSPredicate.init(format: "task_id == %d AND user_type == %d", forId, userType)
        do {
            let objectList: [TasksDB] = try moc.fetch(fetchRequest)
            return objectList.isEmpty ? nil : objectList.first
        } catch let error as NSError {
            Logger.log(message: "getFeeds Could not get getProjects from DB. Error - \(error), \(error.userInfo)", event: .error)
        }
        return nil
    }
    
    class func getContractorTasks(mocType: MOCType) -> [TasksDB]? {
        let moc = APPDBManager.shared.getMoc(for: mocType)
        let fetchRequest = NSFetchRequest<TasksDB>.init(entityName: APPDBConstants.TASK)
        let userType = APPStore.sharedStore.user.user_type ?? 0
        fetchRequest.predicate = NSPredicate.init(format: "user_type == %d", userType)
        do {
            let objectList: [TasksDB] = try moc.fetch(fetchRequest)
            return objectList.isEmpty ? nil : objectList
        } catch let error as NSError {
            Logger.log(message: "getFeeds Could not get getProjects from DB. Error - \(error), \(error.userInfo)", event: .error)
        }
        return nil
    }
    
    
    
    class func getTimeLogs(forId: Int64, mocType: MOCType) -> [TimeLogsDB]? {
        let moc = APPDBManager.shared.getMoc(for: mocType)
        let fetchRequest = NSFetchRequest<TimeLogsDB>.init(entityName: APPDBConstants.TIMELOGS)
        fetchRequest.predicate = NSPredicate.init(format: "task_id == %d", forId)
        do {
            let objectList: [TimeLogsDB] = try moc.fetch(fetchRequest)
            return objectList
        } catch let error as NSError {
            Logger.log(message: "getTimeLogs Could not get getProjects from DB. Error - \(error), \(error.userInfo)", event: .error)
        }
        return nil
    }
    
}
