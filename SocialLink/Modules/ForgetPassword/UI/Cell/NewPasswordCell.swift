//
//  NewPasswordCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

protocol NewPasswordCellDelegate: class {
    func changePasswordSelected()
}
class NewPasswordCell: UITableViewCell {
    weak var delegate: NewPasswordCellDelegate?

    @IBOutlet weak var changeButton: UIButton!
    @IBOutlet weak var confirmPassword: AppTextField!
    @IBOutlet weak var newPassword: AppTextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func getData() -> [String: Any] {
        let data = ["password": self.newPassword.text ?? "",
                    "confirmPassword": self.confirmPassword.text ?? ""]
        return data
    }

    @IBAction func changePasswordClicked(_ sender: Any) {
        self.delegate?.changePasswordSelected()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
