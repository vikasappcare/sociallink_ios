//
//  TimeSheetViewModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 19/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation

class TimeSheetViewModel {
    
    init() {
        
    }
    
    var timesheetArray: [TimeSheet]?
    
    func getContractorTimesheets(parameters: [String: Any],
                         onSuccess: @escaping (TimeSheetModel?) -> Void,
                         onError: @escaping APIErrorHandler) {
        AppDataManager.getContractorTimesheets(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: dataSource)
            if let data = dataSource {
                self.timesheetArray = data.list
            }
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
}
