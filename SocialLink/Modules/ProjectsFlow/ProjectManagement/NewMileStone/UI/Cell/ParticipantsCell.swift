//
//  ParticipantsCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 09/05/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ParticipantsCell: UICollectionViewCell {
    
    @IBOutlet weak var participantsImage: UIImageView!
    
    
    func setData(data: CreatedBy) {
        participantsImage.layer.cornerRadius = participantsImage.frame.size.width / 2
        participantsImage.layer.masksToBounds = true
        participantsImage.sd_setImage(with: URL(string: data.image ?? ""), placeholderImage: #imageLiteral(resourceName: "SampleProfile"))
    }
    
    
}
