//
//  APPDBModel.swift
//  BaseProject
//
//  Created by Santhosh Marripelli on 26/02/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON

class APPDBModel {

    // MARK: - Save User In CoreData
    class func saveUserInDB(_ userJson: JSON, mocType: MOCType = .main) {
        dprint(object: "USER Parsing started: \(Date())")
        Logger.log(message: "Coredata User Parsing started", event: .info)
        let parseStartDate = Date()
        print(userJson)
        APPDBCommon.addOrUpdateUserDataintoCoreData(with: userJson, onSuccess: { (dbUser) in
            guard let dbUserObj = dbUser else {
                return
            }
            dprint(object: dbUserObj)
        })
        APPDBManager.shared.save(with: mocType, wait: true)
        let elapsed = Date().timeIntervalSince(parseStartDate)
        Logger.log(message: "Parsing Done, Parsing Time: \(elapsed) seconds", event: .info)
        dprint(object: "USER Parsing Ended: \(Date()), Parsing Time: \(elapsed) seconds")
        
    }
    
    class func updateUserPictureInDB(_ picture: String, mocType: MOCType = .main) {
        dprint(object: "USER Parsing started: \(Date())")
        Logger.log(message: "Coredata User Parsing started", event: .info)
        let parseStartDate = Date()
        APPDBCommon.updateUserPictureintoCoreData(with: picture, onSuccess: { (bool) in
            guard let boolIs = bool else {
                return
            }
            dprint(object: boolIs)
        })
        APPDBManager.shared.save(with: mocType, wait: true)
        let elapsed = Date().timeIntervalSince(parseStartDate)
        Logger.log(message: "Parsing Done, Parsing Time: \(elapsed) seconds", event: .info)
        dprint(object: "USER Parsing Ended: \(Date()), Parsing Time: \(elapsed) seconds")
        
    }
    
    class func saveTokenInDB(_ userJson: JSON, mocType: MOCType = .main) {
        dprint(object: "USER Parsing started: \(Date())")
        Logger.log(message: "Coredata User Parsing started", event: .info)
        let parseStartDate = Date()
        APPDBCommon.addOrUpdateTokenCoreData(with: userJson, onSuccess: { (token) in
            guard let dbTokenObj = token else {
                return
            }
            dprint(object: dbTokenObj)
        })
        APPDBManager.shared.save(with: mocType, wait: true)
        let elapsed = Date().timeIntervalSince(parseStartDate)
        Logger.log(message: "Parsing Done, Parsing Time: \(elapsed) seconds", event: .info)
        dprint(object: "Token Parsing Ended: \(Date()), Parsing Time: \(elapsed) seconds")
        
    }
    

    
    class func saveProjectsInDB(_ data: JSON, mocType: MOCType = .main) {
        dprint(object: "Project Parsing started: \(Date())")
        Logger.log(message: "Coredata Project Parsing started", event: .info)
        let parseStartDate = Date()
        APPDBCommon.addOrUpdateProjectCoreData(with: data, onSuccess: { (token) in
            guard let dbTokenObj = token else {
                return
            }
            dprint(object: dbTokenObj)
        })
        APPDBManager.shared.save(with: mocType, wait: true)
        let elapsed = Date().timeIntervalSince(parseStartDate)
        Logger.log(message: "Parsing Done, Parsing Time: \(elapsed) seconds", event: .info)
        dprint(object: "Token Parsing Ended: \(Date()), Parsing Time: \(elapsed) seconds")
        
    }
    
    class func updateProjectInDB(_ data: JSON, mocType: MOCType = .main) {
        dprint(object: "Project Parsing started: \(Date())")
        Logger.log(message: "Coredata Project Parsing started", event: .info)
        let parseStartDate = Date()
        APPDBCommon.updateProjectCoreData(with: data, onSuccess: { (token) in
            guard let dbTokenObj = token else {
                return
            }
            dprint(object: dbTokenObj)
        })
        APPDBManager.shared.save(with: mocType, wait: true)
        let elapsed = Date().timeIntervalSince(parseStartDate)
        Logger.log(message: "Parsing Done, Parsing Time: \(elapsed) seconds", event: .info)
        dprint(object: "Token Parsing Ended: \(Date()), Parsing Time: \(elapsed) seconds")
        
    }

    
    class func saveRolesInDB(_ data: JSON, mocType: MOCType = .main) {
        dprint(object: "Role Parsing started: \(Date())")
        Logger.log(message: "Coredata Role Parsing started", event: .info)
        let parseStartDate = Date()
        APPDBCommon.addOrUpdateRoleCoreData(with: data, onSuccess: { (role) in
            guard let dbTokenObj = role else {
                return
            }
            dprint(object: dbTokenObj)
        })
        APPDBManager.shared.save(with: mocType, wait: true)
        let elapsed = Date().timeIntervalSince(parseStartDate)
        Logger.log(message: "Parsing Done, Parsing Time: \(elapsed) seconds", event: .info)
        dprint(object: "Role Parsing Ended: \(Date()), Parsing Time: \(elapsed) seconds")
        
    }
    class func saveServicesInDB(_ data: JSON, mocType: MOCType = .main) {
        dprint(object: "Service Parsing started: \(Date())")
        Logger.log(message: "Coredata Role Parsing started", event: .info)
        let parseStartDate = Date()
        APPDBCommon.addOrUpdateServiceCoreData(with: data, onSuccess: { (role) in
            guard let dbTokenObj = role else {
                return
            }
            dprint(object: dbTokenObj)
        })
        APPDBManager.shared.save(with: mocType, wait: true)
        let elapsed = Date().timeIntervalSince(parseStartDate)
        Logger.log(message: "Parsing Done, Parsing Time: \(elapsed) seconds", event: .info)
        dprint(object: "Role Parsing Ended: \(Date()), Parsing Time: \(elapsed) seconds")
        
    }
    
    
    class func saveFeedsInDB(_ data: JSON, mocType: MOCType = .main) {
        dprint(object: "Feed Parsing started: \(Date())")
        print(data)
        Logger.log(message: "Coredata PrFeedoject Parsing started", event: .info)
        let parseStartDate = Date()
        APPDBCommon.addOrUpdateFeedCoreData(with: data, onSuccess: { (feed) in
            guard let dbTokenObj = feed else {
                return
            }
            dprint(object: dbTokenObj)
        })
        APPDBManager.shared.save(with: mocType, wait: true)
        let elapsed = Date().timeIntervalSince(parseStartDate)
        Logger.log(message: "Parsing Done, Parsing Time: \(elapsed) seconds", event: .info)
        dprint(object: "Feed Parsing Ended: \(Date()), Parsing Time: \(elapsed) seconds")
        
    }
    
    class func saveFeedInDB(_ data: JSON, mocType: MOCType = .main) {
        dprint(object: "Feed Parsing started: \(Date())")
        Logger.log(message: "Coredata PrFeedoject Parsing started", event: .info)
        let parseStartDate = Date()
        APPDBCommon.addOrUpdateNewFeedCoreData(with: data, onSuccess: { (feed) in
            guard let dbTokenObj = feed else {
                return
            }
            dprint(object: dbTokenObj)
        })
        APPDBManager.shared.save(with: mocType, wait: true)
        let elapsed = Date().timeIntervalSince(parseStartDate)
        Logger.log(message: "Parsing Done, Parsing Time: \(elapsed) seconds", event: .info)
        dprint(object: "Feed Parsing Ended: \(Date()), Parsing Time: \(elapsed) seconds")
        
    }
    
    class func updateFeedLike(_ data: JSON, mocType: MOCType = .main) {
        dprint(object: "Feed Parsing started: \(Date())")
        Logger.log(message: "Coredata PrFeedoject Parsing started", event: .info)
        let parseStartDate = Date()
        APPDBCommon.updateFeedLike(with: data, onSuccess: { (feed) in
            guard let dbTokenObj = feed else {
                return
            }
            dprint(object: dbTokenObj)
        })
        APPDBManager.shared.save(with: mocType, wait: true)
        let elapsed = Date().timeIntervalSince(parseStartDate)
        Logger.log(message: "Parsing Done, Parsing Time: \(elapsed) seconds", event: .info)
        dprint(object: "Feed Parsing Ended: \(Date()), Parsing Time: \(elapsed) seconds")
        
    }
    
    class func updateFeedImage(_ data: FileUploadModel, mocType: MOCType = .main) {
        dprint(object: "Feed Parsing started: \(Date())")
        Logger.log(message: "Coredata PrFeedoject Parsing started", event: .info)
        let parseStartDate = Date()
        APPDBCommon.updateFeedImage(with: data, onSuccess: { (feed) in
            guard let dbTokenObj = feed else {
                return
            }
            dprint(object: dbTokenObj)
        })
        APPDBManager.shared.save(with: mocType, wait: true)
        let elapsed = Date().timeIntervalSince(parseStartDate)
        Logger.log(message: "Parsing Done, Parsing Time: \(elapsed) seconds", event: .info)
        dprint(object: "Feed Parsing Ended: \(Date()), Parsing Time: \(elapsed) seconds")
        
    }
    
    class func saveTasksInDB(_ data: JSON, mocType: MOCType = .main) {
        dprint(object: "Tasks Parsing started: \(Date())")
        Logger.log(message: "Coredata Tasks Parsing started", event: .info)
        let parseStartDate = Date()
        APPDBCommon.addOrUpdateTasksCoreData(with: data, onSuccess: { (task) in
            guard let dbTokenObj = task else {
                return
            }
            dprint(object: dbTokenObj)
        })
        APPDBManager.shared.save(with: mocType, wait: true)
        let elapsed = Date().timeIntervalSince(parseStartDate)
        Logger.log(message: "Parsing Done, Parsing Time: \(elapsed) seconds", event: .info)
        dprint(object: "Tasks Parsing Ended: \(Date()), Parsing Time: \(elapsed) seconds")
        
    }
    
    class func saveTaskInDB(_ data: JSON, mocType: MOCType = .main) {
        dprint(object: "Tasks Parsing started: \(Date())")
        Logger.log(message: "Coredata Tasks Parsing started", event: .info)
        let parseStartDate = Date()
        APPDBCommon.addOrUpdateTaskCoreData(with: data, onSuccess: { (task) in
            guard let dbTokenObj = task else {
                return
            }
            dprint(object: dbTokenObj)
        })
        APPDBManager.shared.save(with: mocType, wait: true)
        let elapsed = Date().timeIntervalSince(parseStartDate)
        Logger.log(message: "Parsing Done, Parsing Time: \(elapsed) seconds", event: .info)
        dprint(object: "Tasks Parsing Ended: \(Date()), Parsing Time: \(elapsed) seconds")
        
    }
    
    
    class func updateTaskInDB(_ data: JSON, mocType: MOCType = .main) {
        dprint(object: "Tasks Parsing started: \(Date())")
        Logger.log(message: "Coredata Tasks Parsing started", event: .info)
        let parseStartDate = Date()
        APPDBCommon.updateTaskCoreData(with: data, onSuccess: { (task) in
            guard let dbTokenObj = task else {
                return
            }
            dprint(object: dbTokenObj)
        })
        APPDBManager.shared.save(with: mocType, wait: true)
        let elapsed = Date().timeIntervalSince(parseStartDate)
        Logger.log(message: "Parsing Done, Parsing Time: \(elapsed) seconds", event: .info)
        dprint(object: "Tasks Parsing Ended: \(Date()), Parsing Time: \(elapsed) seconds")
        
    }
    
    
    
    
    class func updateUserRoleInDB(_ userId: Int64, role: Int64, mocType: MOCType = .main) {
        dprint(object: "User Parsing started: \(Date())")
        Logger.log(message: "Coredata User Parsing started", event: .info)
        let parseStartDate = Date()
        APPDBCommon.updateUserRole(with: userId, role: role, mocType: .main, onSuccess: { (task) in
            guard let dbTokenObj = task else {
                return
            }
            dprint(object: dbTokenObj)
        })
        APPDBManager.shared.save(with: mocType, wait: true)
        let elapsed = Date().timeIntervalSince(parseStartDate)
        Logger.log(message: "Parsing Done, Parsing Time: \(elapsed) seconds", event: .info)
        dprint(object: "User Parsing Ended: \(Date()), Parsing Time: \(elapsed) seconds")
        
    }
    
    class func saveTimeLogsInDB(_ data: JSON, mocType: MOCType = .main) {
        dprint(object: "Tasks Parsing started: \(Date())")
        Logger.log(message: "Coredata Tasks Parsing started", event: .info)
        let parseStartDate = Date()
        APPDBCommon.addOrUpdateTimeLogInCoreData(with: data, onSuccess: { (task) in
            guard let dbTokenObj = task else {
                return
            }
            dprint(object: dbTokenObj)
        })
        APPDBManager.shared.save(with: mocType, wait: true)
        let elapsed = Date().timeIntervalSince(parseStartDate)
        Logger.log(message: "Parsing Done, Parsing Time: \(elapsed) seconds", event: .info)
        dprint(object: "saveTimeLogsInDB Parsing Ended: \(Date()), Parsing Time: \(elapsed) seconds")
        
    }
    
    
    
    
    
    
}
