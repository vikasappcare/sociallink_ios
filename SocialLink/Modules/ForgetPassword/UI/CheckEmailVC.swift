//
//  CheckEmailVC.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class CheckEmailVC: AppBaseVC {

    @IBOutlet weak var checkEmailTable: UITableView!
    var viewModel: ForgetPasswordViewModel?
    var otpObject: OTPModule?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = ForgetPasswordViewModel()

        self.setTableViewDatasource()
        ///self.createNavigationItems()
        self.navigationItem.title = "FORGET PASSWORD"

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setOTP(otp: OTPModule) {
        self.otpObject = otp
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//extension CheckEmailVC {
//
//    func createNavigationItems() {
//        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
//        self.navigationItem.leftBarButtonItem = cancelItem
//    }
//
//    @objc func cancelClicked() {
//        self.dismiss(animated: true, completion: nil)
//
//    }
//}

extension CheckEmailVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.checkEmailTable.delegate = self
        self.checkEmailTable.dataSource = self
        self.checkEmailTable.estimatedRowHeight = 300
        self.checkEmailTable.rowHeight = UITableViewAutomaticDimension
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:
            CellIdentifiers.CheckEmailTableCellIdentifier, for: indexPath)
        if let cellIs = cell as? CheckEmailTableCell {
            cellIs.selectionStyle = .none
            cellIs.delegate = self
        }
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UIScreen.main.bounds.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}


extension CheckEmailVC: CheckEmailTableCellDelegate {
    func submitSelected() {
        
        
        /*let indexPath = IndexPath(row: 1, section: 0)
         let cell = checkEmailTable.cellForRow(at: indexPath) as? CheckEmailTableCell
         
         let email = cell?.getData().trimmingCharacters(in: .whitespaces)
         
         if (email?.isEmpty ?? false) || (email == "") {
         self.showAlert(title: "Alert!", message: "Please Otp to continue")
         return
         }
        
         if let model = viewModel {
         let parms = ["email": email,
                      "user_type": 0] as [String : Any]
         self.showHUD(title: "Validating ...")
         model.confirmOTP(parameters: parms, onSuccess: { [weak self] (dataSource) in
         print("Success")
         print(dataSource)
         self?.hideHud()
         }, onError: { (error) in
         Logger.log(message: "Error \(error.statusMessage)", event: .error)
         self.hideHud()
         })
         }*/
        
        
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = checkEmailTable.cellForRow(at: indexPath) as? CheckEmailTableCell
        
        let otp = cell?.getData().trimmingCharacters(in: .whitespaces)
        
        if (otp?.isEmpty ?? false) || (otp == "") {
            self.showAlert(title: "Alert!", message: "Please Enter Otp to continue")
            return
        }
        
        if Int64(otp ?? "")! != (otpObject?.otp)! {
            self.showAlert(title: "Alert!", message: "Please Enter Correct Otp to continue")
            return
        }
        
        
        
        
        
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "NewPasswordVC") as? NewPasswordVC {
            /// let navController = APPNavigationController(rootViewController: controller)
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    
    
}
