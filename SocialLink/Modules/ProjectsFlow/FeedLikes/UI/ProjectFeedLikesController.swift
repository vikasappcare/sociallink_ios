//
//  ProjectFeedLikesController.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 07/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ProjectFeedLikesController: AppBaseVC {
    
    @IBOutlet weak var likesTable: UITableView!
    var likesNameArray = [String]()
    var likesPicArray = [String]()

    var viewModel: FeedikesViewModel?
    var selectedFeedId: Int64?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = FeedikesViewModel()
        //likesArray = [FeedLikesModel]()
        self.setTableViewDatasource()
        self.createNavigationItems()
        self.getLikes()
        self.title = "Likes"
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func getLikes() {
        
        if let model = viewModel {
            if let idIs = self.selectedFeedId {
                let parms = ["fid": idIs] as [String : Any]
                print(parms)
                self.showHUD(title: "")
                model.getLikes(parameters: parms, onSuccess: { [weak self] (dataSource) in
                    print("Success")
                    print(dataSource ?? "")
                    self?.likesNameArray = (dataSource?.ufullname)! as! [String]
                    self?.likesPicArray = (dataSource?.uphoto)! as! [String]

//                    self?.likesArray = dataSource?.lik
                    self?.likesTable.reloadData()
                    self?.hideHud()
                    }, onError: { (error) in
                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
                     //   self.showAlert(title: Message.Alert, message: error.statusMessage)
                        self.hideHud()
                })
            }
            
            
        }
    }
    
    func setFeedId(feedId: Int64) {
        self.selectedFeedId = feedId
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ProjectFeedLikesController {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension ProjectFeedLikesController: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.likesTable.delegate = self
        self.likesTable.dataSource = self
        self.likesTable.estimatedRowHeight = 300
        self.likesTable.rowHeight = UITableViewAutomaticDimension
        self.likesTable.tableFooterView = UIView()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if likesNameArray.count > 0 {
            return likesNameArray.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:
            CellIdentifiers.ProjectLikeCellIdentifier, for: indexPath)
        if let cellIs = cell as? ProjectLikeCell {
            cellIs.selectionStyle = .none
            if likesNameArray.count > 0 {
                cellIs.setData(name: likesNameArray[indexPath.row], pic: likesPicArray[indexPath.row])
            }
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.001, execute: {
//
//        })

//        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactProfileVC") as? ContactProfileVC {
//            if let likes = likesArray {
//              controller.setMemberId(id: likes[indexPath.row].user_id ?? 0)
//            }
//            self.navigationController?.pushViewController(controller, animated: true)
//        }


    }
    
}
