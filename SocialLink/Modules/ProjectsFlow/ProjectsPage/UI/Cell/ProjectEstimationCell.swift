//
//  ProjectEstimationCell.swift
//  SocialLink
//
//  Created by Harsha on 27/03/19.
//  Copyright © 2019 Santhosh Marripelli. All rights reserved.
//

import UIKit



class ProjectEstimationCell: UITableViewCell {
    

    @IBOutlet weak var projectNameLabel: UILabel!
    @IBOutlet weak var estimatedTimeLabel: UILabel!
    @IBOutlet weak var estimatedBudgetLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var declineButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func acceptButtonAction(_ sender: Any) {
        
    }
    @IBAction func declineButtonAction(_ sender: Any) {
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    func setData(data: EstimatedProject, color: UIColor) {
        projectNameLabel.text = data.pname
       // estimatedTimeLabel.text = data.etime
        estimatedBudgetLabel.text = data.ebudget
        descriptionLabel.text = data.quote_scope
    }
}
