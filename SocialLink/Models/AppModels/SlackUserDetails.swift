//
//  SlackUserDetails.swift
//  SocialLink
//
//  Created by Appcare Apple on 12/06/19.
//

import Foundation
import SwiftyJSON

class SlackUserDetails: AppBaseModel {
    var id = [String]()
    var name = [String]()
    var image_48 = [String]()
    var deleted = [String]()
    var real_name = [String]()
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    private func parseResponse(with response: JSON) {
        print(response)
        for i in 0..<response.count{
            self.id.append(response[i]["id"].stringValue)
            self.name .append(response[i]["name"].stringValue)
            self.image_48.append(response[i]["profile"]["image_48"].stringValue)
            self.deleted .append(response[i]["deleted"].stringValue)
            self.real_name.append(response[i]["real_name"].stringValue)
        }
        
    }
    
}
