//
//  QuestionTableCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 11/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class QuestionTableCell: UITableViewCell {

    @IBOutlet weak var questionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
