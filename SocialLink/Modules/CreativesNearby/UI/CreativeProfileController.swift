//
//  CreativeProfileController.swift
//  SocialLink
//
//  Created by Harsha on 09/04/19.
//

import UIKit
import iOSDropDown

var detailsArr = [String]()
class CreativeProfileController: AppBaseVC {
    
    var viewModel: CreativesNearbyViewModel?
    

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var skills: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var place: UILabel!
    @IBOutlet weak var projectTextField: DropDown!
    @IBOutlet weak var sendInvitation: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var backView: UIView!
    var projectNameArr = [String]()
    var projectIdArr = [Int64]()
    var projectId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        profileImage.layer.cornerRadius = profileImage.bounds.width * 0.5
        profileImage.layer.masksToBounds = true

        profileImage.layer.borderWidth = 1
        profileImage.layer.borderColor = UIColor.black.cgColor
        
        self.navigationItem.title = "PROFILE"
        print(detailsArr)
        self.name.text = detailsArr[0]
        self.email.text = detailsArr[2]
        self.phone.text = detailsArr[3]
        self.place.text = detailsArr[1]
        for i in 5..<detailsArr.count{
            self.skills.text? += "\(detailsArr[i]),"
            
        }
        skills.text?.removeLast()

        self.distance.text = detailsArr[4]
        viewModel = CreativesNearbyViewModel()
        callServiceForProjects()
    }
    @IBAction func sendInvitationButton(_ sender: Any) {
        if (projectTextField.text?.isEmpty)!{
            self.showAlert(title: "Alert!", message: "Please select a project.")
        }
        else{
        if let model = viewModel {
            
            let param = ["uid": APPStore.sharedStore.user.userId ?? 0,
                         "project_id": projectId] as [String : Any]
            self.showHUD(title: "Loading...")
            model.invite(parameters: param as [String : Any], onSuccess: { [weak self] (dataSource) in
                self?.hideHud()
                self?.goback()
                
                },  onError: { (error) in
                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
                  //  self.showAlert(title: Message.Alert, message: error.statusMessage)
                    self.hideHud()
            })
            
        }
    }
}
    func goback()
    {
        cancelClicked()
    }
    func callServiceForProjects()
    {
        if let model = viewModel {
            
            let param = ["user_id": APPStore.sharedStore.user.userId ?? 0]
            self.showHUD(title: "Loading...")
            model.projectsList(parameters: param as [String : Any], onSuccess: { [weak self] (dataSource) in
                self?.hideHud()
                if let filteredCustomReqList = dataSource?.list! {
                for i in 0..<filteredCustomReqList.count{
                    print(dataSource?.list![i].project_name as Any)
                    self?.projectNameArr.append((dataSource?.list![i].project_name)!)
                    self?.projectIdArr.append((dataSource?.list![i].project_id)!)

                }
                    self?.textFieldDataLoading()
                }
                },  onError: { (error) in
                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
                   // self.showAlert(title: Message.Alert, message: error.statusMessage)
                    self.hideHud()
            })
            
        }
    }
    func textFieldDataLoading()
    {
        projectTextField.optionArray = projectNameArr
        projectTextField.didSelect{(selectedText , index ,id) in
            self.projectId = String(self.projectIdArr[index])
            
        }
    }
}
extension CreativeProfileController {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue:
            APPNotifications.dismissAddView),
                                        object: nil, userInfo: nil)
    }
}
