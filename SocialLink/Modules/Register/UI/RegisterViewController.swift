//
//  RegisterViewController.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 27/02/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class RegisterViewController: AppBaseVC {

    @IBOutlet weak var registerTableview: UITableView!
    @IBOutlet weak var signinWithGoogle: UIButton!
    var viewModel: RegisterViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = RegisterViewModel()
//        self.createNavigationItems()
        self.addNotification()
//        let logo = UIImage(named: "logo3.png")
//        let imageView = UIImageView(image:logo)
//        self.navigationItem.titleView = imageView
self.navigationController?.isNavigationBarHidden = true
        self.setTableViewDatasource()
        // Do any additional setup after loading the view.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    @IBAction func loginWithGoogle(_ sender: Any) {
        
        GIDSignIn.sharedInstance().uiDelegate = self
        // Uncomment to automatically sign in the user.
        GIDSignIn.sharedInstance().signOut()
        
        GIDSignIn.sharedInstance().signInSilently()
        
        GIDSignIn.sharedInstance().signIn()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension RegisterViewController: GIDSignInUIDelegate {
    
    @objc func googleSignResponse(info: NSNotification) {
        
        let parms: [String: Any] = ["firstname": info.userInfo?["firstname"] ?? "",
                                    "email": info.userInfo?["email"] ?? "",
                                    "phone": info.userInfo?["phone"] ?? "",
                                    "password": "",
                                    "userfrom": AppLoggedInFrom.Google,
                                    "userID": info.userInfo?["userid"] ?? "",
                                    "token": info.userInfo?["token"] ?? ""]
        
        
        
        print(parms)

    }
}

extension RegisterViewController {
    
    func addNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(dismissView),
                                               name:
            NSNotification.Name(rawValue: APPNotifications.dismissRegistrationView),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(googleSignResponse),
                                               name: NSNotification.Name(rawValue:
                                                APPNotifications.googleSignInNotification),
                                               object: nil)
    }
    
    
    @objc func  dismissView(info: NSNotification) {
        //self.dismiss(animated: true, completion: nil)
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "CreateAccountViewController") as? CreateAccountViewController {
            /// let navController = APPNavigationController(rootViewController: controller)
            self.navigationController?.pushViewController(controller, animated: true)
        }

//        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "CreateAccountViewController") as? InviteCodeVC {
//            let navController = APPNavigationController(rootViewController: controller)
//            self.present(navController, animated: true, completion: nil)
//        }
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue:
//            APPNotifications.changeRootView),
//                                        object: nil, userInfo: nil)
    }
    
}

extension RegisterViewController {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension RegisterViewController: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.registerTableview.delegate = self
        self.registerTableview.dataSource = self
        self.registerTableview.estimatedRowHeight = 300
        self.registerTableview.rowHeight = UITableViewAutomaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:
            CellIdentifiers.RegisterTableCellIdentifier, for: indexPath)
        if let cellIs = cell as? RegisterTableCell {
            cellIs.selectionStyle = .none
            cellIs.delegate = self
        }
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UIScreen.main.bounds.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension RegisterViewController: RegisterTableCellDelegate {
    func submitClicked() {
        self.dismissRegisterView()
        return
           let indexPath = IndexPath(row: 0, section: 0)
            let cell = registerTableview.cellForRow(at: indexPath) as? RegisterTableCell
        if let data = cell?.getData() {
//            let name = (data["name"] as? String)?.trimmingCharacters(in: .whitespaces)
            let email = (data["email"] as? String)?.trimmingCharacters(in: .whitespaces)
            let password = (data["password"] as? String)?.trimmingCharacters(in: .whitespaces)
            
//            if (name?.isEmpty ?? false) || (name == "") {
//                self.showAlert(title: "Alert!", message: "Please enter name to continue")
//                return
//            } else
            if (email?.isEmpty ?? false) || (email == "") {
                self.showAlert(title: "Alert!", message: "Please enter email to continue")
                 return
            } else if !APPUtility.validateEmail(enteredEmail: email ?? "") {
                self.showAlert(title: "Alert!", message: "Please enter a valid email to continue")
                 return
            } else if (password?.isEmpty ?? false) || (password == "") {
                self.showAlert(title: "Alert!", message: "Please enter password to continue")
                 return
            }
//            else if (confirmPassword?.isEmpty ?? false) || (confirmPassword == "") {
//                self.showAlert(title: "Alert!", message: "Please confirm your password to continue")
//                 return
//            } else if password != confirmPassword {
//                self.showAlert(title: "Alert!", message: "Password & Confirm Password didn't match")
//                 return
//            }
            
            if let model = viewModel {
                self.showHUD(title: "Loading...")
                let timeString = APPUtility.convertDateToString(date: Date().toGlobalTime())
                
                //Edited :- vikas   devicetoken and device type added
                let deviceToken = UserDefaults.standard.string(forKey: "fcmToken")
                
                let dataIs = ["name": "",
                              "email": email ?? "",
                              "password": password ?? "",
                              "user_type": 1,
                              "created_time": timeString,
                              "os": 0,
                              "device_token":deviceToken ?? "",
                              "device_type":"iOS"] as [String : Any]
                
                model.registerUser(parameters: dataIs, onSuccess: { [weak self] (_) in
                    dprint(object: "Success")
                    self?.hideHud()
                    APPUtility.getUserData()
                    APPUtility.getToken()
                    self?.dismissRegisterView()
                    }, onError: { (error) in
                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
                        self.hideHud()
                })
            }
        }
 

//            let alertView = UIAlertController(title: "Register into SocialLink as", message: nil, preferredStyle: .actionSheet)
//            let employee = UIAlertAction(title: "Employer", style: .default, handler: { (alert) in
//                APPStore.sharedStore.loggedInAs = LoggedInUser.Employer
//                self.navigationController?.popToRootViewController(animated: true)
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue:
//                    APPNotifications.dismissRegistrationView),
//                                                object: nil, userInfo: nil)
//            })
//            alertView.addAction(employee)
//            let contractor = UIAlertAction(title: "Contractor", style: .default, handler: { (alert) in
//                APPStore.sharedStore.loggedInAs = LoggedInUser.Contractor
//                self.navigationController?.popToRootViewController(animated: true)
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue:
//                    APPNotifications.dismissRegistrationView),
//                                                object: nil, userInfo: nil)
//            })
//            alertView.addAction(contractor)
//            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert) in
//
//            })
//            alertView.addAction(cancel)
//        self.present(alertView, animated: true, completion: nil)
    }
    
    func dismissRegisterView() {
        APPStore.sharedStore.loggedInAs = LoggedInUser.Employer
        self.navigationController?.popToRootViewController(animated: true)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue:
            APPNotifications.dismissRegistrationView),
                                        object: nil, userInfo: nil)
    }
}
