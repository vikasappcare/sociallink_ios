//
//  ProjectManagementVC.swift
//  SocialLink
//
//  Created by Santhosh on 08/05/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ProjectManagementVC: UIViewController {

    @IBOutlet weak var projectManagementTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTableViewDatasource()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ProjectManagementVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.projectManagementTable.delegate = self
        self.projectManagementTable.dataSource = self
        self.projectManagementTable.estimatedRowHeight = 300
        self.projectManagementTable.rowHeight = UITableViewAutomaticDimension
        self.projectManagementTable.tableFooterView = UIView()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 3
        default:
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.MilestoneTableCellIdentifier, for: indexPath)
            if let cellIs = cell as? MilestoneTableCell {
                cellIs.selectionStyle = .none
            }
            cell.selectionStyle = .none
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.AddMIlestoneCellIdentifier, for: indexPath)
            if let cellIs = cell as? AddMIlestoneCell {
                cellIs.selectionStyle = .none
            }
            cell.selectionStyle = .none
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.001, execute: {

        if indexPath.section == 1 {
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "NewMileStoneVC") as? NewMileStoneVC {
                controller.milestoneType(type: MILESTONETYPE.NEW)
                let navControl = APPNavigationController(rootViewController: controller)
                self.present(navControl, animated: true, completion: nil)
            }
        } else if indexPath.section == 0 {
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProjectTaskkListVC") as? ProjectTaskkListVC {
                self.navigationController?.pushViewController(controller, animated: true)
            }
            
        }
             })
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        switch indexPath.section {
        case 0:
            return true
        default:
            return false
        }
        
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        
        
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { action, index in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.001, execute: {
                if let controller = self.storyboard?.instantiateViewController(withIdentifier: "NewMileStoneVC") as? NewMileStoneVC {
                    controller.milestoneType(type: MILESTONETYPE.EDIT)
                    let navControl = APPNavigationController(rootViewController: controller)
                    self.present(navControl, animated: true, completion: nil)
                }
            })
        }
        edit.backgroundColor = UIColor.lightGray
        
        let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
            print("more button tapped")
        }
        delete.backgroundColor = UIColor.darkGray
        
            return [delete, edit]
     
    }
    
  
    
    
}
