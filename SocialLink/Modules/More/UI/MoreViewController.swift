//
//  MoreViewController.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 11/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class MoreViewController: AppBaseVC {
    
    @IBOutlet weak var userTypeButton: UIButton!
    var moreArray = ["JobBoard",
                     "Switch to Contractor","Notifications"]
    var moreImageArray = ["JobBoard",
                          "Swipe_UnSelected","moreNotification"]
    
    @IBOutlet weak var logoutButton: UIButton!
    
    var switchImage: String?
    
    @IBOutlet weak var moreTableview: UITableView!
    var viewModel: MoreViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = MoreViewModel()
        switchImage = "Swipe_UnSelected"
        self.navigationItem.title = "MORE"
        // Do any additional setup after loading the view.
        self.setTableViewDatasource()
        self.showProjectsList()
        self.addNotification()
        
    }
    
    func updateRoleType() {
        let role = APPStore.sharedStore.appUserID
        switch role {
        case "1":
            moreArray[moreArray.count - 2] = "Switch to Employer"
        case "2":
            moreArray[moreArray.count - 2] = "Switch to Contractor"
        default:
            break
        }

    }

    @IBAction func logoutClicked(_ sender: Any) {
        
            let alert = UIAlertController(title: "Sure you want to Logout?", message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: {  (_) in
                APPUtility.userLoggedOut()
                
            }))
            alert.addAction(UIAlertAction(title: "No", style: .default, handler: { (_) in
                
            }))
            // 4. Present the alert.
            self.present(alert, animated: true, completion: nil)
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    @IBAction func inviteClicked(_ sender: Any) {
        
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "InviteTeamMember") as? InviteTeamMember {
            let navController = APPNavigationController(rootViewController: controller)
            controller.showBackButton(show: false, hideProject: false)
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.moreTableview.reloadData()
        self.navigationController?.navigationBar.isHidden = false
        if APPStore.sharedStore.user.user_type == 2
        {
            moreArray = ["JobBoard","Notifications"]
            moreImageArray = ["JobBoard","moreNotification"]
        } else if APPStore.sharedStore.user.user_type == 1
        {
            moreArray = ["Notifications"]
            moreImageArray = ["moreNotification"]
        }
        else
        {
            moreArray = ["JobBoard",
                         "Switch to Contractor","Notifications"]
            moreImageArray = ["JobBoard",
                              "Swipe_UnSelected","moreNotification"]
            self.updateRoleType()
        }
        
        if APPStore.sharedStore.user.userId == 2
        {
            moreArray.remove(at: 1)
        }
    }
    
    @IBAction func userTypeButtonClicked(_ sender: Any) {
        
        UIView.transition(with: sender as! UIView, duration: 0.1, options: .transitionCrossDissolve, animations: {
            if self.userTypeButton.isSelected {
                self.userTypeButton.isSelected = false
            } else {
                self.userTypeButton.isSelected = true
            }
        }, completion: nil)
        
        
    }
    
//    func switchRole() {
//        if let model = viewModel {
//            var changeRole = 0
//            let user = APPStore.sharedStore.user
//            if user.user_type == 1 {
//                changeRole = 2
//            } else if user.user_type == 2 {
//                changeRole = 1
//            }
//            let parms = ["role": changeRole,
//                         "token": APPStore.sharedStore.token] as [String : Any]
//            self.showHUD(title: "")
//            model.switchRole(parameters: parms , onSuccess: { [weak self] (dataSource) in
//                print("Success")
//                self?.hideHud()
//               // self?.updateRoles()
//                print(dataSource?.data?.user_type ?? "")
//                }, onError: { (error) in
//                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
//                    self.showAlert(title: "Alert", message: error.statusMessage)
//                    self.hideHud()
//            })
//        }
//    }
    
    
    func updateRoles() {
        if moreArray[1] == "Switch to Contractor" {
            moreImageArray[1] = "Swipe_Selected"
            moreArray[1] = "Switch to Employer"
            moreImageArray.insert("JobBoard", at: 0)
            moreArray.insert("JobBoard", at: 0)
            APPStore.sharedStore.appUserID = "1"
            moreTableview.reloadData()
        } else {
            moreImageArray[1] = "Swipe_UnSelected"
            moreArray[1] = "Switch to Contractor"
            moreArray.removeFirst()
            moreImageArray.removeFirst()
            APPStore.sharedStore.appUserID = "2"
            moreTableview.reloadData()

        }
        print(moreArray)
        NotificationCenter.default.post(name: Notification.Name(APPNotifications.switchTabbar), object: nil)
        NotificationCenter.default.post(name: Notification.Name(APPNotifications.userSwitched), object: nil)
    }
    
}

extension MoreViewController {
    
    func addNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(showAddScreen),
                                               name:
            NSNotification.Name(rawValue: APPNotifications.dismissMoreView),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(dismissView),
                                               name:
            NSNotification.Name(rawValue: APPNotifications.dismissMoreView),
                                               object: nil)
    }
    @objc func  dismissView(info: NSNotification) {
        self.dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue:
            APPNotifications.changeRootView),
                                        object: nil, userInfo: nil)
    }
    
    @objc func  showAddScreen(info: NSNotification) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = mainStoryboard.instantiateViewController(withIdentifier: "AddViewController")
        if let addController = controller as? AddViewController {
            addController.modalPresentationStyle = .overCurrentContext
            addController.view.alpha = 0.0
            self.tabBarController?.present(addController, animated: true) { () -> Void in
                UIView.animate(withDuration: 0.025) { () -> Void in
                    addController.view.alpha = 1.0
                }
            }
        }
        
    }
}

extension MoreViewController: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.moreTableview.delegate = self
        self.moreTableview.dataSource = self
        self.moreTableview.estimatedRowHeight = 300
        self.moreTableview.rowHeight = UITableViewAutomaticDimension
        self.moreTableview.tableFooterView = UIView()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(moreArray)
        switch section {
        case 1:
            switch APPStore.sharedStore.user.user_type {
            case 2,3,4:
                return moreArray.count
            default:
                return 0
            }
        default:
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        switch indexPath.section {
        case 0:
//            switch indexPath.row {
//            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier:
                    CellIdentifiers.MoreProfileCellIdentifier, for: indexPath)
                if let cellIs = cell as? MoreProfileCell {
                    cellIs.selectionStyle = .none
                    cellIs.setData(data: APPStore.sharedStore.user)
                }
                cell.selectionStyle = .none
                return cell

            default:
                let cell = tableView.dequeueReusableCell(withIdentifier:
                    CellIdentifiers.MoreTableViewCellIdentifier, for: indexPath)
                if let cellIs = cell as? MoreTableViewCell {
                    cellIs.selectionStyle = .none
                    let data = ["text": moreArray[indexPath.row],
                                "image": moreImageArray[indexPath.row]]
                    cellIs.setData(data: data)
                }
                cell.selectionStyle = .none
                return cell

            }
        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        
        switch indexPath.section {
        case 0:
            if indexPath.row == 0 {
                if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController {
                    controller.showSkip(bool: false)
                    self.navigationController?.pushViewController(controller, animated: true)
                }
            } else if indexPath.row == 1 {
                
                if let controller = self.storyboard?.instantiateViewController(withIdentifier: "TimeSheetsVC") as? TimeSheetsVC {
                    self.navigationController?.pushViewController(controller, animated: true)
                }
            }
        case 1:
            switch moreArray[indexPath.row] {
            case "Switch to Contractor", "Switch to Employer":
                    let index = IndexPath(row: 0, section: 1)
                    let index1 = IndexPath(row: 1, section: 1)
                    
                    moreTableview.reloadRows(at: [index], with: .automatic)
                    moreTableview.reloadRows(at: [index1], with: .automatic)
                    self.updateRoles()
            
            case "JobBoard":
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Projects", bundle: nil)
                
                if let controller = mainStoryboard.instantiateViewController(withIdentifier: "JobBoardController") as? JobBoardController {
                    let navController = APPNavigationController(rootViewController: controller)
                    self.present(navController, animated: true, completion: nil)
                }
            case "Notifications":
                if let controller = self.storyboard?.instantiateViewController(withIdentifier: "NotificationsVC") as? NotificationsVC {
                    self.navigationController?.pushViewController(controller, animated: true)
                }
                
            default:
                break
            }
            
        default:
            break
        }
    }
}

extension MoreViewController {
    
    func showProjectsList() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(showProjectList),
                                               name:
            NSNotification.Name(rawValue: APPNotifications.showProjectList),
                                               object: nil)
    }
    
    
    @objc func  showProjectList(info: NSNotification) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProjectListController") as? ProjectListController {
            let navController = APPNavigationController(rootViewController: controller)
            self.present(navController, animated: true, completion: nil)
        }
    }
}

