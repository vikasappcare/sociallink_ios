//
//  PlanCollectionCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 28/02/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit
protocol PlanCollectionCellDelegate: class {
    func choosePlanClicked()
}

class PlanCollectionCell: UICollectionViewCell {
    weak var delegate: PlanCollectionCellDelegate?
    @IBOutlet weak var planName: UILabel!
    @IBOutlet weak var planCostLabel: UILabel!
    @IBOutlet weak var planDataTableView: UITableView!
    @IBOutlet weak var backColorView: UIView!
    
    @IBOutlet weak var choosePlanButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        print("Called")
        setTableViewDatasource()
    }
    
    
    func setViewBackGroundColor(color: UIColor) {
        backColorView.backgroundColor = color
    }
    
    
    
    @IBAction func choosePlanClicked(_ sender: Any) {
        self.delegate?.choosePlanClicked()
    }
}


extension PlanCollectionCell: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.planDataTableView.delegate = self
        self.planDataTableView.dataSource = self
        self.planDataTableView.tableFooterView = UIView()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:
            CellIdentifiers.PlanDetailsIdentifier, for: indexPath)
        cell.selectionStyle = .none
        cell.textLabel?.text = "What you get"
        cell.textLabel?.font = UIFont.regularFont(16)
        cell.textLabel?.textAlignment = .center
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let additionalSeparatorThickness = CGFloat(2)
        let additionalSeparator = UIView(frame: CGRect(x: 0, y: cell.frame.size.height - additionalSeparatorThickness,
                                             width: cell.frame.size.width, height: additionalSeparatorThickness))
        additionalSeparator.backgroundColor = UIColor.white
        cell.addSubview(additionalSeparator)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
