
//
//  ProfileViewMOdel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 15/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation


class CreateAccountViewModel {
    init() {
    }
    var editContactProfile: ProfileModel?
    var dataIs: [String: Any]?
   

    
    func constructUpdateProfile(profile: ProfileModel) -> [String: Any]? {
        let updateData = ["token": APPStore.sharedStore.token,
                          "uphone": profile.phone_number ?? "",
                          "ufirstname": profile.full_name ?? "",
                          "ulastname": profile.last_name ?? "",
                          "uzipcode": profile.zipcode ?? "",
                          "uhirefor": profile.hereFor ?? "",
                          ] as [String : Any]
        return updateData as [String : Any]
    }
    
    
}
