//
//  ListOfBanksModel.swift
//  SocialLink
//
//  Created by admin on 3/6/19.
//  Copyright © 2019 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON

class ListOfBanksModel: AppBaseModel {
    var bId = [String]()
    var bName = [String]()

    override init() {
        super.init()
    }

    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)

        if response.exists() {
            self.parseResponse(with: response)
        }

    }
    private func parseResponse(with response: JSON) {

        for i in 0..<response.count{
            self.bId.append(response[i]["bid"].stringValue)
            self.bName .append(response[i]["bank_name"].stringValue)
        }

    }

}
