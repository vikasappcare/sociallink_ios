//
//  TaskAssigneeCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 09/05/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class TaskAssigneeCell: UICollectionViewCell {
    
    
    @IBOutlet weak var asigneeImage: UIImageView!
    
    func setData(data: ProjectContact) {
        asigneeImage.layer.cornerRadius = asigneeImage.frame.size.width / 2
        asigneeImage.layer.masksToBounds = true
       // asigneeImage.sd_setImage(with: URL(string: data.image ?? ""), placeholderImage: #imageLiteral(resourceName: "SampleProfile"))
    }
}
