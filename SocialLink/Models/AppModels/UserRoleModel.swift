//
//  UserRoleModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 04/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON


class UserRoleModel: AppBaseModel {
    
    var service: String?
    var data: SwitchModel?
    
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        self.service = response["service"].stringValue
        self.data = self.createObject(with: response["data"])
        
    }
    
    
    private func createObject(with jsonObject: JSON) -> SwitchModel? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return SwitchModel.init(jsonObject)
        }
    }
}

class SwitchModel: AppBaseModel {
    var user_type: Int64?
    var user_id: Int64?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        self.user_type = response["user_type"].int64Value
        self.user_id = response["user_id"].int64Value
    }
}
