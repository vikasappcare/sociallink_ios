//
//  MyTasksViewController.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 15/06/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class MyTasksViewController: AppBaseVC {

    @IBOutlet weak var taskTableView: UITableView!
    @IBOutlet weak var taskSearchBar: UISearchBar!
    @IBOutlet weak var taskSegment: AppSegmentControl!
    var viewModel: MyTasksViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = MyTasksViewModel()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            self.getActiveTasks(background: true)

        })
        self.taskSegment.selectedSegmentIndex = 0
        self.setTableViewDatasource()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateTasks()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func taskSegmentClicked(_ sender: Any) {
        if let model = viewModel {
            model.updateTasksOnSelection(index: self.taskSegment.selectedSegmentIndex)
            self.taskTableView.reloadData()
        }
    }
    
    @IBAction func taskType(_ sender: Any) {
        
        if taskSegment.selectedSegmentIndex == 0
        {
            getActiveTasks(background: true)
            taskTableView.reloadData()
        }
        else if taskSegment.selectedSegmentIndex == 1
        {
            getInProgressTasks(background: true)
            taskTableView.reloadData()
        }
        else
        {
            getPaymentsTasks(background: true)
            taskTableView.reloadData()
        }
        
    }
    func updateTasks() {
        if let model = viewModel {
            APPUtility.getContractorTasks(forUserType: APPStore.sharedStore.user.user_type ?? 0)
            model.updateTasksOnSelection(index: self.taskSegment.selectedSegmentIndex)
            
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func getActiveTasks(background: Bool) {
        if let model = viewModel {
            if !background {
                self.showHUD(title: "")
            }
            let dataIs = ["tassignto": APPStore.sharedStore.user.email ?? ""] as [String : Any]
            print(dataIs)
            self.addActivityIndicatior()
            DispatchQueue.global(qos: .background).async {
                    model.getContractorAssignedTasks(parameters: dataIs, onSuccess: { [weak self] (object) in
                        dprint(object: "Success")
                        DispatchQueue.main.async {
                            model.taskListArray = object?.list
                            if model.taskListArray == nil {
                                model.taskListArray = [Task]()
                            }
                            //self?.updateTasks()
                            self?.taskTableView.reloadData()
                            self?.hideHud()
                            self?.removeActivityIndicator()
                        }
                        }, onError: { (error) in
                            Logger.log(message: "Error \(error.statusMessage)", event: .error)
                           // self.showAlert(title: Message.Alert, message: error.statusMessage)
                            self.hideHud()
                            self.removeActivityIndicator()
            })
          }
       }
    }
    func getInProgressTasks(background: Bool) {
        if let model = viewModel {
            if !background {
                self.showHUD(title: "")
            }
            let dataIs = ["tassignto": APPStore.sharedStore.user.email ?? ""] as [String : Any]
            print(dataIs)
            self.addActivityIndicatior()
            DispatchQueue.global(qos: .background).async {
                model.getContractorInProgressTasks(parameters: dataIs, onSuccess: { [weak self] (object) in
                    dprint(object: "Success")
                    DispatchQueue.main.async {
                        model.taskListArray = nil
                        model.taskListArray = object?.list
                        if model.taskListArray == nil {
                            model.taskListArray = [Task]()
                        }
                        //self?.updateTasks()
                        self?.taskTableView.reloadData()
                        self?.hideHud()
                        self?.removeActivityIndicator()
                    }
                    }, onError: { (error) in
                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
                       // self.showAlert(title: Message.Alert, message: error.statusMessage)
                        self.hideHud()
                        self.removeActivityIndicator()
                })
            }
        }
        
    }
    func getPaymentsTasks(background: Bool) {
        if let model = viewModel {
            if !background {
                self.showHUD(title: "")
            }
            let dataIs = ["tassignto": APPStore.sharedStore.user.email ?? ""] as [String : Any]
            print(dataIs)
            self.addActivityIndicatior()
            DispatchQueue.global(qos: .background).async {
                model.getContractorPaymentTasks(parameters: dataIs, onSuccess: { [weak self] (object) in
                    dprint(object: "Success")
                    DispatchQueue.main.async {
                        model.taskListArray = object?.list
                        if model.taskListArray == nil {
                            model.taskListArray = [Task]()
                        }
                        //self?.updateTasks()
                        self?.taskTableView.reloadData()
                        self?.hideHud()
                        self?.removeActivityIndicator()
                    }
                    }, onError: { (error) in
                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
                       // self.showAlert(title: Message.Alert, message: error.statusMessage)
                        self.hideHud()
                        self.removeActivityIndicator()
                })
            }
        }
        
    }
}

extension MyTasksViewController: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.taskTableView.delegate = self
        self.taskTableView.dataSource = self
        self.taskTableView.estimatedRowHeight = 300
        self.taskTableView.rowHeight = UITableViewAutomaticDimension
        self.taskTableView.tableFooterView = UIView()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
        if let model = viewModel {
            return model.taskListArray?.count ?? 0
        }
        return 0
        default:
          return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
        let cell = tableView.dequeueReusableCell(withIdentifier:
            CellIdentifiers.MyTaskTableCellIdentifier, for: indexPath)
        if let cellIs = cell as? MyTaskTableCell {
            cellIs.selectionStyle = .none
             if let model = viewModel {
                cellIs.setData(data: model.taskListArray![indexPath.row])
            }
        }
        cell.selectionStyle = .none
        return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.ProjectAddTaskCellIdentifier, for: indexPath)
            if let cellIs = cell as? ProjectAddTaskCell {
                cellIs.selectionStyle = .none
            }
            cell.selectionStyle = .none
            return cell
        default:
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
                if let controller = self.storyboard?.instantiateViewController(withIdentifier: "TaskDetailViewController") as? TaskDetailViewController {
                    if let model = viewModel?.taskListArray {
                        controller.setTask(task: model[indexPath.row], row: indexPath.row, type: 0, projectId: model[indexPath.row].project_id!)
                        self.navigationController?.pushViewController(controller, animated: true)
                    }
              
                }
        case 1:
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Projects", bundle: nil)

            if let controller = mainStoryboard.instantiateViewController(withIdentifier: "NewTaskController") as? NewTaskController {
                let navControl = APPNavigationController(rootViewController: controller)
                self.present(navControl, animated: true, completion: nil)
            }
        default:
            break
        }
    }
}
