//
//  SubTaskListVC.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/05/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class SubTaskListVC: UIViewController {
    
    @IBOutlet weak var subTaskSegmentControl: AppSegmentControl!
    @IBOutlet weak var subTaskListTable: UITableView!
    var filterStatus: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        filterStatus = SubTaskStatus.yetToStart
        self.setTableViewDatasource()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func segmentChanged(_ sender: Any) {
        switch subTaskSegmentControl.selectedSegmentIndex {
        case 0:
            filterStatus = SubTaskStatus.yetToStart
        case 1:
            filterStatus = SubTaskStatus.inProgress
        case 2:
            filterStatus = SubTaskStatus.completed
        case 3:
            filterStatus = SubTaskStatus.confirmed
        default:
            filterStatus = SubTaskStatus.yetToStart
        }
    }
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension SubTaskListVC {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension SubTaskListVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.subTaskListTable.delegate = self
        self.subTaskListTable.dataSource = self
        self.subTaskListTable.estimatedRowHeight = 300
        self.subTaskListTable.rowHeight = UITableViewAutomaticDimension
        self.subTaskListTable.tableFooterView = UIView()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 3
        case 1:
            return 1
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.SubTaskListCellIdentifier, for: indexPath)
            if let cellIs = cell as? SubTaskListCell {
                cellIs.selectionStyle = .none
            }
            cell.selectionStyle = .none
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.SubTaskAddSubTaskCellIdentifier, for: indexPath)
            if let cellIs = cell as? SubTaskAddSubTaskCell {
                cellIs.selectionStyle = .none
            }
            cell.selectionStyle = .none
            return cell
        default:
            return UITableViewCell()
            
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.001, execute: {
            
            if indexPath.section == 1 {
                if let controller = self.storyboard?.instantiateViewController(withIdentifier: "NewSubTaskVC") as? NewSubTaskVC {
                    controller.setSubTaskType(type: SUBTASKTYPE.NEW)
                    let navControl = APPNavigationController(rootViewController: controller)
                    self.present(navControl, animated: true, completion: nil)
                }
                
            } else {
                
                if let controller = self.storyboard?.instantiateViewController(withIdentifier: "NewSubTaskVC") as? NewSubTaskVC {
                    controller.setSubTaskType(type: SUBTASKTYPE.EDIT)
                    controller.setSubTaskStatus(status: self.filterStatus ?? SubTaskStatus.yetToStart)
                    let navControl = APPNavigationController(rootViewController: controller)
                    self.present(navControl, animated: true, completion: nil)
                }
                
                
            }
        })
    }
    
    //    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //        switch section {
    //        case 1:
    //            return 30
    //        default:
    //            return 0
    //        }
    //    }
    //
    //
    //    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //        let headerView = UIView()
    //        headerView.backgroundColor = UIColor.appLightGrayColor
    //        let label = UILabel(frame: CGRect(x: 15, y: 0, width: 300, height: 30))
    //        label.font = UIFont.semiBoldFont(16)
    //        headerView.addSubview(label)
    //        label.text = "Task - Inprogress"
    //        return headerView
    //    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        switch indexPath.section {
        case 0:
            return true
        default:
            return false
        }
        
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        
        
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { action, index in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.001, execute: {
                
                
                if let controller = self.storyboard?.instantiateViewController(withIdentifier: "NewSubTaskVC") as? NewSubTaskVC {
                    controller.setSubTaskType(type: SUBTASKTYPE.EDIT)
                    let navControl = APPNavigationController(rootViewController: controller)
                    self.present(navControl, animated: true, completion: nil)
                }
            })
        }
        edit.backgroundColor = UIColor.lightGray
        
        let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
            print("more button tapped")
        }
        delete.backgroundColor = UIColor.darkGray
        
        return [delete, edit]
        
    }
    
    
    
    
}
