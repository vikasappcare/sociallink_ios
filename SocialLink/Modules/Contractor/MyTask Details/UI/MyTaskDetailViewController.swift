//
//  MyTaskDetailViewController.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 15/06/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class MyTaskDetailViewController: UIViewController {
    
    @IBOutlet weak var myTaskDetailTable: UITableView!
    let projectHeaderArray = ["Project Name",
                              "Mile Stone",
                              "Task",
                              "SubTask"]
    let projectDetailsArray = ["Social Link Project",
                               "iOS Development",
                               "Design Mockups",
                               "Design Screen for Login, Register, Projects, Tasks, Creative nearby"]
    var selectedProject: Project?
     var selectedEstimatedProject: EstimatedProject?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTableViewDatasource()
        
        
        self.navigationItem.title = "Sub Task 1"
        // Do any additional setup after loading the view.
    }
    
    func setProject(project: Project) {
        self.selectedProject = project
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension MyTaskDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.myTaskDetailTable.delegate = self
        self.myTaskDetailTable.dataSource = self
        self.myTaskDetailTable.estimatedRowHeight = 300
        self.myTaskDetailTable.rowHeight = UITableViewAutomaticDimension
        self.myTaskDetailTable.tableFooterView = UIView()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return projectHeaderArray.count
        case 1:
            return 1
        case 2:
            return 1
        case 3:
            return 5
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.MyTaskDetailNameCellIdentifier, for: indexPath)
            if let cellIs = cell as? MyTaskDetailNameCell {
                cellIs.selectionStyle = .none

                let dataIs = ["header": projectHeaderArray[indexPath.row],
                              "description": projectDetailsArray[indexPath.row]]
                cellIs.setData(data: dataIs)
                
            }
            cell.selectionStyle = .none
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.TaskStartStopCellIdentifier, for: indexPath)
            if let cellIs = cell as? TaskStartStopCell {
                cellIs.selectionStyle = .none
            }
            cell.selectionStyle = .none
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.MyTaskUploadCellIdentifier, for: indexPath)
            if let cellIs = cell as? MyTaskUploadCell {
                cellIs.selectionStyle = .none
            }
            cell.selectionStyle = .none
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.MyTaskLogCellIdentifier, for: indexPath)
            if let cellIs = cell as? MyTaskLogCell {
                cellIs.selectionStyle = .none
                
//                let dataIs = ["header": projectHeaderArray[indexPath.row],
//                              "description": projectDetailsArray[indexPath.row]]
//                cellIs.setData(data: dataIs)
                
            }
            cell.selectionStyle = .none
            return cell
        default:
            return UITableViewCell()
        }
      
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        let headerLabel = UILabel(frame: CGRect(x: 15, y: 0, width: 200, height: 40))
        headerLabel.font = UIFont.semiBoldFont(15)
        headerLabel.text = "LOGS"
        headerView.addSubview(headerLabel)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 2 {
            return 40
        }
        return 0
    }
 
}
