//
//  RoleModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 29/08/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON

class RoleModel: AppBaseModel {
    
    var service: String?
    var list: [Role]?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    
    private func parseResponse(with response: JSON) {
        self.service = response["service"].stringValue
        for item in response["List"].arrayValue {
            if let feedModel = self.createObject(with: item) {
                if self.list == nil {
                    self.list = [Role]()
                }
                self.list?.append(feedModel)
            }
        }
    }
    
    private func createObject(with jsonObject: JSON) -> Role? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return Role.init(jsonObject)
        }
    }
    
}


class Role: AppBaseModel {
    var rid: Int64?
    var name: String?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        self.rid = response["rid"].int64Value
        self.name = response["name"].stringValue
    }


}


