//
//  EventTableViewCell.swift
//  SocialLink
//
//  Created by Santhosh on 22/04/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class EventTableViewCell: UITableViewCell {

    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var eventIcon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(data: [String: Any]) {
        
        eventTitle.text = data["title"] as? String ?? ""
        eventIcon.image = UIImage(named: data["icon"] as? String ?? "")
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
