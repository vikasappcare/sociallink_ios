//
//  CreativesNearByModel.swift
//  SocialLink
//
//  Created by Banda Anil Reddy on 13/03/19.
//  Copyright © 2019 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON

class CreativesNearByModel: AppBaseModel {
    
    var map_img = [String]()
    var address = [String]()
    var lat = [String]()
    var lng = [String]()
    var uid = [String]()
    var ufullname = [String]()
    var uskills = [String]()
    var distance = [String]()
    var uphone = [String]()
    var uemailid = [String]()
    var required_skills = [String]()

    
    override init() {
        super.init()
    }
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    private func parseResponse(with response: JSON) {
        
        for i in 0..<response.count{
            
            self.map_img.append(response[i]["map_img"].stringValue)
            self.address .append(response[i]["address"].stringValue)
            self.lat.append(response[i]["lat"].stringValue)
            self.lng .append(response[i]["lng"].stringValue)
            self.uid.append(response[i]["uid"].stringValue)
            self.ufullname .append(response[i]["ufullname"].stringValue)
            self.uskills.append(response[i]["uskills"].stringValue)
            self.distance .append(response[i]["DISTANCE"].stringValue)
            self.uphone.append(response[i]["uphone"].stringValue)
            self.uemailid.append(response[i]["uemailid"].stringValue)
            self.required_skills.append(response[i]["required_skills"].stringValue)

        }
        
    }

}
