//
//  TaskModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 01/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON

class NewTaskModel: AppBaseModel {
    
    var service: String?
    var list: [NewTask]?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    
    private func parseResponse(with response: JSON) {
        self.service = response["service"].stringValue
        for item in response["List"].arrayValue {
            if let model = self.createObject(with: item) {
                if self.list == nil {
                    self.list = [NewTask]()
                }
                self.list?.append(model)
            }
        }
    }
    
    private func createObject(with jsonObject: JSON) -> NewTask? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return NewTask.init(jsonObject)
        }
    }
    
}


class NewTask: AppBaseModel {
    var member_name: String?
    var designation: String?
    var email_id: String?
    var user_id: Int64?
    var image: String?
    var member_type: Int64?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        self.member_name = response["member_name"].stringValue
        self.designation = response["designation"].stringValue
        self.email_id = response["email_id"].stringValue
        self.user_id = response["user_id"].int64Value
        self.image = response["image"].stringValue
        self.member_type = response["m_type"].int64Value
    }
    
    
}
