//
//  FeedCommentViewController.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 11/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class FeedCommentViewController: AppBaseVC {
    
    @IBOutlet weak var commentTableHeight: NSLayoutConstraint!
    var selectedFeedId: Int64?
    var viewModel: CommentFeedViewModel?
    var selectedFeed: Feed?
    @IBOutlet weak var commentsTableView: UITableView!
    var sendButton = UIButton()
    var showCommentBar: Bool?
    var commentData = ["dhagfjasdgfjsagfjkasgfjkagsdjkfsdfs",
                       "dhagfjasdgfjsagfjkasgfjkagsdjkfsdfsdhagfjasdgfjsagfjkasgfjkagsdjkfsdfsdhagfjasdgfjsagfjkasgfjkagsdjkfsdfsdhagfjasdgfjsagfjkasgfjkagsdjkfsdfsdhagfjasdgfjsagfjkasgfjkagsdjkfsdfsdhagfjasdgfjsagfjkasgfjkagsdjkfsdfsdhagfjasdgfjsagfjkasgfjkagsdjkfsdfsdhagfjasdgfjsagfjkasgfjkagsdjkfsdfsdhagfjasdgfjsagfjkasgfjkagsdjkfsdfsdhagfjasdgfjsagfjkasgfjkagsdjkfsdfsdhagfjasdgfjsagfjkasgfjkagsdjkfsdfsdhagfjasdgfjsagfjkasgfjkagsdjkfsdfsdhagfjasdgfjsagfjkasgfjkagsdjkfsdfsdhagfjasdgfjsagfjkasgfjkagsdjkfsdfsdhagfjasdgfjsagfjkasgfjkagsdjkfsdfsdhagfjasdgfjsagfjkasgfjkagsdjkfsdfsdhagfjasdgfjsagfjkasgfjkagsdjkfsdfsdhagfjasdgfjsagfjkasgfjkagsdjkfsdfsdhagfjasdgfjsagfjkasgfjkagsdjkfsdfsdhagfjasdgfjsagfjkasgfjkagsdjkfsdfsdhagfjasdgfjsagfjkasgfjkagsdjkfsdfs",
                       "1",
                       "2"]
    
    
//////////////////COMMENT BAR ////////////
    
    private var inputToolbar: UIView!
    private var textView: UITextView!
    private var textViewBottomConstraint: NSLayoutConstraint!
    ////////////////////////////////
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        viewModel = CommentFeedViewModel()
        self.setTableViewDatasource()
        self.createNavigationItems()
        //showCommentBar = true
        if let showComment = self.showCommentBar {
            if showComment {
                self.commentTableHeight.constant = 51
                self.configureCommentBar()
            } else {
                self.commentTableHeight.constant = 0
            }
        }
        self.title = "Comments"
        self.getComments()
        
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    

    func showCommentBar(show: Bool) {
        self.showCommentBar = show
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setFeedId(feedId: Int64) {
        self.selectedFeedId = feedId
    }
    
    func getComments() {
        if let model = viewModel {
            if let idIS = self.selectedFeedId {
                let parms = ["fid": idIS,] as [String : Any]
                self.showHUD(title: "")
                model.getComments(parameters: parms, onSuccess: { [weak self] (dataSource) in
                    print("Success")
                    print(dataSource ?? "")
                    self?.hideHud()
                    self?.commentsTableView.reloadData()
                    }, onError: { (error) in
                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
                      //  self.showAlert(title: Message.Alert, message: error.statusMessage)
                        self.hideHud()
                })
            }
            
        }
    }
    
    func postComment(comment: String) {
        if let model = viewModel {
            if let idIS = self.selectedFeedId {
                let parms = ["feed_id": idIS,
                             "user_id": APPStore.sharedStore.user.userId ?? "",
                             "comment": comment] as [String : Any]
                print(parms)
                self.showHUD(title: "")
                model.postComment(parameters: parms, onSuccess: { [weak self] (dataSource) in
                    print("Success")
                    print(dataSource ?? "")
                    self?.hideHud()
                    self?.commentsTableView.reloadData()
                    self?.textView.text = ""
                    self?.getComments()
                    
                    self?.textView.resignFirstResponder()
                    }, onError: { (error) in
                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
                        //self.showAlert(title: Message.Alert, message: error.statusMessage)0
                        self.hideHud()
                })
            }
            
        }
        
        
    }
    
}
extension FeedCommentViewController {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true) {
//            let count: String = "\(self.viewModel!.commentsArray?.count ?? 0)"
//            commentsCount = Int64(count)
//            ProjectTextTableCell().updateComments()
//            ProjectImageTableCell().updateComments()
            ProjectFeedVC().getFeed()
        }
    }
    
}

extension FeedCommentViewController: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.commentsTableView.delegate = self
        self.commentsTableView.dataSource = self
        self.commentsTableView.estimatedRowHeight = 300
        self.commentsTableView.rowHeight = UITableViewAutomaticDimension
        self.commentsTableView.tableFooterView = UIView()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let model = viewModel {
            return model.commentsArray?.count ?? 0
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:
            CellIdentifiers.CommentsTableCellIdentifier, for: indexPath)
        if let cellIs = cell as? CommentsTableCell {
            cellIs.selectionStyle = .none
            if let model = viewModel {
                cellIs.setData(data: model.commentsArray![indexPath.row])
            }
            
        }
        cell.selectionStyle = .none
        return cell
        
    }
    
}

extension FeedCommentViewController {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}

extension FeedCommentViewController {
    
    func configureCommentBar() {
        // *** Create Toolbar
        inputToolbar = UIView()
        inputToolbar.backgroundColor = UIColor(white: 0.95, alpha: 1.0)
        inputToolbar.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(inputToolbar)
        
        // *** Create GrowingTextView ***
        textView = UITextView()
        ///textView.delegate = self as! UITextViewDelegate
        textView.layer.cornerRadius = 4.0
        textView.layer.borderColor = UIColor.gray.cgColor
        textView.layer.borderWidth = 1
//        textView.maxLength = 200
//        textView.maxHeight = 70
//        textView.trimWhiteSpaceWhenEndEditing = true
        //textView.placeholder = "Say something..."
        //textView.placeholderColor = UIColor(white: 0.8, alpha: 1.0)
        textView.font = UIFont.systemFont(ofSize: 15)
        textView.translatesAutoresizingMaskIntoConstraints = false
        inputToolbar.addSubview(textView)
        
        
        
        sendButton.frame = CGRect(x: self.view.frame.size.width - 60, y: 0, width: 60, height: 50)
        sendButton.backgroundColor = UIColor.clear
        sendButton.setTitle("   Send   ", for: .normal)
        sendButton.titleLabel?.font = UIFont.boldFont(16)
        sendButton.setTitleColor(UIColor.appBlueColor, for: .normal)
        sendButton.addTarget(self, action: #selector(sendClicked), for: .touchUpInside)
        inputToolbar.addSubview(sendButton)
        
        
        inputToolbar.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -4).isActive = true
        inputToolbar.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 4).isActive = true
        inputToolbar.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -4).isActive = true
        inputToolbar.heightAnchor.constraint(equalToConstant: 46).isActive = true
        
        textView.bottomAnchor.constraint(equalTo: inputToolbar.bottomAnchor, constant: -4).isActive = true
        textView.leftAnchor.constraint(equalTo: inputToolbar.leftAnchor, constant: 4).isActive = true
        textView.rightAnchor.constraint(equalTo: inputToolbar.rightAnchor, constant: -55).isActive = true
        textView.heightAnchor.constraint(equalToConstant: 36).isActive = true
        
        sendButton.bottomAnchor.constraint(equalTo: inputToolbar.bottomAnchor, constant: -4).isActive = true
        sendButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        sendButton.rightAnchor.constraint(equalTo: inputToolbar.rightAnchor, constant: -8).isActive = true
        sendButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        
//        // *** Autolayout ***
//        NSLayoutConstraint.activate([
//            inputToolbar.leftAnchor.constraint(equalTo: view.leftAnchor),
//            inputToolbar.rightAnchor.constraint(equalTo: view.rightAnchor),
//            inputToolbar.bottomAnchor.constraint(equalTo: view.bottomAnchor),
//            textView.topAnchor.constraint(equalTo: inputToolbar.topAnchor, constant: 8),
//
//            ])
//
//        if #available(iOS 11, *) {
//            textViewBottomConstraint = textView.bottomAnchor.constraint(equalTo: inputToolbar.safeAreaLayoutGuide.bottomAnchor, constant: -8)
//            NSLayoutConstraint.activate([
//                textView.leftAnchor.constraint(equalTo: inputToolbar.safeAreaLayoutGuide.leftAnchor, constant: 8),
//                textView.rightAnchor.constraint(equalTo: inputToolbar.safeAreaLayoutGuide.rightAnchor, constant: -60),
//                textViewBottomConstraint
//                ])
//        } else {
//            textViewBottomConstraint = textView.bottomAnchor.constraint(equalTo: inputToolbar.bottomAnchor, constant: -8)
//            NSLayoutConstraint.activate([
//                textView.leftAnchor.constraint(equalTo: inputToolbar.leftAnchor, constant: 8),
//                textView.rightAnchor.constraint(equalTo: inputToolbar.rightAnchor, constant: -60),
//                textViewBottomConstraint, textView.topAnchor.constraint(equalTo: inputToolbar.topAnchor, constant: 8)
//                ])
//        }
        
        // *** Listen to keyboard show / hide ***
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        
        // *** Hide keyboard when tapping outside ***
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureHandler))
        view.addGestureRecognizer(tapGesture)
        
        
    }
   
    
    @objc private func keyboardWillChangeFrame(_ notification: Notification) {
        if let endFrame = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            var keyboardHeight = UIScreen.main.bounds.height - endFrame.origin.y
            if #available(iOS 11, *) {
                if keyboardHeight > 0 {
                    keyboardHeight = keyboardHeight - view.safeAreaInsets.bottom
                }
            }
            
            
            view.layoutIfNeeded()
        }
    }
    
    @objc private func tapGestureHandler() {
        view.endEditing(true)
    }
    
    
    @objc func sendClicked() {
        
        let textIs = self.textView.text.trimmingCharacters(in: .whitespaces)
        
        if !textIs.checkForEmpty() {
            self.postComment(comment: textIs)
        } else {
            self.showAlert(title: "Alert", message: "Enter comment")
        }
        
        
    }
  
}

extension FeedCommentViewController {
    
    // *** Call layoutIfNeeded on superview for animation when changing height ***
    
//    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
//        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: [.curveLinear], animations: { () -> Void in
//            self.view.layoutIfNeeded()
////            self.sendButton.frame = CGRect(x: self.sendButton.frame.origin.x, y: ((self.inputToolbar.frame.size.height) / 2) - self.sendButton.frame.size.height, width: self.sendButton.frame.size.width, height: self.sendButton.frame.size.height)
//        }, completion: nil)
//    }
}




//extension FeedCommentViewController {
//
//    func configureCommentBar() {
//
//        configureScrollView()
//        configureInputBar()
//
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardFrameChanged(notification:)), name: NSNotification.Name(rawValue: ALKeyboardFrameDidChangeNotification), object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
//    }
//
//    func configureScrollView() {
//        view.addSubview(scrollView)
//        let contentView = UIView(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: view.bounds.size.height * 2))
//        contentView.backgroundColor = UIColor(white: 0.8, alpha: 1)
//
//        scrollView.addSubview(contentView)
//        scrollView.contentSize = contentView.bounds.size
//        scrollView.keyboardDismissMode = .interactive
//        scrollView.backgroundColor = UIColor(white: 0.6, alpha: 1)
//    }
//
//    func configureInputBar() {
//        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
//        rightButton.setTitle("Send", for: .normal)
//        keyboardObserver.isUserInteractionEnabled = false
//        textInputBar.showTextViewBorder = true
//        textInputBar.rightView = rightButton
//        textInputBar.frame = CGRect(x: 0, y: view.frame.size.height - textInputBar.defaultHeight, width: view.frame.size.width, height: textInputBar.defaultHeight)
//        textInputBar.backgroundColor = UIColor(white: 0.95, alpha: 1)
//        textInputBar.keyboardObserver = keyboardObserver
//        view.addSubview(textInputBar)
//    }
//
//    @objc func keyboardFrameChanged(notification: NSNotification) {
//        if let userInfo = notification.userInfo {
//            print(userInfo)
//            let frame = userInfo[UIKeyboardFrameEndUserInfoKey] as! CGRect
//            print(frame)
//            textInputBar.frame.origin.y = frame.origin.y
//        }
//    }
//
//    @objc func keyboardWillShow(notification: NSNotification) {
//        if let userInfo = notification.userInfo {
//            let frame = userInfo[UIKeyboardFrameEndUserInfoKey] as! CGRect
//               print(frame)
//            textInputBar.frame.origin.y = frame.origin.y
//        }
//    }
//
//    @objc func keyboardWillHide(notification: NSNotification) {
//        if let userInfo = notification.userInfo {
//            let frame = userInfo[UIKeyboardFrameEndUserInfoKey] as! CGRect
//              print(frame.origin.y)
//            textInputBar.frame.origin.y = frame.origin.y
//        }
//    }
//}
