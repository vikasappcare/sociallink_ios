//
//  TaskDetailAsigneeCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 16/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit
protocol TaskDetailAsigneeCellDelegate: class {
    func profileSelected(id: Int64)
}

class TaskDetailAsigneeCell: UITableViewCell {
    weak var delegate: TaskDetailAsigneeCellDelegate?
    @IBOutlet weak var assignedImage: UIImageView!
    @IBOutlet weak var header: UILabel!
    @IBOutlet weak var asigneeButton: UIButton!
    var assignedId: Int64?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(data: Task) {
        self.assignedId = data.assigned_to?.id
        header.text = "Asignee"
        let userProfileImage = APPURL.feedUserProfileImage  + data.uphoto!
        assignedImage.sd_setImage(with: URL(string: userProfileImage), placeholderImage: #imageLiteral(resourceName: "SampleProfile"))

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func asigneeClicked(_ sender: Any) {
        if let idIs = self.assignedId {
            self.delegate?.profileSelected(id: idIs)
        }
    }
}
