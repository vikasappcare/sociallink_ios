//
//  InviteCodeModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 03/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation

class InviteCodeModel {
    
    init() {
    }
    
    func joinTeam(parameters: [String: Any],
                       onSuccess: @escaping (JoinMemberModel?) -> Void,
                       onError: @escaping APIErrorHandler) {
        AppDataManager.joinTeam(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: dataSource)
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
}
