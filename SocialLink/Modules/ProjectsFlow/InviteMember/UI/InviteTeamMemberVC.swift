//
//  InviteTeamMemberVC.swift
//  InternalMembers
//
//  Created by Santhosh Marripelli on 23/04/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit



class InviteTeamMemberVC: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var teamMemberEmail: UITextField!
    @IBOutlet weak var teamMemberName: UITextField!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var contactsTable: UITableView!
    let nameData = ["Bob Ross", "Patrick Kay", "Sara Tinkerton"]
    let profileData = ["Graphic Designer", "SEO", "ContentSpecialist"]
    let selectedData = [true, false, false]
    let colorData = [UIColor.appGreenColor, UIColor.appOrangeColor, UIColor.appBlueColor]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTableview()
        // Do any additional setup after loading the view.
        backView.layer.borderWidth = 1.0
        backView.layer.borderColor = UIColor.darkGray.cgColor
        backView.layer.masksToBounds = true
        self.createNavigationItems()
        self.navigationItem.title = "INVITE TEAM MEMBER"
        
        
        teamMemberEmail.setReguralFont(size: 15)
        teamMemberName.setReguralFont(size: 15)
        searchBar.clearBackgroundColor()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension InviteTeamMemberVC {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension InviteTeamMemberVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableview() {
        self.contactsTable.tableFooterView = UIView()
        self.contactsTable.delegate = self
        self.contactsTable.dataSource = self
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.ProjectSearchContactCellIdentifier, for: indexPath) as UITableViewCell
        if let cellIs = cell as? ProjectSearchContactCell {
            cellIs.selectionStyle = .none
            
            //            profileImage.image = UIImage(named: data["image"] as? String ?? "")
            //            nameLabel.text = data["name"] as? String ?? ""
            //            profileLabel.text = data["profile"] as? String ?? ""
            //
            //            let selected =  data["selected"] as? Bool ?? false
            //
            //            if selected {
            //                selectImage.image = UIImage(named: "checkedBlue")
            //            } else {
            //                selectImage.image = UIImage(named: "uncheckedBlue")
            //
            //   }
            
            let data = ["image": "",
                        "name": nameData[indexPath.row],
                        "profile": profileData[indexPath.row],
                        "selected": selectedData[indexPath.row],
                        "color": colorData[indexPath.row]] as [String : Any]
            cellIs.setData(data: data)
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    
    
    
}


