//
//  ProjectContactsViewModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 31/08/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
class ProjectContactsViewModel {
    
    init() {
    }
    
    func getProjectContacts(parameters: [String: Any],
                 onSuccess: @escaping (ProjectContactsModel?) -> Void,
                 onError: @escaping APIErrorHandler) {
        AppDataManager.getProjectContacts(parametes: parameters, onSuccess: { (dataSource) in
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
}
