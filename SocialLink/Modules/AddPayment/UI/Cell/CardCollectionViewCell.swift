//
//  CardCollectionViewCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 04/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class CardCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cardImageView: UIImageView!
    
    
    func setData(data: String?) {
        
        cardImageView.image = UIImage(named: data ?? "")
    }
    
}
