//
//  NewSubTaskAsigneeCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/05/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class NewSubTaskAsigneeCell: UICollectionViewCell {
    
    @IBOutlet weak var profileImage: UIImageView!
    
    func setData(data: [String: Any]?) {
        profileImage.layer.cornerRadius = profileImage.frame.size.width / 2
        profileImage.layer.masksToBounds = true
    }
}
