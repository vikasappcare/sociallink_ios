//
//  ProjectListController.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 29/05/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit
protocol ProjectListControllerDelegate: class {
    func projectSelectted(project: Project)
}

class ProjectListController: UIViewController {
    weak var delegate: ProjectListControllerDelegate?
    @IBOutlet weak var projectListTable: UITableView!
    var projectsArray: [Project]?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTableViewDatasource()
        self.createNavigationItems()
        // Do any additional setup after loading the view.
        
        
        if projectsArray == nil {
            projectsArray = [Project]()
        }
        
        APPUtility.getProjects()
        projectsArray = APPStore.sharedStore.projects
      
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
extension ProjectListController {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
}


extension ProjectListController: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.projectListTable.delegate = self
        self.projectListTable.dataSource = self
        self.projectListTable.estimatedRowHeight = 300
        self.projectListTable.rowHeight = UITableViewAutomaticDimension
        self.projectListTable.tableFooterView = UIView()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.projectsArray?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:
            CellIdentifiers.ProjectListTableCellIdentifier, for: indexPath)
        if let cellIs = cell as? ProjectListTableCell {
            cellIs.selectionStyle = .none
            let project = self.projectsArray![indexPath.row]
            cellIs.setData(data: project)
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.001, execute: {
            let selectedProject = self.projectsArray![indexPath.row]
            self.delegate?.projectSelectted(project: selectedProject)
            self.dismiss(animated: true, completion: nil)
        })
    }

}
extension QuotationController
{
    
}
