//
//  RolesViewModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 29/08/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation

class RolesViewModel {
    
    init() {
    }
    
    func getRolesList(parameters: [String: Any],
                        onSuccess: @escaping (RoleModel?) -> Void,
                        onError: @escaping APIErrorHandler) {
        AppDataManager.getRolesList(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: "Success")
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
    
    func getServiceList(parameters: [String: Any],
                      onSuccess: @escaping (ServiceListModel?) -> Void,
                      onError: @escaping APIErrorHandler) {
        AppDataManager.getServiceList(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: "Success")
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
    
    
}
