//
//  RoleTableViewCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 29/08/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class RoleTableViewCell: UITableViewCell {

    @IBOutlet weak var roleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(data: Role) {
        
        if !(data.name?.checkForEmpty() ?? false) {
            roleLabel.text = data.name ?? ""
        } else {
            roleLabel.text = "No Role Name"
        }
        
    }
    
    
    func setServiceData(data: Service) {
        
        if !(data.service_name?.checkForEmpty() ?? false) {
            roleLabel.text = data.service_name ?? ""
        } else {
            roleLabel.text = "No Role Name"
        }
        
    }

}
