//
//  RegisterTableCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 27/02/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit
protocol RegisterTableCellDelegate: class {
    func submitClicked()
}

class RegisterTableCell: UITableViewCell {

    weak var delegate: RegisterTableCellDelegate?
//    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var emailText: AppTextField!
    @IBOutlet weak var paswordText: AppTextField!
    @IBOutlet weak var backgroundImageView: UIView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var checkBoxButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()

        // Initialization code
        // make round corners for txtfield
        
        paswordText.clipsToBounds = true
        paswordText.layer.cornerRadius = 7
        paswordText.layer.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        paswordText.setBorderColor(width: 1, color: .black)
        emailText.clipsToBounds = true
        emailText.layer.cornerRadius = 7
        emailText.layer.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        //emailText.setBorderColor(width: 1, color: .black)
        backgroundImageView.layer.shadowColor = UIColor.black.cgColor
        backgroundImageView.layer.shadowOpacity = 1
        backgroundImageView.layer.shadowOffset = CGSize.zero
        backgroundImageView.layer.shadowRadius = 5
        //backgroundImageView.layer.shadowPath = UIBezierPath(rect: backgroundImageView.bounds).cgPath
//        checkBoxButton.layer.borderColor = UIColor.init(red: 27/255.0, green: 173/255.0, blue: 248/255.0, alpha: 1.0).cgColor
//        checkBoxButton.layer.borderWidth = 1
    }
    
    @IBAction func checkBoxClicked(_ sender: Any) {
        checkBoxButton.isSelected = !checkBoxButton.isSelected
    }

    func getData() -> [String: Any]? {
        let dataIs = ["email": emailText.text ?? "",
                      "password": paswordText.text ?? "",
                      ]
        return dataIs
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func submitClicked(_ sender: Any) {
        self.delegate?.submitClicked()
    }
    
}


extension UITextField{
    func setBorderColor(width:CGFloat,color:UIColor) -> Void{
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
    }
}
