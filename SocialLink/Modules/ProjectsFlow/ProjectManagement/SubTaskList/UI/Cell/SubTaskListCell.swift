//
//  SubTaskListCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/05/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class SubTaskListCell: UITableViewCell {

    @IBOutlet weak var subTaskName: UILabel!
    @IBOutlet weak var participantsCollectionCell: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setCollectionView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension SubTaskListCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func setCollectionView() {
        self.participantsCollectionCell.dataSource = self
        self.participantsCollectionCell.delegate = self
        self.participantsCollectionCell.semanticContentAttribute = .forceRightToLeft
        
        //                if let flow = cardsCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
        //                    let itemSpacing: CGFloat = 10
        //                    let itemsInOneLine: CGFloat = 1
        //                    flow.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        //                    let width = UIScreen.main.bounds.size.width - itemSpacing * CGFloat(itemsInOneLine - 1)
        //                    flow.itemSize = CGSize(width: floor(width/itemsInOneLine), height: width/itemsInOneLine)
        //                    flow.minimumInteritemSpacing = 1
        //                    flow.minimumLineSpacing = 10
        //                }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        print(UIScreen.main.bounds.width)
        return CGSize(width: 40, height: 40)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.SubTaskParticipantsCellIdentifier, for: indexPath)
        cell.tag = indexPath.row
        if let cellIs = cell as? SubTaskParticipantsCell {
            cellIs.setData(data: nil)
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}
