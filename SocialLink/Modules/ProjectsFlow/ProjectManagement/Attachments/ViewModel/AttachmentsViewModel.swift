//
//  AttachmentsViewModel.swift
//  SocialLink
//
//  Created by Harsha on 03/04/19.
//  Copyright © 2019 Santhosh Marripelli. All rights reserved.
//

import Foundation

class AttachmentsViewModel {
    init() {
        
    }
    func getAttachmentsList(parameters: [String: Any],
                        onSuccess: @escaping (AttachmentsModel?) -> Void,
                        onError: @escaping APIErrorHandler) {
        AppDataManager.getAttachmentsList(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: "Success")
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
}
