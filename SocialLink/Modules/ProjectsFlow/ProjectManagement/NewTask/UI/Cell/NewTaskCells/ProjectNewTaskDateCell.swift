//
//  ProjectNewTaskDateCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 09/05/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

protocol ProjectNewTaskDateCellDelegate: class {
    func startDateClicked(withTag: Int)
    func endDateClicked(withTag: Int)
    func updateTextField(tag: Int, data: String)
    func addParticipantClicked()
    
    
}

class ProjectNewTaskDateCell: UITableViewCell {
    
    weak var delegate: ProjectNewTaskDateCellDelegate?
   
    @IBOutlet weak var endButton: UIButton!
    @IBOutlet weak var endDateText: AppRoundBorderTextField!
    @IBOutlet weak var startDateButton: UIButton!
    @IBOutlet weak var startDateText: AppRoundBorderTextField!
    @IBOutlet weak var taskNameText: AppRoundBorderTextField!
    var selectedContact: ProjectContact?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.taskNameText.delegate = self
        self.startDateText.delegate = self
        self.endDateText.delegate = self
        
    //    addParticipantbutton.imageView?.contentMode = .scaleAspectFit

    }
    @IBAction func addParticipantClicked(_ sender: Any) {
        self.delegate?.addParticipantClicked()
    }
    
    func setData(taskData: Task) {
        //////
        self.taskNameText.text = taskData.tname ?? ""
        self.startDateText.text = APPUtility.convertUTCToClaenderFormat(date: taskData.tsdate ?? "")
        self.endDateText.text = APPUtility.convertUTCToClaenderFormat(date: taskData.tedate ?? "")
        
    }
    
    func updateContact(contact: ProjectContact) {
        print(contact)
        self.selectedContact = contact
    }
    
    func startDate(date: String) {
        startDateText.text = date
    }
    
    func endDate(date: String) {
        endDateText.text = date
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func endDateClicked(_ sender: UIButton) {
        self.delegate?.endDateClicked(withTag: sender.tag)
    }
    @IBAction func startDateClicked(_ sender: UIButton) {
        self.delegate?.startDateClicked(withTag: sender.tag)

    }
}


extension ProjectNewTaskDateCell: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.delegate?.updateTextField(tag: textField.tag, data: textField.text ?? "")
    }
    
}

