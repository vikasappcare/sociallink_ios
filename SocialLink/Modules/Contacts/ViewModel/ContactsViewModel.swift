//
//  ContactsViewModel.swift
//  SocialLink
//
//  Created by Harsha on 02/04/19.
//  Copyright © 2019 Santhosh Marripelli. All rights reserved.
//

import Foundation
class ContactsViewModel {
    
    init() {
    }
    
    func chatHistory(parameters: [String: Any],
                    onSuccess: @escaping (ChatHistory?) -> Void,
                    onError: @escaping APIErrorHandler) {
        AppDataManager.chatHistory(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: dataSource)
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
}
    func slackAuth(parameters: [String: Any], url: String,
                    onSuccess: @escaping (SlackAuthModel?) -> Void,
                    onError: @escaping APIErrorHandler) {
        AppDataManager.slackAuth(parametes: parameters, url: url, onSuccess: { (dataSource) in
            dprint(object: dataSource)
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    func slackUsersList(parameters: [String: Any],
                        onSuccess: @escaping (SlackUserDetails?) -> Void,
                        onError: @escaping APIErrorHandler) {
        AppDataManager.slackUserList(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: dataSource)
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
}
