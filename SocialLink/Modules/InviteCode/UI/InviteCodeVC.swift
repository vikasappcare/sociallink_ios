//
//  InviteCodeVC.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class InviteCodeVC: AppBaseVC {

    @IBOutlet weak var inviteTableView: UITableView!
    var viewModel: InviteCodeModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel = InviteCodeModel()
        self.setTableViewDatasource()
        
        self.createNavigationItems()
        
        self.title = "INVITE"

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func joinTeam(withCode: String) {
        if let model = viewModel {
            let parms = ["rcode": withCode]
            self.showHUD(title: "")
            model.joinTeam(parameters: parms , onSuccess: { [weak self] (dataSource) in
                print("Success")
                self?.hideHud()
                if (dataSource?.data) != nil {
                    let alertController = UIAlertController(title: "Success", message: "Member invited successfully", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                        self?.dismiss(animated: true, completion: nil)
                    }))
                    self?.present(alertController, animated: true, completion: nil)
                } else {
                    self?.showAlert(title: "Alert", message: "Member already invited for this project")
                }
                
              
                }, onError: { (error) in
                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
                    self.showAlert(title: "Alert", message: error.statusMessage)
                    self.hideHud()
            })
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension InviteCodeVC {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension InviteCodeVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.inviteTableView.delegate = self
        self.inviteTableView.dataSource = self
        self.inviteTableView.estimatedRowHeight = 300
        self.inviteTableView.rowHeight = UITableViewAutomaticDimension
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:
            CellIdentifiers.InviteTableViewCellIdentifier, for: indexPath)
        if let cellIs = cell as? InviteTableViewCell {
            cellIs.selectionStyle = .none
            cellIs.delegate = self
        }
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UIScreen.main.bounds.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension InviteCodeVC: InviteTableViewCellDelegate {
    func backClicked() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func submitClicked(withCode: String) {
        print(withCode)
        if withCode.checkForEmpty() {
            self.showAlert(title: "Alert!", message: "Please enter Code")
            return
        }
        self.joinTeam(withCode: withCode)
    }
    
    
    
}
