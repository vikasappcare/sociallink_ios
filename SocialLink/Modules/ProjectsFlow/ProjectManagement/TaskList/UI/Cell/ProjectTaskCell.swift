//
//  ProjectTaskCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 09/05/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

protocol ProjectTaskCellDelegate: class {
    func contactProfileSelected(withId: Int64)
}

class ProjectTaskCell: UITableViewCell {

    weak var delegate: ProjectTaskCellDelegate?
    @IBOutlet weak var taskStatusLabel: UILabel!
    @IBOutlet weak var taskLabel: UILabel!
    @IBOutlet weak var startDatetext: UITextField!
    @IBOutlet weak var endDateText: UITextField!
    @IBOutlet weak var startDateButton: UIButton!
    @IBOutlet weak var endDateButton: UIButton!
    @IBOutlet weak var taskParticipantsCollectionView: UICollectionView!
    var selectedTask: Task?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //self.taskProgress.layer.cornerRadius = 5.0
       // self.taskProgress.layer.masksToBounds = true
        
        
        //self.setCollectionView()
    }
    
    func setData(data: Task) {
        self.selectedTask = data
        taskLabel.text = data.tname ?? ""
        startDatetext.text = data.tsdate
        endDateText.text = data.tedate
        taskLabel.text = data.tname ?? ""
        if (data.accepted_state == "0") {
            taskStatusLabel.textColor = .red
            taskStatusLabel.text = "Cancelled"
        } else if (data.accepted_state == "1") {
            taskStatusLabel.text = "In Progress"
        } else if (data.accepted_state == "2") {
            taskStatusLabel.text = "Completed"
        }
        //taskStatusLabel.text = data.pstatus
//        taskLabel.text = data.task_title ?? ""
//        taskLabel.text = data.task_title ?? ""
//        taskLabel.text = data.task_title ?? ""
//        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
//extension ProjectTaskCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
//
//
//    func setCollectionView() {
//        self.taskParticipantsCollectionView.dataSource = self
//        self.taskParticipantsCollectionView.delegate = self
//        self.taskParticipantsCollectionView.semanticContentAttribute = .forceLeftToRight
//    }
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return 1
//    }
//
//
//    func collectionView(_ collectionView: UICollectionView, layout
//        collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        print(UIScreen.main.bounds.width)
//        return CGSize(width: 40, height: 40)
//    }
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.ParticipantsCellIdentifier, for: indexPath)
////        cell.tag = indexPath.row
////        if let cellIs = cell as? ParticipantsCell {
////            if let asigneee = self.selectedTask?.assigned_to {
////                print(asigneee.id)
////                cellIs.setData(data: asigneee)
////            }
////        }
//
//        return cell
//    }
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if let asigneee = self.selectedTask?.assigned_to?.id {
//            print(self.selectedTask?.assigned_to?.id)
//             self.delegate?.contactProfileSelected(withId: asigneee)
//        }
//    }
//}
