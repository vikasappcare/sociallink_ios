//
//  APPDBCommon.swift
//  BaseProject
//
//  Created by Santhosh Marripelli on 26/02/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON
import CoreData

class APPDBCommon {
    

    
    /////////////////////////////////////////////USER DATA SAVING IN CORE DATA /////////////////////////////////////
    class func addOrUpdateUserDataintoCoreData(with data: JSON, mocType: MOCType = .main, onSuccess: @escaping (UserDB?) -> Void) {
        
        print(data)
        self.getUserDataFromDB(with: data[0]["uid"].int64Value, mocType: mocType) { (user) in
            var userObject = user
            if userObject == nil {
                userObject = APPDBManager.shared.getManagedObject(for: APPDBConstants.USER, withMocType: mocType) as? UserDB
            }
            if let userData = userObject {
                userData.userId = data[0]["uid"].int64Value
                userData.name = data[0]["ufullname"].stringValue
                userData.email = data[0]["uemailid"].stringValue
                userData.aboutme = data[0]["uaboutu"].stringValue
                userData.phone_number = data[0]["uphone"].stringValue
                userData.city = data[0]["ucity"].stringValue
                userData.state = data[0]["ustate"].stringValue
                userData.country = data[0]["ucountry"].stringValue
                userData.pincode = data[0]["uzipcode"].int64Value
                userData.image = data[0]["uphoto"].stringValue
                userData.user_type = data[0]["utype"].int64Value
                userData.website = data[0]["uwebsite"].stringValue
                userData.created_time = data[0]["created_time"].stringValue
                userData.planId = data[0]["planId"].stringValue
                userData.plan_expiry = data[0]["plan_expiry"].stringValue
                userData.gender = data[0]["ugender"].stringValue
                userData.education = data[0]["education"].stringValue
                userData.skills = data[0]["uskills"].stringValue
                userData.business = data[0]["business"].stringValue
                userData.industry = data[0]["industry"].stringValue
                userData.department = data[0]["department"].stringValue
                userData.job = data[0]["job"].stringValue
                userData.resume = data[0]["resume"].stringValue
                userData.designation = data[0]["designation"].stringValue
                userData.requiredSkills = data[0]["required_skills"].stringValue
            }
            onSuccess(userObject)
        }
    }
    
    class func updateUserPictureintoCoreData(with picture: String, mocType: MOCType = .main, onSuccess: @escaping (Bool?) -> Void) {
        self.getUserDataFromDB(with: APPStore.sharedStore.user.userId ?? 0, mocType: mocType) { (user) in
            var userObject = user
            if userObject == nil {
                userObject = APPDBManager.shared.getManagedObject(for: APPDBConstants.USER, withMocType: mocType) as? UserDB
            }
            if let userData = userObject {
                userData.image = picture
            }
            onSuccess(true)
        }
    }

    
    private class func getUserDataFromDB(with userId: Int64, mocType: MOCType, onFetch: @escaping (UserDB?) -> Void) {
        let mainMOC = APPDBManager.shared.getMoc(for: mocType)
        mainMOC.performAndWait {
            let fetchRequest = NSFetchRequest<UserDB>.init(entityName: APPDBConstants.USER)
            dprint(object: fetchRequest)
            do {
                let userList = try mainMOC.fetch(fetchRequest)
                let user = userList.isEmpty ? nil : userList.first
                onFetch(user)
            } catch let error as NSError {
                Logger.log(message: "APPDBCommon:getUserFromDB Could not fetch User. Error - \(error), \(error.userInfo)", event: .error)
                onFetch(nil)
            }
        }
    }
    
    class func updateUserRole(with userId: Int64, role: Int64, mocType: MOCType = .main, onSuccess: @escaping (UserDB?) -> Void) {
        
        self.getUserDataFromDB(with: userId, mocType: mocType) { (user) in
            var userObject = user
            if userObject == nil {
                userObject = APPDBManager.shared.getManagedObject(for: APPDBConstants.USER, withMocType: mocType) as? UserDB
            }
            if let userData = userObject {
                userData.user_type = role
            }
            onSuccess(userObject)
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /////////////////////////////////////////////TOKEN DATA SAVING IN CORE DATA /////////////////////////////////////
    class func addOrUpdateTokenCoreData(with data: JSON, mocType: MOCType = .main, onSuccess: @escaping (TokenDB?) -> Void) {
        self.getTokenFromDB(with: data["userId"].int64Value, mocType: mocType) { (token) in
            var tokenObject = token
            if tokenObject == nil {
                tokenObject = APPDBManager.shared.getManagedObject(for: APPDBConstants.TOKEN, withMocType: mocType) as? TokenDB
            }
            if let tokenData = tokenObject {
                tokenData.token = data["token"].stringValue
            }
            onSuccess(tokenObject)
        }
    }
    
    
    private class func getTokenFromDB(with userId: Int64, mocType: MOCType, onFetch: @escaping (TokenDB?) -> Void) {
        let mainMOC = APPDBManager.shared.getMoc(for: mocType)
        mainMOC.performAndWait {
            let fetchRequest = NSFetchRequest<TokenDB>.init(entityName: APPDBConstants.TOKEN)
            dprint(object: fetchRequest)
            do {
                let tokenIs = try mainMOC.fetch(fetchRequest)
                let token = tokenIs.isEmpty ? nil : tokenIs.first
                onFetch(token)
            } catch let error as NSError {
                Logger.log(message: "APPDBCommon:getTokenFromDB Could not fetch User. Error - \(error), \(error.userInfo)", event: .error)
                onFetch(nil)
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    /////////////////////////////////////////////Project DATA SAVING IN CORE DATA /////////////////////////////////////
    class func addOrUpdateProjectCoreData(with data: JSON, mocType: MOCType = .main, onSuccess: @escaping (ProjectsDB?) -> Void) {
        
        for projectIs in data.arrayValue {
            self.getProjectFromDB(with: projectIs["pid"].int64Value, mocType: mocType) { (project) in
                var projectObject = project
                if projectObject == nil {
                    projectObject = APPDBManager.shared.getManagedObject(for: APPDBConstants.PROJECT, withMocType: mocType) as? ProjectsDB
                }
                if let projectData = projectObject {
                    projectData.createdId = projectIs["user_id"].int64Value
                    projectData.createdImage = projectIs["created_by"]["image"].stringValue
                    projectData.createdName = projectIs["created_by"]["name"].stringValue
                    projectData.endDate = projectIs["pdeadline"].stringValue
                    projectData.progress = projectIs["progress"].floatValue
                    projectData.projectId = projectIs["pid"].int64Value
                    projectData.projectName = projectIs["pname"].stringValue
                    projectData.startDate = projectIs["psdate"].stringValue
                    projectData.status = projectIs["pstatus"].int64Value
                    projectData.descriptionIs = projectIs["pscope"].stringValue
                    projectData.service = projectIs["pskills"].stringValue
                    projectData.user_type = APPStore.sharedStore.user.user_type ?? 0
                    projectData.budget = projectIs["ebudget"].stringValue
                    projectData.pupload = projectIs["pupload"].stringValue
                    projectData.estimatedTime = projectIs["etime"].stringValue
                    projectData.p_type = projectIs["ptype"].int64Value
                    projectData.cDate = projectIs["cdate"].stringValue
                }
                onSuccess(projectObject)
            }
        }
    }
    
    class func updateProjectCoreData(with data: JSON, mocType: MOCType = .main, onSuccess: @escaping (ProjectsDB?) -> Void) {
            self.getProjectFromDB(with: data["pid"].int64Value, mocType: mocType) { (project) in
                var projectObject = project
                if projectObject == nil {
                    projectObject = APPDBManager.shared.getManagedObject(for: APPDBConstants.PROJECT, withMocType: mocType) as? ProjectsDB
                }
                if let projectData = projectObject {
                    projectData.createdId = data["user_id"].int64Value
                    projectData.createdImage = data["created_by"]["image"].stringValue
                    projectData.createdName = data["created_by"]["name"].stringValue
                    projectData.endDate = data["pdeadline"].stringValue
                    projectData.progress = data["progress"].floatValue
                    projectData.projectId = data["pid"].int64Value
                    projectData.projectName = data["pname"].stringValue
                    projectData.startDate = data["psdate"].stringValue
                    projectData.status = data["pstatus"].int64Value
                    projectData.descriptionIs = data["pscope"].stringValue
                    projectData.service = data["pskills"].stringValue
                    projectData.user_type = APPStore.sharedStore.user.user_type ?? 0
                    projectData.budget = data["ebudget"].stringValue
                    projectData.pupload = data["pupload"].stringValue
                    projectData.estimatedTime = data["etime"].stringValue
                    projectData.p_type = data["ptype"].int64Value
                    projectData.cDate = data["cdate"].stringValue
                }
                onSuccess(projectObject)
            }
        
    }
    
    
    private class func getProjectFromDB(with projectId: Int64, mocType: MOCType, onFetch: @escaping (ProjectsDB?) -> Void) {
        let mainMOC = APPDBManager.shared.getMoc(for: mocType)
        mainMOC.performAndWait {
            let fetchRequest = NSFetchRequest<ProjectsDB>.init(entityName: APPDBConstants.PROJECT)
             fetchRequest.predicate = NSPredicate.init(format: "projectId == %d", projectId)
            dprint(object: fetchRequest)
            do {
                let projectIs = try mainMOC.fetch(fetchRequest)
                let project = projectIs.isEmpty ? nil : projectIs.first
                onFetch(project)
            } catch let error as NSError {
                Logger.log(message: "APPDBCommon:getTokenFromDB Could not fetch User. Error - \(error), \(error.userInfo)", event: .error)
                onFetch(nil)
            }
        }
    }
    
    /////////////////////////////////////////////Role DATA SAVING IN CORE DATA /////////////////////////////////////
    class func addOrUpdateRoleCoreData(with data: JSON, mocType: MOCType = .main, onSuccess: @escaping (RolesDB?) -> Void) {
        
        for roleIs in data.arrayValue {
            self.getRoleFromDB(with: roleIs["rid"].int64Value, mocType: mocType) { (role) in
                var roleObject = role
                if roleObject == nil {
                    roleObject = APPDBManager.shared.getManagedObject(for: APPDBConstants.ROLE, withMocType: mocType) as? RolesDB
                }
                if let roleData = roleObject {
                    roleData.rid = roleIs["rid"].int64Value
                    roleData.name = roleIs["name"].stringValue
                }
                onSuccess(roleObject)
            }
        }
    }
    
    
    private class func getRoleFromDB(with roleId: Int64, mocType: MOCType, onFetch: @escaping (RolesDB?) -> Void) {
        let mainMOC = APPDBManager.shared.getMoc(for: mocType)
        mainMOC.performAndWait {
            let fetchRequest = NSFetchRequest<RolesDB>.init(entityName: APPDBConstants.ROLE)
            fetchRequest.predicate = NSPredicate.init(format: "rid == %d", roleId)
            dprint(object: fetchRequest)
            do {
                let roleIs = try mainMOC.fetch(fetchRequest)
                let role = roleIs.isEmpty ? nil : roleIs.first
                onFetch(role)
            } catch let error as NSError {
                Logger.log(message: "APPDBCommon:getRoleFromDB Could not fetch User. Error - \(error), \(error.userInfo)", event: .error)
                onFetch(nil)
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    /////////////////////////////////////////////Service DATA SAVING IN CORE DATA /////////////////////////////////////
    class func addOrUpdateServiceCoreData(with data: JSON, mocType: MOCType = .main, onSuccess: @escaping (ServicesDB?) -> Void) {
        
        for roleIs in data.arrayValue {
            self.getServiceFromDB(with: roleIs["service_id"].int64Value, mocType: mocType) { (role) in
                var roleObject = role
                if roleObject == nil {
                    roleObject = APPDBManager.shared.getManagedObject(for: APPDBConstants.SERVICE, withMocType: mocType) as? ServicesDB
                }
                if let roleData = roleObject {
                    roleData.service_id = roleIs["service_id"].int64Value
                    roleData.service_name = roleIs["service_name"].stringValue
                }
                onSuccess(roleObject)
            }
        }
    }
    
    
    private class func getServiceFromDB(with roleId: Int64, mocType: MOCType, onFetch: @escaping (ServicesDB?) -> Void) {
        let mainMOC = APPDBManager.shared.getMoc(for: mocType)
        mainMOC.performAndWait {
            let fetchRequest = NSFetchRequest<ServicesDB>.init(entityName: APPDBConstants.SERVICE)
            fetchRequest.predicate = NSPredicate.init(format: "service_id == %d", roleId)
            dprint(object: fetchRequest)
            do {
                let roleIs = try mainMOC.fetch(fetchRequest)
                let role = roleIs.isEmpty ? nil : roleIs.first
                onFetch(role)
            } catch let error as NSError {
                Logger.log(message: "APPDBCommon:getRoleFromDB Could not fetch User. Error - \(error), \(error.userInfo)", event: .error)
                onFetch(nil)
            }
        }
    }

    /////////////////////////////////////////////FEED DATA SAVING IN CORE DATA /////////////////////////////////////
    class func addOrUpdateFeedCoreData(with data: JSON, mocType: MOCType = .main, onSuccess: @escaping (FeedsDB?) -> Void) {
        
        for feedIs in data.arrayValue {
            self.getFeedFromDB(with: feedIs["feed_id"].int64Value, mocType: mocType) { (feed) in
                var feedObject = feed
                if feedObject == nil {
                    feedObject = APPDBManager.shared.getManagedObject(for: APPDBConstants.FEED, withMocType: mocType) as? FeedsDB
                }
                if let feedData = feedObject {
                    feedData.feed_id = feedIs["fid"].int64Value
                    feedData.project_id = feedIs["project_id"].int64Value
                    feedData.feed_type = feedIs["ftype"].int64Value
                    feedData.feed_type_id = feedIs["ftype_id"].int64Value
                    feedData.feed_details = feedIs["fdetails"].stringValue
                    feedData.document = feedIs["document"].stringValue
                    feedData.feed_extension = feedIs["file_extension"].stringValue
                    feedData.feed_title = feedIs["feed_title"].stringValue
                    feedData.commentsCount = feedIs["comment_count"].int64Value
                    feedData.comments = feedIs["comment"].stringValue
                    feedData.likes_count = feedIs["like_unlike_count"].int64Value
                    feedData.flikes = feedIs["flikes"].int64Value
                    feedData.feed_time = feedIs["ftime"].stringValue
                    feedData.created_Date = feedIs["cdate"].stringValue
                    feedData.created_by_ID = feedIs["fuser"].int64Value
                    feedData.created_by_Name = feedIs["ufullname"].stringValue
                    feedData.created_by_Image = feedIs["uphoto"].stringValue
                    feedData.created_by_Email = feedIs["created_by"]["emailid"].stringValue
                    feedData.created_by_Designation = feedIs["designation"].stringValue
            
                }
                onSuccess(feedObject)
            }
        }
    }
    
    class func addOrUpdateNewFeedCoreData(with data: JSON, mocType: MOCType = .main, onSuccess: @escaping (FeedsDB?) -> Void) {
        
            self.getFeedFromDB(with: data["feed_id"].int64Value, mocType: mocType) { (feed) in
                var feedObject = feed
                if feedObject == nil {
                    feedObject = APPDBManager.shared.getManagedObject(for: APPDBConstants.FEED, withMocType: mocType) as? FeedsDB
                }
                if let feedData = feedObject {
                    feedData.feed_id = data["fid"].int64Value
                    feedData.project_id = data["project_id"].int64Value
                    feedData.feed_type = data["ftype"].int64Value
                    feedData.feed_title = data["fdetails"].stringValue
                    feedData.document = data["document"].stringValue
                    feedData.feed_type_id = data["ftype_id"].int64Value
                    feedData.feed_extension = data["feed_extension"].stringValue
                    feedData.feed_title = data["feed_title"].stringValue
                    feedData.commentsCount = data["comment_count"].int64Value
                    feedData.comments = data["comment"].stringValue
                    feedData.likes_count = data["like_unlike_count"].int64Value
                    feedData.flikes = data["flikes"].int64Value
                    feedData.feed_time = data["ftime"].stringValue
                    feedData.created_Date = data["cdate"].stringValue
                    feedData.created_by_ID = data["fuser"].int64Value
                    feedData.created_by_Name = data["ufullname"].stringValue
                    feedData.created_by_Image = data["uphoto"].stringValue
                    feedData.created_by_Email = data["created_by"]["emailid"].stringValue
                    feedData.created_by_Designation = data["designation"].stringValue
                    print(feedData)
                }
                onSuccess(feedObject)
            }
    }
    
    
    class func updateFeedLike(with data: JSON, mocType: MOCType = .main, onSuccess: @escaping (FeedsDB?) -> Void) {
            self.getFeedFromDB(with: data["fid"].int64Value, mocType: mocType) { (feed) in
                var feedObject = feed
                if feedObject == nil {
                    feedObject = APPDBManager.shared.getManagedObject(for: APPDBConstants.FEED, withMocType: mocType) as? FeedsDB
                }
                if let feedData = feedObject {
                    feedData.flikes = data["flikes"].int64Value
                    feedData.likes_count = data["like_unlike_count"].int64Value
                    print(feedData)
                }
                onSuccess(feedObject)
            }
    }
    
    class func updateFeedImage(with data: FileUploadModel, mocType: MOCType = .main, onSuccess: @escaping (FeedsDB?) -> Void) {
        self.getFeedFromDB(with: data.data?.id ?? 0, mocType: mocType) { (feed) in
            var feedObject = feed
            if feedObject == nil {
                feedObject = APPDBManager.shared.getManagedObject(for: APPDBConstants.FEED, withMocType: mocType) as? FeedsDB
            }
            if let feedData = feedObject {
                feedData.feed_extension = data.data?.extensionIs ?? ""
                print(feedData)
            }
            onSuccess(feedObject)
        }
    }
    
    
    private class func getFeedFromDB(with feedId: Int64, mocType: MOCType, onFetch: @escaping (FeedsDB?) -> Void) {
        let mainMOC = APPDBManager.shared.getMoc(for: mocType)
        mainMOC.performAndWait {
            let fetchRequest = NSFetchRequest<FeedsDB>.init(entityName: APPDBConstants.FEED)
            fetchRequest.predicate = NSPredicate.init(format: "feed_id == %d", feedId)
            dprint(object: fetchRequest)
            do {
                let feedIs = try mainMOC.fetch(fetchRequest)
                let feed = feedIs.isEmpty ? nil : feedIs.first
                onFetch(feed)
            } catch let error as NSError {
                Logger.log(message: "APPDBCommon:getFeedFromDB Could not fetch User. Error - \(error), \(error.userInfo)", event: .error)
                onFetch(nil)
            }
        }
    }
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /////////////////////////////////////////////TASKS DATA SAVING IN CORE DATA /////////////////////////////////////
    class func addOrUpdateTasksCoreData(with data: JSON, mocType: MOCType = .main, onSuccess: @escaping (TasksDB?) -> Void) {
        for objectIs in data.arrayValue {
            self.getTaskFromDB(with: objectIs["task_id"].int64Value, mocType: mocType) { (task) in
                var taskObject = task
                if taskObject == nil {
                    taskObject = APPDBManager.shared.getManagedObject(for: APPDBConstants.TASK, withMocType: mocType) as? TasksDB
                }
                if let taskData = taskObject {
                    taskData.task_title = objectIs["tname"].stringValue
                    taskData.task_id = objectIs["task_id"].int64Value
                    taskData.project_id = objectIs["project_id"].int64Value
                    taskData.project_name = objectIs["project_name"].stringValue
                    taskData.task_details = objectIs["tdetails"].stringValue
                    taskData.task_state = objectIs["task_state"].int64Value
                    taskData.start_date = objectIs["tsdate"].stringValue
                    taskData.end_date = objectIs["tedate"].stringValue
                    taskData.created_by_name = objectIs["created_by"]["name"].stringValue
                    taskData.created_by_id = objectIs["user_id"].int64Value
                    taskData.created_by_designation = objectIs["created_by"]["designation"].stringValue
                    taskData.created_by_image = objectIs["created_by"]["image"].stringValue
                    taskData.created_by_email = objectIs["created_by"]["emailid"].stringValue
                    taskData.assigned_to_name = objectIs["tassignto"].stringValue
                    taskData.assigned_to_id = objectIs["assigned_to"]["id"].int64Value
                    taskData.assigned_to_designation = objectIs["assigned_to"]["designation"].stringValue
                    taskData.assigned_to_image = objectIs["assigned_to"]["image"].stringValue
                    taskData.assigned_to_email = objectIs["assigned_to"]["emailid"].stringValue
                    taskData.accepted_state = objectIs["accepted_state"].stringValue
                    taskData.user_type = APPStore.sharedStore.user.user_type ?? 0
                    print(taskData.user_type)
                }
                onSuccess(taskObject)
            }
        }
    }
 
    
    private class func getTaskFromDB(with taskId: Int64, mocType: MOCType, onFetch: @escaping (TasksDB?) -> Void) {
        let mainMOC = APPDBManager.shared.getMoc(for: mocType)
        mainMOC.performAndWait {
            let fetchRequest = NSFetchRequest<TasksDB>.init(entityName: APPDBConstants.TASK)
            fetchRequest.predicate = NSPredicate.init(format: "task_id == %d", taskId)
            dprint(object: fetchRequest)
            do {
                let objectIs = try mainMOC.fetch(fetchRequest)
                let object = objectIs.isEmpty ? nil : objectIs.first
                onFetch(object)
            } catch let error as NSError {
                Logger.log(message: "APPDBCommon:getTaskFromDB Could not fetch User. Error - \(error), \(error.userInfo)", event: .error)
                onFetch(nil)
            }
        }
    }
    
    class func updateTaskCoreData(with data: JSON, mocType: MOCType = .main, onSuccess: @escaping (TasksDB?) -> Void) {
            self.getTaskFromDB(with: data["task_id"].int64Value, mocType: mocType) { (task) in
                var taskObject = task
                if taskObject == nil {
                    taskObject = APPDBManager.shared.getManagedObject(for: APPDBConstants.TASK, withMocType: mocType) as? TasksDB
                }
                if let taskData = taskObject {
                    taskData.task_title = data["tname"].stringValue
                    taskData.task_id = data["task_id"].int64Value
                    taskData.project_id = data["project_id"].int64Value
                    taskData.project_name = data["project_name"].stringValue
                    taskData.task_details = data["tdetails"].stringValue
                    taskData.task_state = data["task_state"].int64Value
                    taskData.start_date = data["tsdate"].stringValue
                    taskData.end_date = data["tedate"].stringValue
                    taskData.created_by_name = data["created_by"]["name"].stringValue
                    taskData.created_by_id = data["user_id"].int64Value
                    taskData.created_by_designation = data["created_by"]["designation"].stringValue
                    taskData.created_by_image = data["created_by"]["image"].stringValue
                    taskData.created_by_email = data["created_by"]["emailid"].stringValue
                    taskData.assigned_to_name = data["tassignto"].stringValue
                    taskData.assigned_to_id = data["assigned_to"]["id"].int64Value
                    taskData.assigned_to_designation = data["assigned_to"]["designation"].stringValue
                    taskData.assigned_to_image = data["assigned_to"]["image"].stringValue
                    taskData.assigned_to_email = data["assigned_to"]["emailid"].stringValue
                    taskData.accepted_state = data["accepted_state"].stringValue
                }
                onSuccess(taskObject)
            }
        
    }
    class func addOrUpdateTaskCoreData(with data: JSON, mocType: MOCType = .main, onSuccess: @escaping (TasksDB?) -> Void) {
            self.getTaskFromDB(with: data["task_id"].int64Value, mocType: mocType) { (task) in
                var taskObject = task
                if taskObject == nil {
                    taskObject = APPDBManager.shared.getManagedObject(for: APPDBConstants.TASK, withMocType: mocType) as? TasksDB
                }
                if let taskData = taskObject {
                    taskData.task_title = data["tname"].stringValue
                    taskData.task_id = data["task_id"].int64Value
                    taskData.project_id = data["project_id"].int64Value
                    taskData.project_name = data["project_name"].stringValue
                    taskData.task_details = data["tdetails"].stringValue
                    taskData.task_state = data["task_state"].int64Value
                    taskData.start_date = data["tsdate"].stringValue
                    taskData.end_date = data["tedate"].stringValue
                    taskData.created_by_name = data["created_by"]["name"].stringValue
                    taskData.created_by_id = data["created_by"]["id"].int64Value
                    taskData.created_by_designation = data["created_by"]["designation"].stringValue
                    taskData.created_by_image = data["created_by"]["image"].stringValue
                    taskData.created_by_email = data["created_by"]["emailid"].stringValue
                    taskData.assigned_to_name = data["tassignto"].stringValue
                    taskData.assigned_to_id = data["assigned_to"]["id"].int64Value
                    taskData.assigned_to_designation = data["assigned_to"]["designation"].stringValue
                    taskData.assigned_to_email = data["assigned_to"]["emailid"].stringValue
                    taskData.assigned_to_image = data["assigned_to"]["image"].stringValue
                    taskData.accepted_state = data["accepted_state"].stringValue
                }
                onSuccess(taskObject)
            }
        
    }
    
    
    
   
    
    
    
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    ////////////////////////////////////////////////TIME LOGS/////////////////////////////////////////////////
    class func addOrUpdateTimeLogInCoreData(with data: JSON, mocType: MOCType = .main, onSuccess: @escaping (TimeLogsDB?) -> Void) {
        for objectIs in data.arrayValue {
            self.getTaskTimeLogFromDB(with: objectIs["task_id"].int64Value, startTime: objectIs["start_time"].stringValue, mocType: mocType) { (timelog) in
                var logObject = timelog
                if logObject == nil {
                    logObject = APPDBManager.shared.getManagedObject(for: APPDBConstants.TIMELOGS, withMocType: mocType) as? TimeLogsDB
                }
                if let logData = logObject {
                    logData.time_started = objectIs["time_started"].boolValue
                    logData.task_id = objectIs["task_id"].int64Value
                    logData.start_time = objectIs["start_time"].stringValue
                    logData.end_time = objectIs["end_time"].stringValue
                    print(logData)
                }
                onSuccess(logObject)
            }
        }
    }
    
    private class func getTaskTimeLogFromDB(with taskId: Int64, startTime: String, mocType: MOCType, onFetch: @escaping (TimeLogsDB?) -> Void) {
        let mainMOC = APPDBManager.shared.getMoc(for: mocType)
        mainMOC.performAndWait {
            let fetchRequest = NSFetchRequest<TimeLogsDB>.init(entityName: APPDBConstants.TIMELOGS)
            fetchRequest.predicate = NSPredicate.init(format: "task_id == %d AND start_time == %@", taskId, startTime)
            dprint(object: fetchRequest)
            do {
                let objectIs = try mainMOC.fetch(fetchRequest)
                let object = objectIs.isEmpty ? nil : objectIs.first
                onFetch(object)
            } catch let error as NSError {
                Logger.log(message: "APPDBCommon:getTaskFromDB Could not fetch User. Error - \(error), \(error.userInfo)", event: .error)
                onFetch(nil)
            }
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    
    
    
    
}
