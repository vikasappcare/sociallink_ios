//
//  ProfileContentCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 27/02/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

protocol ProfileContentCellDelegate: class {
    
    func dataChanged(data: String, tag: Int)
    func showPickerView(forTag: Int)
    func textfieldData(data: String, tag: Int)

}
class ProfileContentCell: UITableViewCell {
    
    var valuesAre = [String: Any]()

    weak var delegate: ProfileContentCellDelegate?
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var optForLabel: UISegmentedControl!
    @IBOutlet weak var contentText: AppTextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentText.delegate = self
//        self.optForLabel.selectedSegmentIndex = 0
//        self.optForLabel.isHidden = true;
    }

    @IBAction func segmentedControlAction(sender: AnyObject) {
        if(self.optForLabel.selectedSegmentIndex == 0)
        {
            self.contentText.placeholder = "Skill (ex: Web Design)";
        }
        else if(self.optForLabel.selectedSegmentIndex == 1)
        {
            self.contentText.placeholder = "What kind of talent do you need?";
        }
        else if(self.optForLabel.selectedSegmentIndex == 2)
        {
            self.contentText.placeholder = "Enter the details";
        }
    }
    func getData() -> String {
        let dataIs = contentText.text ?? ""
        return dataIs
    }
    func setText(text: String, tag: Int) {
        self.contentText.text = text
        self.contentText.tag = tag
    }

    func setData(data: String?, tag: Int, profile: ProfileModel) {
        let headerText = data ?? ""
        contentText.tag = tag
        self.headerLabel.text =  headerText
        let lowercase = (data ?? "").lowercased()
        contentText.placeholder = "Enter \(lowercase)"
        
        print("tag", tag)
         print("headerText ", headerText)
        
        switch headerText {
        case "Email":
            contentText.text = profile.email ?? ""
            contentText.keyboardType = .emailAddress
            contentText.isEnabled = false
        case "Phone Number":
            contentText.text = profile.phone_number ?? ""
            contentText.keyboardType = .numberPad
            contentText.isEnabled = true
            contentText.tag = 20
        case "Website":
            contentText.keyboardType = .URL
            contentText.text = profile.website ?? ""
            contentText.isEnabled = true
        case "About You":
            contentText.text = profile.aboutme ?? ""
            contentText.isEnabled = true
            contentText.keyboardType = .default

        case "Education":
            contentText.text = profile.education ?? ""
            contentText.isEnabled = true
            contentText.keyboardType = .default

        case "Skills":
            contentText.text = profile.skills ?? ""
            contentText.isEnabled = true
            contentText.keyboardType = .default

        case "Business":
            contentText.text = profile.business ?? ""
            contentText.isEnabled = true
            contentText.keyboardType = .default

        case "Industry":
            contentText.text = profile.industry ?? ""
            contentText.isEnabled = true
        case "Department":
            contentText.text = profile.department ?? ""
            contentText.isEnabled = true
        case "Job":
            contentText.text = profile.job ?? ""
            contentText.isEnabled = true
//        case "Resume":
//            contentText.text = profile.resume ?? ""
//            contentText.isEnabled = true
        case "City":
            contentText.text = profile.city ?? ""
            contentText.isEnabled = true
            contentText.keyboardType = .default

        case "State":
            contentText.text = profile.state ?? ""
            contentText.isEnabled = true
            contentText.keyboardType = .default

        case "Country":
            contentText.text = profile.country ?? ""
            contentText.isEnabled = true
            contentText.keyboardType = .default
        case "Designation":
            contentText.text = profile.designation ?? ""
            contentText.isEnabled = true
            contentText.keyboardType = .default

        case "First Name":
            contentText.text = profile.full_name ?? ""
            contentText.isEnabled = true
            contentText.keyboardType = .default

        case "Last Name":
            contentText.text = profile.last_name ?? ""
            contentText.isEnabled = true
            contentText.keyboardType = .default

        case "Here For":
            contentText.text = profile.hereFor ?? ""
            contentText.isEnabled = true
        default:
            contentText.keyboardType = .default
        }
 
    }
    
    
//
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension ProfileContentCell: UITextFieldDelegate {
//
//
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//
//        switch textField.tag {
//        case 13:
//            self.delegate?.showPickerView(forTag: textField.tag)
//            return false
//        case 14:
//            self.delegate?.showPickerView(forTag: textField.tag)
//            return false
//        case 15:
//            self.delegate?.showPickerView(forTag: textField.tag)
//            return false
//        default:
//            return true
//        }
//    }
//
//
//
//
    func textFieldDidEndEditing(_ textField: UITextField) {
    
        self.delegate?.textfieldData(data: textField.text!, tag: textField.tag)
        
        print("This is the tag of textfield",textField.tag,"and this is the text", textField.text)
        self.delegate?.dataChanged(data: textField.text ?? "", tag: textField.tag)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag != 20{
            let allowedCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
            let typedCharacterSet = CharacterSet(charactersIn: string)
            let alphabet = allowedCharacterSet.isSuperset(of: typedCharacterSet)
            return alphabet
            }
        return true
    }
}
