//
//  ServiceTableViewCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 12/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ServiceTableViewCell: UITableViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var servicename: UILabel!
    @IBOutlet weak var serviceImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    func setData(data: [String: Any]?) {
        
        servicename.text = data!["name"] as? String
        
        let boolValue = data!["roundCorner"] as? Bool
        let iconPath = "https://app.sociallink.com/uploads/map_icons/"
        let image = iconPath + "\(data!["image"] ?? "")"
        serviceImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: ""))

        if boolValue! {
            backView.clipsToBounds = true
            backView.layer.cornerRadius = 20
            if #available(iOS 11.0, *) {
                backView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            } else {
                // Fallback on earlier versions
            }
            
        } else {
            backView.clipsToBounds = false
            backView.layer.cornerRadius = 0
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
