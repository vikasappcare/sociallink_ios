//
//  ChattingViewModel.swift
//  SocialLink
//
//  Created by Harsha on 26/04/19.
//

import Foundation
class ChattingViewModel {
    
    init() {
    }
    
    func postChatMessage(parameters: [String: Any],
                     onSuccess: @escaping (PostChat?) -> Void,
                     onError: @escaping APIErrorHandler) {
        AppDataManager.postChatMessage(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: dataSource)
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
}
}
