//
//  ProjectBillingViewModel.swift
//  SocialLink
//
//  Created by Harsha on 08/05/19.
//

import Foundation
class ProjectBillingViewModel {
    
    init() {
    }
    
    func getProjectContacts(parameters: [String: Any],
                            onSuccess: @escaping (ProjectContactsModel?) -> Void,
                            onError: @escaping APIErrorHandler) {
        AppDataManager.getProjectContacts(parametes: parameters, onSuccess: { (dataSource) in
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
}
