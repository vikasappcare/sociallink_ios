//
//  StateModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 16/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON

class StateModel: AppBaseModel {
    
    var service: String?
    var data: [State]?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    
    private func parseResponse(with response: JSON) {
        self.service = response["service"].stringValue
        for item in response["data"].arrayValue {
            if let model = self.createObject(with: item) {
                if self.data == nil {
                    self.data = [State]()
                }
                self.data?.append(model)
            }
        }
    }
    
    private func createObject(with jsonObject: JSON) -> State? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return State.init(jsonObject)
        }
    }
    
}

class State: AppBaseModel {
    
    var id: Int64?
    var name: String?
    var country_id: Int64?
        
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    
    private func parseResponse(with response: JSON) {
        self.id = response["id"].int64Value
        self.name = response["name"].stringValue
        self.country_id = response["country_id"].int64Value
    }
    
}



