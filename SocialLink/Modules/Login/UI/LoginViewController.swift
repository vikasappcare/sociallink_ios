//
//  LoginViewController.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 26/02/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class LoginViewController: AppBaseVC {
    @IBOutlet weak var loginTableview: UITableView!
    var viewModel: LoginViewModel?
    var registerModel: RegisterViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = LoginViewModel()
        registerModel = RegisterViewModel()
        addNotification()
        self.setTableViewDatasource()
        self.createNavigationItems()
        self.addNotification()
        self.navigationItem.title = "LOGIN"
        
        APPStore.sharedStore.forgetPasswordEmail = ""


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
        deinit {
            NotificationCenter.default.removeObserver(self)
        }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LoginViewController {
    
    func addNotification() {
      
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(dismissView),
                                               name:
            NSNotification.Name(rawValue: APPNotifications.dismissLoginView),
                                               object: nil)
    }
    
    @objc func  dismissView(info: NSNotification) {
        self.dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue:
            APPNotifications.changeRootView),
                                        object: nil, userInfo: nil)
    }
    
}

extension LoginViewController {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
}
extension LoginViewController: GIDSignInUIDelegate {

    @objc func googleSignResponse(info: NSNotification) {

        //Edited :- vikas
        let deviceToken = UserDefaults.standard.string(forKey: "fcmToken")
        
//        let parms: [String: Any] = ["ufullname": info.userInfo?["firstname"] ?? "",
//                                    "uemailid": info.userInfo?["email"] ?? "",
//                                    "u_social_id": info.userInfo?["userid"] ?? "",
//                                    "token": info.userInfo?["token"] ?? ""]

        let parms: [String: Any] = ["ufullname": info.userInfo?["firstname"] ?? "",
                                    "uemailid": info.userInfo?["email"] ?? "",
                                    "u_social_id": info.userInfo?["userid"] ?? "",
                                    "token": deviceToken ?? ""]

        print(parms)
        if let model = registerModel {
            model.registerGmailUser(parameters: parms, onSuccess: { [weak self] (dataSource) in
                APPStore.sharedStore.userIs = USERTYPE.CONTRACTOR
                self?.navigationController?.popViewController(animated: true)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue:
                    APPNotifications.dismissLoginView),
                                                object: nil, userInfo: nil)
                self?.hideHud()
                }, onError: { (error) in
                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
                    self.showAlert(title: Message.Alert, message: error.statusMessage)
                    self.hideHud()
            })
        }
    }
}
extension LoginViewController: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.loginTableview.delegate = self
        self.loginTableview.dataSource = self
        self.loginTableview.estimatedRowHeight = 300
        self.loginTableview.rowHeight = UITableViewAutomaticDimension
        
        
        /// self.landingTableView.backgroundView = nil
        
        
        //        [self.tableView setBackgroundView:nil];
        //        [self.tableView setBackgroundColor:[UIColor clearColor]];
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:
            CellIdentifiers.LoginTableViewCellIdentifier, for: indexPath)
        if let cellIs = cell as? LoginTableViewCell {
            cellIs.selectionStyle = .none
            cellIs.delegate = self
        }
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UIScreen.main.bounds.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension LoginViewController: LoginTableViewCellDelegate {
    func submitClicked() {
      /*  let alertView = UIAlertController(title: "Login into SocialLink as", message: nil, preferredStyle: .actionSheet)
        let employee = UIAlertAction(title: "Employer", style: .default, handler: { (alert) in
            APPStore.sharedStore.loggedInAs = LoggedInUser.Employer
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginPasswordVC") as? LoginPasswordVC {
                self.navigationController?.pushViewController(controller, animated: true)
            }
        })
        alertView.addAction(employee)
        
        
        let contractor = UIAlertAction(title: "Contractor", style: .default, handler: { (alert) in
            APPStore.sharedStore.loggedInAs = LoggedInUser.Contractor
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginPasswordVC") as? LoginPasswordVC {
                self.navigationController?.pushViewController(controller, animated: true)
            }
        })
        alertView.addAction(contractor)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert) in
            
        })
        alertView.addAction(cancel)
        self.present(alertView, animated: true, completion: nil)*/
        
        
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = loginTableview.cellForRow(at: indexPath) as? LoginTableViewCell
        
        if let data = cell?.getData() {
            
            let email = (data["email"] as? String)?.trimmingCharacters(in: .whitespaces)

            if (email?.isEmpty ?? false) || (email == "") {
                self.showAlert(title: "Alert!", message: "Please enter email to continue")
                return
            } else if !APPUtility.validateEmail(enteredEmail: email ?? "") {
                self.showAlert(title: "Alert!", message: "Please enter a valid email to continue")
                return
            }
//            self?.pushToPasswordView(email: object?.profile?[0].email ?? "", profile: profileIs)

            if let data = cell?.getData() {
                let password = (data["password"] as? String)?.trimmingCharacters(in: .whitespaces)
                
                //Edited :- vikas
                let deviceToken = UserDefaults.standard.string(forKey: "fcmToken")
                
                if let model = viewModel {
                    let parms = ["uemailid": email ?? "",
                                 "upassword": password ?? "",
                                 "device_token":deviceToken ?? "",
                                 "device_type":"iOS"] as [String : Any]
                    print(parms, "login params")
                    self.showHUD(title: "Loading...")
                    model.loginPasswordUser(parameters: parms, onSuccess: { [weak self] (dataSource) in
                        print("Success")
                        print(dataSource ?? "")
                        APPUtility.getUserData()
                        APPUtility.getToken()
                        self?.navigationController?.popViewController(animated: true)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue:
                            APPNotifications.dismissLoginView),
                                                        object: nil, userInfo: nil)
                        self?.hideHud()
                        }, onError: { (error) in
                            Logger.log(message: "Error \(error.statusMessage)", event: .error)
                            self.showAlert(title: Message.Alert, message: error.statusMessage)
                            self.hideHud()
                    })
                }
            }
            
//            if let model = viewModel {
//                self.showHUD(title: "Loading...")
//                let dataIs = ["email": email ?? ""] as [String : Any]
//                model.loginUser(parameters: dataIs, onSuccess: { [weak self] (object) in
//                    dprint(object: "Success")
//                    self?.hideHud()
//                    if let profileIs = object {
//                        self?.pushToPasswordView(email: object?.profile?[0].email ?? "", profile: profileIs)
//                    }
//                    }, onError: { (error) in
//                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
//                        self.showAlert(title: Message.Alert, message: error.statusMessage)
//                        self.hideHud()
//                })
//            }
        }
    }
    func gmailLogin() {
        GIDSignIn.sharedInstance().uiDelegate = self
        // Uncomment to automatically sign in the user.
        GIDSignIn.sharedInstance().signOut()

        GIDSignIn.sharedInstance().signInSilently()

        GIDSignIn.sharedInstance().signIn()
    }
    
    func createNewAccountSelected() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "RegisterController") as? RegisterController {
            let navController = APPNavigationController(rootViewController: controller)
            self.present(navController, animated: true, completion: nil)
            /// self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func forgetPasswordSelected() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ForgetPasswordVC") as? ForgetPasswordVC {
            let navController = APPNavigationController(rootViewController: controller)
            self.present(navController, animated: true, completion: nil)
           /// self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func pushToPasswordView(email: String, profile: SignUpModel) {
        APPStore.sharedStore.loggedInAs = LoggedInUser.Employer
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginPasswordVC") as? LoginPasswordVC {
            controller.loggedInEmail(email: email, profile: profile)
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    
    
    
}
