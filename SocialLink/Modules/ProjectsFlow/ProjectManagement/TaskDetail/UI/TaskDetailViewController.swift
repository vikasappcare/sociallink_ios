//
//  TaskDetailViewController.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 01/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit
protocol TaskDetailViewControllerDelegate: class {
    func reloadChangedUpdatedRow(row: Int)
}

class TaskDetailViewController: AppBaseVC {
    weak var delegate: TaskDetailViewControllerDelegate?
    @IBOutlet weak var taskDetailTable: UITableView!
    let titleArray = ["Task Name:",
                      "Start Date:",
                      "End Date:",
                      "Status:",
                      "Task Description"]
    var selectedTasK: Task?
    var selectedContractorTasK: ContractorTask?
    var selectedProjectId: Int64?
    var startTime: String?
    var selectedRow: Int?
    var taskType: Int?
    var startTimeArr = [String]()
    var endTimeArr = [String]()
    var cumilativeTimeArr = [String]()
    var startDateArr = [String]()
    
    
    let watchObject = StopWatch()
    var timerStarted: Bool?
    var automaticallyStartTimer: Bool?
    @IBOutlet weak var timerStack: UIStackView!
    
    @IBOutlet weak var buttonOne: UIButton!
    @IBOutlet weak var buttonTwo: UIButton!
    @IBOutlet weak var actionStack: UIStackView!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var minuteLabel: UILabel!
    @IBOutlet weak var secondsLabel: UILabel!
    @IBOutlet weak var milliSecondsLabel: UILabel!
    @IBOutlet weak var timerView: UIView!
    @IBOutlet weak var completeButton: UIButton!
    
    var viewModel: TaskDetailViewModel?
    
    var timeLogArray: [TimeLog]?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = TaskDetailViewModel()
        self.setTableViewDatasource()
        self.createBackButton()
        self.title = "Task Description"
        
        self.setTimeLogs()
        self.updateTask()
        self.taskLog()
        
        self.watchObject.delegate = self
        completeButton.addTarget(self, action: #selector(taskStatusUpdate), for: .touchUpInside)
        validations()
    }
    
    func setTimeLogs() {
        if let logs = self.selectedTasK?.timeLog {
            if timeLogArray == nil {
                timeLogArray = [TimeLog]()
            }
            timeLogArray = logs
            self.sortTimeLogs()
        }
    }
    
    func sortTimeLogs() {
        var dateFormatter: DateFormatter?
        if dateFormatter == nil {
            dateFormatter = DateFormatter()
        }
        dateFormatter?.dateFormat = Constant.globalDateFormat
        if timeLogArray?.count ?? 0 > 0 {
            timeLogArray = timeLogArray?.sorted { (dateFormatter?.date(from: $0.start_time ?? "")!)! > (dateFormatter?.date(from: $1.start_time ?? "")!)! }
        }
    }
    
    func setStartButtonType() {
        if let timer = self.timerStarted {
            if timer {
                self.buttonOne.setTitle("Stop", for: .normal)
                self.buttonOne.backgroundColor = UIColor.appRedColor
            }
        }
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.taskLog()
    }
    
    func setTask(task: Task, row: Int, type: Int, projectId: Int64) {
        self.selectedTasK = task
        self.selectedRow = row
        self.taskType = type
        self.selectedProjectId = projectId
    }
    func setContractorTask(task: ContractorTask, row: Int, type: Int) {
        self.selectedContractorTasK = task
        self.selectedRow = row
        self.taskType = type
        
    }
    func validations(){
        
        if APPStore.sharedStore.appUserID == "2"{
            buttonOne.isHidden = true
            completeButton.isHidden = true
        }
        else{
            if selectedTasK?.status == "2"{
                buttonOne.isHidden = true
                completeButton.isHidden = true
            }
            else{
                buttonOne.isHidden = false
                completeButton.isHidden = false
            }
        }
    }
    func updateTask() {
        
        switch APPStore.sharedStore.appUserID {
            
        case  "1":
            ///////////////////////////CONTRACTOR//////////////////////
            switch self.taskType {
            case 0:
                
                self.buttonOne.setTitle("Start", for: .normal)
                // self.buttonTwo.setTitle("Reject", for: .normal)
                self.buttonOne.backgroundColor = UIColor.appBlueColor
                // self.buttonTwo.backgroundColor = UIColor.appRedColor
            // self.actionStack.removeArrangedSubview(self.timerView)
            case 1:
                self.buttonOne.setTitle("Start Timer", for: .normal)
                // self.buttonTwo.setTitle("Complete", for: .normal)
                self.buttonOne.backgroundColor = UIColor.appBlueColor
                //self.buttonTwo.backgroundColor = UIColor.appGreenColor
                //self.actionStack.removeArrangedSubview(self.timerView)
                if let model = viewModel, let timeLogs = timeLogArray {
                    if let logIs = model.filterLogTime(logs: timeLogs) {
                        let fromDate = APPUtility.convertUTCToClaenderFormatDate(date: logIs.start_time ?? "")
                        let toDate = Date()
                        watchObject.startTimer(fromDate: fromDate, toDate: toDate)
                        // self.actionStack.removeArrangedSubview(self.buttonTwo)
                        // self.actionStack.addArrangedSubview(self.timerView)
                        self.automaticallyStartTimer = true
                        self.timerStarted = true
                        self.setStartButtonType()
                    }
                }
                
            case 2:
                self.actionStack.removeArrangedSubview(self.buttonOne)
                //self.actionStack.removeArrangedSubview(self.buttonTwo)
                // self.actionStack.removeArrangedSubview(self.timerView)
                
            default:
                break
            }
        default:
            break
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func buttonOneClicked(_ sender: Any) {
        switch APPStore.sharedStore.appUserID {
        case "2":
            switch self.taskType {
            case 1:
                print("Add Comment")
            case 2:
                if let controller = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as? PaymentViewController {
                    self.navigationController?.pushViewController(controller, animated: true)
                }
            default:
                break
            }
        case "1":
            
            if self.timerStarted ?? false {
                //////// STOP
                self.watchObject.stopTimer()
                self.buttonOne.setTitle("Start", for: .normal)
                self.buttonOne.backgroundColor = UIColor.appBlueColor
                // self.actionStack.addArrangedSubview(self.buttonTwo)
                //   self.actionStack.removeArrangedSubview(self.timerView)
                let timerStartTime = UserDefaults.standard.object(forKey: "StartTime") ?? ""
                let cumulativeTime = findDateDiff(time1Str: timerStartTime as! String, time2Str: APPUtility.getPresentDate())
                self.completeButton.isHidden = false
                self.stopTimeUpdateToServer(stopTime: APPUtility.getPresentDate(), cumulate_time: cumulativeTime)
                
                
            } else {
                //////// START
                self.buttonOne.setTitle("Stop", for: .normal)
                self.buttonOne.backgroundColor = UIColor.appRedColor
                //self.actionStack.removeArrangedSubview(self.buttonTwo)
                // self.actionStack.addArrangedSubview(self.timerView)
                self.watchObject.startTimer(fromDate: Date(), toDate: nil)
                self.startTime = APPUtility.getPresentDate()
                UserDefaults.standard.set(self.startTime, forKey: "StartTime")
                self.completeButton.isHidden = true
                self.startTimeUpdateToServer(startTime: APPUtility.getPresentDate())
            }
            
        default:
            break
        }
    }
}

extension TaskDetailViewController {
    
    @objc func taskStatusUpdate() {
        if let model = viewModel {
            if let idIs = selectedTasK?.task_id {
                let parms = ["task_id": idIs,
                             "user_id": APPStore.sharedStore.user.userId!,
                             "project_id": selectedProjectId ?? 0] as [String : Any]
                self.showHUD(title: "")
                print(parms)
                model.taskStatusUpdate(parameters: parms, onSuccess: { [weak self] (dataSource) in
                    self?.dismiss(animated: true, completion: nil)
                    self?.hideHud()
                    }, onError: { (error) in
                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
                        //   self.showAlert(title: Message.Alert, message: error.statusMessage)
                        self.hideHud()
                })
            }
            
        }
    }
    
    func startTimeUpdateToServer(startTime: String)
    {
        if let model = viewModel {
            if let idIs = selectedTasK?.task_id {
                let parms = ["task_id": idIs] as [String : Any]
                print(parms)
                self.showHUD(title: "")
                
                model.timeUpdate(parameters: parms, onSuccess: { [weak self] (dataSource) in
                    print("Print Success")
                    // self?.setTimeLogs()
                    //self?.taskDetailTable.reloadData()
                    // self?.taskLog()
                    self?.hideHud()
                    }, onError: { (error) in
                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
                        //   self.showAlert(title: Message.Alert, message: error.statusMessage)
                        self.hideHud()
                })
            }
        }
    }
    
    func stopTimeUpdateToServer(stopTime: String, cumulate_time: String)
    {
        if let model = viewModel {
            if let idIs = selectedTasK?.task_id {
                let parms = ["task_id": idIs] as [String : Any]
                print(parms)
                self.showHUD(title: "")
                
                model.stopTimeUpdate(parameters: parms, onSuccess: { [weak self] (dataSource) in
                    print("Print Success")
                    self?.setTimeLogs()
                    self?.taskDetailTable.reloadData()
                    self?.taskLog()
                    self?.hideHud()
                    }, onError: { (error) in
                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
                        //  self.showAlert(title: Message.Alert, message: error.statusMessage)
                        self.hideHud()
                })
            }
        }
    }
    
    func taskLog()
    {
        if let model = viewModel {
            
            if let idIs = selectedTasK?.task_id {
                let parms = ["task_id": idIs,
                             "uid": APPStore.sharedStore.user.userId!,
                             "pid": selectedProjectId ?? 0] as [String : Any]
                //print(parms)
                self.showHUD(title: "")
                model.updateTaskLog(parameters: parms, onSuccess: { [weak self] (dataSource) in
                    print("Print Success")
                    
                    if dataSource != nil{
                        if (dataSource?.task_status.count)! > 0{
                            self?.startTimeArr = (dataSource?.start_time)! as! [String]
                            self?.endTimeArr = (dataSource?.end_time)! as! [String]
                            self?.cumilativeTimeArr = (dataSource?.cumulate_time)!
                            self?.startDateArr = (dataSource?.created_on)! as! [String]
                            if dataSource!.task_status.first == "updated"{
                                self?.timerStarted = false
                                self?.watchObject.stopTimer()
                                //self?.taskLog()
                                self?.completeButton.isHidden = false
                                self?.buttonOne.setTitle("Start", for: .normal)
                                self?.buttonOne.backgroundColor = UIColor.appBlueColor
                                // self.actionStack.addArrangedSubview(self.buttonTwo)
                                // self?.actionStack.removeArrangedSubview((self?.timerView)!)
                            }
                            else
                            {
                                self?.completeButton.isHidden = true
                                self?.timerStarted = true
                                self?.buttonOne.setTitle("Stop", for: .normal)
                                self?.buttonOne.backgroundColor = UIColor.appRedColor
                                //self.actionStack.removeArrangedSubview(self.buttonTwo)
                                //self?.actionStack.addArrangedSubview((self?.timerView)!)
                                self?.watchObject.startTimer(fromDate: Date(), toDate: nil)
                            }
                        }
                        else
                        {
                            self?.completeButton.isHidden = true
                            
                        }
                    }
                    else
                    {
                        self?.completeButton.isHidden = true
                        
                    }
                    //  self?.buttonOneClicked((Any).self)
                    self?.taskDetailTable.reloadData()
                    self?.hideHud()
                    }, onError: { (error) in
                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
                        // self.showAlert(title: Message.Alert, message: error.statusMessage)
                        self.hideHud()
                })
            }
        }
    }
}

extension TaskDetailViewController {
    
    func findDateDiff(time1Str: String, time2Str: String) -> String {
        let timeformatter = DateFormatter()
        timeformatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        guard let time1 = timeformatter.date(from: time1Str),
            let time2 = timeformatter.date(from: time2Str) else { return "" }
        
        //You can directly use from here if you have two dates
        let interval = time2.timeIntervalSince(time1)
        //        let hour = interval / 3600;
        //        let minute = interval.truncatingRemainder(dividingBy: 3600) / 60
        //        let seconds = interval.truncatingRemainder(dividingBy: 3600) / 60 / 60
        
        let intervalInt = Int(interval)
        return "\(intervalInt)"
    }
    
    func createBackButton() {
        
    }
    
    
}

extension TaskDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.taskDetailTable.delegate = self
        self.taskDetailTable.dataSource = self
        self.taskDetailTable.estimatedRowHeight = 300
        self.taskDetailTable.rowHeight = UITableViewAutomaticDimension
        self.taskDetailTable.tableFooterView = UIView()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Task Logs"
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 1:
            if timeLogArray?.count == 0 {
                return 38
            } else {
                return 38
            }
        default:
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return titleArray.count
        case 1:
            return endTimeArr.count
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            switch indexPath.row {
                //            case 3:
                //                let cell = tableView.dequeueReusableCell(withIdentifier:
                //                    CellIdentifiers.TaskDetailAsigneeCellIdentifier, for: indexPath)
                //                if let cellIs = cell as? TaskDetailAsigneeCell {
                //                    cellIs.selectionStyle = .none
                //                    cellIs.delegate = self
                //                    if let task = self.selectedTasK {
                //                        cellIs.setData(data: task)
                //                    }
                //                }
            //                return cell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier:
                    CellIdentifiers.TaskDetailCellIdentifier, for: indexPath)
                if let cellIs = cell as? TaskDetailCell {
                    cellIs.selectionStyle = .none
                    
                    var description = ""
                    if let task = self.selectedTasK {
                        switch indexPath.row {
                        case 0:
                            description = task.tname ?? ""
                        case 1:
                            description = task.tsdate ?? ""
                        case 2:
                            description = task.tedate ?? ""
                        case 3:
                            if (task.accepted_state == "0") {
                                description = "Cancelled"
                                self.completeButton.isHidden = true
                                
                            } else if (task.accepted_state == "1") {
                                description = "In Progress"
                                //self.completeButton.isHidden = false
                            } else if (task.accepted_state == "2") {
                                description = "Completed"
                                self.completeButton.isHidden = true
                            }
                        case 4:
                            description = task.tdetails ?? ""
                        default:
                            break
                        }
                        cellIs.setData(title: titleArray[indexPath.row], taskDesc: description)
                    }
                }
                return cell
            }
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.TaskDetailLogCellIdentifier, for: indexPath)
            if let cellIs = cell as? TaskDetailLogCell {
                cellIs.selectionStyle = .none
                if  startTimeArr[indexPath.row] != ""{
                    let startTime = APPUtility.convertUnixToTime(unix: Double(startTimeArr[indexPath.row])!)
                    cellIs.dateLabel.text = startDateArr[indexPath.row]
                    cellIs.startTimeLabel.text = startTime
                    
                }
                if  endTimeArr[indexPath.row] != ""{
                    let endTime = APPUtility.convertUnixToTime(unix: Double(endTimeArr[indexPath.row])!)
                    cellIs.endTimeLabel.text = endTime
                    
                }
                print(cumilativeTimeArr)
                if cumilativeTimeArr[indexPath.row] != ""{
                    cellIs.cumulativeTimeLabel.text = minutesToHoursMinutes(minutes: Int(cumilativeTimeArr[indexPath.row])!)
                }
                if APPStore.sharedStore.appUserID == "2"{
                    cellIs.uploadScreens.isHidden = true
                }
                else
                {
                    cellIs.uploadScreens.isHidden = false
                }
            }
            return cell
        default:
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

extension TaskDetailViewController: ProjectNewTaskVCDelegate, TaskDetailAsigneeCellDelegate {
    func profileSelected(id: Int64) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactProfileVC") as? ContactProfileVC {
            controller.setMemberId(id: id)
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func reloadRows() {
        /////
    }
    
    func reloadUpdatedRow(row: Int) {
        self.delegate?.reloadChangedUpdatedRow(row: row)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
}

extension TaskDetailViewController: StopWatchDelegate {
    
    func currentTimerUpdated(days: String, hours: String, minutes: String, seconds: String, milliseconds: String) {
        if days != "0" {
            self.dayLabel.text = "\(days)d"
        } else {
            //self.timerStack.removeArrangedSubview(self.dayLabel)
        }
        if hours != "0"  {
            //  self.hourLabel.text = " " + "\(hours)h"
        }else {
            // self.timerStack.removeArrangedSubview(self.hourLabel)
        }
        //     self.minuteLabel.text = " " + "\(minutes) m"
        //     self.secondsLabel.text = " " + "\(seconds) s"
        //     self.milliSecondsLabel.text = " " + "\(milliseconds)"
    }
    
    func timerHasStarted() {
        print("Timer Started")
        self.timerStarted = true
    }
    
    func timerHasStopped() {
        print("Timer Stopped")
        self.timerStarted = false
        
    }
    func minutesToHoursMinutes (minutes : Int) -> String {
        
        let hours = minutes / 60
        let minutes = (minutes % 60)
        //let second = (seconds % 3600) % 60
        
        return String(format: "%02d:%02d", hours, minutes)
    }
}
