//
//  NewSubTaskDataCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/05/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class NewSubTaskDataCell: UITableViewCell {

    @IBOutlet weak var subTaskCollection: UICollectionView!
    @IBOutlet weak var subTaskSegmentControl: AppSegmentControl!
    @IBOutlet weak var subTaskName: AppRoundBorderTextField!
    var numberOfItems: Int?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setCollectionView()
    }
    
    
    func setData(data: [String: Any]) {
        
        numberOfItems = data["count"] as? Int
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension NewSubTaskDataCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func setCollectionView() {
        self.subTaskCollection.dataSource = self
        self.subTaskCollection.delegate = self
        ///self.subTaskCollection.semanticContentAttribute = .forceRightToLeft
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfItems ?? 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        print(UIScreen.main.bounds.width)
        return CGSize(width: 40, height: 40)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.NewSubTaskAsigneeCellIdentifier, for: indexPath)
        cell.tag = indexPath.row
        if let cellIs = cell as? NewSubTaskAsigneeCell {
            cellIs.setData(data: nil)
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}
