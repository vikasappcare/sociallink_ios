//
//  PaymentViewController.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 04/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class PaymentViewController: UIViewController {

    @IBOutlet weak var paymentTableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "PAYMENT"

        setTableViewDatasource()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension PaymentViewController: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.paymentTableview.delegate = self
        self.paymentTableview.dataSource = self
        self.paymentTableview.estimatedRowHeight = 300
        self.paymentTableview.rowHeight = UITableViewAutomaticDimension
        self.paymentTableview.tableFooterView = UIView()
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Select Payment Method"
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        view.tintColor = UIColor.appLightGrayColor
        
        if let header: UITableViewHeaderFooterView = view as? UITableViewHeaderFooterView {
            header.textLabel?.textColor = UIColor.lightGray
            header.textLabel?.setSemiBoldFont(size: 20)
        }
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 1:
            return 80
        default:
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        switch section {
        case 0:
            return 1
        case 1:
            return 2
        case 2:
            return 1
        case 3:
            return 1
        default:
            return 0
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.PlanTableViewCellIdentifier, for: indexPath)
            if let cellIs = cell as? PlanTableViewCell {
                cellIs.selectionStyle = .none
            }
            cell.selectionStyle = .none
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.PaymentMethodCellIdentifier, for: indexPath)
            if let cellIs = cell as? PaymentMethodCell {
                cellIs.selectionStyle = .none
            }
            cell.selectionStyle = .none
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.AddPaymentCellIdentifier, for: indexPath)
            if let cellIs = cell as? AddPaymentCell {
                cellIs.selectionStyle = .none
            }
            cell.selectionStyle = .none
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.SelectPaymentCellIdentifier, for: indexPath)
            if let cellIs = cell as? SelectPaymentCell {
                cellIs.selectionStyle = .none
                cellIs.delegate = self
            }
            cell.selectionStyle = .none
            return cell
        default:
            return UITableViewCell()
        }
        
        
    }
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //
    //        return UIScreen.main.bounds.height
    //    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 2:
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddPaymentVC") as? AddPaymentVC {
                /// let navController = APPNavigationController(rootViewController: controller)
                self.navigationController?.pushViewController(controller, animated: true)
            }
        default:
            break
        }
        
    }
}

extension PaymentViewController: SelectPaymentCellDelegate {
    func selectPayment() {
        self.navigationController?.popToRootViewController(animated: true)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue:
            APPNotifications.dismissRegistrationView),
                                        object: nil, userInfo: nil)
        
    }
    
    
    
    
}
