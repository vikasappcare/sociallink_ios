//
//  ProjectUnpaidCell.swift
//  SocialLink
//
//  Created by Santhosh on 22/04/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ProjectUnpaidCell: UITableViewCell {

    @IBOutlet weak var payNow: UIButton!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var invoiceId: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
