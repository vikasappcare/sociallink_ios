//
//  ProjectContactsModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 31/08/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON

class ProjectContactsModel: AppBaseModel {
    
    var service: String?
    var list: [ProjectContact]?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    
    private func parseResponse(with response: JSON) {
        self.service = response["service"].stringValue
        for item in response.arrayValue {
            if let feedModel = self.createObject(with: item) {
                if self.list == nil {
                    self.list = [ProjectContact]()
                }
                self.list?.append(feedModel)
            }
        }
    }
    
    private func createObject(with jsonObject: JSON) -> ProjectContact? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return ProjectContact.init(jsonObject)
        }
    }
    
}


class ProjectContact: AppBaseModel {
    var member_name: String?
    var designation: String?
    var email_id: String?
    var user_id: Int64?
    var m_id: Int64?
    var phoneNumber: String?
    var member_type: Int64?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        self.member_name = response["mname"].stringValue
        self.designation = response["design"].stringValue
        self.email_id = response["emailid"].stringValue
        self.user_id = response["user_id"].int64Value
        self.phoneNumber = response["phone"].stringValue
        self.member_type = response["m_type"].int64Value
        self.m_id = response["mid"].int64Value
    }
    
    
}
