//
//  NewProjectVC.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit
import MobileCoreServices

protocol NewProjectVCDelegate: class {
    
    func newProjectAdded(project: Project)
}
var skillsDic = [String: String]()

class NewProjectVC: AppBaseVC {
    
    @IBOutlet weak var buttonStack: UIStackView!
    @IBOutlet weak var buttonTwo: UIButton!
    @IBOutlet weak var buttonOne: UIButton!
    @IBOutlet weak var statusViewHeight: NSLayoutConstraint!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var segmentHeight: NSLayoutConstraint!
    weak var delegate: NewProjectVCDelegate?
    @IBOutlet weak var newProjectTableview: UITableView!
    @IBOutlet weak var projectSegment: UISegmentedControl!
    var showCrossButton: Bool?
    var showAgreement: Bool?
    var viewModel: NewProjectViewModel?
    var projectType: Int?
    var selectedDate: Date?
    var selectedProject: Project?
    var type: String?
    var statusValue: Int?
    //File Upload
    var uploadModel: FileUploadViewModel?
    var fileUploadData: Data?
    var fileExtension: String?
    var fileName: String?
    var proType: String?
    var status: String?
    ////////////////
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = NewProjectViewModel()
        uploadModel = FileUploadViewModel()
        viewModel?.selectedEditProject = selectedProject
        self.setTableViewDatasource()
        UISegmentedControl.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for: .selected)
        let attr = NSDictionary(object: UIFont.boldFont(16), forKey: NSAttributedStringKey.font as NSCopying)
        UISegmentedControl.appearance().setTitleTextAttributes(attr as [NSObject: AnyObject], for: .normal)
        
        //if showCrossButton ?? false {
        self.createNavigationItems()
        // }
        //callServiceForSkills()
        
        self.validateProject()
        
        if showAgreement ?? false {
            //            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ServiceAgreement") as? ServiceAgreement {
            //                controller.showAccept = true
            //                let navigationController = APPNavigationController.init(rootViewController: controller)
            //                self.present(navigationController, animated: true, completion: nil)
            //            }
            
            //            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ServiceAgreement") as? ServiceAgreement {
            //                controller.showAcceptInVC(bool: true)
            //                   let navigationController = APPNavigationController.init(rootViewController: controller)
            //                self.present(navigationController, animated: true, completion: nil)
            //            }
        }
        
        switch proType {
        case PROTYPE.NEW:
            self.navigationItem.title = "New Project"
        case PROTYPE.EDIT:
            self.navigationItem.title = "Edit Project"
            
        default:
            break
        }
        
    }
    func setProType(type: String) {
        proType = type
    }
    func setProject(project: Project) {
        self.selectedProject = project
        
    }
    override func viewWillAppear(_ animated: Bool) {
        viewModel = NewProjectViewModel()
        uploadModel = FileUploadViewModel()
        viewModel?.selectedEditProject = selectedProject
        if selectedProject != nil{
            if selectedProject?.project_Type != nil
            {
                let segmentIndex = NSNumber(value: selectedProject!.project_Type!).intValue
                projectSegment.selectedSegmentIndex = segmentIndex
                projectType = segmentIndex
                status = "\(selectedProject!.statusIs!)"
                print(status)
            }
        }
        
        self.setTableViewDatasource()
        UISegmentedControl.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for: .selected)
        let attr = NSDictionary(object: UIFont.boldFont(16), forKey: NSAttributedStringKey.font as NSCopying)
        UISegmentedControl.appearance().setTitleTextAttributes(attr as [NSObject: AnyObject], for: .normal)
        
        //if showCrossButton ?? false {
        self.createNavigationItems()
        // }
        //callServiceForSkills()
        
        self.validateProject()
        
    }
    
    //    func skillsTextFieldDataLoading()
    //    {
    //        bankNameTextField.optionArray = banksArray
    //        bankNameTextField.didSelect{(selectedText , index ,id) in
    //            self.bankNameTextField.text = selectedText
    //
    //        }
    //    }
    func validateProject() {
        if selectedProject != nil {
            type = "Edit"
            self.navigationItem.title = "EDIT PROJECT"
            self.segmentHeight.constant = 50
            self.projectSegment.isHidden = false
            // self.statusViewHeight.constant = 60
            //            switch selectedProject?.statusIs {
            //            case 0:
            //                self.buttonStack.removeArrangedSubview(buttonTwo)
            //                self.buttonOne.setTitle("Terminate", for: .normal)
            //                self.buttonOne.backgroundColor = UIColor.appRedColor
            //            case 1:
            //                self.buttonOne.setTitle("Complete", for: .normal)
            //                self.buttonOne.backgroundColor = UIColor.appGreenColor
            //                self.buttonTwo.setTitle("Cancel", for: .normal)
            //                self.buttonTwo.backgroundColor = UIColor.appRedColor
            //            default:
            //                self.statusViewHeight.constant = 0
            //                break
            //}
        } else {
            type = "New"
            self.navigationItem.title = "NEW PROJECT"
            self.segmentHeight.constant = 50
            self.projectSegment.isHidden = false
            //self.statusViewHeight.constant = 0
        }
    }
    
    @IBAction func projectSegmentSelected(_ sender: Any) {
        if projectSegment.selectedSegmentIndex == 0 {
            projectType = 0
        } else {
            projectType = 1
        }
        
        newProjectTableview.reloadData()
    }
    
    func showServiceAgreement(show: Bool) {
        showAgreement = show
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showClose(bool: Bool?) {
        showCrossButton = bool
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    @IBAction func buttonOneClicked(_ sender: Any) {
        switch selectedProject?.statusIs {
        case 0:
            let alertController = UIAlertController(title: "Alert", message: "Sure you want to Terminate the Project?", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                self.statusValue = 3
            }))
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        case 1:
            let alertController = UIAlertController(title: "Alert", message: "Sure you want to Complete the Project?", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                self.statusValue = 2
            }))
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        default:
            break
        }
        
    }
    @IBAction func buttonTwoClicked(_ sender: Any) {
        let alertController = UIAlertController(title: "Alert", message: "Sure you want to Cancel the Project?", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            self.statusValue = 3
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
}

extension NewProjectVC {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue:
            APPNotifications.dismissAddView),
                                        object: nil, userInfo: nil)
    }
}

extension NewProjectVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.newProjectTableview.delegate = self
        self.newProjectTableview.dataSource = self
        self.newProjectTableview.estimatedRowHeight = 300
        self.newProjectTableview.rowHeight = UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:
            CellIdentifiers.NewProjectCellIdentifier, for: indexPath)
        if let cellIs = cell as? NewProjectCell {
            cellIs.selectionStyle = .none
            cellIs.delegate = self
            let skillsArray = Array(skillsDic.values)
            cellIs.services.optionArray = skillsArray
            
            if type == "Edit" {
                if let project = self.selectedProject {
                    cellIs.setData(data: project)
                    
                }
            }
        }
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension NewProjectVC: NewProjectCellDelegate {
    func chooseClicked() {
    }
    
    func showServices() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "RolesViewController") as? RolesViewController {
            controller.delegate = self
            controller.setServiceType(type: ScreenType.SERVICES)
            let navController = APPNavigationController(rootViewController: controller)
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    func showDateAlert() {
        self.showAlert(title: "Alert!", message: "End Date should not be Smaller than Start Date")
    }
    
    func clickedStartDate() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProjectCalenderVC") as? ProjectCalenderVC {
            controller.delegate = self
            controller.controllerTag = 1
            let navController = APPNavigationController(rootViewController: controller)
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    func clickedEndDate() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProjectCalenderVC") as? ProjectCalenderVC {
            controller.delegate = self
            controller.controllerTag = 2
            let navController = APPNavigationController(rootViewController: controller)
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    func showAlert() {
        self.showAlert(title: "Alert", message: "Please Enter Values to Continue")
    }
    func submitClicked(data: [String : Any]) {
        if selectedProject != nil {
            self.updateProject(data: data)
        } else {
            self.addNewProject(data: data)
            
        }
    }
    
    func addNewProject(data: [String: Any]) {
        var dataToSend = data
        dataToSend["user_id"] = APPStore.sharedStore.user.userId ?? 0
        dataToSend["ptype"] = projectSegment.selectedSegmentIndex
        dataToSend["pstatus"] = "0"
       print(projectSegment.selectedSegmentIndex)
        self.updateProjectFile(param: dataToSend)
       
    }
    
    func updateProject(data: [String: Any]) {
        var dataToSend = data
        if let project = self.selectedProject {
            dataToSend["pid"] = project.project_id
            dataToSend["user_id"] = APPStore.sharedStore.user.userId
            dataToSend["user_type"] = 1
            dataToSend["project_type"] = projectType
            dataToSend["pstatus"] = status
            
            print(dataToSend)
            if let model = viewModel {
                self.showHUD(title: "Loading...")
                model.editProject(parameters: dataToSend, onSuccess: { [weak self] (object) in
                    dprint(object: "Success")
                   
                    }, onError: { (error) in
                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
                        self.dismiss(animated: true, completion: nil)
                        self.hideHud()
                })
            }
        }
    }
}

extension NewProjectVC: ProjectCalenderVCDelegate {
    func dateSelected(date: String, tag: Int, dateIs: Date) {
        print("Selected Date : \(date)")
        print(tag)
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = newProjectTableview.cellForRow(at: indexPath)
        switch tag {
        case 1:
            if let cellIs = cell as? NewProjectCell {
                cellIs.setStartDate(date: date)
            }
        case 2:
            if let cellIs = cell as? NewProjectCell {
                cellIs.setEndDate(date: date)
            }
        default:
            break
        }
    }
}

extension NewProjectVC: RolesViewControllerDelegate {
    func serviceSelectted(service: Service) {
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = self.newProjectTableview.cellForRow(at: indexPath)
        if let cellIs = cell as? NewProjectCell {
            cellIs.setService(data: service.service_name ?? "")
        }
    }
    
    func roleSelectted(role: Role) {
        
    }
}

extension NewProjectVC: NewTaskUploadCellDelegate, UIDocumentMenuDelegate,UIDocumentPickerDelegate {
    
    func updateProjectFile(param: [String: Any]) {
        if let model = uploadModel {
            self.showHUD(title: "")
            if let data = self.fileUploadData {
                var uploadData = param
                var endURL = ""
                uploadData["pupload"] = self.fileName ?? ""
                if fileExtension == "PNG" || fileExtension == "jpg" || fileExtension == "JPG"
                {
                    uploadData["file_extension"] = "jpeg"
                    
                }else
                {
                    uploadData["file_extension"] = fileExtension
                }
                if type == "Edit"{
                    uploadData["pid"] = selectedProject?.project_id
                    endURL = APPURL.editProject
                }
                else{
                    endURL = APPURL.addProject
                }
                print(uploadData)
                model.requestWith(imageData: data, parameters: uploadData, endURL:endURL, onCompletion: { [weak self] (dataSource) in
                    self?.hideHud()
                    self?.dismiss(animated: true, completion: nil)

                    }, onError: { (error) in
                        // self.showAlert(title: "Alert", message: error?.localizedDescription ?? "")//
                        self.dismiss(animated: true, completion: nil)
                        self.hideHud()
                })
            }
                
            else
            {
                if let model = viewModel {
                    print(param)
                    model.addProject(parameters: param, onSuccess: { [weak self] (object) in
                        dprint(object: "Success")
                        self?.dismiss(animated: true, completion: nil)

                        }, onError: { (error) in
                            Logger.log(message: "Error \(error.statusMessage)", event: .error)
                            self.dismiss(animated: true, completion: nil)
                            self.hideHud()
                    })
                }
            }
        }
    }
    
    func chooseFileClicked() {
        let types = [kUTTypePDF, kUTTypeText, kUTTypeRTF, kUTTypeSpreadsheet, kUTTypePNG, kUTTypeJPEG]
        let documentPicker: UIDocumentPickerViewController = UIDocumentPickerViewController(documentTypes: types as [String], in: UIDocumentPickerMode.import)
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        self.present(documentPicker, animated: true, completion: nil)
    }
    
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let myURL = url as URL
        print("import result : \(myURL)")
        print(myURL.pathExtension)
        let fileData = try! Data(contentsOf: myURL)
        self.fileUploadData = fileData
        self.fileExtension = myURL.pathExtension
        if viewModel != nil {
            let fileNameIs = APPUtility.getFileName(path: myURL.absoluteString)
            self.fileName = fileNameIs
            let indexPath = IndexPath(row: 0, section: 0)
            let cell = self.newProjectTableview.cellForRow(at: indexPath)
            if let cellIs = cell as? NewProjectCell {
                cellIs.setFileName(name: fileNameIs)
            }
        }
    }
    
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        //dismiss(animated: true, completion: nil)
    }
}
extension Dictionary where Value: Equatable {
    func someKey(forValue val: Value) -> Key? {
        return first(where: { $1 == val })?.key
    }
}
