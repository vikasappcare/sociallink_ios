//
//  ServiceAgreement.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 11/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ServiceAgreement: UIViewController {

    @IBOutlet weak var serviceAgreementTable: UITableView!
    var showAccept: Bool?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setTableViewDatasource()
        
        if showAccept ?? false {
            self.navigationItem.title = "SERVICE AGREEMENT"
        } else {
            self.navigationItem.title = "TERMS & CONDITIONS"
        }

        // Do any additional setup after loading the view.
    }
    
    func showAcceptInVC(bool: Bool?) {
        showAccept = bool
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if  self.navigationController?.navigationBar.backItem == nil {
            self.createNavigationItems()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ServiceAgreement {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
}

extension ServiceAgreement: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.serviceAgreementTable.delegate = self
        self.serviceAgreementTable.dataSource = self
        self.serviceAgreementTable.estimatedRowHeight = 300
        self.serviceAgreementTable.rowHeight = UITableViewAutomaticDimension
        self.serviceAgreementTable.tableFooterView = UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if showAccept ?? false {
           return 3
        } else {
             return 1
        }
       
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.ServiceAgreementCellIdentifier, for: indexPath)
            if let cellIs = cell as? ServiceAgreementCell {
                cellIs.selectionStyle = .none
            }
            cell.selectionStyle = .none
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.ServiceTermsCellIdentifier, for: indexPath)
            if let cellIs = cell as? ServiceTermsCell {
                cellIs.selectionStyle = .none
            }
            cell.selectionStyle = .none
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.ServiceAcceptCellIdentifier, for: indexPath)
            if let cellIs = cell as? ServiceAcceptCell {
                cellIs.selectionStyle = .none
            }
            cell.selectionStyle = .none
            return cell
        default:
            return UITableViewCell()
        }

    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
