//
//  LandingPageViewController.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 11/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import CoreLocation
import CoreData

var mapDataResponse: [String: Any]?

class LandingPageViewController: AppBaseVC {
    var viewModel: CreativesNearbyViewModel?
    
    // var viewModel: CreativesNearbyViewModel?
    var skillsViewModel: RegisterViewModel?
    
    @IBOutlet weak var landingTable: UITableView!
    var servicesArray = [String]()
    var servicesID = [String]()
    var servicesImage = [String]()
    
    let servicesImageArray = ["CreativesNeraby_Graphic", "CreativesNeraby_SEO", "CreativesNeraby_Web", "CreativesNeraby_Social", "CreativesNeraby_Content"]
    
    var mapData: [String: Any]?
    var latitudeArr = [String]()
    var longitudeArr = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapData = ["lat": LocationSingleton.sharedInstance.currentLoc?.coordinate.latitude ?? 0.0,
                   "long": LocationSingleton.sharedInstance.currentLoc?.coordinate.longitude ?? 0.0,
                   "address": LocationSingleton.sharedInstance.currentAddress ?? ""]
        skillsViewModel = RegisterViewModel()
        
        callServiceForSkills()
        viewModel = CreativesNearbyViewModel()
        
        //        mapDataResponse = ["lat":[36.179282, 36.179200, 35.784195, 36.179302], "lng" : [-86.730911, -86.730911, -86.489128, -86.730911]]
        //  self.createNavigationItems()
        self.setTableViewDatasource()
        
        self.navigationItem.title = "CREATIVES NEARBY"
        // Do any additional setup after loading the view.
        //        viewModel = CreativesNearbyViewModel()
        
        if mapData!["lat"] as? Double == 0 {
            
        }
    }
    func callServiceForSkills()
    {
        if let model = skillsViewModel {
            
            let param = ["": ""]
            self.showHUD(title: "Loading...")
            model.skillsList(parameters: param as [String : Any], onSuccess: { [weak self] (dataSource) in
                self?.hideHud()
                self?.servicesArray = (dataSource?.sName)!
                self?.servicesID = (dataSource?.sId)!
                self?.servicesImage = (dataSource?.mapImage)!
                self?.landingTable.reloadData()
                },  onError: { (error) in
                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
                    // self.showAlert(title: Message.Alert, message: error.statusMessage)
                    self.hideHud()
            })
            
        }
    }
    func callApiForCreativesNearby(skill: String)
    {
        if let model = viewModel {
            
            let latitude = mapData?["lat"]
            let longitude = mapData?["long"]
            let requiredSkills = Int(skill)
            
            let param = ["user_id": APPStore.sharedStore.user.userId ?? 0, "lat":latitude ?? 0.0, "lng":longitude ?? 0.0, "skills":requiredSkills] as [String : Any]
            print("This is param:",param)
            model.creativesNearby(parameters: param as [String : Any], onSuccess: { [weak self] (dataSource) in
                
                let creativesData: [String: Any] =  ["lat": dataSource?.lat as Any, "lng": dataSource?.lng as Any, "ufullname": dataSource?.ufullname ?? "", "address": dataSource?.address ?? "","distance": dataSource?.distance ?? "", "uskills": dataSource?.uskills ?? "", "map_img": dataSource?.map_img ?? "","uid": dataSource?.uid as Any,"uphone": dataSource?.uphone  ?? "","uemailid": dataSource?.uemailid  ?? "","required_skills": dataSource?.required_skills  ?? ""]
                //if dataSource?.uid.count
                // print(dataSource?.uid.count)
                if dataSource?.uid != nil{
                    mapDataResponse = creativesData
                }
                else
                {
                    mapDataResponse = nil
                }
                self?.landingTable.reloadData()
                self?.scrollToFirstRow()
                
                },  onError: { (error) in
                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
                    // self.showAlert(title: Message.Alert, message: error.statusMessage)
            })
        }
        else
        {
            self.showAlert(title: Message.Alert, message: "Entered details are not valid")
            
        }
    }
    
    //    func callService(){
    //        var requiredSkills = ""
    //        let users = APPDBUtility. fgetUserData(mocType: .main)
    //        if users?.count ?? 0 > 0 {
    //            if let user = users {
    //                APPStore.sharedStore.user = APPUtility.convertDBUserToUser(dbUser: user[0])
    //                requiredSkills = user[0].requiredSkills!
    //            }
    //        }
    //         if let model = viewModel {
    //
    //            let latitude = "36.179302"
    //            let longitude =  "-86.730911"
    //
    //            let param = ["lat": latitude, "long": longitude, "skills": requiredSkills] as [String : Any]
    //            self.showHUD(title: "Loading...")
    //            model.creativesNearby(parameters: param as [String : Any], onSuccess: { [weak self] (dataSource) in
    //                self?.hideHud()
    //
    //                self!.mapDataResponse = ["lat": dataSource?.lat as Any, "lng": dataSource?.lng as Any]
    //
    //
    //                },  onError: { (error) in
    //                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
    //                    self.showAlert(title: Message.Alert, message: error.statusMessage)
    //                    self.hideHud()
    //            })
    //        }
    //    else
    //         {
    //            self.showAlert(title: Message.Alert, message: "Entered details are not valid")
    //
    //        }
    //
    //}
}


extension LandingPageViewController {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue:
            APPNotifications.dismissAddView),
                                        object: nil, userInfo: nil)
    }
}

extension LandingPageViewController: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.landingTable.delegate = self
        self.landingTable.dataSource = self
        self.landingTable.estimatedRowHeight = 300
        self.landingTable.rowHeight = UITableViewAutomaticDimension
        self.landingTable.tableFooterView = UIView()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return 1
        case 1:
            return 1
        case 2:
            return 1
        case 3:
            return servicesArray.count
        case 4:
            return 1
        default:
            return 0
        }
        
    }
    func scrollToFirstRow() {
        let indexPath = IndexPath(row: 0, section: 0)
        self.landingTable.scrollToRow(at: indexPath, at: .top, animated: true)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.MapViewTableCellIdentifier, for: indexPath)
            if let cellIs = cell as? MapViewTableCell {
                cellIs.selectionStyle = .none
                cellIs.delegate = self
                if mapDataResponse == nil{
                    cellIs.setDataForNoData(data: mapData!)
                    print(mapData)
                    // self.showAlert(title: "Alert", message: "No creatives found with your required skill")
                }
                else
                {
                    cellIs.setData(data: mapDataResponse as! [String : Array<String>])
                    
                }
                
            }
            return cell
        case 1:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier:
                    CellIdentifiers.ChooseServiceCellIdentifier, for: indexPath)
                if let cellIs = cell as? ChooseServiceCell {
                    cellIs.selectionStyle = .none
                }
                cell.selectionStyle = .none
                return cell
                //            case 1:
                //                let cell = tableView.dequeueReusableCell(withIdentifier:
                //                    CellIdentifiers.DistanceTableViewCellIdentifier, for: indexPath)
                //                if let cellIs = cell as? DistanceTableViewCell {
                //                    cellIs.selectionStyle = .none
                //                }
                //                cell.selectionStyle = .none
            //                return cell
            default:
                return UITableViewCell()
            }
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.SearchContactCellIdentifier, for: indexPath)
            if let cellIs = cell as? SearchContactCell {
                cellIs.selectionStyle = .none
            }
            cell.selectionStyle = .none
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.ServiceTableViewCellIdentifier, for: indexPath)
            if let cellIs = cell as? ServiceTableViewCell {
                cellIs.selectionStyle = .none
                var showRoundCorner: Bool?
                if indexPath.row == servicesArray.count - 1 {
                    showRoundCorner = true
                } else {
                    showRoundCorner = false
                }
                
                let dataIs = ["name": servicesArray[indexPath.row],
                              "roundCorner": showRoundCorner ?? false, "image": servicesImage[indexPath.row]] as [String: Any]
                if mapDataResponse != nil{
                }
                cellIs.setData(data: dataIs )
                
            }
            cell.selectionStyle = .none
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.FindMemberCellIdentifier, for: indexPath)
            if let cellIs = cell as? FindMemberCell {
                cellIs.selectionStyle = .none
            }
            cell.selectionStyle = .none
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //return 547.0
        
        
        switch indexPath.section {
        case 0:
            return 406
        case 1:
            if indexPath.row == 0 {
                return 125
            } else {
                return 65
            }
        case 2:
            return 57
        case 3:
            return 70
        case 4:
            return 100
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 3:
            let requiredSkills = servicesID[indexPath.row]
            callApiForCreativesNearby(skill: requiredSkills)
            //            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ServiceProviderVC") as? ServiceProviderVC {
            //                /// let navController = APPNavigationController(rootViewController: controller)
            //                self.navigationController?.pushViewController(controller, animated: true)
        //}
        default:
            break
        }
    }
}

extension LandingPageViewController: MapViewTableCellDelegate {
    
    func markerClicked(details: [String]) {
        detailsArr = details
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if let controller = mainStoryboard.instantiateViewController(withIdentifier: "CreativeProfileController") as? CreativeProfileController {
            //let navController = APPNavigationController(rootViewController: controller)
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func locationSearchClicked() {
        let placePickerController = GMSAutocompleteViewController()
        placePickerController.delegate = self
        self.present(placePickerController, animated: true, completion: nil)
    }
}
extension LandingPageViewController: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error \(error)")
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        mapData = ["lat": place.coordinate.latitude,
                   "long": place.coordinate.longitude,
                   "address": place.formattedAddress ?? ""]
        landingTable.reloadSections(IndexSet(integer: 0), with: .automatic)
        self.dismiss(animated: true, completion: nil)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}


