//
//  AddingPaymentViewModel.swift
//  SocialLink
//
//  Created by admin on 3/6/19.
//  Copyright © 2019 Santhosh Marripelli. All rights reserved.
//

import Foundation

class AddingPaymentViewModel  {
    init() {
    }
    func banksList(parameters: [String: Any],
                    onSuccess: @escaping (ListOfBanksModel?) -> Void,
                    onError: @escaping APIErrorHandler) {
        AppDataManager.getBanksList(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: dataSource)
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    func addingBankDetails(parameters: [String: Any],
                      onSuccess: @escaping (SignUpModel?) -> Void,
                      onError: @escaping APIErrorHandler) {
        AppDataManager.bankDetails(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: dataSource)
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    func addingCardDetails(parameters: [String: Any],
                           onSuccess: @escaping (SignUpModel?) -> Void,
                           onError: @escaping APIErrorHandler) {
        AppDataManager.cardDetails(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: dataSource)
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
}
