//
//  ForgetPasswordCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit
protocol ForgetPasswordCellDelegate: class {
    func submitSelected()
    func goBack()
}
class ForgetPasswordCell: UITableViewCell {

     weak var delegate: ForgetPasswordCellDelegate?
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var emailText: AppTextField!
    @IBOutlet weak var goBackButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    func getData() -> String {
        return emailText.text ?? ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func goBackClicked(_ sender: Any) {
        self.delegate?.goBack()
    }
    @IBAction func submitClicked(_ sender: Any) {
        self.delegate?.submitSelected()
    }
    @IBAction func forgetUsernameOrPasswordClicked(_ sender: Any) {
    }
    @IBAction func createAccountClicked(_ sender: Any) {
    }
}
