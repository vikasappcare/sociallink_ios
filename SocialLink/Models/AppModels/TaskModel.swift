//
//  TaskModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 01/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON

class TaskModel: AppBaseModel {
    
    var service: String?
    var count: Int64?
    var list: [Task]?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    
    private func parseResponse(with response: JSON) {
        print("This is response from task Api:",response)
        self.service = response["service"].stringValue
        self.count = response["count"].int64Value
        for item in response.arrayValue {
            if let model = self.createObject(with: item) {
                if self.list == nil {
                    self.list = [Task]()
                }
                self.list?.append(model)
            }
        }
    }
    
    private func createObject(with jsonObject: JSON) -> Task? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return Task.init(jsonObject)
        }
    }
    
}
class ContractorTaskModel: AppBaseModel {
    
    var service: String?
    var count: Int64?
    var list: [ContractorTask]?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    
    private func parseResponse(with response: JSON) {
        print("This is response from task Api:",response)
        self.service = response["service"].stringValue
        self.count = response["count"].int64Value
        for item in response.arrayValue {
            if let model = self.createObject(with: item) {
                if self.list == nil {
                    self.list = [ContractorTask]()
                }
                self.list?.append(model)
            }
        }
    }
    
    private func createObject(with jsonObject: JSON) -> ContractorTask? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return ContractorTask.init(jsonObject)
        }
    }
    
}
class ContractorTask: AppBaseModel
{
    var pid: Int64?
    var user_id: Int64?
    var ptype: Int64?
    var pname: String?
    var pservice: String?
    var pscope: String?
    var psdate: String?
    var pskills: String?
    var ebudget: String?
    var etime: String?
    var pdeadline: String?
    var pupload: String?
    var pamount: String?
    var pstatus: Int64?
    var cdate: String?
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        self.pid = response["pid"].int64Value
        self.user_id = response["user_id"].int64Value
        self.ptype = response["ptype"].int64Value
        self.pname = response["pname"].stringValue
        self.pservice = response["pservice"].stringValue
        self.pscope = response["pscope"].stringValue
        self.psdate = response["psdate"].stringValue
        self.pskills = response["pskills"].stringValue
        self.ebudget = response["ebudget"].stringValue
        self.etime = response["etime"].stringValue
        self.pdeadline = response["pdeadline"].stringValue
        self.pupload = response["pupload"].stringValue
        self.pamount = response["pamount"].stringValue
        self.pstatus = response["pstatus"].int64Value
        self.cdate = response["cdate"].stringValue
    }
   
}
class Task: AppBaseModel {
    var log_id: Int64?
    var task_id: Int64?
    var project_id: Int64?
    var user_id: Int64?
    var start_time: String?
    var project_name: String?
    var end_time: String?
    var cumulate_time: String?
    var task_status: String?
    var tfile: String?
    var tassignto: String?
    var tcomment: String?
    var doc_id: String?
    var dname: String?
    var description: String?
    var dfile: String?
    var ufullname: String?
    var uphoto: String?
    var ptype: String?
    var pname: String?
    var pservice: String?
    var pscope: String?
    var psdate: String?
    var pskills: String?
    var ebudget: String?
    var etime: String?
    var pdeadline: String?
    var pupload: String?
    var pamount: String?
    var pstatus: String?
    var cdate: String?
    var tname: String?
    var tdetails: String?
    var tsdate: String?
    var tedate: String?
    var created_on: String?
    var task_state: Int64?
    var accepted_state: String?
    var timeLog: [TimeLog]?
    var created_by: CreatedBy?
    var assigned_to: CreatedBy?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {  
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        
        print("This is response from task Api from task:",response)
        
        self.task_id = response["task_id"].int64Value
        self.log_id = response["log_id"].int64Value
        self.project_id = response["project_id"].int64Value
        self.user_id = response["user_id"].int64Value
        self.start_time = response["start_time"].stringValue
        self.end_time = response["end_time"].stringValue
        self.cumulate_time = response["cumulate_time"].stringValue
        self.project_name = response["project_name"].stringValue
        self.task_status = response["task_status"].stringValue
        self.tfile = response["tfile"].stringValue
        self.tassignto = response["tassignto"].stringValue
        self.tcomment = response["tcomment"].stringValue
        self.doc_id = response["doc_id"].stringValue
        self.dname = response["dname"].stringValue
        self.description = response["description"].stringValue
        self.dfile = response["dfile"].stringValue
        self.ufullname = response["ufullname"].stringValue
        self.uphoto = response["uphoto"].stringValue
        self.ptype = response["ptype"].stringValue
        self.pname = response["pname"].stringValue
        self.pservice = response["pservice"].stringValue
        self.pscope = response["pscope"].stringValue
        self.psdate = response["psdate"].stringValue
        self.pskills = response["pskills"].stringValue
        self.ebudget = response["ebudget"].stringValue
        self.etime = response["etime"].stringValue
        self.pdeadline = response["pdeadline"].stringValue
        self.pupload = response["pupload"].stringValue
        self.pamount = response["pamount"].stringValue
        self.pstatus = response["pstatus"].stringValue
        self.cdate = response["cdate"].stringValue
        self.tname = response["tname"].stringValue
        self.tdetails = response["tdetails"].stringValue
        self.tsdate = response["tsdate"].stringValue
        self.tedate = response["tedate"].stringValue
        self.created_on = response["created_on"].stringValue
        self.task_status = response["task_status"].stringValue
        self.accepted_state = response["accepted_state"].stringValue
        
        for item in response["timeLog"].arrayValue {
            if let model = self.createTimeLogObject(with: item) {
                if self.timeLog == nil {
                    self.timeLog = [TimeLog]()
                }
                self.timeLog?.append(model)
            }
        }
        self.created_by = self.createObject(with: response["created_by"])
    }
    
    private func createObject(with jsonObject: JSON) -> CreatedBy? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return CreatedBy.init(jsonObject)
        }
    }
    
    private func createTimeLogObject(with jsonObject: JSON) -> TimeLog? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return TimeLog.init(jsonObject)
        }
    }
    
    
}

class TaskLog: AppBaseModel{
    var log_id = [Int64?]()
    var task_id = [Int64?]()
    var project_id = [Int64?]()
    var user_id = [Int64?]()
    var start_time = [String?]()
    var project_name = [String?]()
    var end_time = [String?]()
    var cumulate_time = [String]()
    var task_status = [String?]()
    var tfile = [String?]()
    var tassignto = [String?]()
    var tcomment = [String?]()
    var doc_id = [String?]()
    var dname = [String?]()
    var description = [String?]()
    var dfile = [String?]()
    var ufullname = [String?]()
    var uphoto = [String?]()
    var ptype = [String?]()
    var pname = [String?]()
    var pservice = [String?]()
    var pscope = [String?]()
    var psdate = [String?]()
    var pskills = [String?]()
    var ebudget = [String?]()
    var etime = [String?]()
    var pdeadline = [String?]()
    var pupload = [String?]()
    var pamount = [String?]()
    var pstatus = [String?]()
    var cdate = [String?]()
    var tname = [String?]()
    var tdetails = [String?]()
    var tsdate = [String?]()
    var tedate = [String?]()
    var created_on = [String?]()
    var task_state = [Int64?]()
    var accepted_state = [String?]()
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        print(response)
        for i in 0..<response.count{
            self.task_id.append(response[i]["task_id"].int64Value)
            self.log_id.append(response[i]["log_id"].int64Value)
            self.project_id.append(response[i]["project_id"].int64Value)
            self.user_id.append(response[i]["user_id"].int64Value)
            self.start_time.append(response[i]["start_time"].stringValue)
            self.end_time.append(response[i]["end_time"].stringValue)
            self.cumulate_time.append(response[i]["cumulate_time"].stringValue)
            self.project_name.append(response[i]["project_name"].stringValue)
            self.task_status.append(response[i]["task_status"].stringValue)
            self.tfile.append(response[i]["tfile"].stringValue)
            self.tassignto.append(response[i]["tassignto"].stringValue)
            self.tcomment.append(response[i]["tcomment"].stringValue)
            self.doc_id.append(response[i]["doc_id"].stringValue)
            self.dname.append(response[i]["dname"].stringValue)
            self.description.append(response[i]["description"].stringValue)
            self.dfile.append(response[i]["dfile"].stringValue)
            self.ufullname.append(response[i]["ufullname"].stringValue)
            self.uphoto.append(response[i]["uphoto"].stringValue)
            self.ptype.append(response[i]["ptype"].stringValue)
            self.pname.append(response[i]["pname"].stringValue)
            self.pservice.append(response[i]["pservice"].stringValue)
            self.pscope.append(response[i]["pscope"].stringValue)
            self.psdate.append(response[i]["psdate"].stringValue)
            self.pskills.append(response[i]["pskills"].stringValue)
            self.ebudget.append(response[i]["ebudget"].stringValue)
            self.etime.append(response[i]["etime"].stringValue)
            self.pdeadline.append(response[i]["pdeadline"].stringValue)
            self.pupload.append(response[i]["pupload"].stringValue)
            self.pamount.append(response[i]["pamount"].stringValue)
            self.pstatus.append(response[i]["pstatus"].stringValue)
            self.cdate.append(response[i]["cdate"].stringValue)
            self.tname.append(response[i]["tname"].stringValue)
            self.tdetails.append(response[i]["tdetails"].stringValue)
            self.tsdate.append(response[i]["tsdate"].stringValue)
            self.tedate.append(response[i]["tedate"].stringValue)
            self.created_on.append(response[i]["created_on"].stringValue)
            self.accepted_state.append(response[i]["accepted_state"].stringValue)
        }
        
    }
}

class TimeLog: AppBaseModel {
    
    var task_id: Int64?
    var start_time: String?
    var time_started: Bool?
    var end_time: String?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        self.task_id = response["task_id"].int64Value
        self.start_time = response["start_time"].stringValue
        self.time_started = response["time_started"].boolValue
        self.end_time = response["end_time"].stringValue
        
    }
}
