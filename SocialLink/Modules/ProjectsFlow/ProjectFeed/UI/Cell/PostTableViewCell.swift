//
//  PostTableViewCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 24/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

protocol PostTableViewCellDelegate: class {
    func openPostController()
    func postAttachment()
    func postAdd()
    func postCalender()
    func postTag()
  
}

class PostTableViewCell: UITableViewCell {
    var viewModel: ProfileViewModel?
    var contactProfile: ProfileModel?

    weak var delagate: PostTableViewCellDelegate?
    @IBOutlet weak var postTextField: UITextField!
    
    @IBOutlet weak var profilePicImage: UIImageView!
    @IBOutlet weak var postAttachment: UIButton!

    @IBOutlet weak var postAdd: UIButton!
    @IBOutlet weak var postCalender: UIButton!
    
    @IBOutlet weak var postTag: UIButton!
 
    override func awakeFromNib() {
        super.awakeFromNib()
        viewModel = ProfileViewModel()
        getusersFromDB()
        // Initialization code
        let userProfileImage = APPURL.feedUserProfileImage  + contactProfile!.image!
        profilePicImage.sd_setImage(with: URL(string: userProfileImage), placeholderImage: UIImage(named: ""))
        postTextField.delegate = self
        profilePicImage.layer.cornerRadius = profilePicImage.frame.size.width / 2
        profilePicImage.layer.masksToBounds = true
       // let userProfileImage = APPURL.feedUserProfileImage  + profile.image!
       // profilePicImage.sd_setImage(with: URL(string: userProfileImage), placeholderImage: UIImage(named: ""))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func getusersFromDB() {
        let users = APPDBUtility.getUserData(mocType: .main)
        if users?.count ?? 0 > 0 {
            if let user = users {
                if viewModel != nil {
                    contactProfile = APPUtility.convertDBUserToUser(dbUser: user[0])
                }
                
            }
        }
    }
    @IBAction func postAttachmentClicked(_ sender: Any) {
        self.delagate?.postAttachment()
    }
    
    @IBAction func postAddClicked(_ sender: Any) {
        self.delagate?.postAdd()

    }
    @IBAction func postCalenderClicked(_ sender: Any) {
        self.delagate?.postCalender()
    }
    @IBAction func postTagClicked(_ sender: Any) {
        self.delagate?.postTag()
    }
    
    
    
    

}

extension PostTableViewCell: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.delagate?.openPostController()
        return false
    }
    
}
