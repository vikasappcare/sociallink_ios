//
//  TeamInfoCell.swift
//  SocialLink
//
//  Created by Santhosh on 21/04/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class TeamInfoCell: UITableViewCell {

    @IBOutlet weak var teamInfoImage: UIImageView!
    @IBOutlet weak var teamMemberName: UILabel!
    @IBOutlet weak var teamMemberDes: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        teamInfoImage.layer.cornerRadius = teamInfoImage.frame.size.width / 2
        teamInfoImage.layer.masksToBounds = true
    }
    
    func setData(data: ProjectContact) {
        teamMemberName.text = data.member_name ?? ""
        teamMemberDes.text = data.email_id ?? ""
        //teamInfoImage.sd_setImage(with: URL(string: data.image ?? ""), placeholderImage: #imageLiteral(resourceName: "SampleProfile"))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
