//
//  TeamInfoVC.swift
//  SocialLink
//
//  Created by Santhosh on 21/04/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class TeamInfoVC: AppBaseVC {
    
    @IBOutlet weak var reportHeight: NSLayoutConstraint!
    @IBOutlet weak var headerview: UIView!
    @IBOutlet weak var teamPogress: UIProgressView!
    @IBOutlet weak var teamSegment: UISegmentedControl!
    @IBOutlet weak var teamInfoTable: UITableView!
    @IBOutlet weak var reportBUtton: UIButton!
    var selectedProject: Project?
    var membersArray: [ProjectContact]?
    var viewModel: ProjectContactsViewModel?
    var contactType = MemberType.SocialLink
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = ProjectContactsViewModel()
        // Do any additional setup after loading the view.
        membersArray = [ProjectContact]()
        self.setTableViewDatasource()
        ///self.teamSegment.selectedSegmentIndex = 0
        self.navigationItem.title = "CONTACTS"
        ///reportHeight.constant = 0
        self.getTeamInfo()
        
        //        if let project = self.selectedProject {
        //            let progress = (project.progress ?? 0.0) / 100
        //            teamPogress.progress = progress
        //        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //    override func viewDidLayoutSubviews() {
    //        self.teamPogress.layer.cornerRadius = self.teamPogress.frame.height / 2
    //        self.teamPogress.layer.masksToBounds = true
    //    }
    
    func setProject(project: Project) {
        self.selectedProject = project
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func segmentClciked(_ sender: Any) {
        switch teamSegment.selectedSegmentIndex {
        case 0:
            reportHeight.constant = 0
            contactType = MemberType.SocialLink
        default:
            reportHeight.constant = 50
            contactType = MemberType.Internal
        }
        teamInfoTable.reloadData()
    }
    
    @IBAction func InviteInternalMembersClicked(_ sender: Any) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "InviteTeamMemberVC") as? InviteTeamMemberVC {
            let navigationController = APPNavigationController.init(rootViewController: controller)
            self.present(navigationController, animated: true, completion: nil)
        }
        
    }
    
    func getTeamInfo() {
        if let model = viewModel {
            self.showHUD(title: "")
            //            let parms = ["token": APPStore.sharedStore.token,
            //                         "project_id": self.selectedProject?.project_id ?? 0] as [String : Any]
            
            if let idIs = self.selectedProject {
                let parms = ["project_id": idIs.project_id as Any] as [String : Any]
                print(parms)
                model.getProjectContacts(parameters: parms, onSuccess: { [weak self] (dataSource) in
                    print("Success")
                    self?.membersArray?.removeAll()
                    self?.membersArray = dataSource?.list
                    self?.hideHud()
                    self?.teamInfoTable.reloadData()
                    }, onError: { (error) in
                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
                       // self.showAlert(title: Message.Alert, message: error.statusMessage)
                        self.hideHud()
                })
            }
            
            
        }
    }
}



extension TeamInfoVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.teamInfoTable.delegate = self
        self.teamInfoTable.dataSource = self
        self.teamInfoTable.estimatedRowHeight = 300
        self.teamInfoTable.rowHeight = UITableViewAutomaticDimension
        self.teamInfoTable.tableFooterView = UIView()
        self.teamInfoTable.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        return view
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if membersArray?.count != nil{
        return self.membersArray!.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:
            CellIdentifiers.TeamInfoCellIdentifier, for: indexPath)
        if let cellIs = cell as? TeamInfoCell {
            cellIs.selectionStyle = .none
            //            let data = ["profile": profileArray[indexPath.row],
            //                        "color": colorArray[indexPath.row]] as [String: Any]
            //            cellIs.setData(data: data)
            if let contact = self.membersArray {
                cellIs.setData(data: contact[indexPath.row])
            }
        }
        cell.selectionStyle = .none
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        
        
        let flag = UITableViewRowAction(style: .normal, title: "") { action, index in
            print("Report button tapped")
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ReportViewController") as? ReportViewController {
                let navigationController = APPNavigationController.init(rootViewController: controller)
                self.present(navigationController, animated: true, completion: nil)
            }
            
        }
        flag.backgroundColor = UIColor.green
        flag.backgroundColor = UIColor(patternImage: UIImage(named: "TeamFlag")!)
        
        let message = UITableViewRowAction(style: .normal, title: "") { action, index in
            print("more button tapped")
        }
        message.backgroundColor = UIColor.blue
        message.backgroundColor = UIColor(patternImage: UIImage(named: "TeamMessage")!)
        
        
        let timer = UITableViewRowAction(style: .normal, title: "") { action, index in
            print("Timer button tapped")
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProjectTimeSheetVC") as? ProjectTimeSheetVC {
                let navigationController = APPNavigationController.init(rootViewController: controller)
                self.present(navigationController, animated: true, completion: nil)
            }
        }
        timer.backgroundColor = UIColor.blue
        timer.backgroundColor = UIColor(patternImage: UIImage(named: "TeamTimer")!)
        
        //// Delete
        
        let delete = UITableViewRowAction(style: .normal, title: "") { action, index in
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProjectTimeSheetVC") as? ProjectTimeSheetVC {
                let navigationController = APPNavigationController.init(rootViewController: controller)
                self.present(navigationController, animated: true, completion: nil)
            }
        }
        delete.backgroundColor = UIColor.blue
        delete.backgroundColor = UIColor(patternImage: UIImage(named: "TeamDelete")!)
        
        return [timer, flag, message]
        /*  switch self.teamSegment.selectedSegmentIndex {
         case 0:
         return [flag, message, timer]
         case 1:
         return [delete, flag, message]
         default:
         return [flag, message]
         }*/
        
    }
    
    
}
