//
//  AppSingleton.swift
//  BaseProject
//
//  Created by Santhosh Marripelli on 22/02/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
class APPStore {
    static let sharedStore: APPStore = APPStore()
    private init() {
    }
    var environment: String = Environment.Local
    ///  var userEmailLogin: SignUpModel = SignUpModel()
    var loggedInAs: String = "LoggedInUser.Employer"
    var forgetPasswordEmail: String = ""
    
    var appUserID: String = ""
    var userIs: String = ""
    var token: String = ""
    var user = ProfileModel()
    var projects = [Project]()
    var contractorProjects = [Project]()
    var employerProjects = [Project]()

    var estimatedProjects = [EstimatedProject]()
    var roles = [Role]()
    var services = [Service]()
    var feeds = [Feed]()
    var tasks = [Task]()
    var contractorTasks = [ContractorTask]()
    
    
    
    
    
}
