//
//  SearchCollectionCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class SearchCollectionCell: UICollectionViewCell {
    @IBOutlet weak var searchTypeImage: UIImageView!
    
    @IBOutlet weak var searchTypename: UILabel!
    
    
    
    func setData(data: [String: Any]?) {
        searchTypename.text = data!["type"] as? String ?? ""
        searchTypeImage.layer.borderWidth = 2.0
        searchTypeImage.layer.borderColor = UIColor.appBlueColor.cgColor
    }
    
    
}
