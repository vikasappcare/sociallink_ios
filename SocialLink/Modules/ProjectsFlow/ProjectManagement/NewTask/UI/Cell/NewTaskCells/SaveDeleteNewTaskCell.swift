//
//  SaveDeleteNewTaskCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/05/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

protocol SaveDeleteNewTaskCellDelegate: class {
    func saveClicked()
    func deleteClicked()
}

class SaveDeleteNewTaskCell: UITableViewCell {
    
    @IBAction func deleteClicked(_ sender: Any) {
        self.delegate?.deleteClicked()
    }
    @IBAction func saveClicked(_ sender: Any) {
        self.delegate?.saveClicked()
    }
    
    weak var delegate: SaveDeleteNewTaskCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
