//
//  LoginPasswordVC.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 05/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class LoginPasswordVC: AppBaseVC {

    @IBOutlet weak var loginPasswordTable: UITableView!
    var viewModel: LoginViewModel?
    var emailIs: String?
    var userModel: SignUpModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = LoginViewModel()
        self.setTableViewDatasource()
        self.navigationItem.title = "LOGIN"

        // Do any additional setup after loading the view.
    }
    
    func loggedInEmail(email: String, profile: SignUpModel) {
        emailIs = email
        userModel = profile
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension LoginPasswordVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.loginPasswordTable.delegate = self
        self.loginPasswordTable.dataSource = self
        self.loginPasswordTable.estimatedRowHeight = 300
        self.loginPasswordTable.rowHeight = UITableViewAutomaticDimension
        
        
        /// self.landingTableView.backgroundView = nil
        
        
        //        [self.tableView setBackgroundView:nil];
        //        [self.tableView setBackgroundColor:[UIColor clearColor]];
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:
            CellIdentifiers.LoginPasswordCellIdentifier, for: indexPath)
        if let cellIs = cell as? LoginPasswordCell {
            cellIs.selectionStyle = .none
            cellIs.delegate = self
            
            if let dataIs = self.userModel {
                cellIs.setData(data: dataIs)
            }
        }
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UIScreen.main.bounds.height - 64
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension LoginPasswordVC: LoginPasswordCellDelegate {
    func passwordForgetClicked() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ForgetPasswordVC") as? ForgetPasswordVC {
            let navController = APPNavigationController(rootViewController: controller)
            self.present(navController, animated: true, completion: nil)
            /// self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func passwordRegisterClicked() {
        self.dismiss(animated: true, completion: {
            
            self.dismiss(animated: true, completion: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue:
                APPNotifications.createNewAccountNotification),
                                            object: nil, userInfo: nil)
            
        })
        
        
        
    }
    
    func submitClicked() {
        let email = emailIs ?? ""
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = loginPasswordTable.cellForRow(at: indexPath) as? LoginPasswordCell
        if let data = cell?.getData() {
             let password = (data["password"] as? String)?.trimmingCharacters(in: .whitespaces)
            if let model = viewModel {
                let parms = ["email": email,
                             "password": password ?? ""] as [String : Any]
                self.showHUD(title: "Loading...")
                model.loginPasswordUser(parameters: parms, onSuccess: { [weak self] (dataSource) in
                    print("Success")
                    print(dataSource ?? "")
                    APPUtility.getUserData()
                    APPUtility.getToken()
                    self?.navigationController?.popViewController(animated: true)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue:
                        APPNotifications.dismissLoginView),
                                                    object: nil, userInfo: nil)
                    self?.hideHud()
                    }, onError: { (error) in
                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
                        self.showAlert(title: Message.Alert, message: error.statusMessage)
                        self.hideHud()
                })
            }
        }
    }
}
