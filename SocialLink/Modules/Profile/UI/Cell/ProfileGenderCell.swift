//
//  ProfileGenderCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 27/02/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit
protocol ProfileGenderCellDelegate: class {
    func genderChanged(gender: String, tag: Int)
}

class ProfileGenderCell: UITableViewCell {
    weak var delegate: ProfileGenderCellDelegate?
    @IBOutlet weak var genderSegment: UISegmentedControl!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        UISegmentedControl.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for: .selected)
        let attr = NSDictionary(object: UIFont.boldFont(16), forKey: NSAttributedStringKey.font as NSCopying)
        UISegmentedControl.appearance().setTitleTextAttributes(attr as [NSObject: AnyObject], for: .normal)
    }
    
    
    func setData(gender: String) {
        switch gender {
        case "male":
            genderSegment.selectedSegmentIndex = 0
        case "female":
            genderSegment.selectedSegmentIndex = 1
        default:
            break
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func genderSegmentClicked(_ sender: Any) {
        var gender = ""
        switch genderSegment.selectedSegmentIndex {
        case 0:
            gender = "male"
        case 1:
            gender = "female"
        default:
            break
        }
        self.delegate?.genderChanged(gender: gender, tag: 3)
    }
    
}
