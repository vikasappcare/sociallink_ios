//
//  FAQViewController.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 11/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class FAQViewController: UIViewController {

    @IBOutlet weak var faqTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
self.navigationItem.title = "FREQUENTLY ASKED QUESTIONS"
        self.setTableViewDatasource()
         self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension FAQViewController: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.faqTableView.delegate = self
        self.faqTableView.dataSource = self
        self.faqTableView.estimatedRowHeight = 300
        self.faqTableView.rowHeight = UITableViewAutomaticDimension
        self.faqTableView.tableFooterView = UIView()
    }
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row % 2 == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.QuestionTableCellIdentifier, for: indexPath)
            if let cellIs = cell as? QuestionTableCell {
                cellIs.selectionStyle = .none
                //            let data = ["profile": profileArray[indexPath.row],
                //                        "color": colorArray[indexPath.row]] as [String: Any]
                //            cellIs.setData(data: data)
            }
            cell.selectionStyle = .none
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.AnswerTableCellIdentifier, for: indexPath)
            if let cellIs = cell as? AnswerTableCell {
                cellIs.selectionStyle = .none
                //            let data = ["profile": profileArray[indexPath.row],
                //                        "color": colorArray[indexPath.row]] as [String: Any]
                //            cellIs.setData(data: data)
            }
            cell.selectionStyle = .none
            return cell
        }
        
        
        
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
}
