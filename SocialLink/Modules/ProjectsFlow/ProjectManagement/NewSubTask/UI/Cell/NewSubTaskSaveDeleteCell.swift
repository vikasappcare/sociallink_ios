//
//  NewSubTaskSaveDeleteCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/05/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class NewSubTaskSaveDeleteCell: UITableViewCell {

    @IBOutlet weak var submitStackView: UIStackView!
    @IBOutlet weak var submitButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var deleteBUTTON: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var completeButton: UIButton!
    var typeIs: String = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setType(type: String) {
        typeIs = type
    }
    
    func createSubmitButton() {
        let submitbutton = UIButton()
        submitbutton.addTarget(self, action: #selector(submitCLicked), for: .touchUpInside)
        submitbutton.setTitle("Submit", for: .normal)
        submitbutton.layer.cornerRadius = 5.0
        submitbutton.layer.masksToBounds = true
        submitbutton.backgroundColor = UIColor.appGreenColor
        submitStackView.insertArrangedSubview(submitbutton, at: 1)
    }
    
    @objc func submitCLicked() {
        ///
        
        
    }
    
    func setData(data: [String: Any]) {
        
        completeButton.setTitle(data["completeTitle"] as? String ?? "", for: .normal)
        deleteBUTTON.setTitle(data["deleteTitle"] as? String ?? "", for: .normal)
        
        completeButton.backgroundColor = data["backColor"] as? UIColor
        
        if data["deleteTitle"] as? String ?? "" == "" {
            deleteBUTTON.isHidden = true
        } else {
            deleteBUTTON.isHidden = false
        }
        
        if typeIs == SubTaskStatus.completed {
            self.createSubmitButton()
        }
    }

    @IBAction func deleteClicked(_ sender: Any) {
    }
    @IBAction func completeClicked(_ sender: Any) {
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
