//
//  NewPasswordVC.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class NewPasswordVC: AppBaseVC {
    
    @IBOutlet weak var newPasswordTable: UITableView!
    var viewModel: ForgetPasswordViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "NEW PASSWORD"
        viewModel = ForgetPasswordViewModel()

        
        self.setTableViewDatasource()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension NewPasswordVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.newPasswordTable.delegate = self
        self.newPasswordTable.dataSource = self
        self.newPasswordTable.estimatedRowHeight = 300
        self.newPasswordTable.rowHeight = UITableViewAutomaticDimension
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:
            CellIdentifiers.NewPasswordCellIdentifier, for: indexPath)
        if let cellIs = cell as? NewPasswordCell {
            cellIs.selectionStyle = .none
            cellIs.delegate = self
        }
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UIScreen.main.bounds.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
extension NewPasswordVC: NewPasswordCellDelegate {
    func changePasswordSelected() {
        
        
        let indexPath = IndexPath(row: 0, section: 0)
         let cell = newPasswordTable.cellForRow(at: indexPath) as? NewPasswordCell
         
        if let data = cell?.getData() {
            let password = (data["password"] as? String)?.trimmingCharacters(in: .whitespaces)
            let confirmPassword = (data["confirmPassword"] as? String)?.trimmingCharacters(in: .whitespaces)
            
            if (password?.isEmpty ?? false) || (password == "") {
                self.showAlert(title: "Alert!", message: "Please enter password to continue")
                return
            } else if (confirmPassword?.isEmpty ?? false) || (confirmPassword == "") {
                self.showAlert(title: "Alert!", message: "Please confirm your password to continue")
                return
            } else if password != confirmPassword {
                self.showAlert(title: "Alert!", message: "Password & Confirm Password didn't match")
                return
            }
            
            if let model = viewModel {
                let parms = ["email": APPStore.sharedStore.forgetPasswordEmail ,
                             "password": password ?? ""] as [String : Any]
                
                self.showHUD(title: "Validating ...")
                model.updatePassword(parameters: parms, onSuccess: { [weak self] (dataSource) in
                    print("Success")
                    print(dataSource ?? "")
                    ///self?.showAlert(title: "Success..!", message: "")
                    self?.navigationController?.popToRootViewController(animated: true)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue:
                        APPNotifications.removeForgetPassword),
                                                    object: nil, userInfo: nil)
                    self?.hideHud()
                    }, onError: { (error) in
                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
                        self.hideHud()
                })
             }
 
        }

        
        
    }
    
    
    
}
