//
//  RegistrationSkillsModel.swift
//  SocialLink
//
//  Created by admin on 3/6/19.
//  Copyright © 2019 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON

class RegistrationSkillsModel: AppBaseModel {
    var sId = [String]()
    var sName = [String]()
    var mapImage = [String]()
    
    override init() {
        super.init()
    }

    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)

        if response.exists() {
            self.parseResponse(with: response)
        }

    }
    private func parseResponse(with response: JSON) {

        for i in 0..<response.count{
            self.sId.append(response[i]["sid"].stringValue)
            self.sName.append(response[i]["sname"].stringValue)
            self.mapImage.append(response[i]["map_img"].stringValue)
        }
        
    }

}
