//
//  AddPaymentVC.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 04/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class AddPaymentVC: UIViewController {

    @IBOutlet weak var paymentTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "PAYMENT"

        self.setTableViewDatasource()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddPaymentVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.paymentTableView.delegate = self
        self.paymentTableView.dataSource = self
        self.paymentTableView.estimatedRowHeight = 300
        self.paymentTableView.rowHeight = UITableViewAutomaticDimension
        self.paymentTableView.tableFooterView = UIView()
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Add New Card"
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        view.tintColor = UIColor.appLightGrayColor
        
        if let header: UITableViewHeaderFooterView = view as? UITableViewHeaderFooterView {
            header.textLabel?.textColor = UIColor.lightGray
            header.textLabel?.setSemiBoldFont(size: 20)
        }
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 80
        default:
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 5
        case 2:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
  
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.NewCardCellIdentifier, for: indexPath)
            if let cellIs = cell as? NewCardCell {
                cellIs.selectionStyle = .none
            }
            cell.selectionStyle = .none
            return cell
        case 1:
            
            switch indexPath.row {
            case 0, 2, 3:
                let cell = tableView.dequeueReusableCell(withIdentifier:
                    CellIdentifiers.CardNumberCellIdentifier, for: indexPath)
                if let cellIs = cell as? CardNumberCell {
                    cellIs.selectionStyle = .none
                
                    var data: [String: Any]?
                    
                    if indexPath.row == 0 {
                        data = ["header": "Card Number",
                                "placeHolder": "XXXX - XXXX - XXXX - XXXX"]
                    } else if indexPath.row == 2 {
                        data = ["header": "Address",
                                "placeHolder": "Street Name"]
                    } else if indexPath.row == 3 {
                        data = ["header": "City",
                                "placeHolder": "City"]
                    }
                    
                   
                    
                    cellIs.setData(data: data)
                }
                cell.selectionStyle = .none
                return cell
            case 1, 4:
                let cell = tableView.dequeueReusableCell(withIdentifier:
                    CellIdentifiers.ExpiryDateCellIdentifier, for: indexPath)
                if let cellIs = cell as? ExpiryDateCell {
                    cellIs.selectionStyle = .none
                    
                    var data: [String: Any]?
                    if indexPath.row == 1 {
                        data = ["header1": "Exp. Date",
                                "header2": "CVV",
                                "contentText1": "MM - YY",
                                "contextText2": "XXX"]
                    } else if indexPath.row == 4 {
                        data = ["header1": "State",
                                "header2": "Zip Code",
                                "contentText1": "State",
                                "contextText2": "XXXXX"]
                    }
                    cellIs.setData(data: data)
                    
                }
                cell.selectionStyle = .none
                return cell
            default:
                return UITableViewCell()
            }

        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.ContinueCellIdentifier, for: indexPath)
            if let cellIs = cell as? ContinueCell {
                cellIs.selectionStyle = .none
            }
            cell.selectionStyle = .none
            return cell
        default:
            return UITableViewCell()
        }
        
        
    }
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //
    //        return UIScreen.main.bounds.height
    //    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
