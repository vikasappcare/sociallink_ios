//
//  StopWatch.swift
//  TimerSample
//
//  Created by Santhosh Marripelli on 18/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation

protocol StopWatchDelegate: class {
    func currentTimerUpdated(days: String, hours: String, minutes: String, seconds: String, milliseconds: String)
    func timerHasStarted()
    func timerHasStopped()
}


class StopWatch {
    
    init() {}
    weak var delegate: StopWatchDelegate?
    private  var milliseconds: Int = 0
    private var seconds: Int = 0
    private var minutes: Int = 0
    private  var hours: Int = 0
    private var days: Int = 0
    
    private(set) var timer: Timer?
    
    init(delegate: StopWatchDelegate) {
        self.delegate = delegate
    }
    
    func startTimer(fromDate: Date, toDate: Date?) {
            self.timer = Timer.scheduledTimer(timeInterval: 0.01, target: self,
                                              selector: #selector(self.timerStarted),
                                         userInfo: nil, repeats: true)
            /// RunLoop.current.add(timer, forMode: RunLoopMode.commonModes)
            self.caculateTime(from: fromDate, to: toDate)
            self.delegate?.timerHasStarted()
    }
    
    func caculateTime(from: Date, to: Date?) {
        
        if let todateIs = to {
            let present =  APPUtility.differcnceBetweenDatesinHours(fromDate: from, toDate: todateIs)
            print(present)
            let timerArray = present.components(separatedBy: "||")
            if timerArray.count > 3 {
                days = Int(timerArray[0]) ?? 0
                hours = Int(timerArray[1]) ?? 0
                minutes = Int(timerArray[2]) ?? 0
                seconds = Int(timerArray[3]) ?? 0
            } else if timerArray.count > 2 {
                hours = Int(timerArray[0]) ?? 0
                minutes = Int(timerArray[1]) ?? 0
                seconds = Int(timerArray[2]) ?? 0
            } else if timerArray.count > 1 {
                minutes = Int(timerArray[0]) ?? 0
                seconds = Int(timerArray[1]) ?? 0
            } else if timerArray.count == 1 {
                seconds = Int(timerArray[0]) ?? 0
            }
        } else {
            days = 0
            hours = 0
            minutes = 0
            seconds = 0
        }
        
        
        
    }
    
    /// Pause the Timer and any current lap.
    func stopTimer() {
        days = 0
        hours = 0
        minutes = 0
        seconds = 0
        milliseconds = 0
        timer?.invalidate()
        timer = nil
        delegate?.timerHasStopped()
    }
    
    @objc func timerStarted() {
        milliseconds += 1
        if milliseconds == 99 {
            milliseconds = 0
            seconds += 1
        }
        if seconds == 60 {
            seconds = 0
            minutes += 1
        }
        if minutes == 60 {
            minutes = 0
            hours += 1
        }
        if hours == 24 {
            hours = 0
            days += 1
        }
        self.delegate?.currentTimerUpdated(days: "\(days)", hours: "\(hours)", minutes: "\(minutes)", seconds: "\(seconds)", milliseconds: "\(milliseconds)")
    }
}
