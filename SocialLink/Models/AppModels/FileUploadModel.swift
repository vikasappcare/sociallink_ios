//
//  FileUploadModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 20/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON



class FileUploadModel: AppBaseModel {
    
    var service: String?
    var data: UploadData?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
    }
    
    private func parseResponse(with response: JSON) {
        self.service = response["service"].stringValue
        self.data = self.createObject(with: response["data"])
    }
    
    private func createObject(with jsonObject: JSON) -> UploadData? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return UploadData.init(jsonObject)
        }
    }
}

class UploadData: AppBaseModel {
    var dfile: String?
    var id: Int64?
    var upload_type: String?
    var extensionIs: String?
    
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        self.dfile = response["dfile"].stringValue
        self.id = response["id"].int64Value
        self.upload_type = response["upload_type"].stringValue
        self.extensionIs = response["extension"].stringValue
    }
}
