 //
//  CreateAccountVC
//  SocialLink
//
//  Created by Santhosh Marripelli on 27/02/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit
import MobileCoreServices
class CreateAccountVC: AppBaseVC {
    
    
    let contentPlaceHolder = ["First Name",
                              "Last Name",
                              "Email",
                              "Password",
                              "Phone Number",
                              "Resume",
                              "Zip code",
                              "Hire For"]
    
    let keyValue = ["First Name",
                    "Last Name",
                    "Email",
                    "Password",
                    "Phone Number",
                    "Resume",
                    "Zip code",
                    "Hire For"]
    
    var uploadModel: FileUploadViewModel?
    @IBOutlet weak var profileTableView: UITableView!
    var skip: Bool?
    var contactProfile: ProfileModel?
    var viewModel: ProfileViewModel?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = ProfileViewModel()
        self.createNavigationItems()
        let logo = UIImage(named: "logo3.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView

        self.getusersFromDB()
        // Do any additional setup after loading the view.
        
        self.setTableViewDatasource()
        
    }
    func getusersFromDB() {
        let users = APPDBUtility.getUserData(mocType: .main)
        if users?.count ?? 0 > 0 {
            if let user = users {
                if viewModel != nil {
                    contactProfile = APPUtility.convertDBUserToUser(dbUser: user[0])
                }
                
            }
        }
    }
    
    func showSkip(bool: Bool) {
        skip = bool
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}


extension CreateAccountVC {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
        let skipItem = UIBarButtonItem(title: "Skip", style: .done, target: self, action: #selector(skipClicked))
        self.navigationItem.rightBarButtonItem = skipItem
    }
    
    @objc func skipClicked() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ChoosePlanVC") as? ChoosePlanVC {
            /// let navController = APPNavigationController(rootViewController: controller)
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension CreateAccountVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.profileTableView.delegate = self
        self.profileTableView.dataSource = self
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contentPlaceHolder.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    private func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        label.text = "Create Account"
        
        headerView.addSubview(label)
        
        return headerView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if(indexPath.row == 0) {
//            let cell = tableView.dequeueReusableCell(withIdentifier:
//                "ProfiletImageCellIdentifier", for: indexPath)
//            return cell
//        }
        let cell = tableView.dequeueReusableCell(withIdentifier:
            CellIdentifiers.ProfileContentCellIdentifier, for: indexPath)
        if let cellIs = cell as? ProfileContentCell {
            cellIs.selectionStyle = .none
            cellIs.delegate = self as? ProfileContentCellDelegate
            if let profile = contactProfile {
                cellIs.setData(data: contentPlaceHolder[indexPath.row], tag: indexPath.row, profile: profile)
            }
        }
        cell.selectionStyle = .none
        return cell

    }
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //
    //        return UIScreen.main.bounds.height
    //    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let headerView = UIView()
//        var rect = tableView.frame
//        rect.size.height = 30
//        headerView.frame = rect
//        headerView.backgroundColor = UIColor.red
//        let headerLabel:UILabel = UILabel(frame: tableView.bounds)
//        headerLabel.text = "Create Account"
//        headerView.addSubview(headerLabel)
//        return headerView
//
//    }
    
}

 extension CreateAccountVC {
    
    func dataChanged(data: String, tag: Int) {
        self.updateModel(text: data, tag: tag)
    }
    
    func textChanged(data: String, tag: Int) {
        self.updateModel(text: data, tag: tag)
    }
    
    
    func updateModel(text: String, tag: Int) {
        switch tag {
        case 0:
            if !text.checkForEmpty() {
                self.contactProfile?.first_name = text
            } else {
                self.showAlert(title: "Alert", message: "First Name can't be empty")
            }
        case 1:
            if !text.checkForEmpty() {
                self.contactProfile?.last_name = text
            } else {
                self.showAlert(title: "Alert", message: "Last Name can't be empty")
            }
        case 2:
            if !APPUtility.validateEmail(enteredEmail: text) {
                self.showAlert(title: "Alert", message: "Enter a valid Email")
            } else {
                self.contactProfile?.email = text
            }
        case 3:
            if !text.checkForEmpty() {
                self.contactProfile?.password = text
            } else {
                self.showAlert(title: "Alert", message: "Password can't be empty")
            }
        case 4:
            self.contactProfile?.phone_number = text
        case 5:
            self.contactProfile?.zipcode = text
        case 6:
            self.contactProfile?.hereFor = text
        default:
            break
        }
    }
}


 
