//
//  TaskListViewModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 01/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation

class TaskListViewModel {
    
    init() {
    }
    
    var taskListArray: [ContractorTask]?
    var projectTaskListArray: [Task]?
    
    func getProjectTasks(parameters: [String: Any],
                   onSuccess: @escaping (TaskModel?) -> Void,
                   onError: @escaping APIErrorHandler) {
        AppDataManager.getProjectTasks(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: dataSource)
            onSuccess(dataSource)

        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
//    func updateTasksOnSelection(index: Int) {
//        var taskState: Int64?
//        var acceptedState: String?
//        switch APPStore.sharedStore.userIs {
//        case USERTYPE.EMPLOYER:
//            switch index {
//            case 0:
//                print("Employeer Asigned")
//                taskState = EMPLOYERTASKSTATES.ASSIGNED
//                acceptedState = "" //// Empty First
//            case 1:
//                print("Employeer Active")
//                taskState = EMPLOYERTASKSTATES.ACTIVE
//                acceptedState = "\(ACCEPTEDSTATES.ACCEPT)"
//            case 2:
//                print("Employeer Payment")
//                taskState = EMPLOYERTASKSTATES.PAYMENT
//                acceptedState = "\(ACCEPTEDSTATES.COMPLETED)"
//            default:
//                break
//            }
//        case USERTYPE.CONTRACTOR, USERTYPE.ONLYCONTRACTOR:
//            switch index {
//            case 0:
//                print("Contractor Asigned")
//                taskState = CONTRACTORTASKSTATES.ASSIGNED
//                acceptedState = "" //// Empty First
//            case 1:
//                print("Contractor In Progress")
//                taskState = CONTRACTORTASKSTATES.INPROGRESS
//                acceptedState = "\(ACCEPTEDSTATES.ACCEPT)"
//            case 2:
//                print("Contractor Completed")
//                taskState = CONTRACTORTASKSTATES.COMPLETED
//                acceptedState = "\(ACCEPTEDSTATES.COMPLETED)"
//            default:
//                break
//            }
//        default:
//            break
//        }
//        print("TaskState: \(String(describing: taskState ?? 0))")
//        print("AcceptedState: \(acceptedState ?? "")")
//        
//        if let state = taskState {
//            if let accepted = acceptedState {
//                if self.taskListArray == nil {
//                    self.taskListArray = [ContractorTask]()
//                }
//                
//                if APPStore.sharedStore.userIs == USERTYPE.EMPLOYER {
//                    if acceptedState == "" {
//                        let taskArrayIs = APPStore.sharedStore.tasks.filter {$0.task_state == state && ($0.accepted_state == accepted || $0.accepted_state == "0")}
//                        self.projectTaskListArray = taskArrayIs
//                    } else {
//                        let taskArrayIs = APPStore.sharedStore.tasks.filter {$0.task_state == state && $0.accepted_state == accepted}
//                        self.projectTaskListArray = taskArrayIs
//                    }
//                } else {
//                    let taskArrayIs = APPStore.sharedStore.tasks.filter {$0.task_state == state && $0.accepted_state == accepted}
//                    self.projectTaskListArray = taskArrayIs
//                }
//                
//
//                
//                return
//            }
//            let taskArrayIs = self.taskListArray?.filter {$0.pstatus == state}
//            self.taskListArray = taskArrayIs
//        }
//    }
    
}
