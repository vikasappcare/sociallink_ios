//
//  POSDDConstants.swift
//  POSSalesSheet
//
//  Created by Marripelli, Santosh Kumar on 05/07/17.
//  Copyright © 2017 Verizon. All rights reserved.
//


import UIKit

internal struct POSDDConstants {
    
    internal struct KeyPath {
        
        static let Frame: String = "frame"
        
    }
    
    internal struct ReusableIdentifier {
        
        static let POSDropDownCell: String = "POSDropDownCell"
        
    }
    
    internal struct UI {
        
        static let TextColor: UIColor = UIColor.black
        static let TextFont: UIFont = UIFont.systemFont(ofSize: 15)
        static let BackgroundColor: UIColor = UIColor(white: 0.94, alpha: 1)
        static let SelectionBackgroundColor: UIColor = UIColor(white: 0.89, alpha: 1)
        static let SeparatorColor: UIColor = UIColor.clear
        static let CornerRadius: CGFloat = 2
        static let RowHeight: CGFloat = 44
        static let HeightPadding: CGFloat = 20
        // swiftlint:disable nesting
        struct Shadow {
            static let Color: UIColor = UIColor.darkGray
            static let Offset: CGSize = CGSize.zero
            static let Opacity: Float = 0.4
            static let Radius: CGFloat = 8
        }
        // swiftlint:enable nesting
        
    }
    
    internal struct Animation {
        
        static let Duration: Double = 0.15
        static let EntranceOptions: UIViewAnimationOptions = [.allowUserInteraction, .curveEaseOut]
        static let ExitOptions: UIViewAnimationOptions = [.allowUserInteraction, .curveEaseIn]
        static let DownScaleTransform: CGAffineTransform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        
    }
    
}
