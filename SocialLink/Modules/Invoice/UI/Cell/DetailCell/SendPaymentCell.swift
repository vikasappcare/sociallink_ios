//
//  SendPaymentCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 11/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit
protocol SendPaymentCellDelegate: class {
    func sendPaymentSelected()
   
}
class SendPaymentCell: UITableViewCell {

    weak var delegate: SendPaymentCellDelegate?
    @IBOutlet weak var sendPayment: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func sendPaymentClicked(_ sender: Any) {
        self.delegate?.sendPaymentSelected()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
