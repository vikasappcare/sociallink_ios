//
//  TaskDetailLogCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 17/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class TaskDetailLogCell: UITableViewCell {

    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    @IBOutlet weak var cumulativeTimeLabel: UILabel!
    @IBOutlet weak var viewScreens: UIButton!
    @IBOutlet weak var uploadScreens: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(data: TimeLog) {
        
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
