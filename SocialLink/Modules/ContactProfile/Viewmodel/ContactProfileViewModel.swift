//
//  ContactProfileViewModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
class ContactProfileViewModel {
    
    init() {
    }
    var userProfile: ContractorProfile?
    
    func getContactProfile(parameters: [String: Any],
                 onSuccess: @escaping (ContractorProfileModel?) -> Void,
                 onError: @escaping APIErrorHandler) {
        AppDataManager.getContactProfile(parametes: parameters, onSuccess: { (dataSource) in
            if let dataIs = dataSource {
                self.userProfile = dataIs.data
            }
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
    func constructValueArray() -> [String: String]? {
        if let profile = self.userProfile {
            let dataIs = ["About Me": profile.uaboutu,
                          "Email": profile.uemailid,
                          "Phone": profile.uphone,
                          "Gender": profile.ugender,
                          "Website": profile.uwebsite,
                          "Education": profile.education,
                          "Skills": profile.uskills,
                          "Business": profile.business,
                          "Industry": profile.industry,
                          "Department": profile.department,
                          "Job": profile.job,
                          "Resume": profile.resume,
                          "Designation": profile.designation]
            return dataIs as? [String : String]
        }
        return nil
    }
}
