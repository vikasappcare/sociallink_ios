//
//  ServiceListModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 28/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON

class ServiceListModel: AppBaseModel {
    
    var service: String?
    var list: [Service]?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    
    private func parseResponse(with response: JSON) {
        self.service = response["service"].stringValue
        for item in response["List"].arrayValue {
            if let feedModel = self.createObject(with: item) {
                if self.list == nil {
                    self.list = [Service]()
                }
                self.list?.append(feedModel)
            }
        }
    }
    
    private func createObject(with jsonObject: JSON) -> Service? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return Service.init(jsonObject)
        }
    }
    
}


class Service: AppBaseModel {
    var service_id: Int64?
    var service_name: String?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        self.service_id = response["service_id"].int64Value
        self.service_name = response["service_name"].stringValue
    }
    
    
}


