//
//  ContactTableViewCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {

    @IBOutlet weak var contactImage: UIImageView!
    @IBOutlet weak var contactName: UILabel!
    @IBOutlet weak var contactProfile: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        contactImage.layer.borderWidth = 2.0
        contactImage.layer.borderColor = UIColor.lightGray.cgColor
        
        //contactProfile.sizeToFit()
    }
    
    func setData(data: [String: Any]?) {
        contactName.text = "\(data!["name"] as? String ?? "")"
       // contactProfile.text = " \(data!["profile"] as? String ?? "")     "
       // contactProfile.backgroundColor = data!["color"] as? UIColor
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
