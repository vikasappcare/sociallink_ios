//
//  FeedLikeUnlikeModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 01/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON


class FeedLikeUnlikeModel: AppBaseModel {
    
    var service: String?
    var data: LikeUnlikeData?
    
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        self.service = response["service"].stringValue
        self.data = self.createObject(with: response["data"])
        
    }
    
    
    private func createObject(with jsonObject: JSON) -> LikeUnlikeData? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return LikeUnlikeData.init(jsonObject)
        }
    }
}

class LikeUnlikeData: AppBaseModel {
    var feed_id: Int64?
    var likes: Int64?
    var like_unlike: Int64?
     var member_id: String?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        self.feed_id = response["feed_id"].int64Value
        self.likes = response["likes"].int64Value
        self.like_unlike = response["like_unlike"].int64Value
        self.member_id = response["member_id"].stringValue
    }
}
