//
//  RegisterViewModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 25/06/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation

class RegisterViewModel {
    
    init() {
    }

    func skillsList(parameters: [String: Any],
                      onSuccess: @escaping (RegistrationSkillsModel?) -> Void,
                      onError: @escaping APIErrorHandler) {
        AppDataManager.getSkills(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: dataSource)
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
    func registerUser(parameters: [String: Any],
                   onSuccess: @escaping (SignUpModel?) -> Void,
                   onError: @escaping APIErrorHandler) {
        AppDataManager.registerUser(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: dataSource)
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
    func registerGmailUser(parameters: [String: Any],
                      onSuccess: @escaping (SignUpModel?) -> Void,
                      onError: @escaping APIErrorHandler) {
        AppDataManager.registerGmailUser(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: dataSource)
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }

    
}
