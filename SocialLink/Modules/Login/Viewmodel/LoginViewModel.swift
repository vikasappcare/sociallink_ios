//
//  LoginViewModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 17/04/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation

class LoginViewModel {
    
    init() {
    }
    
    func loginUser(parameters: [String: Any],
                       onSuccess: @escaping (SignUpModel?) -> Void,
                       onError: @escaping APIErrorHandler) {
        AppDataManager.loginUser(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: dataSource?.profile?[0].email)
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
    func loginPasswordUser(parameters: [String: Any],
                   onSuccess: @escaping (SignUpPasswordModel?) -> Void,
                   onError: @escaping APIErrorHandler) {
        AppDataManager.loginPasswordUser(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: dataSource?.profile?.email)
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
}
