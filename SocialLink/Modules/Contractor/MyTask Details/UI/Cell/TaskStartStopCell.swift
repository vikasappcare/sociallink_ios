//
//  TaskStartStopCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 16/06/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class TaskStartStopCell: UITableViewCell {

    @IBOutlet weak var startButton: UIButton!
   
    @IBOutlet weak var stopButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        stopButton.isHidden = true
    }
    @IBAction func startButtonClicked(_ sender: Any) {
        
        if startButton.isSelected {
            startButton.isSelected = false
            startButton.setTitle("Resume", for: .normal)
            startButton.backgroundColor = UIColor.planBlue
           //// stopButton.isHidden = true
        } else {
            startButton.isSelected = true
            startButton.setTitle("Pause", for: .selected)
            stopButton.isHidden = false
            startButton.backgroundColor = UIColor.appGreenColor
        }
        
    }
    @IBAction func stopButtonClicked(_ sender: Any) {
        
        stopButton.isHidden = true
        startButton.isSelected = false
        startButton.setTitle("Start", for: .normal)
        startButton.backgroundColor = UIColor.appGreenColor

        
    }
    
//    func startTimer(from: Double) {
//        var logTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
//    }
//    @objc func updateCounter() {
////        if counter > 0 {
////            print("\(counter) seconds to the end of the world")
////            counter -= 1
////        }
//
//
//
//
//    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
