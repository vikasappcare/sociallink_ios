//
//  InviteTeamMemberModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 29/08/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON

class InviteTeamMemberModel: AppBaseModel {
    
    var service: String?
    var data: TeamMember?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    
    private func parseResponse(with response: JSON) {
        self.service = response["service"].stringValue
        self.data = self.createObject(with: response["data"])
    }
    
    private func createObject(with jsonObject: JSON) -> TeamMember? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return TeamMember.init(jsonObject)
        }
    }
    
}


class TeamMember: AppBaseModel {
    var mname: String?
    var memailid: String?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        self.mname = response["mname"].stringValue
        self.memailid = response["memailid"].stringValue
    }
    
    
}


