//
//  NewPostModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON


class NewPostModel: AppBaseModel {
    
    var service: String?
    var count: Int64?
    var feed: Feed?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
    }
    
    private func parseResponse(with response: JSON) {
        self.service = response["service"].stringValue
        self.count = response["count"].int64Value
        self.feed = createObject(with: response["project_feed"])
    }
    
    private func createObject(with jsonObject: JSON) -> Feed? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return Feed.init(jsonObject)
        }
    }
    
}
