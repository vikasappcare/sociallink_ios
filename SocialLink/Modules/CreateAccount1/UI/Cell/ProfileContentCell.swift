//
//  ProfileContentCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 27/02/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

protocol ProfileContentCellDelegate: class {
    
    func dataChanged(data: String, tag: Int)
    func showPickerView(forTag: Int)
}
class ProfileContentCell: UITableViewCell {
    
    weak var delegate: ProfileContentCellDelegate?
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var contentText: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentText.delegate = self
    }
    
    func setText(text: String, tag: Int) {
        self.contentText.text = text
        self.contentText.tag = tag
    }

    func setData(data: String?, tag: Int, profile: ProfileModel) {
        let headerText = data ?? ""
        contentText.tag = tag
        self.headerLabel.text =  headerText
        let lowercase = (data ?? "").lowercased()
        contentText.placeholder = "Enter \(lowercase)"
        
        print(tag)
         print(headerText)
        
        switch headerText {
        case "Email":
            contentText.text = profile.email ?? ""
            contentText.keyboardType = .emailAddress
            contentText.isEnabled = false
        case "Phone Number":
            contentText.text = profile.phone_number ?? ""
            contentText.keyboardType = .numberPad
            contentText.isEnabled = true
        case "Website":
            contentText.keyboardType = .URL
            contentText.text = profile.website ?? ""
            contentText.isEnabled = true
        case "About You":
            contentText.text = profile.aboutme ?? ""
            contentText.isEnabled = true
        case "Education":
            contentText.text = profile.education ?? ""
            contentText.isEnabled = true
        case "Skills":
            contentText.text = profile.skills ?? ""
            contentText.isEnabled = true
        case "Business":
            contentText.text = profile.business ?? ""
            contentText.isEnabled = true
        case "Industry":
            contentText.text = profile.industry ?? ""
            contentText.isEnabled = true
        case "Department":
            contentText.text = profile.department ?? ""
            contentText.isEnabled = true
        case "Job":
            contentText.text = profile.job ?? ""
            contentText.isEnabled = true
        case "Resume":
            contentText.text = profile.resume ?? ""
            contentText.isEnabled = true
        case "City":
            contentText.text = profile.city ?? ""
            contentText.isEnabled = true
        case "State":
            contentText.text = profile.state ?? ""
            contentText.isEnabled = true
        case "Country":
            contentText.text = profile.country ?? ""
            contentText.isEnabled = true
        case "Designation":
            contentText.text = profile.designation ?? ""
            contentText.isEnabled = true
        default:
            contentText.keyboardType = .default
        }
 
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension ProfileContentCell: UITextFieldDelegate {
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        switch textField.tag {
        case 13:
            self.delegate?.showPickerView(forTag: textField.tag)
            return false
        case 14:
            self.delegate?.showPickerView(forTag: textField.tag)
            return false
        case 15:
            self.delegate?.showPickerView(forTag: textField.tag)
            return false
        default:
            return true
        }
    }
    
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.delegate?.dataChanged(data: textField.text ?? "", tag: textField.tag)
    }
}
