//
//  CheckYourEmail.swift
//  SocialLink
//
//  Created by admin on 2/25/19.
//  Copyright © 2019 Santhosh Marripelli. All rights reserved.
//

import UIKit

class CheckYourEmail: UIViewController {
    @IBOutlet weak var homeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func goHomeButtonClicked(_ sender: Any) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
            let navController = APPNavigationController(rootViewController: controller)
            self.present(navController, animated: true, completion: nil)
            /// self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
