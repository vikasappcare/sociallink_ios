//
//  ContactProfileCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit
protocol ContactProfileCellDelegate: class {
    func messageClicked()
}

class ContactProfileCell: UITableViewCell {

    weak var delegate: ContactProfileCellDelegate?
    @IBOutlet weak var messageButton: UIButton!
    @IBOutlet weak var contactImage: AppRoundImage!
    @IBOutlet weak var contactPlace: UILabel!
    @IBOutlet weak var contactName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setData(profile: ContractorProfile) {
        contactImage.sd_setImage(with: URL(string: profile.uphoto ?? ""), placeholderImage: #imageLiteral(resourceName: "SampleProfile"))
        var city = ""
        var state = ""
        if !(profile.city ?? "").checkForEmpty() {
            city = profile.city ?? ""
        }
        if !(profile.ustate ?? "").checkForEmpty() {
            state = "," + (profile.ustate ?? "")
        }
        contactPlace.text = city + state
        contactName.text = profile.ufullname ?? ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func messageButtonClicked(_ sender: Any) {
        self.delegate?.messageClicked()
    }
    
}
