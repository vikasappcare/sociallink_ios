//
//  ProjectFeedVC.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 22/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ProjectFeedVC: AppBaseVC {
    
    @IBOutlet weak var projectFeedTable: UITableView!
    var feedType: String?
    var imagePicker = UIImagePickerController()
    var viewModel: FeedViewModel?
    var idIs: Int64?
    var refreshControl = UIRefreshControl()
    
    
    let itemsPerBatch = 10
    
    // Where to start fetching items (database OFFSET)
    var offset = 0
    
    // a flag for when all database items have already been loaded
    var reachedEndOfItems = false
    
    var contactProfile: ProfileModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        reloadData()
       getusersFromDB()
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func addRefreshControl() {
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        projectFeedTable.addSubview(refreshControl)
    }
    @objc func refresh(sender:AnyObject) {
       getFeed()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    func setProjectId(projectId: Int64) {
        if viewModel == nil {
            viewModel = FeedViewModel()
        }
        viewModel?.projectId = projectId
        idIs = projectId
    }
    func getusersFromDB() {
        let users = APPDBUtility.getUserData(mocType: .main)
        if users?.count ?? 0 > 0 {
            if let user = users {
                if viewModel != nil {
                    contactProfile = APPUtility.convertDBUserToUser(dbUser: user[0])
                }
                
            }
        }
    }
    
    
    func reloadData()
    {
        if viewModel == nil {
            viewModel = FeedViewModel()
        }

        self.addNotification()
        self.setTableViewDatasource()
        
        self.tabBarController?.delegate = self
        
        
        let token = APPDBUtility.getToken(mocType: .main)
        print(token?.token ?? "")
        
        self.addRefreshControl()
        self.createNavigationItems()
        
        self.navigationItem.title = "FEED"
        if let idIs = viewModel?.projectId {
            viewModel?.updateFeeds(withProjectId: idIs)
        }
            self.getFeed()
        
        self.addUserTypeSwitchNotification()
        
    
    }
    
    func setProjectFeedType(type: String) {
        feedType = type
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getFeed() {
      
        if let model = viewModel {
            var parms = ["user_id": APPStore.sharedStore.user.userId ?? 0] as [String : Any]
            print(parms)
            if model.projectId != 0 {
                parms["project_id"] =  model.projectId
            }
          //  self.addActivityIndicatior()
            self.showHUD(title: "Loading...")

            DispatchQueue.global(qos: .background).async {
                //print("This is run on the background queue")
                model.getFeed(parameters: parms, onSuccess: { [weak self] (dataSource) in
                  //  print("Success")
                    //DispatchQueue.main.async {
                    self?.hideHud()
                        self?.refreshControl.endRefreshing()
                        model.feedArray = dataSource?.list
                        self?.offset += (dataSource?.list?.count ?? 0)
                        self?.projectFeedTable.reloadData()
                   // }
                    }, onError: { (error) in
                        Logger.log(message: "Error \(error.statusMessage)", event: .error)
                            self.refreshControl.endRefreshing()
                           // self.showAlert(title: Message.Alert, message: error.statusMessage)
                            self.hideHud()
                    
                })
            }
        }
    }
}

extension ProjectFeedVC {
    
    func createNavigationItems() {
        let notificationItem = UIBarButtonItem(image: UIImage(named: "moreNotification"), style: .done, target: self, action: #selector(moreClicked))
        self.navigationItem.rightBarButtonItem = notificationItem
    }
    
    @objc func moreClicked() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)

        if let controller = mainStoryboard.instantiateViewController(withIdentifier: "NotificationsVC") as? NotificationsVC {
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }
}

extension ProjectFeedVC {
    
    func addNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(showAddScreen),
                                               name:
            NSNotification.Name(rawValue: APPNotifications.plusNewsFeed),
                                               object: nil)
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(showLoginAfterLogout),
                                               name:
            NSNotification.Name(rawValue: APPNotifications.showLoginAfterLogout),
                                               object: nil)
        
        
    }
    @objc func  showAddScreen(info: NSNotification) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = mainStoryboard.instantiateViewController(withIdentifier: "AddViewController")
        if let addController = controller as? AddViewController {
            addController.modalPresentationStyle = .overCurrentContext
            addController.view.alpha = 0.0
            self.tabBarController?.present(addController, animated: true) { () -> Void in
                UIView.animate(withDuration: 0.025) { () -> Void in
                    addController.view.alpha = 1.0
                }
            }
        }
        
    }
}

extension ProjectFeedVC {
    
    func addUserTypeSwitchNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(userSwitched),
                                               name:
            NSNotification.Name(rawValue: APPNotifications.userSwitched),
                                               object: nil)
    }
    @objc func userSwitched(info: NSNotification) {
       // self.callServiceCall()
    }
}

extension ProjectFeedVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.projectFeedTable.delegate = self
        self.projectFeedTable.dataSource = self
        self.projectFeedTable.estimatedRowHeight = 300
        self.projectFeedTable.rowHeight = UITableViewAutomaticDimension
        self.projectFeedTable.tableFooterView = UIView()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            if let modelFeed = viewModel?.feedArray {
                print(modelFeed.count)
                 return modelFeed.count
            }
        default:
            return 0
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier:
                CellIdentifiers.PostTableViewCellIdentifier, for: indexPath)
            if let cellIs = cell as? PostTableViewCell {
                cellIs.selectionStyle = .none
                cellIs.delagate = self
            }
            cell.selectionStyle = .none
            return cell
        case 1:
            if let feed = viewModel?.feedArray {
                if feed.isEmpty == false{
                switch feed[indexPath.row].feed_document?.isEmpty {
                case true:
                    let cell = tableView.dequeueReusableCell(withIdentifier:
                        CellIdentifiers.ProjectTextTableCellIdentifier, for: indexPath)
                    if let cellIs = cell as? ProjectTextTableCell {
                        cellIs.selectionStyle = .none
                        
                        cellIs.delegate = self
                        cellIs.setData(data: feed[indexPath.row], tag: indexPath.row, profile: contactProfile!)
                        
                    }
                    cell.selectionStyle = .none
                    return cell
                default:
                    let cell = tableView.dequeueReusableCell(withIdentifier:
                        CellIdentifiers.ProjectImageTableCellIdentifier, for: indexPath)
                    if let cellIs = cell as? ProjectImageTableCell {
                        cellIs.selectionStyle = .none
                        cellIs.delegate = self
                        cellIs.setData(feed: feed[indexPath.row], tag: indexPath.row, profile: contactProfile!)
                    }
                    cell.selectionStyle = .none
                    return cell
                }
                }
                else
                {
                    tableView.reloadData()
                }
            }
        default:
            return UITableViewCell()
        }
        return UITableViewCell()
    }
    
    
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0001, execute: {
//            if let feed = self.viewModel?.feedArray {
//                //                print(feed[indexPath.row].feed_extension ?? "")
//                let feedURL = feed[indexPath.row].feed_external_link ?? ""
//                switch feed[indexPath.row].feed_extension {
//                case FileType.PDF, FileType.DOC, FileType.DOCX, FileType.PPT, FileType.PPTX, FileType.XLSX, FileType.XLS:
//                    if !feedURL.checkForEmpty() {
//                        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "FileLoadVC") as? FileLoadVC {
//                            let navControl = APPNavigationController(rootViewController: controller)
//                            controller.setUrl(url: feedURL)
//                            self.present(navControl, animated: true, completion: nil)
//                        }
//                    }
//                default:
//                    break
//                }
//            }
//        })
//        
//    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        // UITableView only moves in one direction, y axis
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        // Change 10.0 to adjust the distance from bottom
        if maximumOffset - currentOffset <= 10.0 {
            print("Load More")
            self.loadMore()
        }
    }
    
}


extension ProjectFeedVC {
    
    func loadMore() {
        // don't bother doing another db query if already have everything
        guard !self.reachedEndOfItems else {
            return
        }
            self.getFeed()
    }
    
}

extension ProjectFeedVC: PostTableViewCellDelegate {
    func postAttachment() {
        ///
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func postAdd() {
        ///
        
        let alertView = UIAlertController(title: "", message: "Choose", preferredStyle: .actionSheet)
        let module = UIAlertAction(title: "Create a Module", style: .default, handler: { (alert) in
            
        })
        alertView.addAction(module)
        let task = UIAlertAction(title: "Create a Task", style: .default, handler: { (alert) in
            
        })
        alertView.addAction(task)
        let subTask = UIAlertAction(title: "Create a Subtask", style: .default, handler: { (alert) in
            
        })
        alertView.addAction(subTask)
        let invite = UIAlertAction(title: "Invite members to the Project", style: .default, handler: { (alert) in
            
        })
        alertView.addAction(invite)
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert) in
            
        })
        alertView.addAction(cancel)
        
        self.present(alertView, animated: true, completion: nil)
    }
    
    func postCalender() {
        ///
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProjectCalenderVC") as? ProjectCalenderVC {
            let navController = APPNavigationController(rootViewController: controller)
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    func postTag() {
        ///
        
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactsViewController") as? ContactsViewController {
            controller.showCloseButton(show: true, forScreen: "")
            let navController = APPNavigationController(rootViewController: controller)
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    
    func openPostController() {
        
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "PostViewController") as? PostViewController {
            controller.delegate = self
            controller.setProjectId(id: idIs ?? 0)
            let navController = APPNavigationController(rootViewController: controller)
            self.present(navController, animated: true, completion: nil)
        }
        
    }
    
}
extension ProjectFeedVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        ////
    }
    
}

extension ProjectFeedVC: UITabBarControllerDelegate {
    
    //    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
    //        let tabBarIndex = tabBarController.selectedIndex
    //        if tabBarIndex == 0 {
    //            self.projectFeedTable.setContentOffset(CGPoint.zero, animated: true)
    //        }
    //    }
    
}

extension ProjectFeedVC: ProjectTextTableCellDelegate  {
    func dispalyComments(feedId: Int64) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "FeedCommentViewController") as? FeedCommentViewController {
            controller.setFeedId(feedId: feedId)
            controller.showCommentBar(show: false)
            let navController = APPNavigationController(rootViewController: controller)
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    func showCommentsClicked(feedId: Int64) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "FeedCommentViewController") as? FeedCommentViewController {
            controller.setFeedId(feedId: feedId)
            controller.showCommentBar(show: true)
            let navController = APPNavigationController(rootViewController: controller)
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    func showLikesClicked(feedId: Int64) {
        //////Need to Implement
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProjectFeedLikesController") as? ProjectFeedLikesController {
            controller.setFeedId(feedId: feedId)
            let navController = APPNavigationController(rootViewController: controller)
            self.present(navController, animated: true, completion: nil)
        }
        
    }
    
    
    func likeSelected(feedID: Int64, likeUnlike: Int, forRow: Int) {
        print(feedID)
        print(likeUnlike)
        self.sendLikeUnLike(forFeed: feedID, likeUnlike: likeUnlike, forRow: forRow, textCell: true)
        
    }
}

extension ProjectFeedVC {
    
    func sendLikeUnLike(forFeed: Int64, likeUnlike: Int, forRow: Int, textCell: Bool) {
        if let model = viewModel {
            /// self.showHUD(title: "Loading...")
            let dataIs = ["member_id": APPStore.sharedStore.user.userId ?? "",
                          "feed_id": forFeed,
                          "like_unlike": likeUnlike] as [String : Any]
            print(dataIs)
            model.sendLikeUnLike(parameters: dataIs, onSuccess: { [weak self] (object) in
                dprint(object: "Success")
                print(object?.data?.likes ?? "")
                //if let objectIs = object {
                //self?.reloadCell(row: forRow, likes: object?.data?.likes ?? 0, likeUnlike: object?.data?.like_unlike ?? 0, forId: forFeed, feed: Feed, textCell: textCell)
               // }
                
                self?.hideHud()
                }, onError: { (error) in
                    Logger.log(message: "Error \(error.statusMessage)", event: .error)
                 //   self.showAlert(title: Message.Alert, message: error.statusMessage)
                    /// self.hideHud()
            })
        }
    }
    
    func reloadCell(row: Int, likes: Int64, likeUnlike: Int64, forId: Int64, feed: Feed, textCell: Bool) {
        let indexPath = IndexPath(row: row, section: 1)
        
        if textCell {
            let cell = self.projectFeedTable.cellForRow(at: indexPath)
            if let cellIs = cell as? ProjectTextTableCell {
                cellIs.updateLikeCell(likeData: feed)
            }
        } else {
            let cell = self.projectFeedTable.cellForRow(at: indexPath)
            if let cellIs = cell as? ProjectImageTableCell {
                cellIs.updateLikeCell(likeData: feed)
            }
        }
        
        
        
        if let model = viewModel {
            if let arrayIs = model.feedArray {
                if arrayIs.index(where: {$0.feed_id == forId}) != nil {
                    var liked = false
                    switch feed.userliked {
                    case "0":
                        liked = false
                    case "1":
                        liked = true
                    default:
                        break
                    }
//                    model.feedArray![indexOf].likes?.liked = liked
//                    model.feedArray![indexOf].likes?.count = feed.likes
                }
            }
            
        }
    }
}

extension ProjectFeedVC: PostViewControllerDelegate {
    func postedFeed(feedId: Int64) {
        if let feedIs = APPUtility.getFeedWithFeedId(withFeedId: feedId) {
            if let model = viewModel {
                model.feedArray?.insert(feedIs, at: 0)
                self.projectFeedTable.reloadSections(IndexSet(integer: 1), with: .automatic)
            }
        }
    }
}

extension ProjectFeedVC {
    @objc func showLoginAfterLogout(info: NSNotification) {
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        guard let rootViewController = window.rootViewController else {
            return
        }
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginLandingPage")
        vc.view.frame = rootViewController.view.frame
        vc.view.layoutIfNeeded()
        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
            window.rootViewController = vc
        }, completion: { completed in
        })
    }
    
}

extension ProjectFeedVC: ProjectImageTableCellDelegate {
    func imageLikeSelected(feedID: Int64, likeUnlike: Int, forRow: Int) {
        print(feedID)
        print(likeUnlike)
        self.sendLikeUnLike(forFeed: feedID, likeUnlike: likeUnlike, forRow: forRow, textCell: false)
        
    }
    
    func imageShowLikesClicked(feedId: Int64) {
        //////Need to Implement
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProjectFeedLikesController") as? ProjectFeedLikesController {
            controller.setFeedId(feedId: feedId)
            let navController = APPNavigationController(rootViewController: controller)
            self.present(navController, animated: true, completion: nil)
        }
        
    }
    
    func imageShowCommentsClicked(feedId: Int64) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "FeedCommentViewController") as? FeedCommentViewController {
            controller.setFeedId(feedId: feedId)
            controller.showCommentBar(show: true)
            let navController = APPNavigationController(rootViewController: controller)
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    func imageDispalyComments(feedId: Int64) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "FeedCommentViewController") as? FeedCommentViewController {
            controller.setFeedId(feedId: feedId)
            controller.showCommentBar(show: false)
            let navController = APPNavigationController(rootViewController: controller)
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    
    
}

