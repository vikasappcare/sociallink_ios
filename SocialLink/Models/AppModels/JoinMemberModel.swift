//
//  JoinMemberModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 03/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON


class JoinMemberModel: AppBaseModel {
    
    var service: String?
    var data: JoinModel?
    
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        self.service = response["service"].stringValue
        self.data = self.createObject(with: response["data"])
        
    }
    
    
    private func createObject(with jsonObject: JSON) -> JoinModel? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return JoinModel.init(jsonObject)
        }
    }
}

class JoinModel: AppBaseModel {
    var emailid: String?
    var project_id: Int64?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        self.emailid = response["emailid"].stringValue
        self.project_id = response["project_id"].int64Value
    }
}
