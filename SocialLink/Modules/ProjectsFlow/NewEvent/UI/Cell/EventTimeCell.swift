//
//  EventTimeCell.swift
//  SocialLink
//
//  Created by Santhosh on 22/04/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class EventTimeCell: UITableViewCell {

    @IBOutlet weak var allDaySwitch: UISwitch!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var startDate: UIButton!
    @IBOutlet weak var endDate: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(data: [String: Any]) {
        iconImage.image = UIImage(named: data["icon"] as? String ?? "")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func startDateClicked(_ sender: Any) {
    }
    @IBAction func endDateClicked(_ sender: Any) {
    }
    @IBAction func allDayChanged(_ sender: Any) {
    }
    
}
