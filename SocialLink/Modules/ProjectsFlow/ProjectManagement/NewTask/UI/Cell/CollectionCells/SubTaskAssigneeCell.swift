//
//  SubTaskAssigneeCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/05/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class SubTaskAssigneeCell: UICollectionViewCell {
    
    @IBOutlet weak var participantImage: UIImageView!
    
    func setData(data: [String: Any]?) {
        participantImage.layer.cornerRadius = participantImage.frame.size.width / 2
        participantImage.layer.masksToBounds = true
    }
}
