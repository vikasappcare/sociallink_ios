//
//  MoreProfileCell.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 11/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class MoreProfileCell: UITableViewCell {

    @IBOutlet weak var profileImage: AppRoundImage!
    @IBOutlet weak var profileName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(data: ProfileModel) {
        let userProfileImage = APPURL.feedUserProfileImage  + data.image!
        profileImage.sd_setImage(with: URL(string: userProfileImage), placeholderImage: UIImage(named: ""))
        profileName.text = data.full_name ?? ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
