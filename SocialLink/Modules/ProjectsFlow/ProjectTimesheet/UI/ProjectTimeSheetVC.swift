//
//  ProjectTimeSheetVC.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 23/04/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class ProjectTimeSheetVC: UIViewController {
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLineHeader: UIView!
    @IBOutlet weak var timeLineTable: UITableView!
    @IBOutlet weak var timelineSegment: AppSegmentControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTableViewDatasource()
        self.createNavigationItems()
        self.navigationItem.title = "TIMESHEETS"
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension ProjectTimeSheetVC {
    
    func createNavigationItems() {
        let cancelItem = UIBarButtonItem(image: UIImage(named: "CloseNavigation"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func cancelClicked() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension ProjectTimeSheetVC: UITableViewDelegate, UITableViewDataSource {
    
    func setTableViewDatasource() {
        self.timeLineTable.delegate = self
        self.timeLineTable.dataSource = self
        self.timeLineTable.estimatedRowHeight = 300
        self.timeLineTable.rowHeight = UITableViewAutomaticDimension
        self.timeLineTable.tableFooterView = UIView()
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        return view
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:
            CellIdentifiers.ProjectTimeLineCellIdentifier, for: indexPath)
        if let cellIs = cell as? ProjectTimeLineCell {
            cellIs.selectionStyle = .none
            //            let data = ["profile": profileArray[indexPath.row],
            //                        "color": colorArray[indexPath.row]] as [String: Any]
            //            cellIs.setData(data: data)
        }
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
