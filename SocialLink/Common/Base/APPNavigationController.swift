//
//  APPNavigationController.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 10/03/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import UIKit

class APPNavigationController: UINavigationController, UIGestureRecognizerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setNavigationProprites()
        // Do any additional setup after loading the view.
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap))
        tap.delegate = self // This is not required
        tap.numberOfTapsRequired = 2
        self.navigationBar.addGestureRecognizer(tap)
        

    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
      //  APPNativeCallAnalyzer.sharedInstance.openNativeCallAnalyzer(in: self, frame: self.view.bounds)
    }
    
    
    func setNavigationProprites() {
        
        UINavigationBar.appearance().barTintColor = UIColor.white
        UINavigationBar.appearance().tintColor = UIColor.darkGray
//        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedStringKey.font: UIFont.lightFont(16)]
//        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
//        imageView.contentMode = .scaleAspectFit
//        let image = UIImage(named: "logo3.png")
//        imageView.image = image
//        navigationItem.titleView = imageView
        
        
       
        let color = UIColor.appNavigationTitleColor
        let attrs = [NSAttributedStringKey.foregroundColor: color,
                     NSAttributedStringKey.font: UIFont.semiBoldFont(16)]
        UINavigationBar.appearance().titleTextAttributes = attrs
        
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
