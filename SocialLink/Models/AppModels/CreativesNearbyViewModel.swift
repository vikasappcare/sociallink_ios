//
//  CreativesNearbyViewModel.swift
//  SocialLink
//
//  Created by Banda Anil Reddy on 13/03/19.
//  Copyright © 2019 Santhosh Marripelli. All rights reserved.
//

import Foundation
class CreativesNearbyViewModel {
    
    init() {
    }
    
    func creativesNearby(parameters: [String: Any],
                   onSuccess: @escaping (CreativesNearByModel?) -> Void,
                   onError: @escaping APIErrorHandler) {
        AppDataManager.creativesNearby(parametes: parameters, onSuccess: { (dataSource) in
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
}
    func projectsList(parameters: [String: Any],
                    onSuccess: @escaping (ProjectListModel?) -> Void,
                    onError: @escaping APIErrorHandler) {
        AppDataManager.projectsList(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: dataSource)
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    func invite(parameters: [String: Any],
                       onSuccess: @escaping (OTPModule?) -> Void,
                       onError: @escaping APIErrorHandler) {
        AppDataManager.invite(parametes: parameters, onSuccess: { (dataSource) in
            dprint(object: dataSource)
            onSuccess(dataSource)
        }, onFailure: { (error) in
            onError(error)
            Logger.log(message: "Error: \(error.statusMessage) \(error.statusCode)", event: .error)
        })
    }
    
}
