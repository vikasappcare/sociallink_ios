//
//  TImeSheetModel.swift
//  SocialLink
//
//  Created by Santhosh Marripelli on 19/09/18.
//  Copyright © 2018 Santhosh Marripelli. All rights reserved.
//

import Foundation
import SwiftyJSON

class TimeSheetModel: AppBaseModel {
    
    var service: String?
    var count: Int64?
    var list: [TimeSheet]?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }
        
    }
    
    private func parseResponse(with response: JSON) {
        self.service = response["service"].stringValue
        self.count = response["count"].int64Value
        for item in response["data"].arrayValue {
            if let model = self.createObject(with: item) {
                if self.list == nil {
                    self.list = [TimeSheet]()
                }
                self.list?.append(model)
            }
        }
    }
    
    private func createObject(with jsonObject: JSON) -> TimeSheet? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return TimeSheet.init(jsonObject)
        }
    }
    
}


class TimeSheet: AppBaseModel {
    var task_id: Int64?
    var project_id: Int64?
    var project_name: String?
    var task_title: String?
    var task_details: String?
    var start_date: String?
    var end_date: String?
    var totaltime: String?
    var task_state: Int64?
    var accepted_state: String?
    var timeLog: [TimeLog]?
    var created_by: CreatedBy?
    var assigned_to: CreatedBy?
    
    override init() {
        super.init()
    }
    
    init?(_ response: JSON) {
        if response.isEmpty {
            return nil
        }
        super.init(with: response)
        
        if response.exists() {
            self.parseResponse(with: response)
        }    }
    
    private func parseResponse(with response: JSON) {
        self.task_id = response["task_id"].int64Value
        self.project_id = response["project_id"].int64Value
        self.project_name = response["project_name"].stringValue
        self.task_title = response["task_title"].stringValue
        self.task_details = response["task_details"].stringValue
        self.start_date = response["start_date"].stringValue
        self.end_date = response["end_date"].stringValue
        self.totaltime = response["totaltime"].stringValue
        self.task_state = response["task_state"].int64Value
        self.accepted_state = response["accepted_state"].stringValue
        
        
        for item in response["List"].arrayValue {
            if let model = self.createTimeLogObject(with: item) {
                if self.timeLog == nil {
                    self.timeLog = [TimeLog]()
                }
                self.timeLog?.append(model)
            }
        }
        self.created_by = self.createObject(with: response["created_by"])
        self.assigned_to = self.createObject(with: response["assigned_to"])
    }
    
    private func createObject(with jsonObject: JSON) -> CreatedBy? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return CreatedBy.init(jsonObject)
        }
    }
    
    private func createTimeLogObject(with jsonObject: JSON) -> TimeLog? {
        if jsonObject.isEmpty {
            return nil
        } else {
            return TimeLog.init(jsonObject)
        }
    }
}

